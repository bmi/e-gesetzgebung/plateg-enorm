// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import sinon from 'sinon';

import { EntwurfstypType, FileControllerApi } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { ManageFilesController } from './controllerManageFiles';

describe('Test ManageFilesController', () => {
  const manageFilesCtrl = new ManageFilesController();
  const fileController = GlobalDI.getOrRegister<FileControllerApi>('fileController', () => new FileControllerApi());
  describe('Test prepareFilesList', () => {
    it('Success case', () => {
      const input = {
        dtos: [
          { base: { id: '1' }, dto: { fileName: 'Hello', entwurfStatusType: EntwurfstypType.Regelungsentwurf } },
          { base: { id: '2' }, dto: { fileName: 'World', entwurfStatusType: EntwurfstypType.Zusatzinfo } },
          { base: { id: '3' }, dto: { fileName: 'NKR', entwurfStatusType: EntwurfstypType.Stellungnahme } },
        ],
      };
      const expectedOutput = {
        files: [
          { name: 'Hello', uid: '1', type: 'Hello', size: 1 },
          {
            name: 'NKR',
            uid: '3',
            type: 'NKR',
            size: 1,
          },
        ],
        additionalFiles: [{ name: 'World', uid: '2', type: 'World', size: 1 }],
      };
      const result = manageFilesCtrl.prepareFilesList(input);
      expect(result).eql(expectedOutput);
    });
  });
  describe('Test prepareListOfuploadFilesObservables', () => {
    it('Success case', (done) => {
      const deleteFileStub = sinon.stub(fileController, 'deleteFile');
      const modifyFileStub = sinon.stub(fileController, 'modifyFile');
      const createFileStub = sinon.stub(fileController, 'createFile');
      manageFilesCtrl.prepareListOfuploadFilesObservables(
        '123',
        [{ uid: '1', name: 'Hello' }],
        [{ uid: '2', name: 'World' }],
        [{ uid: '3', name: 'NKR' }],
        [{ uid: '4', name: 'Vice' }],
      );
      setTimeout(() => {
        sinon.assert.calledTwice(deleteFileStub);
        sinon.assert.notCalled(modifyFileStub);
        sinon.assert.calledTwice(createFileStub);
        deleteFileStub.restore();
        modifyFileStub.restore();
        createFileStub.restore();
        done();
      }, 20);
    });
  });
});
