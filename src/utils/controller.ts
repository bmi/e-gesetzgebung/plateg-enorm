// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import isPast from 'date-fns/isPast';
import i18n from 'i18next';
import { useLocation } from 'react-router-dom';

import {
  AbstimmungEntityDTO,
  AbstimmungEntityResponseDTO,
  AbstimmungstypType,
  FristEntityDTO,
  FristtypType,
  OberabstimmungEntityDTO,
  servers,
} from '@plateg/rest-api';
import { getDateTimeString } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

export function incFileNameVersionNumber(
  fileName: string,
  increaseNumber: boolean,
  fileNameSuggestion?: string,
  version?: number,
): string {
  if (fileNameSuggestion === undefined) {
    // If currently no suggestion for a file name is available do not rename file yet
    return fileName;
  }
  const fileFormat = fileName.slice(fileName.lastIndexOf('.'));
  const currentDate = new Date().toISOString().split('T')[0].replace(/-/g, '');
  let newFileName = '';
  if (increaseNumber && version) {
    newFileName = version.toString();
  }
  newFileName = `${currentDate}_${fileNameSuggestion}_${newFileName}${fileFormat}`;
  return newFileName;
}

const getPlaceholder = (text: string | undefined, placeholder: string): string => {
  return text && text.trim().length ? text : `(${placeholder})`;
};

export const getStandardPlaceholder = (): string => {
  return i18n.t('enorm.vote.emailPlaceholders.standardPlaceholder');
};
export const getPhoneOfReferentPlaceholder = (): string => {
  return i18n.t('enorm.vote.emailPlaceholders.phonePlaceholder') + getStandardPlaceholder();
};

export interface MailSubjectParams {
  abbreviationOfVote?: string;
  typeOfVote?: string;
  roundOfVote?: number;
}

export function createMailSubject(params: MailSubjectParams): string {
  const abbreviationOfVote = getPlaceholder(params.abbreviationOfVote, getStandardPlaceholder());
  const typeOfVote = getPlaceholder(params.typeOfVote, getStandardPlaceholder());
  const roundOfVote = params.roundOfVote || 1;
  return `${typeOfVote} ${abbreviationOfVote} (#${roundOfVote}): Bitte um Mitzeichnung`;
}

export interface MailBodyHeaderParams {
  verschweigensfristGekennzeichnet: boolean;
  nameOfReferent?: string;
  ressortOfReferent?: string;
  typeOfVote?: string;
  titleOfVote?: string;
  titleOfAssociatedRegulatoryProject?: string;
  mainDeadlineDate?: string;
  replyInternetDate?: string;
  replyDate?: string;
  additionalFristenString?: string;
}

export interface MailBodyFooterParams {
  nameOfReferent?: string;
  ressortOfReferent?: string;
  typeOfVote?: string;
  titleOfVote?: string;
  titleOfAssociatedRegulatoryProject?: string;
  messageFromReferent?: string;
  fileName?: string;
  linkToVote?: string;
  mailOfReferent?: string;
  phoneOfReferent?: string;
  eingeschraenkt: boolean;
}

export function createMailBody(
  paramsHeader: MailBodyHeaderParams | string,
  paramsFooter: MailBodyFooterParams | string,
  messageFromReferent?: string,
): string {
  let bodyHeader;
  if (typeof paramsHeader === 'string') {
    bodyHeader = paramsHeader;
  } else {
    bodyHeader = createMailBodyHeader(paramsHeader);
  }
  let bodyFooter;
  if (typeof paramsFooter === 'string') {
    bodyFooter = paramsFooter;
  } else {
    bodyFooter = createMailBodyFooter(paramsFooter);
  }

  const standardPlaceholderMessageFromReferent = `(${getStandardPlaceholder()})`;
  return `${bodyHeader}
${messageFromReferent?.trim().length ? messageFromReferent : standardPlaceholderMessageFromReferent}
${bodyFooter}`;
}

export function createMailBodyHeader(params: MailBodyHeaderParams): string {
  const mainDeadlineDate = getPlaceholder(params.mainDeadlineDate, getStandardPlaceholder());
  const nameOfReferent = getPlaceholder(params.nameOfReferent, getStandardPlaceholder());
  const ressortOfReferent = getPlaceholder(params.ressortOfReferent, getStandardPlaceholder());
  const typeOfVote = getPlaceholder(params.typeOfVote, getStandardPlaceholder());
  const titleOfVote = getPlaceholder(params.titleOfVote, getStandardPlaceholder());
  const titleOfRegulatoryProject = getPlaceholder(params.titleOfAssociatedRegulatoryProject, getStandardPlaceholder());
  const replyVerschweigensfristGekennzeichnet = params.verschweigensfristGekennzeichnet
    ? '\nDiese Frist wurde als Verschweigensfrist gekennzeichnet.'
    : '';
  const replyInternetDateSentence = params.replyInternetDate
    ? `- Frist Rückmeldung zur Veröffentlichung im Internet/Intranet, ${params.replyInternetDate}`
    : '';
  const replyDateSentence = params.replyDate
    ? `- Frist Rückmeldung zur frühzeitigen Beteiligung der Länder und Verbände, ${params.replyDate}`
    : '';
  const artikelVotetype = typeOfVote === i18n.t('enorm.vote.typeOfVote.typeVorhabenclearing') ? 'zum' : 'zu der';

  return `Sehr geehrte Nutzerin, sehr geehrter Nutzer,

Sie wurden von ${nameOfReferent}, ${ressortOfReferent} ${artikelVotetype} ${typeOfVote} ${titleOfVote} bezüglich des Vorhabens ${titleOfRegulatoryProject} hinzugefügt.

Folgende Frist/en ist/sind zu beachten:
 
- Ende der Abstimmungsrunde, ${mainDeadlineDate}${replyVerschweigensfristGekennzeichnet}
${replyInternetDateSentence} 
${replyDateSentence} 
${params.additionalFristenString || ''}
Es wurde folgender Kommentar hinzugefügt:
`;
}

export function createMailBodyFooter(params: MailBodyFooterParams): string {
  const linkToVote = getPlaceholder(params.linkToVote, getStandardPlaceholder());
  const mailOfReferent = getPlaceholder(params.mailOfReferent, getStandardPlaceholder());
  const phoneOfReferent = getPlaceholder(params.phoneOfReferent, getPhoneOfReferentPlaceholder());
  const replyEingeschraenkt = params.eingeschraenkt
    ? 'Diese E-Mail darf an weitere Personen weiterleitet werden, die aus Ihrer Sicht an der Abstimmung teilnehmen sollten. Diese Personen dürfen über den obigen Link zum Regelungsentwurf die "Teilnahme anfragen". Auch eine Weiterleitung an Postfächer ist möglich, die Teilnahmeanfrage allerdings nur für Personen.'
    : 'Diese E-Mail darf an weitere Personen weiterleitet werden, die aus Ihrer Sicht an der Abstimmung teilnehmen sollten. Über den obigen Link können diese direkt an der Abstimmung teilnehmen. Auch eine Weiterleitung an Postfächer ist möglich, die Teilnahme allerdings nur für Personen.';

  return `
Bitte beachten Sie bei Ihrer Antwort auch die neuen Anforderungen des § 43 Abs. 1 Nr. 13 GGO (“Exekutiver Fußabdruck”).

Hier können Sie den Regelungsentwurf und die Anlagen aufrufen und Ihre Änderungsvorschläge direkt im Dokument einfügen:

${linkToVote} 

${replyEingeschraenkt}

Dies ist eine automatisch generierte Nachricht. Bitte antworten Sie nicht auf diese E-Mail. Bei Fragen wenden Sie sich an ${mailOfReferent}, ${phoneOfReferent}.`;
}

export function getFileExtension(fileName: string | undefined | null): string {
  const empty = '';
  return fileName ? /\.([^.]*?)(?=\?|#|$)/.exec(fileName.trim())?.[0] || empty : empty;
}

export function getFileLink(id: string): string {
  return servers[0].getUrl() + '/file/download/{fileId}'.replace('{fileId}', encodeURI(id.toString()));
}

export function getTyp(typ?: AbstimmungstypType): string {
  if (typ === AbstimmungstypType.Ressortabstimmung) {
    return i18n.t('enorm.vote.typeOfVote.typeDepartmentVote');
  }
  if (typ === AbstimmungstypType.Hausabstimmung) {
    return i18n.t('enorm.vote.typeOfVote.typeHomeVote');
  }
  if (typ === AbstimmungstypType.Unterabstimmung) {
    return i18n.t('enorm.vote.typeOfVote.typeNestedVote');
  }
  if (typ === AbstimmungstypType.SonstigeAbstimmung) {
    return i18n.t('enorm.vote.typeOfVote.typeOtherVote');
  }
  if (typ === AbstimmungstypType.Vorhabenclearing) {
    return i18n.t('enorm.vote.typeOfVote.typeVorhabenclearing');
  }
  if (typ === AbstimmungstypType.Schlussabstimmung) {
    return i18n.t('enorm.vote.typeOfVote.typeFinalVote');
  }
  if (typ === AbstimmungstypType.AbstimmungDerVorlageFuerDenRegierungsentwurf) {
    return i18n.t('enorm.vote.typeOfVote.typeHouseManagementVote');
  }
  return '';
}

export function getDateString(dateString: string): string {
  return `${Filters.date(new Date(dateString))}`;
}

export function getDeadline(type: FristtypType, fristen?: FristEntityDTO[], initial?: boolean): string {
  let matchedFrist = '';
  fristen?.forEach((frist) => {
    const fristString = initial ? frist.initialFristablauf : frist.fristablauf;
    if (frist.fristTyp === type && fristString) {
      matchedFrist = getDateTimeString(fristString);
    }
  });

  return matchedFrist;
}

export function getDeadlineExceededString(type: FristtypType, fristen?: FristEntityDTO[]): string {
  let exceededString = '';
  fristen?.forEach((frist) => {
    if (frist.fristTyp === type && frist.initialFristablauf) {
      exceededString = getDeadlineExceededStringFromString(frist.initialFristablauf);
    }
  });

  return exceededString;
}

export const checkDeadlineExceededString = (abstimmung: AbstimmungEntityResponseDTO | undefined) =>
  !abstimmung?.dto.fristen.find((item) => item.fristTyp === FristtypType.Abstimmungsende)?.deadlineChanged
    ? getDeadlineExceededString(FristtypType.Abstimmungsende, abstimmung?.dto.fristen)
    : '';

export function getDeadlineExceededStringFromString(frist: string): string {
  return isPast(new Date(frist)) ? ' - Abgelaufen' : '';
}

export function getGeschlossenString(oberabstimmung?: OberabstimmungEntityDTO): string | undefined {
  return oberabstimmung?.geschlossen ? ' - Geschlossen' : undefined;
}

export function getAdditionalDeadlines(fristen?: FristEntityDTO[], showTitel?: boolean): string[] {
  if (showTitel) {
    return fristen
      ?.filter((frist) => frist.fristTyp === FristtypType.ZusaetzlicheFrist)
      .map((frist) =>
        frist?.fristablauf ? `${frist.titel as string}  ·  ${getDateTimeString(frist.fristablauf)}` : '',
      ) as string[];
  } else {
    return fristen
      ?.filter((frist) => frist.fristTyp === FristtypType.ZusaetzlicheFrist)
      .map((frist) => (frist?.fristablauf ? getDateTimeString(frist.fristablauf) : '')) as string[];
  }
}

export function useQuery(): URLSearchParams {
  return new URLSearchParams(useLocation().search);
}

export function getAbstimmungsrundeName(version: string | number, titel?: string): string {
  return `${titel ? titel : ''} (#${version})`;
}

export function getAbstimmungsbezeichner(abstimmung?: AbstimmungEntityDTO): string {
  const rvName = abstimmung?.regelungsvorhaben?.dto.abkuerzung
    ? `${abstimmung?.regelungsvorhaben?.dto.abkuerzung} `
    : '';
  let abstimmungsTyp = abstimmung?.typ ? `${abstimmung?.typ} ` : '';

  if (
    abstimmung?.typ === AbstimmungstypType.SonstigeAbstimmung ||
    abstimmung?.typ === AbstimmungstypType.AbstimmungDerVorlageFuerDenRegierungsentwurf
  ) {
    abstimmungsTyp = abstimmungsTyp.split('_').join(' ');
  }

  const abstimmungsVersion = abstimmung?.version || '1';
  return `${rvName}${abstimmungsTyp}(#${abstimmungsVersion})`;
}

export enum MyVotesColumnNamesType {
  regelungsvorhaben = 'regelungsvorhaben',
  mitzeichnungsanfrage = 'mitzeichnungsanfrage',
  regelungsvorhabenType = 'regelungsvorhabenType',
  anfragenType = 'anfragenType',
  regelungsvorhabenNoSubvoteType = 'regelungsvorhabenNoSubvoteType',
  initalTab = 'initalTab',
  status = 'status',
  ersteller = 'ersteller',
  statusMitzeichnung = 'statusMitzeichnung',
}
