// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { UploadFile } from 'antd/lib/upload/interface';
import { forkJoin, Observable, of } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';
import { map, shareReplay } from 'rxjs/operators';

import {
  EntwurfstypType,
  FileControllerApi,
  FileEntityListResponseDTO,
  FileEntityModifyDTO,
  FileEntityResponseDTO,
} from '@plateg/rest-api';
import { ErrorController, GlobalDI, LoadingStatusController } from '@plateg/theme';

export interface Files {
  files: UploadFile[];
  additionalFiles: UploadFile[];
}

export class ManageFilesController {
  private readonly fileController = GlobalDI.getOrRegister<FileControllerApi>(
    'fileController',
    () => new FileControllerApi(),
  );
  private readonly loadingStatusController = GlobalDI.getOrRegister<LoadingStatusController>(
    'loadingStatusController',
    () => new LoadingStatusController(),
  );
  private readonly errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  //api call
  public createNewFileCall(
    file: Blob,
    fileName: string,
    entwurfStatusType: EntwurfstypType,
    abstimmungId: string,
  ): Observable<FileEntityResponseDTO> {
    const body = {
      file,
      fileName,
      entwurfStatusType,
      abstimmungId,
    };
    return this.fileController.createFile(body);
  }

  //api call
  public getFilesCall(abstimmungId: string): Observable<Files> {
    const body = { abstimmungId };

    const observerFiles = this.fileController.getErstellerFiles(body).pipe(
      map(
        (data: FileEntityListResponseDTO) => {
          return this.prepareFilesList(data);
        },
        (error: string) => {
          console.error(`unable to load content from server: ${error}`);
        },
      ),
      shareReplay(1),
    );
    observerFiles.subscribe({
      error: this.errorCb,
    });
    return observerFiles;
  }

  //api call
  public modifyFileCall(id: string, fileEntityModifyDTO: FileEntityModifyDTO): Observable<FileEntityResponseDTO> {
    const body = {
      id,
      fileEntityModifyDTO,
    };
    return this.fileController.modifyFile(body);
  }

  //api call
  public deleteFileCall(id: string): Observable<void> {
    const body = {
      fileId: id,
    };

    return this.fileController.deleteFile(body);
  }

  //api call
  public deleteAllFilesCall(id: string, successCb: (text: string) => void, errorCb: (error: string) => void): void {
    const body = {
      abstimmungId: id,
    };
    const call: Observable<void> = this.fileController.deleteFiles(body);
    call.subscribe({
      next: (data) => {
        successCb(`success delete all`);
      },
      error: (error: string) => {
        errorCb(`unable to delete all: ${error}`);
      },
    });
  }

  //service function
  public handleFileChange = (
    fileList: UploadFile[],
    type: EntwurfstypType,
    abstimmungId: string,
    initalFileList: UploadFile[],
  ): Observable<any> => {
    let fileListToBeUploaded: UploadFile[] = [];
    let preExistingFiles: UploadFile[] = [];
    fileList.forEach(() => {
      const ids = initalFileList?.map((a) => a.uid);
      fileListToBeUploaded = fileList.filter((item) => !ids?.includes(item.uid));
      preExistingFiles = fileList.filter((item) => ids?.includes(item.uid));
    });
    const deleteFilesObservable = this.deleteFiles(preExistingFiles, initalFileList);
    const uploadFilesObservable = this.uploadFiles(fileListToBeUploaded, type, abstimmungId);
    const modifyFilesObservable = this.modifyFiles(preExistingFiles, initalFileList);

    return forkJoin([deleteFilesObservable, uploadFilesObservable, modifyFilesObservable]);
  };

  //service function
  public modifyFiles = (fileList: UploadFile[], initalFileList: UploadFile[]): Observable<FileEntityResponseDTO[]> => {
    let toBeModified: UploadFile[] = [];

    const names = initalFileList?.map((a) => a.name);
    const ids = initalFileList?.map((a) => a.uid);
    toBeModified = fileList.filter((item) => !names?.includes(item.name) && ids?.includes(item.uid));

    if (!toBeModified.length) {
      return of([]);
    }

    return forkJoin(
      toBeModified?.map((element) => {
        return this.modifyFileCall(element.uid, { fileName: element.name });
      }),
    );
  };

  //service function
  public deleteFiles = (fileList: UploadFile[], initalFileList: UploadFile[]): Observable<void[]> => {
    let toBeDeleted: UploadFile[] = [];
    const ids = fileList?.map((a) => a.uid);

    toBeDeleted = initalFileList.filter((item) => !ids?.includes(item.uid));

    if (!toBeDeleted.length) {
      return of([]);
    }

    return forkJoin(
      toBeDeleted?.map((element) => {
        return this.deleteFileCall(element.uid);
      }),
    );
  };

  //service function
  public uploadFiles = (
    fileList: UploadFile[],
    type: EntwurfstypType,
    abstimmungId: string,
  ): Observable<FileEntityResponseDTO[]> => {
    if (!fileList.length) {
      return of([]);
    }
    return forkJoin(
      fileList?.map((element) => {
        const blob = new Blob([element.originFileObj ?? 'File'], { type: element?.originFileObj?.type });
        return this.createNewFileCall(blob, element.fileName || element.name, type, abstimmungId);
      }),
    );
  };

  //service function
  public deleteAllFiles = (existingVoteId: string): void => {
    this.deleteAllFilesCall(
      existingVoteId,
      (text: string) => {
        /* success */
        console.debug(`upload files: ${text}`);
      },
      (error: string) => {
        console.error('delete all error', error);
      },
    );
  };
  public prepareListOfuploadFilesObservables(
    id: string,
    fileList: UploadFile[],
    initalFileList: UploadFile[],
    additionalFileList: UploadFile[],
    initalAdditionalFileList: UploadFile[],
    type: EntwurfstypType = EntwurfstypType.Regelungsentwurf,
  ): Observable<any>[] {
    const listOfuploadFilesObservables: Observable<any>[] = [];
    if (fileList) {
      listOfuploadFilesObservables.push(this.handleFileChange(fileList, type, id, initalFileList ?? []));
    }

    if (additionalFileList) {
      listOfuploadFilesObservables.push(
        this.handleFileChange(additionalFileList, EntwurfstypType.Zusatzinfo, id, initalAdditionalFileList ?? []),
      );
    }

    return listOfuploadFilesObservables;
  }

  public prepareFilesList(filesDTO: FileEntityListResponseDTO): Files {
    const result: Files = { files: [], additionalFiles: [] };
    filesDTO.dtos.forEach((element) => {
      const file: UploadFile = {
        name: element.dto.fileName,
        uid: element.base.id.toString(),
        type: /[^.]+$/.exec(element.dto.fileName)?.[0] as string,
        size: 1,
      };
      if (element.dto.entwurfStatusType !== EntwurfstypType.Zusatzinfo) {
        result.files.push(file);
      }
      if (element.dto.entwurfStatusType === EntwurfstypType.Zusatzinfo) {
        result.additionalFiles.push(file);
      }
    });
    return result;
  }

  private readonly errorCb = (error: AjaxError) => {
    console.error('Error sub', error);
    this.loadingStatusController.setLoadingStatus(false);
    this.errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
  };
}
