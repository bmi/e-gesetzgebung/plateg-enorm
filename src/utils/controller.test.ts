// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai, { expect, should } from 'chai';
import chaiString from 'chai-string';

import {
  AbstimmungEntityDTO,
  AbstimmungstypType,
  FristEntityDTO,
  FristtypType,
  OberabstimmungEntityDTO,
} from '@plateg/rest-api';
import { Constants } from '@plateg/theme';

import i18n from '../shares/i18n';
import {
  createMailBody,
  createMailSubject,
  getAbstimmungsbezeichner,
  getAbstimmungsrundeName,
  getAdditionalDeadlines,
  getDateString,
  getDeadline,
  getDeadlineExceededString,
  getDeadlineExceededStringFromString,
  getFileExtension,
  getFileLink,
  getGeschlossenString,
  getStandardPlaceholder,
  getTyp,
  incFileNameVersionNumber,
  MailBodyFooterParams,
  MailBodyHeaderParams,
  MailSubjectParams,
} from './controller';

chai.use(chaiString);

describe('Increment Version Number in File Name', () => {
  const currentDate = new Date().toISOString().split('T')[0].replace(/-/g, '');
  const regelungsvorhabenTitle = 'Regelentwurf';
  it('Can Modify Version Number', () => {
    expect(incFileNameVersionNumber('20201108_Regelentwurf_2.doc', true, regelungsvorhabenTitle, 3)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_3.doc`,
    );
    expect(incFileNameVersionNumber('20201108_Regelentwurf_02.doc', true, regelungsvorhabenTitle, 3)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_3.doc`,
    );
    expect(incFileNameVersionNumber('20201108_Regelentwurf_99.pdf', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.pdf`,
    );
    expect(incFileNameVersionNumber('20201108_Regelentwurf_099.pdf', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.pdf`,
    );
    expect(incFileNameVersionNumber('20201108_Regelentwurf_ 000.docx', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.docx`,
    );
    expect(incFileNameVersionNumber('20201108_Regelentwurf_00099.pdf', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.pdf`,
    );
    expect(incFileNameVersionNumber('20201108_Regelentwurf_000200.doc', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.doc`,
    );
    expect(incFileNameVersionNumber('20201108_Regelentwurf_2.DOCX', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.DOCX`,
    );
    expect(incFileNameVersionNumber('JJJJMMTT_ RVAbkürzung_123.xyz', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.xyz`,
    );
    expect(incFileNameVersionNumber('Filename_0.docx', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.docx`,
    );
    expect(incFileNameVersionNumber('Filename_3   .docx', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.docx`,
    );
    expect(incFileNameVersionNumber('Filename_   4.docx', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.docx`,
    );
    expect(incFileNameVersionNumber('Filename_   5    .docx', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.docx`,
    );
    expect(incFileNameVersionNumber('File_344_name_1.docx', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.docx`,
    );
    expect(incFileNameVersionNumber('344_10.pdf', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.pdf`,
    );
    expect(incFileNameVersionNumber('some_filename _ 4.DOC', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.DOC`,
    );
    expect(incFileNameVersionNumber('Regelentwurf_02.a', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.a`,
    );
    expect(incFileNameVersionNumber('Regelentwurf_03.ab', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.ab`,
    );
    expect(incFileNameVersionNumber('Regelentwurf_04.abc', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.abc`,
    );
    expect(incFileNameVersionNumber('Regelentwurf_05.abcd', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.abcd`,
    );
  });

  it('Cannot Modify Version Number', () => {
    expect(incFileNameVersionNumber('4.docx', true, regelungsvorhabenTitle)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_.docx`,
    );
    expect(incFileNameVersionNumber('Filename_5db.docx', true, regelungsvorhabenTitle)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_.docx`,
    );
    expect(incFileNameVersionNumber('Filename_db5.docx', true, regelungsvorhabenTitle)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_.docx`,
    );
    expect(incFileNameVersionNumber('Filename_5d6.PDF', true, regelungsvorhabenTitle)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_.PDF`,
    );
    expect(incFileNameVersionNumber('Filename_5 6.PDF', true, regelungsvorhabenTitle)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_.PDF`,
    );
    expect(incFileNameVersionNumber('Filename_5 06.PDF', true, regelungsvorhabenTitle)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_.PDF`,
    );
    expect(incFileNameVersionNumber('some_filename.DOC', true, regelungsvorhabenTitle)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_.DOC`,
    );
    expect(incFileNameVersionNumber('Regelentwurf_09.abcde', true, regelungsvorhabenTitle)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_.abcde`,
    );
  });
});

describe('Get File Extension', () => {
  it('Positives', () => {
    expect(getFileExtension('test.PDF')).is.eq('.PDF');
    expect(getFileExtension('test.pdf')).is.eq('.pdf');
    expect(getFileExtension('.pdf')).is.eq('.pdf');
    expect(getFileExtension('test1.test2.pdf')).is.eq('.pdf');
    expect(getFileExtension('test.1')).is.eq('.1');
    expect(getFileExtension('test.12')).is.eq('.12');
    expect(getFileExtension('test.123')).is.eq('.123');
    expect(getFileExtension('test.1234')).is.eq('.1234');
    expect(getFileExtension('test.test.1234567890')).is.eq('.1234567890');
    expect(getFileExtension('test..pdf')).is.eq('.pdf');
    expect(getFileExtension('1_. docx')).is.eq('. docx');
    expect(getFileExtension('1_. Doc ')).is.eq('. Doc');
    expect(getFileExtension('test.')).is.eq('.');
    expect(getFileExtension(' .')).is.eq('.');
    expect(getFileExtension('.')).is.eq('.');
    expect(getFileExtension('..')).is.eq('.');
    expect(getFileExtension('...')).is.eq('.');
  });
  it('Negatives', () => {
    expect(getFileExtension(null)).is.eq('');
    expect(getFileExtension(undefined)).is.eq('');
    expect(getFileExtension('')).is.eq('');
    expect(getFileExtension(' ')).is.eq('');
    expect(getFileExtension('test')).is.eq('');
  });
});

describe('Create Mail Text', () => {
  it('Body contains all information (1)', () => {
    const paramsHeader: MailBodyHeaderParams = {
      nameOfReferent: 'Ricarda Rechtsetzungsreferentin',
      ressortOfReferent: 'BMI',
      typeOfVote: 'Ressortabstimmung',
      titleOfVote: 'RückWG (#1)',
      titleOfAssociatedRegulatoryProject: 'RückWG- Gesetz zur Unterstützung des Rückenwindes beim Fahrradfahren',
      mainDeadlineDate: '19.12.2020, 18 Uhr',
      replyDate: '19.12.2020, 18 Uhr',
      verschweigensfristGekennzeichnet: false,
    };
    const paramsFooter: MailBodyFooterParams = {
      linkToVote: 'Link zu Meine Abstimmungen von eNorm Dateien',
      mailOfReferent: 'Mail Adresse Ricarda',
      phoneOfReferent: 'Telefonnummer Ricarda',
      eingeschraenkt: false,
    };
    const message = `Hallo User1, 
Rückenwind auf dem Rad ist super.
Liebe Grüße
User2
`;
    const body = createMailBody(paramsHeader, paramsFooter, message);
    should().not.equal(body, undefined);
    expect(body.length).to.be.equal(1278);
    expect(body).to.have.entriesCount('\n', 26);
    expect(body).to.have.entriesCount('undefined', 0);
    expect(body).to.have.entriesCount('null', 0);
    expect(body).to.have.entriesCount(paramsHeader.nameOfReferent as string, 1);
    expect(body).to.have.entriesCount(paramsHeader.ressortOfReferent as string, 1);
    expect(body).to.have.entriesCount(paramsHeader.typeOfVote as string, 1);
    expect(body).to.have.entriesCount(paramsHeader.titleOfVote as string, 1);
    expect(body).to.have.entriesCount(paramsHeader.nameOfReferent as string, 1);
    expect(body).to.have.entriesCount(paramsHeader.titleOfAssociatedRegulatoryProject as string, 1);
    expect(body).to.have.entriesCount(paramsFooter.linkToVote as string, 1);
    expect(body).to.have.entriesCount(paramsFooter.mailOfReferent as string, 1);
    expect(body).to.have.entriesCount(paramsFooter.phoneOfReferent as string, 1);
    expect(body).to.have.entriesCount(message, 1);
  });
  it('Body contains all information (2)', () => {
    const paramsHeader = `Sehr geehrte/r Nutzer:in,

Ricarda Rechtsetzungsreferentin, BMI hat soeben ein Dokument/Dokumente zu der Ressortabstimmung RückWG (#1) bezüglich des Vorhabens RückWG- Gesetz zur Unterstützung des Rückenwindes beim Fahrradfahren hinzugefügt.

Es wurde folgender Kommentar hinzugefügt:`;
    const paramsFooter = `
Sie können die Dokumente im Bereich „Link zu Meine Abstimmungen von eNorm Dateien“ herunterladen. 

Dies ist eine automatisch generierte Nachricht. Bitte antworten Sie nicht auf diese E-Mail. Bei Fragen wenden Sie sich an Mail Adresse Ricarda, Telefonnummer Ricarda.`;
    const message = `Hallo User1, 
Rückenwind auf dem Rad ist super.
Liebe Grüße
User2`;
    const body = createMailBody(paramsHeader, paramsFooter, message);
    should().not.equal(body, undefined);
    expect(body.length).to.be.equal(617);
    expect(body).to.have.entriesCount('\n', 12);
    expect(body).to.have.entriesCount('undefined', 0);
    expect(body).to.have.entriesCount('null', 0);
    expect(body).to.have.entriesCount(message, 1);
  });
  it('Subject contains all information', () => {
    const paramsSubject: MailSubjectParams = {
      abbreviationOfVote: 'RückWG',
      typeOfVote: 'Ressortabstimmung',
      roundOfVote: 42,
    };
    const subject = createMailSubject(paramsSubject);
    should().not.equal(subject, undefined);
    expect(subject.length).to.be.equal(53);
    expect(subject).to.have.entriesCount('\n', 0);
    expect(subject).to.have.entriesCount('undefined', 0);
    expect(subject).to.have.entriesCount('null', 0);
    expect(subject).to.have.entriesCount(paramsSubject.abbreviationOfVote as string, 1);
    expect(subject).to.have.entriesCount(paramsSubject.typeOfVote as string, 1);
  });
  it('Body contains all placeholders (1)', () => {
    const paramsHeader: MailBodyHeaderParams = { verschweigensfristGekennzeichnet: false };
    const paramsFooter: MailBodyFooterParams = { eingeschraenkt: false };
    const message = undefined;
    const body = createMailBody(paramsHeader, paramsFooter, message);
    should().not.equal(body, undefined);
    expect(body.length).to.be.equal(1272);
    expect(body).to.have.entriesCount('\n', 22);
    expect(body).to.have.entriesCount('undefined', 0);
    expect(body).to.have.entriesCount('null', 0);
    expect(body).to.have.entriesCount(getStandardPlaceholder(), 10);
  });
  it('Subject contains all placeholders (1)', () => {
    const paramsSubject: MailSubjectParams = {};
    const subject = createMailSubject(paramsSubject);
    should().not.equal(subject, undefined);
    expect(subject.length).to.be.equal(103);
    expect(subject).to.have.entriesCount('\n', 0);
    expect(subject).to.have.entriesCount('undefined', 0);
    expect(subject).to.have.entriesCount('null', 0);
    expect(subject).to.have.entriesCount(getStandardPlaceholder(), 2);
  });
  it('Body contains all placeholders (2)', () => {
    const paramsHeader: MailBodyHeaderParams = {
      nameOfReferent: ' ',
      ressortOfReferent: ' ',
      typeOfVote: ' ',
      titleOfVote: ' ',
      titleOfAssociatedRegulatoryProject: ' ',
      verschweigensfristGekennzeichnet: false,
    };
    const paramsFooter: MailBodyFooterParams = {
      linkToVote: ' ',
      mailOfReferent: ' ',
      phoneOfReferent: ' ',
      eingeschraenkt: false,
    };
    const message = ' ';
    const body = createMailBody(paramsHeader, paramsFooter, message);
    should().not.equal(body, undefined);
    expect(body.length).to.be.equal(1272);
    expect(body).to.have.entriesCount('\n', 22);
    expect(body).to.have.entriesCount('undefined', 0);
    expect(body).to.have.entriesCount('null', 0);
    expect(body).to.have.entriesCount(getStandardPlaceholder(), 10);
  });
  it('Subject contains all placeholders (2)', () => {
    const paramsSubject: MailSubjectParams = {
      abbreviationOfVote: ' ',
      typeOfVote: ' ',
      roundOfVote: undefined,
    };
    const subject = createMailSubject(paramsSubject);
    should().not.equal(subject, undefined);
    expect(subject.length).to.be.equal(103);
    expect(subject).to.have.entriesCount('\n', 0);
    expect(subject).to.have.entriesCount('undefined', 0);
    expect(subject).to.have.entriesCount('null', 0);
    expect(subject).to.have.entriesCount(getStandardPlaceholder(), 2);
  });
});

describe('Validate mail address format', () => {
  it('Mail is in good shape', () => {
    const mails = [
      'test@test.com',
      'bund@test.de',
      '1bund@test.de',
      'bund@1test.de',
      'bund@test.1de',
      '1bund1@1test1.1de1',
      'dude94@somedomain68.pl',
      'my-awesome.pet@collection.com',
      'my-awesome@pet.collection.com',
      'BIG@CHAR.COM',
      'thisisan_very_very_long_mail_adress@with-some.formatting.characters-inside.fr',
    ];
    mails.forEach(
      (mail, index) => expect(Constants.EMAIL_REGEXP_PATTERN.test(mail), `Index: ${index}: '${mail}'`).to.be.true,
    );
  });
  it('Mail is in bad shape', () => {
    const mails = [
      ' ',
      '@',
      ' @ ',
      'somestring',
      '@test.com',
      'test.com',
      'test@test .com',
      'test @test.com',
      'test@ test.com',
      'test@test.',
      'test@test',
      'extralargewhichcrypticcharsinsideäöü$§%Ü&@test.com',
      'test@cryptichcharsÄÖÜ§43.com',
      'test@test.likelyinvaliddomain',
      '1@2.3',
      'a@b.c',
      '_my-awesome_.pet_@_collection_._com-',
      '_my-awesome/.pet\\@_collection_._com-',
      'veryshort@domain.a',
      ' test@test.com',
      'BIG@CHAR.S',
      'one@and@two.ats',
      'this(at)isinvalid.de',
      'this[at]isinvalid.de',
    ];
    mails.forEach(
      (mail, index) => expect(Constants.EMAIL_REGEXP_PATTERN.test(mail), `Index: ${index}: '${mail}'`).to.be.false,
    );
  });
});

describe('Test: getDeadlineExceededString', () => {
  it('frist is in the past', () => {
    const fristen: FristEntityDTO[] = [
      { fristTyp: FristtypType.Abstimmungsende, initialFristablauf: '2021-07-28 09:23:04', deadlineChanged: false },
    ];
    expect(getDeadlineExceededString(FristtypType.Abstimmungsende, fristen)).to.eql(' - Abgelaufen');
  });
  it('frist is in the future', () => {
    const fristen: FristEntityDTO[] = [
      { fristTyp: FristtypType.Abstimmungsende, initialFristablauf: '3021-07-28 09:23:04', deadlineChanged: false },
    ];
    expect(getDeadlineExceededString(FristtypType.Abstimmungsende, fristen)).to.eql('');
  });
});

describe('Test: getDeadlineExceededStringFromString', () => {
  it('frist is in the past', () => {
    expect(getDeadlineExceededStringFromString('2021-07-28 09:23:04')).to.eql(' - Abgelaufen');
  });
  it('frist is in the future', () => {
    expect(getDeadlineExceededStringFromString('3021-07-28 09:23:04')).to.eql('');
  });
});

describe('Test: getGeschlossenString', () => {
  it('oberabstimmung geschlossen', () => {
    expect(getGeschlossenString({ geschlossen: true } as OberabstimmungEntityDTO)).to.eql(' - Geschlossen');
  });
  it('oberabstimmung not geschlossen', () => {
    expect(getGeschlossenString({ geschlossen: false } as OberabstimmungEntityDTO)).to.be.undefined;
  });
});

describe('Test: getAbstimmungsrundeName', () => {
  it('existing titel', () => {
    expect(getAbstimmungsrundeName('1', 'test')).to.eql('test (#1)');
  });
  it('frist is in the future', () => {
    expect(getAbstimmungsrundeName('1')).to.eql(' (#1)');
  });
});

describe('Test: getAbstimmungsbezeichner', () => {
  const testAbstimmung = {
    regelungsvorhaben: { dto: { abkuerzung: 'test' } },
    typ: 'Testabstimmung',
    version: '2',
  } as unknown as AbstimmungEntityDTO;
  it('all data available', () => {
    expect(getAbstimmungsbezeichner(testAbstimmung)).to.eql('test Testabstimmung (#2)');
  });
  it('no data available', () => {
    expect(getAbstimmungsbezeichner()).to.eql('(#1)');
  });
});

describe('Test: getDateString', () => {
  it('dateString', () => {
    expect(getDateString('2022-08-18 09:23:04')).to.eql('18.08.2022');
  });
});

describe('Test get vote types', () => {
  it('getType', () => {
    expect(getTyp(AbstimmungstypType.Ressortabstimmung)).to.equal(i18n.t('enorm.vote.typeOfVote.typeDepartmentVote'));
    expect(getTyp(AbstimmungstypType.Hausabstimmung)).to.equal(i18n.t('enorm.vote.typeOfVote.typeHomeVote'));
    expect(getTyp(AbstimmungstypType.SonstigeAbstimmung)).to.equal(i18n.t('enorm.vote.typeOfVote.typeOtherVote'));
    expect(getTyp(AbstimmungstypType.Unterabstimmung)).to.equal(i18n.t('enorm.vote.typeOfVote.typeNestedVote'));
    expect(getTyp(AbstimmungstypType.Vorhabenclearing)).to.equal(i18n.t('enorm.vote.typeOfVote.typeVorhabenclearing'));
    expect(getTyp(AbstimmungstypType.Schlussabstimmung)).to.equal(i18n.t('enorm.vote.typeOfVote.typeFinalVote'));
    expect(getTyp(AbstimmungstypType.AbstimmungDerVorlageFuerDenRegierungsentwurf)).to.equal(
      i18n.t('enorm.vote.typeOfVote.typeHouseManagementVote'),
    );
  });
});

describe('Test get additional deadlines with title of deadlines', () => {
  const fristen: FristEntityDTO[] = [
    {
      titel: 'Frist Rückmeldung zur Veröffentlichung im Internet/Intranet',
      fristablauf: '2023-09-01T09:11:00Z',
      fristTyp: FristtypType.VeroeffentlichungImInternet,
      deadlineChanged: false,
    },
    {
      titel: 'Test Frist 1',
      fristablauf: '2023-08-17T14:00:00Z',
      fristTyp: FristtypType.ZusaetzlicheFrist,
      deadlineChanged: false,
    },
    {
      titel: 'Frist Ende der Abstimmungsrunde',
      fristablauf: '2023-08-24T16:00:00Z',
      fristTyp: FristtypType.Abstimmungsende,
      deadlineChanged: false,
    },
    {
      titel: 'Frist Rückmeldung zur frühzeitigen Beteiligung der Länder und Verbände',
      fristablauf: '2023-08-29T10:12:00Z',
      fristTyp: FristtypType.ZuleitungAnLaenderUndVerbaende,
      deadlineChanged: false,
    },
  ];
  it('getAdditionalDeadlines with title of deadline', () => {
    expect(getAdditionalDeadlines(fristen, true)).to.eqls(['Test Frist 1  ·  17.08.2023  ·  14:00']);
  });
});

describe('Test get deadlines', () => {
  const fristen: FristEntityDTO[] = [
    {
      titel: 'Frist Rückmeldung zur Veröffentlichung im Internet/Intranet',
      fristablauf: '2023-09-01T09:11:00Z',
      fristTyp: FristtypType.VeroeffentlichungImInternet,
      deadlineChanged: false,
    },
    {
      titel: 'Test Frist 1',
      fristablauf: '2023-08-17T14:00:00Z',
      fristTyp: FristtypType.ZusaetzlicheFrist,
      deadlineChanged: false,
    },
    {
      titel: 'Frist Ende der Abstimmungsrunde',
      fristablauf: '2023-08-24T16:00:00Z',
      fristTyp: FristtypType.Abstimmungsende,
      deadlineChanged: false,
    },
    {
      titel: 'Frist Rückmeldung zur frühzeitigen Beteiligung der Länder und Verbände',
      fristablauf: '2023-08-29T10:12:00Z',
      fristTyp: FristtypType.ZuleitungAnLaenderUndVerbaende,
      deadlineChanged: false,
    },
  ];
  it('getAdditionalDeadlines', () => {
    expect(getAdditionalDeadlines(fristen)).to.eqls(['17.08.2023  ·  14:00']);
  });
});

describe('Test for getDeadline', () => {
  const fristen: FristEntityDTO[] = [
    {
      titel: 'Frist Rückmeldung zur Veröffentlichung im Internet/Intranet',
      initialFristablauf: '2023-09-01T09:11:00Z',
      fristTyp: FristtypType.VeroeffentlichungImInternet,
      fristablauf: '2023-09-01T09:11:00Z',
      deadlineChanged: false,
    },
    {
      titel: 'Test Frist 1',
      initialFristablauf: '2023-08-17T14:00:00Z',
      fristTyp: FristtypType.ZusaetzlicheFrist,
      fristablauf: '2023-08-17T14:00:00Z',
      deadlineChanged: false,
    },
    {
      titel: 'Frist Ende der Abstimmungsrunde',
      initialFristablauf: '2023-08-24T16:00:00Z',
      fristTyp: FristtypType.Abstimmungsende,
      fristablauf: '2023-06-24T16:00:00Z',
      deadlineChanged: false,
    },
  ];
  it('getDeadline', () => {
    expect(getDeadline(FristtypType.Abstimmungsende, fristen)).to.equal('24.06.2023  ·  16:00');
  });
  it('getDeadline initial', () => {
    expect(getDeadline(FristtypType.Abstimmungsende, fristen, true)).to.equal('24.08.2023  ·  16:00');
  });
});

describe('Test for getFileLink', () => {
  it('getFileLink', () => {
    expect(getFileLink('123')).to.equal('/egesetzgebung-platform-backend/file/download/123');
  });
});
