// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { de_contacts } from './contacts';

export const de = {
  enorm: {
    help: {
      drawerTitle: 'Abstimmung von Regelungsentwürfen',
      section1:
        '<p>In der Anwendung Abstimmung können Sie Abstimmungen mit Regelungsentwürfen einleiten sowie an Abstimmungen teilnehmen, zu denen Sie eingeladen wurden.</p>' +
        '<p>Zudem haben Sie die Möglichkeit, eine Abstimmung vor Versand als Entwurf zwischenzuspeichern und den Prozess zu einem späteren Zeitpunkt fortzusetzen. Geschlossene Abstimmungen bzw. Bitten um Mitzeichnung können archiviert werden. Sie behalten weiterhin Lese-Zugriff auf archivierte Elemente.</p>' +
        '<p>In der Anwendung werden Sie durch spezifische Hilfetexte zu den wichtigsten Begrifflichkeiten unterstützt.</p>',
    },
    generalErrorMsg:
      'Es ist ein unerwarteter Fehler aufgetreten. Bitte wenden Sie sich an den Support. Wir entschuldigen uns für entstandene Umstände. {{fehlercode}}',
    header: {
      linkBack: 'Zurück',
      linkHelp: 'Hilfe',
      linkHome: 'Startseite Abstimmung von Regelungsentwürfen',
      btnContinueLater: {
        title: 'Abstimmungseinleitung später fortsetzen?',
        hint: 'Speichern Sie Ihren Zwischenstand, um das Formular zu einem späteren Zeitpunkt abzuschicken. Unter dem Menüpunkt „Abstimmung“ im Reiter „Entwürfe“ können Sie es erneut aufrufen.',
        btnText: 'Zwischenspeichern',
      },
    },
    breadcrumbs: {
      projectName: 'Abstimmung',
      tabName: {
        meineAbstimmung: 'Meine Abstimmungen',
        bitteUmMitzeichnung: 'Mitzeichnungsbitten',
        meineUnterabstimmung: 'Meine Unterabstimmungen',
        entwuerfe: 'Entwürfe',
        anfragen: 'Anfragen',
        archiv: 'Archiv',
      },
      action: {
        abstimmungAnlegen: 'Neue Abstimmung einleiten',
        unterabstimmungAnlegen: 'Neue Unterabstimmung einleiten',
        abstimmungBearbeiten: 'Abstimmung bearbeiten',
        nachsteAbstimmungsrunde: 'Neue Abstimmungsrunde einleiten',
        abstimmungPruefen: 'Eingaben prüfen',
        abstimmungsrundePruefen: 'Eingaben prüfen',
        abstimmungBeantwortenPruefen: 'Bitte um Mitzeichnung',
        abstimmungBeantworten: 'Abstimmungsrunde beantworten',
        abstimmungBeantwortenAendern: 'Antwort ändern',
        abstimmungsrunde: 'Abstimmungsrunde',
        anfrageAnlegen: 'Teilnahme an der Abstimmung {{abstimmungName}} anfragen',
      },
    },
    vote: {
      noAnswer: 'Keine Angabe',
      generalInfoTitle: 'Allgemeine Informationen zur Abstimmung',
      typeOfDocument: {
        label: 'Format des Dokuments',
        typeEnormDocument: 'eNorm-Dokument',
        typeEditorDocument: 'Editor-Dokument',
        error: 'Bitte geben Sie die Art des Dokuments an.',
        UnterabstimmungInfo:
          'Hinweis: Unterabstimmungen zu laufenden Abstimmungen können aktuell nur mit e-Norm Dokumenten durchgeführt werden.',
      },
      typeOfVote: {
        label: 'Art der Abstimmung',
        typeHomeVote: 'Hausabstimmung',
        typeVorhabenclearing: 'Vorhabenclearing',
        typeDepartmentVote: 'Ressortabstimmung',
        typeFinalVote: 'Schlussabstimmung',
        typeHouseManagementVote: 'Abstimmung der Vorlage für den Regierungsentwurf mit Hausleitung',
        typeNestedVote: 'Unterabstimmung',
        typeOtherVote: 'Sonstige Abstimmung',
        error: 'Bitte geben Sie die Art der Abstimmung an.',
        homeVoteInfo: {
          title: 'Hausabstimmung',
          text: 'Die Funktionen der Hausabstimmung können Sie für alle Abstimmungen im eigenen Haus wie z.B. Abstimmungen innerhalb einer Abteilung nutzen.',
          endingInfo: 'letzte Hausabstimmung beendet am: ',
        },
        vorhabenClearingInfo: {
          title: 'Vorhabenclearing',
          text1:
            'Für die Einleitung einer Ressortabstimmung zu einem Gesetzentwurf ist die Zustimmung des Bundeskanzleramtes bzw. der im Vorhabenclearing zu beteiligenden Ressorts notwendig.',
          text2:
            'Sie haben die Möglichkeit, eine Abstimmung vor dem Versand als Entwurf zwischenzuspeichern und die Bearbeitung zu einem späteren Zeitpunkt fortzusetzen.',
        },
        departmentInfo: {
          title: 'Ressortabstimmung',
          text1: `Hier können Sie eine Ressortabstimmung von Regelungsentwürfen gemäß <a href={{linkGGO_45_1}} target="_blank">§45 Absatz 1 GGO</a> einleiten. Es werden alle Stellen hinzugefügt, die an einer Ressortabstimmung zu beteiligen sind (<a href={{linkGGO_45_1}} target="_blank">§45 Absatz 1</a> und <a href={{linkGGO_45_3}} target="_blank">3 GGO</a>, <a href={{linkGGO_74_5}} target="_blank">§74 Absatz 5 GGO</a> und <a href={{linkGGO_21_1}} target="_blank">§21 Absatz 1 GGO</a> sowie <a href={{linkGGO_24_1}} target="_blank">§24 Absatz 1 GGO</a>).`,
          text2: `Sie haben die Möglichkeit, eine Abstimmung vor Versand als Entwurf zwischenzuspeichern und die Bearbeitung zu einem späteren Zeitpunkt fortzusetzen.`,
        },
        finalVoteInfo: {
          title: 'Schlussabstimmung',
          text1:
            'Um die Kabinettreife eines Regelungsentwurfs herbeizuführen, muss zunächst eine Schlussabstimmung mit den am Vorhaben beteiligten Ressorts durchgeführt werden. Spätestens jetzt muss das Bundesministerium der Justiz bestätigen, dass die rechtssystematische und rechtsförmliche Prüfung nach <a href="{{link}}" target="_blank">§ 46 GGO</a> durchgeführt wurde.',
          text2:
            'Sie haben die Möglichkeit, eine Abstimmung vor Versand als Entwurf zwischenzuspeichern und die Bearbeitung zu einem späteren Zeitpunkt fortzusetzen.',
        },
        houseManagementVoteInfo: {
          title: 'Abstimmung der Vorlage für den Regierungsentwurf mit Hausleitung',
          text1:
            'Bevor eine Vorlage für den Regierungsentwurf ins Kabinett eingebracht werden kann, muss sie von der Hausleitung unterzeichnet werden und dann vom Bundeskanzleramt die Kabinettvorlage erstellt werden. Die Hausleitung erhält die Vorlage in der Regel über das Kabinett- und Parlamentsreferat.',
        },
        nestedVoteInfo: {
          title: 'Unterabstimmung',
          text: '<p>Die Funktionen der Unterabstimmung können Sie für Abstimmungen mit ausgewählten Teilnehmenden einer Abstimmung nutzen. Wählen Sie dazu die Option „Unterabstimmung einleiten“ aus dem Aktionsmenü neben einer laufenden Abstimmung aus der Tabelle auf der Startseite des Moduls Abstimmung aus.</p>',
        },
        otherVoteInfo: {
          title: 'Sonstige Abstimmung',
          text: 'Sie können die Funktion „Sonstige Abstimmung“ für alle Abstimmungen nutzen, bei denen es sich nicht um die zuvor genannten Arten der Abstimmung handelt.',
        },
      },
      datenBlattAnPkp: {
        label: 'Regelungsvorhaben an PKP senden',
        sendToPkp: 'Ja, Regelungsvorhaben an PKP senden',
        doNotSendAnPkP: 'Nein, Regelungsvorhaben nicht an PKP senden',
        infoText: `<p>Sie können hier schon vorab das Planungs- und Kabinettmanagement-Programm (PKP) darüber informieren, dass Ihr Regelungsvorhaben in den Abstimmungsprozess eingetreten ist. Damit wird zu diesem Zeitpunkt das Regelungsvorhaben in PKP übernommen. Das Regelungsvorhaben wird dann in PKP in die interne Vorhabenplanung (iV) übernommen. Um das Regelungsvorhaben auch für Mitarbeitende außerhalb des eigenen Ressorts sichtbar zu machen, muss es in PKP manuell in die übergreifende Vorhabenplanung (üV) verschoben werden.</p>
        <p>Das Senden an PKP sowie etwaige dortige Verschiebungen ändern nichts an Ihren Zugriffsrechten auf das Regelungsvorhaben hier in der E-Gesetzgebung.</p>`,
      },
      earlyVoteBundeskanzleramt: {
        label: 'Vorhabenclearing durchgeführt',
        answerYes: 'Ja, ist erfolgt am:',
        answerNo: 'Nein',
        errorRequired: 'Bitte wählen Sie ein Datum für das Vorhabenclearing.',
        info: `<li> 1. Die Bundesministerien unterrichten das Bundeskanzleramt frühzeitig über alle Angelegenheiten von grundsätzlicher politischer Bedeutung (<a href={{linkGGO_24}} target="_blank">§ 24 GGO</a>). Darüber hinaus informieren sie das Bundeskanzleramt regelmäßig im Rahmen des gemeinsamen Planungs- und Kabinettmanagement-Programms (PKP) sowie für die wöchentlich aktualisierte Kabinettzeitplanung über geplante Gesetzentwürfe (<a href={{linkGGO_40}} target="_blank">§ 40 GGO</a>). </li> <li>2. Vor der Einleitung der Ressortabstimmung sendet das federführende Ressort den Gesetzentwurf bzw. das Eckpunktepapier zur Befassung im Bundeskabinett an das Kanzleramt. Dabei soll das federführende Ressort begleitend darauf hinweisen, inwieweit mit dem Entwurf ein Vorhaben des Koalitionsvertrages umgesetzt wird, ob mit dem Entwurf EU-Recht umgesetzt wird (1:1 Umsetzung oder darüber hinausgehend) und ob von Seiten der Länder oder von Verbänden mit Widerstand zu rechnen ist. BMWK und BMF werden als koordinierende Ressorts vom federführenden Ressort zeitgleich darüber informiert, welches Vorhaben in das Vorhabenclearing gegeben wurde. Diese Übersendung soll in der Regel mindestens drei Werktage vor der geplanten Einleitung der Ressortabstimmung erfolgen.</li>`,
        labelDate: 'Datum des Vorhabenclearings (tt.mm.jjjj)',
      },
      participationBriefing: {
        label: 'Beteiligung/Unterrichtung',
        placeholder: 'Bitte auswählen',
        sign: 'Bitte um Mitzeichnung (§45 Absatz 1 GGO)',
        comment: 'Bitte um Stellungnahme',
        informOthers: 'Unterrichtung anderer Stellen (§48 GGO)',
        error: 'Bitte füllen Sie das Pflichtfeld aus.',
      },
      optionalTests: {
        label: 'Optionale Prüfungen ergänzen',
        legalSystematics: 'Rechtssystematik- und Rechtsförmlichkeitsprüfung (§46 Absatz 1 GGO)',
        europeanLaw: 'Europarechtliche Prüfung (§§45 Absatz 1, 74 Absatz 1 GGO)',
        basicLaw: 'Prüfung der Vereinbarkeit mit dem Grundgesetz (§45 Absatz 1 GGO)',
      },
      regelungsvorhaben: {
        label: 'Dazugehöriges Regelungsvorhaben',
        placeholder: 'Bitte auswählen',
        error: 'Bitte geben Sie das dazugehörige Regelungsvorhaben an.',
      },
      regelungsentwurf: {
        label: 'Regelungsentwurf (Editor-Dokumentenmappe)',
        placeholder: 'Bitte auswählen',
        hinweisTitle: 'Auswahl eines Regelungsentwurfs',
        hinweisContent:
          'Sie haben keinen Regelungsentwurf für dieses Regelungsvorhaben erstellt. Bitte wählen Sie ein anderes Regelungsvorhaben aus.',
        error: 'Bitte wählen Sie einen Regelungsentwurf aus.',
        errorNoRE:
          'Sie haben keinen Regelungsentwurf dür dieses Regelungsvorhaben erstellt. Bitte wählen Sie ein anderes Regelungsvorhaben aus.',
        editorDoc: {
          hinweisTitle: 'Ihre Zugriffsrechte',
          hinweisContent:
            'Mit Einleitung einer Abstimmung werden die Zugriffsrechte für das abzustimmende Dokument geändert. Die an der Abstimmung Teilnehmenden können das Dokument lesen. Die Person, die das Dokument erstellt hat, verliert gleichzeitig die Möglichkeit, es zu bearbeiten. Dadurch ist sichergestellt, dass nachvollziehbar bleibt, welcher Stand des Dokuments zu welchem Zeitpunkt abgestimmt wurde.',
        },
      },
      titleVote: {
        label: 'Titel der Abstimmung',
        additionalLabel: '(z.B. Abkürzung des Regelungsvorhabens)',
        placeholder: 'Wählen Sie bitte erst ein Regelungsvorhaben aus',
        error: {
          noInput: 'Bitte geben Sie den Titel der Abstimmung an.',
          inputTooLong: 'Die maximal zulässige Anzahl von {{maxChars}} Zeichen für einen Titel wurde überschritten.',
        },
      },
      regelungsentwurfUpload: {
        error: 'Bitte laden Sie einen Regelungsentwurf hoch.',
      },
      uploadErrorConfirm: 'Okay, gelesen',
      uploadFilesTitle: 'Dateien auswählen',
      uploadDraftTitle: 'Regelungsentwurf hochladen (PDF/Word-Dokument, max. 20 MB)',
      uploadDraftFormatShort: 'DOCX (eNorm oder Word)',
      uploadDraftErrorTitle: 'Abschnitt „Regelungsentwurf hochladen“',
      uploadDraftTitleDrawer: 'Regelungsentwurf hochladen',
      uploadDraftTitleDrawerText:
        '<p>Mit Upload der Dokumente wird bestätigt, dass diese keine personenbezogenen Daten nach <a href="https://dejure.org/gesetze/DSGVO/4.html" target="_blank">Art. 4 Nr. 1 DSGVO</a> beinhalten, die personenbezogenen Daten anonymisiert wurden oder eine Einwilligung der betroffenen Person nach <a href="https://dejure.org/gesetze/DSGVO/6.html" target="_blank">Art. 6 Nr. 1 lit. a DSGVO</a> vorliegt.</p>',
      uploadDraftDrawerTooltip:
        'Hier kann nur ein Regelungsentwurf hochgeladen werden. Bitte wählen Sie eine Datei aus. Bei Bedarf können Sie die Bezeichnung der Datei über das Stift-Icon anpassen.',
      uploadedDraftTitle: 'Hochgeladener Regelungsentwurf',
      uploadAttachmentsTitle: 'Hochladen von Anlagen ( PDF/Word-Dokumente, max. 20 MB)',
      uploadAttachmentsFormatShort: 'DOCX (Word) oder PDF',
      uploadAttachmentsErrorTitle: 'Abschnitt „Hochladen von Anlagen“',
      uploadedAttachmentsTitle: 'Hochgeladene Anlagen',
      uploadAttachmentsDeleteAll: 'Alle Anlagen entfernen',
      uploadAttachmentsDeleteAllPopover: {
        title: 'Möchten Sie alle Anlagen löschen?',
        text: 'Wenn Sie die Anlagen löschen, werden sie hier entfernt.',
        delete: 'Löschen',
        cancel: 'Abbrechen',
      },
      uploadButtonRegelungsentwurf: 'Regelungsentwurf hochladen',
      uploadButtonAttachments: 'Anlagen hochladen',
      uploadHintRegelungsentwurf:
        'Ziehen Sie den Regelungsentwurf einfach in dieses Feld um ihn hochzuladen oder klicken Sie hinein, um den Auswahldialog zu öffnen. Eine Mehrfachauswahl ist nicht möglich.',
      uploadHintAttachments:
        'Ziehen Sie Ihre Anlagen einfach in dieses Feld, um sie hochzuladen, oder klicken Sie hinein, um den Auswahldialog zu öffnen. Eine Mehrfachauswahl ist möglich.',
      uploadError:
        'Dieses Dateiformat kann nicht verarbeitet werden. Bitte laden Sie die Datei im Format {{format}} erneut hoch. Achten Sie dabei auf die maximale Dateigröße von {{fileSize}} MB.',
      uploadErrorFileNameLength: 'Maximale Länge der Dateiname {{maxChars}} wurde überschritten',
      uploadChangeFileNameLabel: 'Neuer Dateiname',
      uploadChangeFileNameError: 'Bitte geben Sie einen Dateinamen ein.',
      uploadChangeFileNameCancelTextButton: 'Abbrechen',
      uploadChangeFileNameModalTitel: 'Dateinamen ändern',
      deadlinesTitle: 'Fristen',
      deadlinesTitleDrawerTitle: 'Fristen',
      deadlinesTitleDrawerText:
        '<p>Sie können verschiedene Fristen angeben:</p>' +
        '<p><strong>Frist Ende der Abstimmungsrunde (gemäß <a href="{{link50}}" target="_blank">§50 GGO</a>):</strong></p>' +
        '<p>Diese Frist gibt an, bis wann die Eingeladenen an der Abstimmungsrunde teilnehmen können. Bei Überschreitung dieser Frist, müssen die Teilnehmenden eine nachträgliche Beteiligung bei Ihnen anfragen.</p>' +
        '<p><strong>Zusätzliche Fristen hinzufügen:</strong></p>' +
        '<p>An dieser Stelle können Sie bei Bedarf weitere Fristen wie zum Beispiel eine Frist zur Rückmeldung bei Einspruch gegen eine frühzeitige Länder- und Verbändebeteiligung (gemäß <a href="{{link47}}" target="_blank">§47 Abs.1 GGO</a>) anlegen. Diese Fristen werden den anderen Beteiligten angezeigt.</p>',
      deadlineVote: {
        title: 'Frist Ende der Abstimmungsrunde',
        dateLabel: 'Datum (tt.mm.jjjj)',
        timeLabel: 'Uhrzeit (hh:mm)',
        checkboxTimeLabel: 'Frist auf Dienstschluss des gewählten Tages setzen',
        checkboxVerschweigensfristLabel: 'Als Verschweigensfrist kennzeichnen',
        checkboxVerschweigensfristDrawerText: `<p>Wenn Sie eine Frist als Verschweigensfrist kennzeichnen, werden die Teilnehmerinnen und Teilnehmer darüber informiert.</p>
          <p>Eine ausbleibende Antwort der Abstimmungsteilnehmerinnen und -teilnehmer kann dann als Mitzeichnung gewertet werden.</p>`,
        errorDate: 'Bitte wählen Sie ein Datum für die Frist Ende der Abstimmungsrunde aus.',
        errorTime: 'Bitte wählen Sie eine Uhrzeit für die Frist Ende der Abstimmungsrunde aus.',
        errorTimeFormat: 'Bitte geben Sie eine gültige Uhrzeit im Format „hh:mm“ ein.',
        errorDateFormat: 'Bitte geben Sie ein gültiges Datum im Format „tt.mm.jjjj“ ein.',
        errorTimeFuture: 'Die Frist muss in der Zukunft liegen.',
        errorTimeAfterMax: 'Die Frist muss vor {{date}} enden.',
        errorTimeBeforeMin: 'Die Frist muss nach {{date}} enden.',
      },
      replyInternetDate: {
        title: 'Frist Rückmeldung zur Veröffentlichung im Internet/Intranet',
        dateLabel: 'Datum (tt.mm.jjjj)',
        timeLabel: 'Uhrzeit (hh:mm)',
        errorDate: 'Bitte geben Sie ein Datum für die Frist zur Veröffentlichung im Internet/Intranet ein.',
        errorTime: 'Bitte geben Sie eine Uhrzeit für die Frist zur Veröffentlichung im Internet/Intranet ein.',
        errorTimeFormat: 'Bitte geben Sie eine gültige Uhrzeit im Format „hh:mm“ ein.',
        errorDateFormat: 'Bitte geben Sie ein gültiges Datum im Format „tt.mm.jjjj“ ein.',
        errorTimeFuture: 'Die Frist muss in der Zukunft liegen.',
        errorTimeAfterMax: 'Die Frist muss vor {{date}} enden.',
      },
      replyDate: {
        title: 'Frist Rückmeldung zur frühzeitigen Beteiligung der Länder und Verbände',
        dateLabel: 'Datum (tt.mm.jjjj)',
        timeLabel: 'Uhrzeit (hh:mm)',
        errorDate: 'Bitte geben Sie ein Datum für die Frist Länder und Verbände ein oder löschen Sie die Uhrzeit.',
        errorTime: 'Bitte geben Sie eine Uhrzeit für die Frist Länder und Verbände ein oder löschen Sie das Datum.',
        errorTimeFormat: 'Bitte geben Sie eine gültige Uhrzeit im Format „hh:mm“ ein.',
        errorDateFormat: 'Bitte geben Sie ein gültiges Datum im Format „tt.mm.jjjj“ ein.',
        errorTimeFuture: 'Die Frist muss in der Zukunft liegen.',
        errorTimeAfterMax: 'Die Frist muss vor {{date}} enden.',
      },
      additionalDeadline: {
        title: 'Zusätzliche Frist',
        label: 'Titel der zusätzlichen Frist',
        placeholder: 'Bitte ausfüllen',
        addNewDeadline: 'Frist hinzufügen',
        deleteDeadline: 'Frist entfernen',
        errorTitle: 'Bitte geben Sie einen Titel für die zusätzliche Frist {{index}} ein oder löschen Sie diese Frist.',
        errorDate: 'Bitte geben Sie ein Datum für die zusätzliche Frist {{index}} ein oder löschen Sie diese Frist.',
        errorTime: 'Bitte geben Sie eine Uhrzeit für die zusätzliche Frist {{index}} ein oder löschen Sie diese Frist.',
        errorTitleLength: 'Maximale Länge des Titels für die Frist {{maxChars}} wurde überschritten',
        errorTimeFormat: 'Bitte geben Sie eine gültige Uhrzeit im Format „hh:mm“ ein.',
        errorDateFormat: 'Bitte geben Sie ein gültiges Datum im Format „tt.mm.jjjj“ ein.',
        errorTimeFuture: 'Die Frist muss in der Zukunft liegen.',
        errorTimeAfterMax: 'Die Frist muss vor {{date}} enden.',
      },
      emailTitle: 'Einladungs-E-Mail',
      leserechte: {
        hinweisTitle: 'Leserechte für das Datenblatt des Regelungsvorhabens',
        hinweisContent:
          'Mit Einleitung dieser Abstimmung erhalten alle Adressatinnen und Adressaten Leserechte für das Datenblatt des zugehörigen Regelungsvorhabens. Dieser Zugriff bleibt auch nach Fristende der Abstimmungsrunde erhalten.',
      },
      emailAddress: {
        error: 'Bitte geben Sie die Adressatinnen und Adressaten an.',
        errorShouldBeEmail: 'Bitte geben Sie eine gültige E-Mail Adresse ein.',
        drawerHausabstimmungTitle: 'Beteiligung in den Hausabstimmungen',
        drawerHausabstimmungText:
          'Wenn ein Vorgang mehrere Organisationseinheiten betrifft, müssen diese nach <a href="{{link15}}" target="_blank">§15 GGO</a> rechtzeitig beteiligt werden.',
        drawerRessortabstimmungTitle: 'Beteiligung in den Ressortabstimmungen',
        drawerRessortabstimmungText: `<p>Bitte fügen Sie an dieser Stelle nur Personen als Adressatinnen und Adressaten hinzu. E-Mail Adressen von Postfächern und Verteilern können Sie im nachfolgenden Feld „CC/E-Mail-Verteiler“ ergänzen.</p>
          <h4>Obligatorische Beteiligungen:</h4>
          <p>In einer Ressortabstimmung zu beteiligen sind immer alle Stellen nach <a href="{{link45}}" target="_blank">§45 Absatz 1</a> und <a href="{{link3}}" target="_blank">3 GGO</a>, <a href="{{link74}}" target="_blank">§74 Absatz 5 GGO</a> und <a href="{{link21}}" target="_blank">§21 Absatz 1 GGO</a> sowie <a href="{{link24}}" target="_blank">§24 Absatz 1 GGO</a> insbesondere:</p>
          <ul>
            <li>alle Bundesministerien, die vom Entwurf betroffen bzw. deren Geschäftsbereiche berührt sind</li>
            <li>der Nationale Normenkontrollrat</li>
            <li>Bundesministerien des Innern und der Justiz</li>
            <li>Bundesbeauftragte für Wirtschaftlichkeit in der Verwaltung</li>
            <li>das Bundeskanzleramt</li>
          </ul>
          <p>Ein entsprechender Vorschlag für die Postfächer der immer zu beteiligenden Stellen wurden bereits im Eingabefeld CC/E-Mail-Verteiler.</p>
          <h4>Fakultative Beteiligung der Beauftragten und Koordinatoren (<a href="{{link21}}" target="_blank">gemäß §21 Absatz 1 GGO</a>; <a href="{{link45}}" target="_blank">§45 Absatz 3 GGO</a>):</h4>
          <p>Je nach Fachlichkeit sind einige Beauftragte und Koordinatoren zu beteiligen. Diese müssen von Ihnen ausgewählt und händisch hinzugefügt werden. Eine entsprechende Aufstellung finden Sie auf der Webseite des Bundesministerium des Innern für Bau und Heimat.</p>
          `,
        teilnehmer: {
          label: 'Für Teilnahme vorgemerkte Adressatinnen und Adressaten*',
          placeholder: 'Bitte ausfüllen',
          error: 'Bitte geben Sie die Adressatinnen und Adressaten an.',
          errorShouldBeEmail: 'Bitte geben Sie eine gültige E-Mail Adresse ein.',
          drawer: {
            title: 'Für Teilnahme vorgemerkte Adressatinnen und Adressaten',
            text: `<p>Sofern Sie bereits eine oder mehrere Personen ausgewählt haben, sehen Sie hier die für die Teilnahme ausgewählten Adressatinnen und Adressaten.</p><h2>Systemseitige Vorbefüllung</h2> <p>Wenn Sie als Art der Abstimmung „Ressortabstimmung“ oder „Schlussabstimmung“ auswählen, werden die zu beteiligenden Stellen (gemäß <a href="{{link45}}" target="_blank">§45 Absatz 1</a> und <a href="{{link45}}" target="_blank">3 GGO</a>, <a href="{{link74}}" target="_blank">§74 Absatz 5 GGO</a> und <a href="{{link21}}" target="_blank">§21 Absatz 1 GGO</a> sowie <a href="{{link24}}" target="_blank">§24 Absatz 1 GGO</a>) automatisch durch das System als Adressaten hinzugefügt.</p>
            <p>Auch für das „Vorhabenclearing“ werden die zu beteiligenden Stellen automatisch hinzugefügt. Eine Teilnahme weiterer Adressatinnen und Adressaten ist hier nicht möglich.</p>`,
          },
          search: {
            label: 'Suche nach Adressatinnen und Adressaten',
            hint: '(z. B. Name, Ressort, Verteiler, E-Mail-Adresse, E-Mail-Liste)',
            drawer: {
              title: 'Suche nach Adressatinnen und Adressaten',
              text: `<p>Hier können Sie die Adressdatenbank durchsuchen. Tippen Sie dazu einfach den Namen, das Ressort, einen Verteiler oder eine E-Mail-Adresse ein. Sie können hier auch E-Mail-Listen verwenden, die Sie über die Einstellungen verwalten können.<br>
                    Sie können mehrere Suchbegriffe eintragen. Trennen Sie diese dazu einfach durch Leerzeichen. Die Begriffe werden dann für die Suche kombiniert. Die Treffer enthalten alle angegebenen Suchbegriffe. Es wird eine Und-Verknüpfung der Begriffe verwendet.</p>
                  <p>Sie können außerdem gültige E-Mail-Adressen von Personen eingeben, die noch nicht in der Plattform angemeldet sind. Gemeinsam mit der Einladung zur Abstimmung erhalten diese dann die Möglichkeit, sich zu registrieren. Danach können sie auf die Inhalte der Abstimmung zugreifen.</p>
                  <p>Adressatinnen und Adressaten dürfen die Einladungs-E-Mail an weitere Personen weiterleiten, die aus ihrer Sicht an der Abstimmung teilnehmen sollten. Diese Personen können bei Ihnen die Teilnahme an der Abstimmung anfragen. Auch eine Weiterleitung an Postfächer ist möglich, die Teilnahmeanfrage allerdings nur für Personen.</p>
                  `,
            },
          },
        },
        teilnehmerInCc: {
          label: 'Zur Kenntnisnahme vorgemerkte Adressatinnen und Adressaten',
          placeholder: 'Bitte ausfüllen',
          error: 'Bitte geben Sie Adressatinnen und Adressaten in CC/E-Mail-Verteiler an.',
          errorShouldBeEmail: 'Bitte geben Sie eine gültige E-Mail Adresse ein.',
          drawer: {
            title: 'Zur Kenntnisnahme vorgemerkte Adressatinnen und Adressaten',
            text: `<p>Sofern Sie bereits eine oder mehrere Personen ausgewählt haben, sehen Sie hier die zur Kenntnisnahme ausgewählten Adressatinnen und -Adressaten.</p>`,
          },
          search: {
            label: 'Suche nach CC-Adressatinnen und -Adressaten',
            hint: '(z. B. Name, Ressort, Verteiler, E-Mail-Adresse, E-Mail-Liste)',
            drawer: {
              title: 'Suche nach CC-Adressatinnen und -Adressaten',
              text: `<p>Hier können Sie die Adressdatenbank durchsuchen. Tippen Sie dazu einfach den Namen, das Ressort, einen Verteiler oder eine E-Mail-Adresse ein. Sie können hier auch E-Mail-Listen verwenden, die Sie über die Einstellungen verwalten können.<br>
                      Sie können mehrere Suchbegriffe eintragen. Trennen Sie diese dazu einfach durch Leerzeichen. Die Begriffe werden dann für die Suche kombiniert. Die Treffer enthalten alle angegebenen Suchbegriffe. Es wird eine Und-Verknüpfung der Begriffe verwendet.</p>
                    <p>Sie können außerdem gültige E-Mail-Adressen von Personen eingeben, die noch nicht in der Plattform angemeldet sind. Gemeinsam mit der Einladung zur Abstimmung erhalten diese dann die Möglichkeit, sich zu registrieren. Danach können sie im Lesezugriff auf die Inhalte der Abstimmung zugreifen.</p>
                    <p>Adressatinnen und Adressaten dürfen die Einladungs-E-Mail an weitere Personen weiterleiten, die aus ihrer Sicht an der Abstimmung teilnehmen sollten. Diese Personen können bei Ihnen die Teilnahme an der Abstimmung anfragen. Auch eine Weiterleitung an Postfächer ist möglich, die Teilnahmeanfrage allerdings nur für Personen.</p>
                    `,
            },
          },
        },
        msg: {
          noAddresses: 'Es wurde noch keine Adressatin bzw. kein Adressat übernommen.',
          notExist: 'Es wurde kein Ergebnis gefunden oder eine ungültige E-Mail-Adresse eingegeben.',
          moreThan10: 'Es gibt mehr als 10 Ergebnisse. Bitte verfeinern Sie Ihre Suche.',
          tooShort: 'Der Suchstring ist zu kurz. Für die Suche muss mindestens ein Wort 3 Zeichen oder länger sein',
          btnApply: 'Übernehmen',
          load: 'Laden...',
          results: 'Ergebnisse',
          delete: 'Löschen',
          moreItems: 'weitere einblenden',
        },
        obligatoryEmails: de_contacts.enorm.vote.emailAddress.obligatoryEmails,
        vorhabenclearing: de_contacts.enorm.vote.emailAddress.vorhabenclearing,
      },
      emailAddressInCC: {
        label: 'CC/E-Mail-Verteiler',
        placeholder: 'Bitte ausfüllen',
        error: 'Bitte geben Sie Adressatinnen und Adressaten in CC/E-Mail-Verteiler an.',
        errorShouldBeEmail: 'Bitte geben Sie eine gültige E-Mail Adresse ein.',
        drawerCcTitle: 'CC/E-Mail-Verteiler - Personen über Abstimmung(-srunde) informieren',
        drawerCcText:
          'Hier können Sie Personen über die Abstimmung(-srunde) informieren sowie Mailpostfächer bzw. -verteiler hinzufügen. Beachten Sie, dass Adressatinnen und Adressaten die im CC/E-Mail-Verteiler hinzugefügt wurden, eine E-Mail zur Abstimmungs(-runde) erhalten aber nicht an der Abstimmung(-srunde) teilnehmen können.',
        hinweisTitle: 'Systemseitige Vorbefüllung',
        hinweisContent:
          'Wenn Sie als Art der Abstimmung „Ressortabstimmung“ auswählen, werden die gemäß <a href="{{link45}}" target="_blank">§45 Absatz 1</a> und <a href="{{link45}}" target="_blank">3 GGO</a>, <a href="{{link74}}" target="_blank">§74 Absatz 5 GGO</a> und <a href="{{link21}}" target="_blank">§21 Absatz 1 GGO</a> sowie <a href="{{link24}}" target="_blank">§24 Absatz 1 GGO</a> zu beteiligenden Stellen automatisch durch das System als Adressaten im folgenden Feld „CC“ hinzugefügt.',
      },
      emailSubject: {
        label: 'Betreff der Einladung',
        placeholder: 'Wählen Sie bitte erst ein Regelungsvorhaben und die Art der Abstimmung aus',
        placeholder2: 'Wählen Sie bitte die Art der Abstimmung aus',
        placeholder3: 'Wählen Sie bitte ein Regelungsvorhaben aus',
        error: 'Bitte geben Sie den Betreff der Einladung an.',
        errorSubjectLength: 'Maximale Länge des Betreffs {{maxChars}} wurde überschritten.',
      },
      emailText: {
        label: 'Einladungstext',
        hint: 'Der Einladungstext wird gemäß Ihrem Anliegen automatisiert erstellt und kann nicht geändert werden.',
      },
      emailComment: {
        label: 'Ihre Kommentare',
        hint: '(maximal {{maxChars}} Zeichen)',
        error: 'Die maximal zulässige Anzahl von {{maxChars}} Zeichen wurde überschritten.',
      },
      emailPlaceholders: {
        phonePlaceholder: 'Telefonnummer: ',
        standardPlaceholder: 'die Person hat keine Angabe gemacht',
        emptyPlaceholder: '',
      },
      btnSubmit: 'Eingaben prüfen',
      btnCancel: 'Abbrechen',
      successCreatedMsg: 'Die Zusammenfassung der Abstimmung wurde erfolgreich erstellt.',
      successUpdatedMsg: 'Die Zusammenfassung der Abstimmung wurde erfolgreich aktualisiert.',
      successSaveCurrentVoteMsg: 'Die Änderungen wurden erfolgreich gespeichert.',
      errorParticipant: 'Sie dürfen sich nicht selbst als Adressatin oder Adressat hinzufügen',
      errorParticipantCC: 'Sie dürfen sich nicht selbst zum CC/E-Mail-Verteiler hinzufügen',
      errorParticipantCCDuplication:
        'Sie dürfen dieselbe Person nicht gleichzeitig in der Adress- und CC-Zeile auswählen.',
      errorDraftDeleted:
        'Der Abstimmungsentwurf konnte nicht gefunden werden. Möglicherweise wurde der Entwurf zwischenzeitlich gelöscht, weil der Status des Regelungsvorhabens sich geändert hat.',
      errorRvInactive: 'Sie können diese Aktion nicht tätigen. Das Regelungsvorhaben ist nicht mehr aktiv.',
      continueLater: 'Abstimmungseinleitung später fortsetzen?',
      continueLaterTooltip:
        'Speichern Sie Ihren Zwischenstand, um das Formular zu einem späteren Zeitpunkt abzuschicken. Unter dem Menüpunkt „Abstimmung“ im Reiter „Entwürfe“ können Sie es erneut aufrufen.',
      zwischenspeichernLink: 'Zwischenspeichern',
      confirmModal: {
        title: 'Möchten Sie die Einleitung der Abstimmung wirklich abbrechen?',
        btnCancelConfirmOk: 'Ja, abbrechen',
        btnCancelConfirmNo: 'Nein, nicht abbrechen',
      },
      emailsSearch: {
        placeholder: 'Suchbegriff eingeben',
        btnText: 'Suchen',
        btnClear: 'Suche zurücksetzen',
      },
      ressortAbstimmungWarningMessage: {
        title1: 'Regelungsvorhaben an PKP senden',
        title2: 'Unterrichtung des Bundeskanzleramts',
        body: 'Ihr Regelungsvorhaben wird mit Einleitung dieser Ressortabstimmung auch an PKP gesendet. Um den Anforderungen aus <a target="_blank" href={{link}}>{{linkTitle}}</a> zu genügen, muss das Regelungsvorhaben in PKP manuell aus der internen Vorhabenplanung (iV) in die übergeordnete Vorhabenplanung (üV) verschoben werden, damit es auch für Mitarbeitende außerhalb des eigenen Ressorts sichtbar wird.',
      },
      internetIntranetTitle: 'Veröffentlichung im Internet/Intranet der Bundesregierung',
      internetIntranetDrawerText:
        '<p> Gemäß <a href={{link}} target="_blank">§48 Abs. 3 GGO</a> ist vor der Einstellung des Gesetzentwurfes in das Internet oder das Intranet der Bundesregierung das Einvernehmen des Bundeskanzleramtes sowie das Benehmen der übrigen beteiligten Bundesministerien einzuholen. Das Bundeskanzleramt muss also der Veröffentlichung zustimmen, während beteiligte Ressorts informiert werden und innerhalb einer Frist Rückmeldung zur geplanten Veröffentlichung geben können.</p><p>Den Zeitpunkt der Veröffentlichung sowie die jeweilige Einbindung können Sie selbst entscheiden. Beispielsweise können Sie zunächst auf Einvernehmen des Bundeskanzleramts warten und in einer folgenden Abstimmungsrunde die beteiligten Ressorts um Benehmen bitten. Wenn Sie die Veröffentlichung nach Mitzeichnung aller Ressorts veranlassen möchten, können Sie dies im Rahmen einer letzten Abstimmungsrunde der Ressortabstimmung tun.</p><p>Laut <a href="https://www.open-government-deutschland.de/resource/blob/1567548/1871458/3eb272d7adece1680649212178782fdb/2018-11-15-transparenz-gesetzgebungsverfahren-data-data.pdf?download=1" target="_blank">Beschluss zur Transparenz in Gesetzgebungsverfahren</a> ist vorgesehen, dass Regelungsentwürfe spätestens nach dem Kabinettbeschluss im Intranet/Internet veröffentlichen werden.</p>',
      internetIntranetDescription:
        'Falls Sie das Bundeskanzleramt oder Teilnehmende dieser Abstimmung bezüglich einer geplanten Veröffentlichung im Internet oder Intranet einbinden möchten, wählen Sie diese entsprechend aus.',
      internetIntranetVeroffentlichungDescription:
        'Wählen Sie bitte aus, ob Sie die Veröffentlichung im Internet und/oder im Intranet der Bundesregierung vornehmen möchten.',
      bkAmtUmEinvernehmenBittenText: 'Bundeskanzleramt um Einvernehmen bitten',
      teilnehmendeUmBenehmenBittenText: 'Teilnehmende dieser Abstimmung um Benehmen bitten',
      bkAmtUmEinvernehmenBittenDrawerText:
        'Wenn Sie diese Option auswählen, erhält das Bundeskanzleramt mit Einleitung der Abstimmung eine E-Mail mit der Anfrage zur Veröffentlichung. Sie müssen das Bundeskanzleramt nicht zusätzlich zu den Adressatinnen und Adressaten dieser Abstimmung hinzufügen. Wenn das Bundeskanzleramt außerhalb der E-Gesetzgebung Rückmeldung gibt, können Sie diese Rückmeldung auf der Detailsicht eintragen.',
      teilnehmendeUmBenehmenBittenDrawerText:
        '<p>Mit Auswahl dieser Option können Sie Teilnehmenden dieser Abstimmung eine Frist zur Rückmeldung bezüglich der Veröffentlichung einstellen. Wenn Teilnehmende sich nicht innerhalb der Frist zurückmelden, gilt dies als Zustimmung.</p><p>Möchten Sie andere/weitere Ressorts zur Veröffentlichung beteiligen, als die Adressatinnen und Adressaten dieser Abstimmung, können Sie stattdessen eine Unterabstimmung mit den Ressorts zur Veröffentlichung einleiten.</p>',
      internetIntranetJa: 'Ja',
      internetIntranetNein: 'Nein',
      veroffentlichungInternetText: 'Veröffentlichung im Internet',
      veroffentlichungIntranetText: 'Veröffentlichung im Intranet der Bundesregierung',
      eingeschraenkt: {
        title: 'Zugang zur Abstimmung einschränken',
        label: 'Abstimmungsteilnahme einschränken',
        drawerText: `<p>Bei der Einleitung einer Abstimmung können Sie als Einleiterin oder Einleiter den Zugriff zu der Abstimmung einschränken. Diese Einstellung kann beim späteren Bearbeiten der Abstimmung auch noch geändert werden.</p>
                     <p>Bei offener Teilnahme können Personen mit dem Abstimmungslink direkt an der Abstimmung teilnehmen, ohne dass Sie der Teilnahme zuvor zustimmen müssen.</p>
                     <p>Bei eingeschränkter Teilnahme können Personen mit dem Abstimmungslink nur an der Abstimmung teilnehmen, nachdem Sie der Teilnahme zugestimmt haben.</p>`,
        hinweis:
          'Wenn diese Option aktiviert ist, müssen Personen mit dem Abstimmungslink zunächst eine Teilnahmeanfrage an Sie als Einleiterin oder Einleiter der Abstimmung senden. Erst nachdem Sie der Teilnahme zugestimmt haben, werden diese Personen der Abstimmung hinzugefügt.',
      },
    },
    abstimmungAnlegen: {
      mainTitle: 'Neue Abstimmung einleiten',
      mainTitleDrawer: 'Abstimmung einleiten',
      mainTitleDrawerText:
        'Geben Sie auf dieser Seite die benötigten Informationen ein, um eine Abstimmung (gemäß <a href="{{link15}}" target="_blank">§ 15 GGO</a> bzw. <a href="{{link45}}" target="_blank">§ 45 Absatz 1 GGO</a>) einzuleiten. Ebenfalls können Sie hier Stellungnahmen wie etwa die des Normenkontrollrats anfordern. Die Funktionen der Hausabstimmung können Sie für alle Abstimmungen im eigenen Haus wie z.B. Abstimmungen innerhalb einer Abteilung nutzen.',
    },
    nachsteAbstimmungsrunde: {
      mainTitle: 'Neue Abstimmungsrunde einleiten',
      mainTitleDrawer: 'Abstimmungsrunde einleiten',
      mainTitleDrawerText:
        'Geben Sie auf dieser Seite die benötigten Informationen für eine neue Abstimmungsrunde einer bestehenden Abstimmung ein, um eine Abstimmung (gemäß <a href="{{link15}}" target="_blank">§ 15 GGO</a> bzw. <a href="{{link45}}" target="_blank">§ 45 Absatz 1 GGO</a>) einzuleiten.',
    },
    unterabstimmungAnlegen: {
      mainTitle: 'Neue Unterabstimmung einleiten',
      mainTitleDrawer: 'Unterabstimmung einleiten',
      mainTitleDrawerText: '',
    },
    abstimmungBearbeiten: {
      mainTitle: 'Abstimmung bearbeiten',
      mainTitleDrawer: 'Abstimmung bearbeiten',
      mainTitleDrawerText:
        'Geben Sie auf dieser Seite die benötigten Informationen ein, um eine Abstimmung (gemäß <a href="{{link15}}" target="_blank">§ 15 GGO</a> bzw. <a href="{{link45}}" target="_blank">§ 45 Absatz 1 GGO</a>) einzuleiten. Ebenfalls können Sie hier Stellungnahmen wie etwa die des Normenkontrollrats anfordern.',
    },
    myVotes: {
      mainTitle: 'Abstimmung von Regelungsentwürfen',
      btnNewVote: 'Neue Abstimmung einleiten',
      placeholder: 'PLACEHOLDER',
      archiveAction: {
        archiveActionTitle:
          'Eine Archivierung können Sie nicht rückgängig machen. Möchten Sie diese Abstimmung wirklich archivieren?',
        archivedSuccess: 'Sie haben die Abstimmung erfolgreich archiviert',
        archiveConfirm: 'Ja, archivieren',
        archiveCancel: 'Nein, nicht archivieren',
      },
      fristStatusInfo: {
        title: 'Erklärung zu: Frist',

        content: {
          paragraphMeineAbstimmungen:
            'Innerhalb dieser Frist haben die Teilnehmerinnen und Teilnehmer der Abstimmung die Möglichkeit, Ihnen auf Ihre „Bitte um Mitzeichnung“ zu antworten. Sie können die Frist vor Ablauf anpassen, indem Sie die Abstimmung bearbeiten.',
          paragraphMitzeichnung:
            'Innerhalb dieser Frist können Sie die „Bitte um Mitzeichnung“ beantworten. Sie können bei der Einleiterin oder beim Einleiter der Abstimmung eine Fristverlängerung anfragen. Sofern Sie eine Fristverlängerung angefragt haben, wird der Status dieser Anfrage hier ebenfalls ausgegeben.',
          subtitle: 'Folgende Status der Frist sind möglich',
          entries: [
            { title: 'Abgelaufen', content: 'Die Frist der Abstimmungsrunde ist abgelaufen.' },
            {
              title: 'Geschlossen',
              content:
                'Der Status kann im Rahmen von Unterabstimmungen auftreten, wenn die zugehörige Hauptabstimmung abgelaufen ist, die Einleiterin oder der Einleiter der Unterabstimmung aus der dazugehörigen Hauptabstimmung entfernt wurde oder die Erstellerin oder der Ersteller der Unterabstimmung, an welcher Sie teilnehmen, entfernt wurde.',
            },
            {
              title: 'Fristverlängerung angefragt',
              content:
                ' Sie haben eine Fristverlängerung bei der Einleiterin oder beim Einleiter der Abstimmung angefragt. Die Einleiterin oder der Einleiter hat Ihre Anfrage noch nicht beantwortet.',
            },
            {
              title: 'Fristverlängerung abgelehnt',
              content: 'Ihre angefragte Fristverlängerung wurde abgelehnt.',
            },
          ],
        },
      },
      tabs: {
        myVotes: {
          tabNav: 'Meine Abstimmungen',
          imgText1: 'Abstimmungen schnell und übersichtlich einleiten',
          imgText2: 'Mitzeichnungen und Ablehnungen stets im Blick behalten',
          text: 'Hier haben Sie die Möglichkeit, Abstimmungen einzuleiten. Abstimmungen können Sie sowohl mit Editor-Dokumenten als auch mit eNorm/Word-Dateien durchführen.',
          table: {
            thead1: 'Regelungsvorhaben',
            thead2: 'Abstimmungsrunde',
            thead3: 'Dokument',
            thead4: 'Frist',
            thead5: 'Rückmeldungen',
            thead6: 'Aktionen',
            fristStatus: 'Abgelaufen',
            fristStatusGeschlossen: 'Geschlossen',
            fristStatusAnfrage: {
              OFFEN: 'Angefragt',
              ZUGESTIMMT: 'Zugestimmt',
              ABGELEHNT: 'Abgelehnt',
              ABGELAUFEN: 'Abgelaufen',
              ZUERUCKGEZOGEN: 'Zurückgezogen',
            },
            btnTextNextRound: 'Nächste Abstimmungsrunde einleiten',
            votingFinished: 'Alle Teilnehmer haben mitgezeichnet',
            menuItemHausabstimmungArchiv: 'Hausabstimmung archivieren',
            menuItemAbstimmungBearbeiten: 'Abstimmung bearbeiten',
            menuItemUnterabstimmungEinleiten: 'Unterabstimmung einleiten',
            menuItemZurueckziehen: {
              ABSTIMMUNGSTEILNAHME: 'Teilnahmeanfrage zurückziehen',
              FRISTVERLAENGERUNG: 'Fristverlängerungsanfrage zurückziehen',
            },

            filter: {
              displayname: {
                regelungsvorhaben: 'Regelungsvorhaben',
                regelungsvorhabenType: 'Abstimmungsarten',
                regelungsvorhabenNoSubvoteType: 'Abstimmungsarten',
                mitzeichnungsanfrage: 'Mitzeichnungsanfrage',
                anfragenType: 'Anfragentypen',
                initalTab: 'Ursprung',
                status: 'Status',
                statusMitzeichnung: 'Status',
                ersteller: 'Erstellerin oder Ersteller',
              },
              labelContent: 'Filtern nach',
            },
            EingangAsc: 'Erstellt (älteste zuerst)',
            EingangDesc: 'Erstellt (neueste zuerst)',
            fristAsc: 'Frist (älteste zuerst)',
            fristDesc: 'Frist (neueste zuerst)',
            bearbeitetAsc: 'Zuletzt bearbeitet (älteste zuerst)',
            bearbeitetDesc: 'Zuletzt bearbeitet (neueste zuerst)',
            abstimmungsrundeAsc: 'Abstimmungsrunde (älteste zuerst)',
            abstimmungsrundeDesc: 'Abstimmungsrunde (neueste zuerst)',
            sorterTitle: 'Sortieren nach',
            menuItemArchive: 'Abstimmung archivieren',
          },
        },
        sign: {
          tabNav: 'Mitzeichnungsbitten',
          imgText1: 'Mitzeichnungsbitten erhalten',
          imgText2: 'Anfragen beantworten',
          imgText3: 'Unterabstimmungen einleiten',
          text: 'Hier erhalten Sie Anfragen zu Abstimmungen mit der „Bitte um Mitzeichnung“ von anderen Nutzenden der E-Gesetzgebung. In der Übersicht können Sie alle Anfragen verfolgen und beantworten. Sie können außerdem Unterabstimmungen zu diesen Anfragen einleiten.',
          table: {
            thead1: 'Regelungsvorhaben',
            thead2: 'Abstimmungsrunde',
            thead3: 'Federführung',
            thead4: 'Erstellt',
            thead5: 'Eingang',
            thead6: 'Status',
            thead7: 'Frist',
            thead8: 'Aktionen',
            fristStatus: 'Abgelaufen',
            btnAnswerButtonText: 'Beantworten',
            menuItemAusserhalbFristBeantworten: 'Fristverlängerung anfragen',
            menuItemMitzeichnungArchiv: 'Mitzeichnung archivieren',
            status: {
              MITZEICHNUNG: 'Mitgezeichnet',
              MITZEICHNUNG_UNTER_VORBEHALT: 'Mitgezeichnet u. V.',
              MITZEICHNUNG_DURCH_VERSCHWEIGEN: 'Mitzeichnung durch Verschweigen',
              KEINE_MITZEICHNUNG: 'Abgelehnt',
              NICHT_BEARBEITET: 'Offen',
              CC: 'Kenntnisnahme',
              KEINE_MITZEICHNUNG_STELLUNGNAHME_DES_NATIONALEN_NORMENKONTROLLRATS: 'NKR-Stellungnahme abgegeben',
              TEILNAHME_ANGEFRAGT: 'Teilnahme angefragt',
              TEILNAHME_ZURUECKGEZOGEN: 'Teilnahmeanfrage zurückgezogen',
              TEILNAHME_ABGELEHNT: 'Teilnahme abgelehnt',
            },
            statusAsc: 'Bearbeitet',
            statusDesc: 'Offen',
            titleExpandableMail: 'Einladungstext',
            verlauf: {
              title: 'Verlauf',
              sentDate: 'Gesendet',
              from: 'Von',
              to: 'An',
              toData: '{{name}} (Sie)',
              comments: 'Kommentar',
              na: 'Keine Angaben',
              mitzeichnungsbitteTitle: 'Mitzeichnungsbitte für die {{type}} {{name}}',
              mitgezeichnetTitle: 'Mitgezeichnet: Mitzeichnungsbitte für die {{type}} {{name}}',
              mitgezeichnetUVTitle: 'Mitgezeichnet unter Vorbehalt: Mitzeichnungsbitte für die {{type}} {{name}}',
              keineMitzeichnungTitle: 'Abgelehnt: Mitzeichnungsbitte für die {{type}} {{name}}',
              stellungnahmeTitle: 'NKR-Stellungnahme abgegeben: Mitzeichnungsbitte für die {{type}} {{name}}',
              abstimmungType: {
                type: 'Abstimmung',
                typeHAUSABSTIMMUNG: 'Hausabstimmung',
                typeVORHABENCLEARING: 'Vorhabenclearing',
                typeRESSORTABSTIMMUNG: 'Ressortabstimmung',
                typeUNTERABSTIMMUNG: 'Unterabstimmung',
                typeSONSTIGE_ABSTIMMUNG: 'Sonstige Abstimmung',
                typeSCHLUSSABSTIMMUNG: 'Schlussabstimmung',
                typeABSTIMMUNG_DER_VORLAGE_FUER_DEN_REGIERUNGSENTWURF:
                  'Abstimmung der Vorlage für den Regierungsentwurf mit der Hausleitung',
              },
              extendAnswerTitle: {
                ZUGESTIMMT: 'Angenommen: Anfrage zur Fristverlängerung für die {{type}} {{name}}',
                ABGELEHNT: 'Abgelehnt: Anfrage zur Fristverlängerung für die {{type}} {{name}}',
              },
              extendQuestionTitle: 'Anfrage zur Fristverlängerung für die {{type}} {{name}}',
            },
          },
          fristAnfrage: {
            title: 'Anfrage zur Fristverlängerung von {{title}} senden',
            heading: 'Allgemeine Informationen zur Fristverlängerung',
            infos: {
              abstimmungsrunde: 'Abstimmungsrunde',
              adressat: 'Adressatin oder Adressat',
              initialeFrist: 'Initiale Frist',
              fristverlaengerung: 'Fristverlängerung',
              fristverlaengerungContent: 'Anfrage zur Fristverlängerung',
              na: 'Keine Angabe',
            },
            fields: {
              anmerkungen: 'Anmerkungen',
              textLengthError: 'Die maximal zulässige Anzahl von {{maxChars}} Zeichen wurde überschritten.',
            },
            submit: 'Fristverlängerung anfragen',
            cancel: 'Abbrechen',
            archiveSuccess: 'Für „{{titel}}“ wurde eine Fristverlängerung angefragt.',
          },
        },
        subVote: {
          tabNav: 'Meine Unterabstimmungen',
          imgText1: 'Unterabstimmungen bearbeiten',
          imgText2: 'Mitzeichnungen und Ablehnungen stets im Blick behalten',
          text: 'Hier können Sie Unterabstimmungen bearbeiten sowie Mitzeichnungen und Ablehnungen zu Ihren Unterabstimmungen im Blick behalten.',
          table: {
            thead1: 'Mitzeichnungsanfrage',
            thead2: 'Unterabstimmungsrunde',
          },
        },
        drafts: {
          tabNav: 'Entwürfe',
          imgText1: 'Zwischenstände beim Anlegen von Abstimmungen speichern',
          imgText2: 'Bearbeitung später problemlos fortsetzen',
          text: 'Beim Anlegen einer neuen Abstimmung können Sie Zwischenstände speichern und diese zu einem späteren Zeitpunkt fortsetzen und versenden. Den gespeicherten Zwischenstand finden Sie in diesem Bereich.',
          table: {
            thead1: 'Regelungsvorhaben',
            thead2: 'Abstimmungsrunde',
            thead3: 'Dokument',
            thead4: 'Zuletzt bearbeitet',
            thead5: 'Frist',
            thead6: 'Aktionen',
            displayName1: 'Regelungsvorhaben',
            displayName2: 'Abstimmungsarten',
            btnTextEntwurfBearbeiten: 'Entwurf bearbeiten',
            menuItem1: 'Entwurf löschen',
            menuItemArchive: 'Entwurf archivieren',
            deleteMsg: {
              success: 'Der Entwurf wurde gelöscht',
              error: 'Der Entwurf konnte nicht gelöscht werden {{fehlercode}}',
            },
            contactPersonDrawerTitle: 'Kontakt',
          },
        },
        requests: {
          tabNav: 'Anfragen',
          imgText1: 'Eingegangene Anfragen beantworten',
          imgText2: 'Verlauf von gesendeten Anfragen verfolgen',
          text: 'In diesem Bereich können Sie erhaltene Anfragen beantworten sowie den Verlauf gesendeter Anfragen übersichtlich verfolgen. Zu den Anfragen zählen Fristverlängerungsanfragen und Teilnahmeanfragen. Fristverlängerungsanfragen können Teilnehmende einer Abstimmung vor Fristablauf an die Einleiterin oder den Einleiter stellen. Gehören Sie noch nicht zu den Abstimmungsteilnehmenden, können Sie hier die Teilnahme anfragen. Als Einleiterin oder Einleiter lassen sich hier alle an Sie gerichtete Anfragen einsehen und beantworten.',
          table: {
            thead1: 'Anfragentyp',
            thead2: 'Regelungsvorhaben',
            thead3: 'Anfrage',
            thead4: 'Anfrage von',
            thead5: 'Gesendet',
            thead6: 'Neue Frist',
            thead7: 'Status',
            thead8: 'Aktionen',
            btnText: 'Freigeben',
            menuItemMain: 'Freigeben',
            menuItemReject: 'Ablehnen',
            menuItemArchive: 'Anfrage archivieren',
            anfragetyp: 'Bitte um Freigabe \n (Fristablauf)',
            contactPersonDrawerTitle: 'Kontakt',
            answerButton: 'Beantworten',
            archiveRequest: 'Archivieren',
            andererTeilnehmer: 'anderer Teilnehmer',
            status: {
              ZUGESTIMMT: 'Zugestimmt',
              OFFEN: 'Offen',
              ABGELEHNT: 'Abgelehnt',
              ZUERUCKGEZOGEN: 'Zurückgezogen',
              ABGELAUFEN: 'Abgelaufen',
            },
            type: {
              FRISTVERLAENGERUNG: 'Fristverlängerung',
              ABSTIMMUNGSTEILNAHME: 'Abstimmungsteilnahme',
            },
            voteAnfrageType: {
              FRISTVERLAENGERUNG: 'Fristverlängerung',
              ABSTIMMUNGSTEILNAHME: 'Teilnahmeanfrage',
            },
            sorter: {
              gesendetAsc: 'Gesendet (älteste zuerst)',
              gesendetDesc: 'Gesendet (neueste zuerst)',
              fristAsc: 'Neue Frist (älteste zuerst)',
              fristDesc: 'Neue Frist (neueste zuerst)',
            },
            statusInfo: {
              title: 'Erklärung zu: Status',
              content:
                '<p><strong>Folgende Status sind möglich:</strong></p>' +
                '<p><strong>Offen: </strong>Es wurde eine Anfrage gestellt. Die Einleiterin oder der Einleiter der Abstimmungsrunde hat noch nicht geantwortet.</p>' +
                '<p><strong>Zugestimmt: </strong>Die Einleiterin oder der Einleiter hat einer Anfrage zugestimmt. Bei Fristverlängerungsanfragen wurde nun die Frist der jeweiligen Abstimmungsrunde für alle Teilnehmenden verlängert. Bei Teilnahmeanfragen kann die oder der Anfragende nun an der Abstimmung teilnehmen.</p>' +
                '<p><strong>Abgelehnt: </strong>Die Einleiterin oder der Einleiter hat eine Anfrage abgelehnt.</p>' +
                '<p><strong>Zurückgezogen: </strong>Die oder der Anfragende hat die Anfrage zurückgezogen.</p>' +
                '<p><strong>Abgelaufen: </strong>Die Einleiterin oder der Einleiter hat die Anfrage vor Ende der Abstimmung nicht beantwortet.</p>',
            },
            verlauf: {
              title: 'Verlauf der Anfrage',
              element: {
                subject: {
                  FRISTVERLAENGERUNG: 'Anfrage zur Fristverlängerung von {{title}}',
                  ABSTIMMUNGSTEILNAHME: 'Anfrage zur Teilnahme an der {{abstimmungsTyp}} {{title}}',
                },
                sentDate: 'Gesendet',
                from: 'Von',
                to: 'An',
                comments: 'Anmerkungen',
                reasonTeilnahme: 'Begründung der Teilnahmeanfrage',
                na: 'Keine Angaben',
              },
            },
            archiveModalContent: {
              offen: `Möchten Sie die noch offene Teilnahmeanfrage archivieren? Eine Archivierung können Sie nicht rückgängig machen.  Die Archivierung führt automatisch zur Ablehnung der bislang offenen Anfrage.`,
              default:
                'Eine Archivierung können Sie nicht rückgängig machen. Möchten Sie diese Anfrage wirklich archivieren?',
            },
            archiveSuccess: 'Sie haben die Anfrage erfolgreich archiviert',
          },
          fristAnfrageAntwort: {
            title: 'Anfrage zur Fristverlängerung von {{title}} beantworten',
            heading: 'Allgemeine Informationen zur Fristverlängerung',
            infos: {
              abstimmungsrunde: 'Abstimmungsrunde',
              absender: 'Absenderin oder Absender',
              initialeFrist: 'Initiale Frist',
              fristverlaengerung: 'Fristverlängerung',
              fristverlaengerungContent: 'Anfrage zur Fristverlängerung',
              anmerkungen: 'Anmerkungen',
              na: 'Keine Angabe',
            },
            fields: {
              subtitle: 'Ihre Antwort',
              approval: {
                title: 'Bitte wählen Sie',
                approve: 'Zustimmen und Frist verlängern',
                decline: 'Ablehnen',
              },
              deadline: 'Frist',
              anmerkungen: 'Anmerkungen',
              textLengthError: 'Die maximal zulässige Anzahl von {{maxChars}} Zeichen wurde überschritten.',
            },
            submit: 'Anfrage beantworten',
            cancel: 'Abbrechen',
            archiveSuccess: 'Fristverlängerung für <b>{{titel}}</b> wurde {{status}}',
          },
          teilnahmeAnfrageAntwort: {
            title: 'Anfrage zur Teilnahme an der Abstimmung {{voteNameRound}}  beantworten',
            infoHeading: 'Informationen zur Teilnahmeanfrage',
            infoSection: {
              rv: 'Regelungsvorhaben',
              voteType: 'Abstimmungsart',
              votingRound: 'Abstimmungsrunde',
              deadlineVR: 'Frist der Abstimmungsrunde',
              initiatorVR: 'Einleitung',
              reasonRequest: 'Begründung der Teilnahmeanfrage',
            },
            fields: {
              subtitle: 'Ihre Antwort',
              approval: {
                title: 'Bitte wählen Sie',
                approve: 'Zustimmen und zur Abstimmung hinzufügen',
                decline: 'Ablehnen',
              },
              anmerkungen: 'Anmerkungen',
              textLengthError: 'Die maximal zulässige Anzahl von {{maxChars}} Zeichen wurde überschritten.',
            },
            submit: 'Anfrage beantworten',
            cancel: 'Abbrechen',
            messageSuccessAdd:
              'Sie haben {{participant}} erfolgreich zu Ihrer Abstimmung <b>{{voteNameRound}}</b> hinzugefügt.',
            messageSucessDenide:
              'Sie haben die Anfrage von {{participant}} zur Teilnahme an Ihrer Abstimmung <b>{{voteNameRound}}</b> abgelehnt.',
          },
        },
        archive: {
          tabNav: 'Archiv',
          imgText1: 'Alle Vorgänge zu einer Abstimmung archivieren',
          imgText2: 'Lesezugriff auf alle Dateien bleibt erhalten',
          text: 'Abstimmungen, Mitzeichnungsbitten, Fristverlängerungsanfragen sowie Teilnahmeanfragen, die für Sie nicht mehr relevant sind, können Sie ins Archiv verschieben. Sie behalten weiterhin Lesezugriff auf alle Dateien und Angaben von Rückmeldungen, welche Sie erhalten oder versandt haben. Eine Archivierung kann nicht rückgängig gemacht werden.',
          table: {
            thead1: 'Regelungsvorhaben',
            thead2: 'Abstimmungsrunde',
            thead3: 'Ursprung',
            thead4: 'Archiviert',
            sorter: {
              archivedAsc: 'Archiviert (älteste zuerst)',
              archivedDesc: 'Archiviert (neueste zuerst)',
            },
            filter: {
              tabs: {
                MEINE_ABSTIMMUNGEN: 'Meine Abstimmungen',
                BITTE_UM_MITZEICHNUNG: 'Bitte um Mitzeichnung',
                ANFRAGEN: 'Anfragen',
              },
            },
          },
        },
        rvDrawer: {
          title: 'Information zum Regelungsvorhaben',
          infoText: 'Diese Informationen entsprechen dem Stand des Regelungsvorhabens zu Abstimmungsbeginn',
          regelungsvorhabenLabel: 'Regelungsvorhaben',
          kurzbezeichnungLabel: 'Kurzbezeichnung',
          abkuerzungLabel: 'Abkürzung',
          kurzbeschreibungLabel: 'Kurzbeschreibung',
          vorhabentypLabel: 'Vorhabentyp',
          ressortLabel: 'Federführendes Ressort',
          rvLink: 'Zum vollständigen Datenblatt',
        },
      },
    },
    reviewVote: {
      mainTitle: 'Eingaben prüfen',
      mainTitleInfo:
        'Ihre Angaben wurden noch nicht an die Teilnehmenden verschickt. Sie können Ihre Eingaben prüfen, bei Bedarf bearbeiten und die Abstimmung einleiten.',
      generalInfo: {
        title: 'Allgemeine Informationen zur Abstimmung',
        type: 'Art der Abstimmung:',
        typeHAUSABSTIMMUNG: 'Hausabstimmung',
        typeVORHABENCLEARING: 'Vorhabenclearing',
        typeRESSORTABSTIMMUNG: 'Ressortabstimmung',
        typeUNTERABSTIMMUNG: 'Unterabstimmung',
        typeSONSTIGE_ABSTIMMUNG: 'Sonstige Abstimmung',
        typeSCHLUSSABSTIMMUNG: 'Schlussabstimmung',
        typeABSTIMMUNG_DER_VORLAGE_FUER_DEN_REGIERUNGSENTWURF:
          'Abstimmung der Vorlage für den Regierungsentwurf mit der Hausleitung',
        info: 'Beteiligung/Unterrichtung:',
        infoABSTIMMUNG: 'Bitte um Mitzeichnung (§45 Absatz 1 GGO)',
        infoSTELLUNGNAHME: 'Bitte um Stellungnahme',
        infoUNTERRICHTUNG: 'Unterrichtung anderer Stellen (§48 GGO)',
        optionalExams: 'Optionale Prüfungen:',
        regulationPlan: 'Dazugehöriges Regelungsvorhaben:',
        voteTitle: 'Titel der Abstimmung(#Runde):',
        earlyVoteBundeskanzleramt: 'Vorhabenclearing:',
        anPKPSenden: 'Regelungsvorhaben an PKP senden:',
        anPKPSendenRESSORTABSTIMMUNG_yes: 'Ja, Regelungsvorhaben senden (gem. §40 GGO)',
        anPKPSendenHAUSABSTIMMUNG_yes: 'Ja, Regelungsvorhaben an PKP senden',
        anPKPSendenHAUSABSTIMMUNG_no: 'Nein, Regelungsvorhaben nicht an PKP senden',
        anPKPSendenDrawerTitle: 'Regelungsvorhaben an PKP senden',
        anPKPSendenDrawerText:
          'Sie können hier schon vorab das Planungs- und Kabinettmanagement-Programm (PKP) darüber informieren, dass Ihr Regelungsvorhaben in den Abstimmungsprozess eingetreten ist. Damit wird zu diesem Zeitpunkt das Regelungsvorhaben in das Planungs- und Kabinettmanagement-Programm übernommen. Dies ändert nichts an Ihren Zugriffsrechten auf das Regelungsvorhaben hier in der E-Gesetzgebung.',
        optionsBkAmt: {
          erfolgt: 'Erfolgt am {{date}}',
          nichtErfolgt: 'Nicht erfolgt',
        },
      },
      uploadedFiles: {
        title: 'Dateien',
        uploadedDraft: 'Hochgeladener Regelungsentwurf:',
        uploadedAttachments: 'Hochgeladene Anlagen:',
        documentType: 'Format des Dokuments:',
        selectedRE: 'Ausgewähltes Dokument:',
      },
      documentType: {
        label: 'Format des Dokuments:',
        ENORM: 'eNorm',
        EDITOR: 'Editor',
      },
      deadlines: {
        title: 'Fristen',
        deadlineVoteTitle: 'Frist Ende der Abstimmungsrunde',
        deadlineData: 'Datum:',
        deadlineTime: 'Uhrzeit:',
        deadlineInternetFeedbackTitle: 'Frist Rückmeldung zur Veröffentlichung im Internet/Intranet:',
        deadlineFeedbackTitle: 'Frist Rückmeldung zur frühzeitigen Beteiligung der Länder und Verbände',
        deadlineAdditionalTitle: 'Zusätzliche Frist',
      },
      invitationEmail: {
        title: 'Einladungs-E-Mail',
        emails: 'Adressatinnen und Adressaten für Teilnahme:',
        cc: 'Adressatinnen und Adressaten zur Kentnisnahme:',
        subject: 'Betreff der Einladung:',
        emailText: 'Einladungstext:',
      },
      successMessage: 'Ihre Abstimmung wurde erfolgreich eingeleitet.',
      btnStart: 'Abstimmung jetzt einleiten',
      btnAktualisierung: 'Aktualisierung jetzt senden',
      btnEdit: 'Eingaben bearbeiten',
    },
    votingRound: {
      header: {
        mainTitle: 'Abstimmungsrunde:',
        labelRegelungsvorhaben: 'Regelungsvorhaben:',
        labelAbstimmungsschritt: 'Abstimmungsart:',
        labelZugang: 'Zugang:',
        optionsZugang: {
          eingeschraenkt: 'Teilnahme eingeschränkt',
          offen: 'Teilnahme offen',
        },
        labelDeadline: 'Frist der Abstimmungsrunde:',
        labelVerschweigensfrist: 'Verschweigensfrist:',
        optionsVerschweigensfrist: {
          true: 'Ja',
          false: 'Nein',
        },
        labelNewDeadline: 'Neue Frist:',
        labelInternetDeadlineEarlyReply: 'Frist Rückmeldung zur Veröffentlichung im Internet/Intranet:',
        labelDeadlineEarlyReply: 'Frist Rückmeldung zur frühzeitigen Beteiligung der Länder und Verbände:',
        labelCoordinationBkAmt: 'Vorhabenclearing:',
        optionsBkAmt: {
          erfolgt: 'Erfolgt am {{date}}',
          nichtErfolgt: 'Nicht erfolgt',
        },
        labelDeadlineAdditional: 'Zusätzliche Fristen:',
        labelDownload: 'Regelungsentwurf:',
        labelEntwurfEditor: 'Regelungsentwurf mit Rückmeldungen:',
        unterabstimmungDetails: '{{unterabstimmung}} zu {{hauptabstimmung}} ',
        teilnehmerNotAccepted: 'Zugriff erst bei Teilnahme',
        documentTypeName: {
          ANLAGE: 'Stammgesetz-Anlage',
          ANSCHREIBEN_MANTELGESETZ: 'Mantelgesetz-Anschreiben',
          ANSCHREIBEN_MANTELVERORDNUNG: 'Mantelverordnung-Anschreiben',
          ANSCHREIBEN_STAMMGESETZ: 'Stammgesetz-Anschreiben',
          ANSCHREIBEN_STAMMVERORDNUNG: 'Stammverordnung-Anschreiben',
          BEGRUENDUNG_MANTELGESETZ: 'Mantelgesetz-Begründung',
          BEGRUENDUNG_MANTELVERORDNUNG: 'Mantelverordnung-Begründung',
          BEGRUENDUNG_STAMMGESETZ: 'Stammgesetz-Begründung',
          BEGRUENDUNG_STAMMVERORDNUNG: 'Stammverordnung-Begründung',
          FREITEXT: 'Freitext',
          RECHTSETZUNGSDOKUMENT_MANTELGESETZ: 'Mantelgesetz-Rechtsetzungsdokument',
          RECHTSETZUNGSDOKUMENT_MANTELVERORDNUNG: 'Mantelverordnung-Rechtsetzungsdokument',
          RECHTSETZUNGSDOKUMENT_STAMMGESETZ: 'Stammgesetz-Rechtsetzungsdokument',
          RECHTSETZUNGSDOKUMENT_STAMMVERORDNUNG: 'Stammverordnung-Rechtsetzungsdokument',
          REGELUNGSTEXT_MANTELGESETZ: 'Mantelgesetz-Regelungsentwurf',
          REGELUNGSTEXT_MANTELVERORDNUNG: 'Mantelverordnung-Regelungsentwurf',
          REGELUNGSTEXT_STAMMGESETZ: 'Stammgesetz-Regelungsentwurf',
          REGELUNGSTEXT_STAMMVERORDNUNG: 'Stammverordnung-Regelungsentwurf',
          VORBLATT_MANTELGESETZ: 'Mantelgesetz-Vorblatt',
          VORBLATT_MANTELVERORDNUNG: 'Mantelverordnung-Vorblatt',
          VORBLATT_STAMMGESETZ: 'Stammgesetz-Vorblatt',
          VORBLATT_STAMMVERORDNUNG: 'Stammverordnung-Vorblatt',
        },
        labelAdditionalDownload: 'Hochgeladene zusätzliche Anlagen:',
        labelInvitation: 'Einladungstext:',
        labelInvitationButtonOpened: 'Text ausblenden',
        labelInvitationButtonClosed: 'Text einblenden',
        exportButton: 'Datenexport (ZIP)',
        editButton: 'Abstimmung bearbeiten',
        rvDrawer: {
          buttonLabel: 'Informationen zum Regelungsvorhaben',
          mainTitle: 'Informationen zum Regelungsvorhaben',
          introText: 'Diese Informationen entsprechen dem Stand des Regelungsvorhabens zu Abstimmungsbeginn.',
        },

        anfragen: {
          title: 'Anfragen',
          verlauf: {
            types: {
              frist: { title: 'Fristverlängerungen', linkTitle: 'Zurück zu Anfragen' },
              teilnahme: { title: 'Teilnahmeanfragen', linkTitle: 'Zurück zu Anfragen' },
            },
            verlaufItem: {
              von: {
                vonTitle: 'Von',
                drawerTitle: 'Kontakt',
                content: 'am {{date}} Uhr',
              },
              status: {
                statusTitle: 'Beantwortung',
                content: 'am {{date}} Uhr',
                status: {
                  ZUGESTIMMT: 'Zugestimmt',
                  OFFEN: 'Offen',
                  ABGELEHNT: 'Abgelehnt',
                  ZUERUCKGEZOGEN: 'Zurückgezogen',
                  ABGELAUFEN: 'Abgelaufen',
                },
              },
            },
          },
        },
      },
      mid: {
        downloadAll: 'Alle Dateien herunterladen',
      },
      table: {
        theadRessortAndReferat: 'Ressort und Referat',
        theadBeteiligt: 'Beteiligt',
        theadFiles: 'Dateien',
        theadDocuments: 'Dokumente',
        theadStatus: 'Status',
        theadActions: 'Aktionen',
        readOnly: 'Kenntnisnahme',
        participantRemoved: 'Person entfernt',
        menuItemTeilnehmerEntfernen: 'Person entfernen',
        menuItemNurLesezugriff: 'Kenntnisnahme',
        expandedTitle: 'Kommentare von',
        deleteErrorMsg: 'Sie dürfen nicht alle Person löschen. {{fehlercode}}',
      },
      modalSABestaetigungDurchfuehrung: {
        modalTitel:
          'Bestätigung der Durchführung der rechtssystematischen und rechtsförmlichen Prüfung durch das Bundesministerium der Justiz',
        modalSubTitel: 'Rückmeldung des Bundesministeriums der Justiz eintragen',
        labelMain: 'Aktuelle Rückmeldung des Bundesministeriums der Justiz:',
        RUECKMELDUNG_OFFEN: 'Rückmeldung offen ',
        EINVERNEHMEN_ERTEILT: 'Bestätigung erteilt',
        EINVERNEHMEN_ABGELEHNT: 'Bestätigung abgelehnt',
        hinweisTitle: 'Bundesministerium der Justiz wird per E-Mail in Kenntnis gesetzt',
        hinweisContent:
          'Wenn Sie eine Rückmeldung des Bundesministeriums der Justiz eintragen, wird das Bundesministerium der Justiz über diese Änderung benachrichtigt.',
      },
      modalReImInternet: {
        modalTitel: 'Einvernehmen Bundeskanzleramt zur Veröffentlichung im Intranet/Internet',
        modalCancel: 'Abbrechen',
        modalConfirm: 'Rückmeldung eintragen',
        modalSubTitel: 'Rückmeldung des Bunderkanzleramts eintragen',
        modalRadioGroupTitel: 'Aktuelle Rückmeldung des Bundeskanzleramts',
        modalPflichtfelderParagraph: 'Pflichtfelder sind mit einem * gekennzeichnet.',
        labelMain: 'Aktuelle Rückmeldung des Bundeskanzleramts:',
        RUECKMELDUNG_OFFEN: 'Rückmeldung offen ',
        EINVERNEHMEN_ERTEILT: 'Einvernehmen erteilt ',
        EINVERNEHMEN_ABGELEHNT: 'Einvernehmen abgelehnt ',
        hinweisTitle: 'Bundeskanzleramt wird per E-Mail in Kenntnis gesetzt',
        hinweisContent:
          'Wenn Sie eine Rückmeldung des Bundeskanzleramts eintragen, wird das Bundeskanzleramt per E-Mail über diese Änderung benachrichtigt.',
        saveError: 'Es ist ein Fehler aufgetreten. Ihre Rückmeldung konnte leider nicht gespeichert werden.',
      },
      msgEinleiterTeilnahmeanfrage:
        'Sie möchten einen Teilnahmelink für die Abstimmung <b>{{abstimmungTitel}}</b> öffnen. Da Sie Einleiterin oder Einleiter dieser Abstimmung sind, können Sie nicht gleichzeitig Teilnehmerin oder Teilnehmer sein. Wir haben Sie deshalb zu Ihrer Abstimmung weitergeleitet.',
    },
    reviewVoteAnswer: {
      header: {
        exportButton: 'Datenexport (ZIP)',
      },
      mainTitle: 'Bitte um Mitzeichnung',
      sentAnswer: {
        title: 'Ihre gesendete Antwort',
        titleModify: 'Ihre geänderte Antwort',
        subTitle: 'Ihre Antwort',
        choice: 'Ihre Auswahl:',
        noParticipation: 'Sie haben bei dieser Abstimmung nicht teilgenommen',
        noParticipant: 'Kenntnisnahme: Sie dürfen bei der Abstimmung nicht teilnehmen',
        submitModified: 'Geänderte Antwort senden',
        submit: 'Antwort senden',
        btnCancelModified: 'Eingaben bearbeiten',
        placeholder: 'PLACEHOLDER',
      },
      files: {
        title: 'Hochgeladene Dateien',
        uploadChangeTitle: 'Hochgeladener Änderungsvorschlag:',
        uploadChangeTitleNKR: 'Hochgeladene Stellungnahme',
        uploadAttachmentsTitle: 'Hochgeladene Anlagen:',
      },
      comments: {
        title: 'Hinweise und Kommentare {{document}}',
        yourComments: 'Ihre Kommentare:',
      },
      noParticipant: {
        abgelaufen: {
          title: 'Abstimmung abgelaufen',
          text: 'Die Abstimmung ist abgelaufen, deswegen können Sie keine Anfrage zur Teilnahme mehr stellen.',
          linkText: 'Startseite E-Gesetzgebung',
        },
        eingeschraenkt: {
          title: 'Teilnahme nur auf Anfrage möglich',
          text: 'Die Einleiterin oder der Einleiter hat die Teilnahme an dieser Abstimmung eingeschränkt. Bevor Sie an der Abstimmung teilnehmen können, müssen Sie zunächst eine Teilnahmeanfrage stellen.',
          buttonText: 'Teilnahme jetzt anfragen',
        },
        offen: {
          title: 'Teilnahme ohne Anfrage möglich',
          text: 'Die Einleiterin oder der Einleiter hat die Teilnahme an dieser Abstimmung nicht eingeschränkt. Sie können ohne Teilnahmeanfrage an der Abstimmung teilnehmen.',
          buttonText: 'An der Abstimmung teilnehmen',
        },
      },
      changeAnswerBtn: 'Antwort ändern',
      answerBtn: 'Beantworten',
    },
    beantwortenVote: {
      modify: {
        mainTitle: 'Antwort ändern',
        maintTitleReview: 'Änderungen prüfen',
        btnSubmit: 'Änderungen prüfen',
        generalInfoTitle: 'Allgemeine Informationen zur Abstimmungsrunde',
      },
      mainTitle: 'Abstimmungsrunde beantworten',
      info: {
        title: 'Abstimmungsrunde beantworten',
        text: 'Ihre Antwort auf eine Abstimmung ist nur für die einleitende Person einsehbar. Die anderen Teilnehmenden haben keine Berechtigung Ihre Antwort zu sehen.',
      },
      maintTitleReview: 'Eingaben prüfen',
      generalInfo: {
        title: 'Allgemeine Informationen',
        regelungsvorhaben: 'Regelungsvorhaben:',
        abstimmungsschritt: 'Abstimmungsart:',
        fristDerAbstimmungsrunde: 'Frist der Abstimmungsrunde:',
        verschweigensfrist: 'Verschweigensfrist:',
        neueFristDerAbstimmungsrunde: 'Neue Frist:',
        fristZurRuckmeldungInternet: 'Frist Rückmeldung zur Veröffentlichung im Internet/Intranet:',
        fristZurRuckmeldung: 'Frist Rückmeldung zur frühzeitigen Beteiligung der Länder und Verbände:',
        fruehkoordinierungBKAmt: 'Vorhabenclearing:',
        additionalDeadlines: 'Zusätzliche Fristen:',
        einleiter: 'Einleitung:',
        contactPersonTitle: 'Kontakt',
        email: 'E-Mail Adresse:',
        phone: 'Telefon:',
        ressortTeilnehmer: 'Teilnehmerinnen und Teilnehmer aus dem {{ressort}}',
        showTeilnehmerList: 'Teilnehmerliste einblenden',
        hideTeilnehmerList: 'Teilnehmerliste ausblenden',
        draft: 'Regelungsentwurf:',
        labelAdditionalDownload: 'Zusätzliche Anlagen:',
        einladungstext: 'Einladungstext:',
        showText: 'Text einblenden',
        hideText: 'Text ausblenden',
        verlauf: {
          title: 'Verlauf der Abstimmungsrunde',
          unterabstimmungOngoing:
            '<p>Sie haben am {{date}} eine Unterabstimmung eingeleitet. Die <a href="{{link}}" target="_blank">Unterabstimmung</a> endet {{dateEnd}}.</p> ',
          unterabstimmungExpired:
            'Sie haben am {{date}} eine Unterabstimmung eingeleitet. Die <a href="{{link}}" target="_blank">Unterabstimmung</a> wurde am {{dateEnd}} beendet. ',
          content: {
            ZUGESTIMMT: 'Einer Anfrage zur Fristverlängerung wurde am {{date}} zugestimmt.',
            OFFEN:
              'Eine Anfrage zur Fristverlängerung ist am {{date}} bei der Einleiterin oder beim Einleiter der Abstimmung eingegangen. Diese wurde noch nicht bearbeitet.',
            ABGELEHNT: 'Eine Anfrage zur Fristverlängerung wurde am {{date}} abgelehnt.',
            ZUERUCKGEZOGEN: 'Eine Anfrage zur Fristverlängerung wurde am {{date}} zurückgezogen.',
            ABGELAUFEN:
              'Ihre Anfrage zur Fristverlängerung wurde nicht innerhalb der Frist der Abstimmung beantwortet. Die Frist der Abstimmung ist abgelaufen.',
          },
          ABSTIMMUNGSTEILNAHME: {
            OFFEN:
              'Eine Anfrage zur Teilnahme an der Abstimmung ist am {{date}} bei der Einleiterin oder dem Einleiter der Abstimmung eingegangen. Diese wurde noch nicht bearbeitet. ',
            ZUGESTIMMT:
              'Die Anfrage zur Teilnahme an der Abstimmung wurde am {{date}} von der Einleiterin oder dem Einleiter angenommen.',
            ABGELEHNT:
              'Die Anfrage zur Teilnahme an der Abstimmung wurde am {{date}} von der Einleiterin oder dem Einleiter abgelehnt.',
            ZUERUCKGEZOGEN: 'Die Anfrage zur Teilnahme an der Abstimmung wurde am {{date}} zurückgezogen.',
            ABGELAUFEN:
              'Ihre Teilnahmeanfrage wurde nicht innerhalb der Frist der Abstimmung beantwortet. Die Frist der Abstimmung ist abgelaufen.',
          },
        },
        stellvertretungLabel: 'i.V. ',
      },

      mitzeichnung: {
        title: 'Ihre Antwort',
        mitzeichnungDrawerTitle: 'Ihre Antwort',
        mitzeichnungDrawerText:
          '<p>Bitte wählen Sie hier den Status Ihrer Mitzeichnung aus:<p>' +
          '<p><strong>Mitzeichnen:</strong><br>Wählen Sie „Mitzeichnen“, wenn Sie dem Inhalt des Regelungsentwurfes ohne Änderungsvorbehalte zustimmen. Sie haben trotz Ihrer Mitzeichnung die Möglichkeit Hinweise oder Anregungen in einem Textfeld zu ergänzen Die Einarbeitung Ihrer Hinweise und Anregungen bedingen in diesem Fall nicht Ihre Mitzeichnung.</p>' +
          '<p><strong>Mitzeichnen unter Vorbehalt:</strong><br>Wählen Sie „Mitzeichnen unter Vorbehalt“, wenn Sie dem Inhalt des Regelungsentwurfs vorbehaltlich Ihrer Änderungswünsche zustimmen. Ihre Änderungswünsche können Sie direkt im Regelungsentwurf und/oder im untenstehenden Texteingabefeld „Ihre Kommentare“ einfügen.</p>' +
          '<p><strong>Mitzeichnung ablehnen:</strong><br>Wählen Sie „Mitzeichnung ablehnen“, wenn Sie den Inhalt des Regelungsentwurfes grundsätzlich ablehnen. Sie sollten diese Option nur wählen, wenn Sie Ihre zukünftige Mitzeichnung auch nach einigen Anpassungen oder Änderungen kategorisch ausschließen.</p>',
        labelMain: 'Bitte wählen Sie:',
        regelungsentwurf: 'Regelungsentwurf (Editor-Dokumentenmappe)',
        error: 'Bitte wählen Sie',
        regelungsentwurfHinWeisTitle: 'Dokumentenmappe',
        regelungsentwurfHinWeisText:
          'Sie haben noch keine eigene Version der in Abstimmung befindlichen Dokumentenmappe erstellt.',
        regelungsentwurfDrawerText:
          'Die Schnittstelle des Editors liefert die Information zur Version der Dokumentenmappe. Diese ist im Drop-Down bei der Auswahl der Dokumentenmappe sichtbar.',
        mitzeichnenLabel: 'Mitzeichnen',
        mitzeichnenUnterVorbehaltLabel: 'Mitzeichnen unter Vorbehalt',
        mitzeichnungAblehnenLabel: 'Mitzeichnung ablehnen',
        keineMitzeichnungStellungnahmeNKR: 'Keine Mitzeichnung: Stellungnahme des Nationalen Normenkontrollrats',
      },
      upload: {
        mainTitle: 'Dateien hochladen',
        delete: 'Datei löschen',
        rename: 'Datei umbenennen',
        uploadDrawerText:
          '<p>Mit Upload der Dokumente wird bestätigt, dass diese keine personenbezogenen Daten nach <a href="https://dejure.org/gesetze/DSGVO/4.html" target="_blank">Art. 4 Nr. 1 DSGVO</a> beinhalten, die personenbezogenen Daten anonymisiert wurden oder eine Einwilligung der betroffenen Person nach <a href="https://dejure.org/gesetze/DSGVO/6.html" target="_blank">Art. 6 Nr. 1 lit. a DSGVO</a> vorliegt.</p>',
        popover: {
          title: 'Möchten Sie das Dokument löschen?',
          text: 'Wenn Sie das hochgeladene Dokument „{{title}}“ löschen, wird es hier entfernt.',
          delete: 'Löschen',
          cancel: 'Abbrechen',
        },
      },
      uploadFileTitle: 'Änderungsvorschlag hochladen (eNorm, max. 20 MB)',
      uploadFileTitleNKR: 'Stellungnahme hochladen (PDF, max. 20 MB)',
      uploadFileFormatShort: 'DOCX (eNorm oder Word)',
      uploadFileErrorTitle: 'Abschnitt „Änderungsvorschlag hochladen“',
      commentBox: {
        title: 'Hinweise und Kommentare zum Änderungsvorschlag',
        titleStellungnahme: 'Hinweise und Kommentare zur Stellungnahme',
        label: 'Ihre Kommentare',
        hint: '(maximal {{maxChars}} Zeichen)',
        error: 'Die maximal zulässige Anzahl von {{maxChars}} Zeichen wurde überschritten.',
      },
      btnSubmit: 'Eingaben prüfen',
      btnCancel: 'Abbrechen',
      continueLater: 'Abstimmungsrunde beantworten später fortsetzen?',
      continueLaterTooltip:
        'Speichern Sie Ihren Zwischenstand, um das Formular zu einem späteren Zeitpunkt abzuschicken.',
      zwischenspeichernLink: 'Zwischenspeichern',
      confirmModal: {
        title: 'Möchten Sie die Einleitung die Beantwortung wirklich abbrechen?',
        btnCancelConfirmOk: 'Ja, abbrechen',
        btnCancelConfirmNo: 'Nein, nicht abbrechen',
      },
      successAnswerMsg: 'Die Abstimmung wurde erfolgreich beantwortet',
      successAnswerModifyMsg: 'Antwortänderung gespeichert',
      successSaveAnswerMsg: 'Antwort zwischengespeichert',
      beantwortenActionAbbrechen: {
        antwortConfirm: 'Ok, gelesen',
        antwortCancel: 'Abbrechen',
        modalTitle: 'Hinweis',
        actionTitle:
          'Zur Abstimmung {{titel}} #{{id}} haben Sie eine Unterabstimmung eingeleitet. Sind Sie sich sicher, dass Sie Ihre Antwort jetzt senden möchten?',
      },
    },
    notificationPages: {
      error: {
        title: 'Achtung, es ist ein Fehler aufgetreten.',
        text: 'Ihre Abstimmung konnte nicht eingeleitet werden. Bitte korrigieren Sie Ihre Eingaben im Formular „Neue Abstimmung einleiten“.',
        buttonText: 'Abstimmung korrigieren',
        pkp: '<p>Ihre Abstimmung wurde erfolgreich an die Adressatinnen und Adressaten übermittelt.</p><p>Die Datenübertragung an das Planungs- und Kabinettmanagement-Programm (PKP) war leider nicht erfolgreich.</p><p>Bitte stoßen Sie den Prozess in der Anwendung „Regelungsvorhaben“ erneut an.</p>',
        pkpTitle: 'Ihre Abstimmung konnte nur teilweise eingeleitet werden',
        pkpRessortTitle: 'Ihre Abstimmung konnte noch nicht eingeleitet werden.',
        pkpRessort:
          '<p>Für das Einleiten einer Ressortabstimmung werden noch weitere Informationen benötigt. Bis dahin finden Sie die Abstimmung im Bereich „Entwürfe“.</p><p>Bitte ergänzen Sie im Datenblatt des Regelungsvorhabens folgende Angaben:</p><p>Federführende Abteilung</p><p>Federführendes Referat</p>',
        pkpRessortBtnText: 'Informationen im Datenblatt ergänzen',
      },
      success: {
        title: 'Ihre {{abstimmung}} wurde erfolgreich {{type}}.',
        text: 'Die Adressatinnen und Adressaten erhalten in Kürze eine E-Mail Benachrichtigung und können bis zum Fristende die Abstimmung beantworten.',
        buttonText: 'Zur Übersicht „Meine {{abstimmungen}}“',
        pkp: '<p>Die Adressatinnen und Adressaten erhalten in Kürze eine E-Mail Benachrichtigung und können bis zum Fristende die Abstimmung beantworten.</p><p>Das Regelungsvorhaben wurde erfolgreich an das Planungs- und Kabinettmanagement-Programm (PKP) übertragen.</p>',
      },
    },
    teilnahmeAnfragen: {
      commentBox: {
        label: 'Begründung der Teilnahmeanfrage',
        error: 'Bitte geben Sie Ihr Begründung an',
        errorMaxChars: 'Die maximal zulässige Anzahl von {{maxChars}} Zeichen wurde überschritten.',
      },
      pflichtfelderText: 'Pflichtfelder sind mit einem <sup>*</sup> gekennzeichnet.',
      info: {
        title: 'Allgemeine Informationen zur Teilnahmeanfrage',
        noAnswer: 'Keine Angabe',
        generalInfo: {
          regelungsvorhaben: 'Regelungsvorhaben:',
          abstimmungsschritt: 'Abstimmungsart:',
          abstimmungsrunde: 'Abstimmungsrunde:',
          fristDerAbstimmungsrunde: 'Frist der Abstimmungsrunde:',
          einleiter: 'Einleitung:',
          kommentar: 'Begründung der Teilnahmeanfrage:',
        },
      },
      reviewPage: {
        title: 'Eingaben prüfen',
      },
      formControls: {
        btnSubmit: 'Eingaben prüfen',
        btnCancel: 'Abbrechen',
        btnEdit: 'Eingaben bearbeiten',
        modify: {
          btnSubmit: 'Teilnahme jetzt anfragen',
        },
      },
      successPage: {
        title: 'Ihre Teilnahme an der Abstimmung wurde angefragt.',
        text: 'Sie werden benachrichtigt, sobald Ihre Anfrage angenommen wurde.',
        buttonText: 'Zur Übersichtsseite „Anfragen“',
      },
      zurueckziehenABSTIMMUNGSTEILNAHME: {
        modalTitle: 'Möchten Sie die Teilnahmeanfrage zurückziehen?',
        title:
          'Wenn Sie zu einem späteren Zeitpunkt an der Abstimmung teilnehmen möchten, müssen Sie eine erneute Teilnahmeanfrage stellen. Möchten Sie die Teilnahmeanfrage wirklich zurückziehen?',
        confirm: 'Ja, Anfrage zurückziehen',
        cancel: 'Abbrechen',
        successMsg: 'Die Anfrage zur Teilnahme an der Abstimmung wurde zurückgezogen.',
      },
      zurueckziehenFRISTVERLAENGERUNG: {
        modalTitle: 'Möchten Sie die Fristverlängerungsanfrage zurückziehen?',
        title:
          'Wenn Sie zu einem späteren Zeitpunkt eine Fristverlängerung anfragen möchten, müssen Sie eine erneute Anfrage stellen. Möchten Sie die Fristverlängerungsanfrage wirklich zurückziehen?',
        confirm: 'Ja, Anfrage zurückziehen',
        cancel: 'Abbrechen',
        successMsg: 'Die Anfrage zur Fristverlängerung an der Abstimmung wurde zurückgezogen.',
      },
    },
  },
};
