// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export const de_contacts = {
  enorm: {
    vote: {
      emailAddress: {
        obligatoryEmails:
          'poststelle1@example.com;poststelle2@example.com',
        vorhabenclearing: 'poststelle3@example.com;poststelle4@example.com',
      },
    },
  },
};
