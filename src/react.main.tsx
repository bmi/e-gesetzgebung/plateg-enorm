// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

// Only used to make it work standalone...not sure if this is really making sense
import './shares/i18n';
import './style.less';

import { ConfigProvider } from 'antd';
import deDE from 'antd/es/locale/de_DE';
import React, { version } from 'react';
import { createRoot } from 'react-dom/client';
import { HashRouter as Router, Redirect } from 'react-router-dom';

import { px2remTransformer, StyleProvider } from '@ant-design/cssinjs';
import { GlobalDI, plategThemeConfig, ScrollToTop } from '@plateg/theme';
import { KeycloakAuth } from '@plateg/theme/src/components/auth/KeycloakAuth';
import { LoginRedirect } from '@plateg/theme/src/components/auth/login-redirect';
import { store, StoreProvider } from '@plateg/theme/src/components/store';
import { StoreInit } from '@plateg/theme/src/components/storeInit/component.react';

import { AppEnorm } from './components/app/component.react';

GlobalDI.register('Framework', {
  name: 'React',
  version,
});
// Nessecary in order to use date-fns instead of moment
deDE.DatePicker.lang.locale = 'de';
const htmlDivElement: HTMLDivElement | null = document.querySelector('div#app');
if (htmlDivElement === null) {
  throw new Error('Root container missing in index.html');
}
const root = createRoot(htmlDivElement);
const px2rem = px2remTransformer({
  rootValue: 10, // 10px = 1rem;
});
root.render(
  <ConfigProvider theme={plategThemeConfig} locale={deDE}>
    <Router>
      <ScrollToTop />
      <StoreProvider store={store}>
        <KeycloakAuth>
          <StoreInit>
            <>
              <StyleProvider transformers={[px2rem]}>
                <AppEnorm />
              </StyleProvider>
              <LoginRedirect>
                <Redirect to="/hra" />
              </LoginRedirect>
            </>
          </StoreInit>
        </KeycloakAuth>
      </StoreProvider>
    </Router>
  </ConfigProvider>,
);
