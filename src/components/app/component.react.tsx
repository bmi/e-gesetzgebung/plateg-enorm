// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../styles/plateg-enorm.less';

import React from 'react';
import { Route, Switch } from 'react-router-dom';

import {
  AbstimmungControllerApi,
  CodelistenControllerApi,
  Configuration,
  EditorRemoteControllerApi,
  FileControllerApi,
  RegelungsvorhabenControllerApi,
} from '@plateg/rest-api';
import { configureRestApi, GlobalDI, HeaderController, LoadingStatusController } from '@plateg/theme';

import { LayoutWrapperEnorm } from '../enorm/component.react';

export function AppEnorm(): React.ReactElement {
  GlobalDI.getOrRegister('enormHeadercontroller', () => new HeaderController());
  configureRestApi(registerRestApis);

  return (
    <Switch>
      <Route path="/hra">
        <LayoutWrapperEnorm />
      </Route>
    </Switch>
  );
}

function registerRestApis(configRestCalls: Configuration) {
  GlobalDI.getOrRegister('enormController', () => new AbstimmungControllerApi(configRestCalls));
  GlobalDI.getOrRegister('fileController', () => new FileControllerApi(configRestCalls));
  GlobalDI.getOrRegister('regelungsvorhabenController', () => new RegelungsvorhabenControllerApi(configRestCalls));
  GlobalDI.getOrRegister('loadingStatusController', () => new LoadingStatusController());
  GlobalDI.getOrRegister('editorRemoteControllerApi', () => new EditorRemoteControllerApi(configRestCalls));
  GlobalDI.getOrRegister('codelistenControllerApi', () => new CodelistenControllerApi(configRestCalls));
}
