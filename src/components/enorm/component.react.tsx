// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Layout, Spin } from 'antd';
import React, { useEffect, useState } from 'react';
import { AjaxError } from 'rxjs/ajax';

import { ErrorController, GlobalDI, HeaderComponent, HeaderController, LoadingStatusController } from '@plateg/theme';
import { Loading } from '@plateg/theme/src/components/icons/Loading';

import { MainRoutesEnorm } from './main/component.react';

const { Content } = Layout;

export function LayoutWrapperEnorm(): React.ReactElement {
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const headerController = GlobalDI.get<HeaderController>('enormHeadercontroller');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const [loadingStatus, setLoadingStatus] = useState<boolean>(false);

  useEffect(() => {
    const subs = loadingStatusController.subscribeLoadingStatus().subscribe({
      next: (state: boolean) => {
        setLoadingStatus(state);
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
        console.error('Error sub', error);
      },
    });
    loadingStatusController.initLoadingStatus();
    return () => subs.unsubscribe();
  }, []);

  return (
    <Spin
      {...{ role: loadingStatus ? 'progressbar' : undefined }}
      aria-busy={loadingStatus}
      spinning={loadingStatus}
      size="large"
      tip="Laden..."
      wrapperClassName="loading-screen"
      indicator={<Loading />}
    >
      <Layout
        style={{
          height: '100%',
          display: loadingStatus === true ? 'none' : 'unset',
        }}
        className="site-layout-background"
      >
        <HeaderComponent ctrl={headerController} headerId="enorm-header" />

        <Content className="main-content-area has-drawer">
          <div className="ant-layout-content">
            <MainRoutesEnorm />
          </div>
        </Content>
      </Layout>
    </Spin>
  );
}
