// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './vote-details.less';

import { Button, Col, Row } from 'antd';
import isPast from 'date-fns/isPast';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useRouteMatch } from 'react-router';
import { Link } from 'react-router-dom';
import { Subscription } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  AbstimmungEntityResponseDTO,
  AnfrageEntityResponseDTO,
  AnfrageStatusType,
  AnfragetypType,
  BASE_PATH,
  CodelistenControllerApi,
  CodelistenResponseDTO,
  FristtypType,
} from '@plateg/rest-api';
import {
  BreadcrumbComponent,
  displayMessage,
  DropdownMenu,
  ErrorController,
  GlobalDI,
  HeaderController,
  LoadingStatusController,
} from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';
import { openFile } from '@plateg/theme/src/controllers/RestConfigController';

import { RegelungsvorhabenDetailsDrawer } from '../../../../shares/regelungsvorhaben-details-drawer/component.react';
import { routes } from '../../../../shares/routes';
import { getAbstimmungsbezeichner } from '../../../../utils/controller';
import { EnormHelpBreadcrumb } from '../help/component.react';
import { getMeineAbstimmungCall } from './controller';
import { VoteDetailsBody } from './vote-details-body/component.react';
import { VoteDetailsHead } from './vote-details-head/component.react';

export function VoteDetails(): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  const routeMatcherVorbereitung = useRouteMatch<{
    id: string;
    tabName: string;
    action: string;
    additionalAction: string;
  }>('/hra/:tabName/:id/:action/:additionalAction?');
  const tabName = routeMatcherVorbereitung?.params.tabName ? routeMatcherVorbereitung?.params.tabName : '';
  const action = routeMatcherVorbereitung?.params.action ? routeMatcherVorbereitung?.params.action : '';
  const additionalAction = routeMatcherVorbereitung?.params.additionalAction
    ? routeMatcherVorbereitung?.params.additionalAction
    : '';
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const headerController = GlobalDI.get<HeaderController>('enormHeadercontroller');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const codelistenControllerApi = GlobalDI.getOrRegister(
    'codelistenControllerApi',
    () => new CodelistenControllerApi(),
  );

  const [abstimmung, setAbstimmung] = useState<AbstimmungEntityResponseDTO>();
  const [finished, setFinished] = useState(false);
  const [loggedUserEmail, setLoggedUserEmail] = useState<string>('');
  const [codeList, setCodeList] = useState<CodelistenResponseDTO>();
  const [isTeilnehmerAccepted, setIsTeilnehmerAccepted] = useState<boolean | null>(null);
  const appStore = useAppSelector((state) => state.user);

  const fristExceeded = (abstimmungEntity: AbstimmungEntityResponseDTO) => {
    return isPast(
      new Date(
        abstimmungEntity.dto.fristen.find((frist) => {
          return frist.fristTyp === FristtypType.Abstimmungsende;
        })?.fristablauf || '',
      ),
    );
  };
  const prepareData = (val: AbstimmungEntityResponseDTO) => {
    setAbstimmung(val);

    setFinished(val.dto.oberabstimmung?.geschlossen || fristExceeded(val));
    setBreadcrumb(val, val.dto.oberabstimmung?.geschlossen || fristExceeded(val), loggedUserEmail, false);
    loadingStatusController.setLoadingStatus(false);
    if (additionalAction === routes.ABSTIMMUNGSRUNDE_TEILNAHMER) {
      displayMessage(
        t(`enorm.votingRound.msgEinleiterTeilnahmeanfrage`, { abstimmungTitel: val.dto.titel }),
        'success',
      );
    }
  };

  const checkError = (error: AjaxError) => {
    loadingStatusController.setLoadingStatus(false);
    errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
  };

  const unsub = (sub: Subscription) => {
    if (sub) {
      sub.unsubscribe();
    }
  };

  useEffect(() => {
    let sub: Subscription;
    if (routeMatcherVorbereitung?.isExact) {
      loadingStatusController.setLoadingStatus(true);
      sub = getMeineAbstimmungCall(routeMatcherVorbereitung.params.id).subscribe({
        next: (val: AbstimmungEntityResponseDTO) => {
          prepareData(val);
        },
        error: (error: AjaxError) => {
          checkError(error);
        },
      });
    }

    const codelistSub = codelistenControllerApi.getCodelisten().subscribe({
      next: (codeListResponse: CodelistenResponseDTO) => setCodeList(codeListResponse),
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
        console.error('Error sub', error);
      },
    });

    return () => {
      unsub(sub);
      unsub(codelistSub);
    };
  }, [routeMatcherVorbereitung?.params.id]);
  useEffect(() => {
    if (appStore.user) {
      setLoggedUserEmail(appStore.user.dto.email);
    }
  }, [appStore.user]);
  useEffect(() => {
    if (abstimmung) {
      setBreadcrumb(
        abstimmung,
        abstimmung.dto.oberabstimmung?.geschlossen || fristExceeded(abstimmung),
        loggedUserEmail,
        isTeilnehmerAccepted,
      );
      setIsTeilnehmerAccepted(checkTeilnehmerAccepted(abstimmung.dto.anfragen, loggedUserEmail, action));
    }
  }, [abstimmung, loggedUserEmail, isTeilnehmerAccepted]);

  const checkTeilnehmerAccepted = (
    anfragen: AnfrageEntityResponseDTO[] | undefined,
    loggedUserEmail: string,
    action: string,
  ) => {
    if (action === routes.ABSTIMMUNGSRUNDE || !anfragen || !anfragen.length) {
      return null;
    }

    return !anfragen.some(
      (anfrage) =>
        anfrage.dto.erstelltVon?.dto.email === loggedUserEmail &&
        anfrage.dto.typ === AnfragetypType.Abstimmungsteilnahme &&
        anfrage.dto.status !== AnfrageStatusType.Zugestimmt,
    );
  };

  const setBreadcrumb = (
    vote: AbstimmungEntityResponseDTO,
    fristExceeded: boolean,
    loggedInUserEmail: string,
    teilnehmerAccepted: boolean | null,
  ) => {
    const tabLink = (
      <Link id="enorm-voteRound-breadcrumbsTabNavigation-link" key={`enorm-${tabName}`} to={`/hra/${tabName}`}>
        {t(`enorm.breadcrumbs.projectName`)} - {t(`enorm.breadcrumbs.tabName.${tabName}`)}
      </Link>
    );

    const breadcrumbTitle = <span key={`vote-${action}`}>{getAbstimmungsbezeichner(abstimmung?.dto)}</span>;
    const regelungsvorhaben = vote.dto.regelungsvorhaben;
    const rvInfoDrawer = (
      <RegelungsvorhabenDetailsDrawer
        key="breadcrumb-rv-detail-drawer"
        regelungsvorhaben={regelungsvorhaben}
        isCreator={regelungsvorhaben?.dto.erstelltVon?.dto.email === loggedInUserEmail}
        codeList={codeList}
      />
    );
    let headerRightBtns = [
      <Button
        id="enorm-datenExport-btn"
        key="export-button"
        type="primary"
        onClick={() =>
          openFile(`${BASE_PATH}/file/fullexport/${vote.base.id}`, {
            fileName: `${abstimmung?.dto.titel ?? 'Abstimmung'}_${abstimmung?.dto.version ?? 0}_full.zip`,
          })
        }
        disabled={!fristExceeded}
      >
        {t('enorm.votingRound.header.exportButton')}
      </Button>,
      <Button
        id="enorm-abstimmungsrundeBearbeiten-btn"
        key="enorm-edit-button"
        type="primary"
        disabled={fristExceeded}
        onClick={() => history.push(`/hra/${tabName}/${vote.base.id}/${routes.ABSTIMMUNG_BEARBEITEN}`)}
      >
        {t('enorm.votingRound.header.editButton')}
      </Button>,
      rvInfoDrawer,
      <DropdownMenu
        key="votingRound_drobdown"
        items={[
          {
            element: t('enorm.votingRound.header.exportButton'),
            onClick: () => {
              openFile(`${BASE_PATH}/file/fullexport/${vote.base.id}`, {
                fileName: `${abstimmung?.dto.titel ?? 'Abstimmung'}_${abstimmung?.dto.version ?? 0}_full.zip`,
              });
            },
            disabled: () => !fristExceeded,
          },
          {
            element: t('enorm.votingRound.header.editButton'),
            onClick: () => history.push(`/hra/${tabName}/${vote.base.id}/${routes.ABSTIMMUNG_BEARBEITEN}`),
            disabled: () => fristExceeded,
          },
          {
            element: rvInfoDrawer,
          },
        ]}
        elementId={'headerRightAlternative'}
        overlayClass={'headerRightAlternative-overlay'}
      />,
    ];
    if (teilnehmerAccepted) {
      headerRightBtns = [
        rvInfoDrawer,
        <DropdownMenu
          key="votingRound_teilnehmerAccepted_drobdown"
          items={[
            {
              element: rvInfoDrawer,
            },
          ]}
          elementId={'headerRightAlternative'}
          overlayClass={'headerRightAlternative-overlay'}
        />,
      ];
    } else if (teilnehmerAccepted === false) {
      headerRightBtns = [];
    }

    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[tabLink, breadcrumbTitle]} />],
      headerRight: headerRightBtns,
      headerLast: [<EnormHelpBreadcrumb key="enormhelp-breadcrumb" />],
      headerCenter: [],
    });
  };

  return (
    <Row>
      {abstimmung && (
        <Col xs={{ span: 22, offset: 1 }}>
          <div className="voting-round">
            <div className="heading-holder">
              <VoteDetailsHead
                action={action}
                abstimmung={abstimmung}
                tabName={tabName}
                isTeilnehmerAccepted={isTeilnehmerAccepted}
              />
            </div>
          </div>
          {action !== routes.ANFRAGE_TEILNAHME_DETAILS && (
            <div>
              <VoteDetailsBody finished={finished} abstimmung={abstimmung} />
            </div>
          )}
        </Col>
      )}
    </Row>
  );
}
