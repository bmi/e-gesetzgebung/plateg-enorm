// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ColumnsType } from 'antd/lib/table';
import Text from 'antd/lib/typography/Text';
import i18n from 'i18next';
import React, { ReactElement } from 'react';
import { Observable } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import { AbstimmungControllerApi } from '@plateg/rest-api';
import {
  AbstimmungEntityResponseDTO,
  DocumentType,
  TeilnehmerWithFilesEntityListResponseDTO,
} from '@plateg/rest-api/models';
import {
  ContactPerson,
  DocumentListPopover,
  Download,
  DropdownMenu,
  ErrorController,
  GlobalDI,
  InfoComponent,
  TwoLineText,
} from '@plateg/theme';
import { DropdownMenuItem } from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';

import { getFileLink } from '../../../../../utils/controller';
import { TableProps, TeilnehmerWithFilesEntityResponseDTORow } from './component.react';

const getKommentarRenderer = (record: TeilnehmerWithFilesEntityResponseDTORow): ReactElement => {
  return (
    <>
      <label htmlFor={'expandable-comment-' + record.id.toString()}>
        {i18n.t('enorm.votingRound.table.expandedTitle')} {record.dto.user.dto.name}{' '}
      </label>
      <Text id={'expandable-comment-' + record.id.toString()} className="expandable-row-comment">
        {record.dto.kommentar}
      </Text>
    </>
  );
};

const getFilesRenderer = (
  record: TeilnehmerWithFilesEntityResponseDTORow,
  documentType: DocumentType,
): ReactElement => {
  record.dto.files.dtos.sort((a, b) => (a.dto.entwurfStatusType > b.dto.entwurfStatusType ? 1 : -1));

  const files = record.dto.files.dtos.map((fileDto) => {
    const element = <Download link={getFileLink(fileDto.base.id)} isfile name={fileDto.dto.fileName}></Download>;
    const key = fileDto.dto.fileName + fileDto.base.id.toString();
    return record.dto.deleted ? <del key={key}>{element}</del> : <div key={key}>{element}</div>;
  });
  if (documentType === DocumentType.Editor) {
    return (
      <>
        {record.dto.dokumentenmappe && record.dto.dokumentenmappe.dokumente?.length && (
          <DocumentListPopover dokumentenmappe={record.dto.dokumentenmappe} recordId={record.id} />
        )}
        {files}
      </>
    );
  }
  return <>{files}</>;
};

export function getAbstimmungTableVals(
  refreshContent: Function,
  finished: boolean,
  abstimmung: AbstimmungEntityResponseDTO,
): TableProps<TeilnehmerWithFilesEntityResponseDTORow> {
  const columns: ColumnsType<TeilnehmerWithFilesEntityResponseDTORow> = [
    {
      title: i18n.t('enorm.votingRound.table.theadRessortAndReferat').toString(),
      key: 'cRessort',
      render: (record: TeilnehmerWithFilesEntityResponseDTORow): ReactElement => {
        const ressort = record.dto.user.dto.ressort?.kurzbezeichnung || '';
        let fachreferatAndAbteilung = '';
        if (record.dto.user.dto.abteilung) {
          fachreferatAndAbteilung = `(${record.dto.user.dto.abteilung} ${record.dto.user.dto.fachreferat || ''})`;
        }
        const element = (
          <TwoLineText
            firstRow={ressort}
            firstRowBold={false}
            secondRow={fachreferatAndAbteilung}
            secondRowBold={false}
            secondRowLight={true}
            elementId={record.id}
          ></TwoLineText>
        );

        return record.dto.deleted ? <del>{element}</del> : element;
      },
    },
    {
      title: i18n.t('enorm.votingRound.table.theadBeteiligt').toString(),
      key: 'cBeteiligt',
      render: (record: TeilnehmerWithFilesEntityResponseDTORow) => {
        const element = (
          <>
            {record.dto.user.dto.name && (
              <>
                <InfoComponent
                  isContactPerson={true}
                  title={i18n.t('enorm.beantwortenVote.generalInfo.contactPersonTitle')}
                  buttonText={record.dto.user.dto.name || ''}
                  id={record.id}
                >
                  <ContactPerson user={record.dto.user.dto} />
                </InfoComponent>
                <br />
              </>
            )}
            <Text className="text-light">{record.dto.user.dto.email || ''}</Text>
          </>
        );
        return record.dto.deleted ? <del>{element}</del> : element;
      },
    },
    {
      title: i18n
        .t(
          `enorm.votingRound.table.${
            abstimmung.dto.documentTyp === DocumentType.Enorm ? 'theadFiles' : 'theadDocuments'
          }`,
        )
        .toString(),
      key: 'cFiles',
      className: 'file-column',
      render: (record: TeilnehmerWithFilesEntityResponseDTORow) => {
        return getFilesRenderer(record, abstimmung.dto.documentTyp);
      },
    },
    {
      title: i18n.t('enorm.votingRound.table.theadStatus').toString(),
      key: 'cStatus',
      render: (record: TeilnehmerWithFilesEntityResponseDTORow) => {
        if (record.dto.deleted) {
          return <Text>{i18n.t('enorm.votingRound.table.participantRemoved').toString()}</Text>;
        }
        if (record.dto.readOnlyAccess) {
          return <Text>{i18n.t('enorm.votingRound.table.readOnly').toString()}</Text>;
        }
        return <Text>{i18n.t('enorm.myVotes.tabs.sign.table.status.' + record.dto.status).toString()}</Text>;
      },
    },
    {
      title: i18n.t('enorm.votingRound.table.theadActions').toString(),
      key: 'cActions',
      render: (record: TeilnehmerWithFilesEntityResponseDTORow) => {
        const items: DropdownMenuItem[] = [
          {
            element: i18n.t(`enorm.votingRound.table.menuItemTeilnehmerEntfernen`),
            onClick: () => {
              deleteTeilnehmer(abstimmung.base.id, record.dto.user.base.id, refreshContent);
            },
            disabled: () => finished,
          },
          { element: i18n.t(`enorm.votingRound.table.menuItemNurLesezugriff`), disabled: () => true },
        ];
        return <DropdownMenu items={items} elementId={record.id} />;
      },
    },
  ];
  return {
    columns,
    expandable: true,
    expandedRowRenderer: getKommentarRenderer,
    rowClassName: (record: TeilnehmerWithFilesEntityResponseDTORow) =>
      record.dto.deleted ? 'table-row-inactive' : 'table-row-active',
    expandableCondition: (record: TeilnehmerWithFilesEntityResponseDTORow) => {
      return !!record.dto.kommentar;
    },
  };
}

export function sortDeleted(
  content: TeilnehmerWithFilesEntityResponseDTORow[],
): TeilnehmerWithFilesEntityResponseDTORow[] {
  return content.sort((a, b) => {
    return Number(a.dto.deleted) - Number(b.dto.deleted);
  });
}

export function sortInital(
  content: TeilnehmerWithFilesEntityResponseDTORow[],
): TeilnehmerWithFilesEntityResponseDTORow[] {
  return content.sort((a, b) => {
    if (a.dto.deleted || b.dto.deleted) {
      return Number(a.dto.deleted) - Number(b.dto.deleted);
    }
    if (a.dto.readOnlyAccess || b.dto.readOnlyAccess) {
      return Number(a.dto.readOnlyAccess) - Number(b.dto.readOnlyAccess);
    } else {
      return 0;
    }
  });
}

export function getTeilnehmerCall(id: string): Observable<TeilnehmerWithFilesEntityListResponseDTO> {
  const abstimmungController = GlobalDI.get<AbstimmungControllerApi>('enormController');
  return abstimmungController.getTeilnehmer({ id });
}

export function getAbstimmungCall(id: string): Observable<AbstimmungEntityResponseDTO> {
  const abstimmungController = GlobalDI.get<AbstimmungControllerApi>('enormController');
  return abstimmungController.getAbstimmung({ id });
}

function deleteTeilnehmer(id: string, userId: string, refreshContent: Function) {
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  deleteTeilnehmerCall(id, userId).subscribe({
    next: () => {
      refreshContent();
    },
    error: (error: AjaxError) => {
      console.error(error, error);
      errorCtrl.displayErrorMsg(error, 'enorm.votingRound.table.deleteErrorMsg');
    },
  });
}

export function deleteTeilnehmerCall(id: string, userId: string): Observable<void> {
  const abstimmungController = GlobalDI.get<AbstimmungControllerApi>('enormController');
  return abstimmungController.deleteTeilnehmer({ id, userId });
}
