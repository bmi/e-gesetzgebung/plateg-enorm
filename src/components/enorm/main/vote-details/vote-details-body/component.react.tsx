// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './vote-details-body.less';

import { ColumnsType } from 'antd/lib/table';
import React, { ReactElement, useEffect, useState } from 'react';
import { AjaxError } from 'rxjs/ajax';

import {
  AbstimmungEntityResponseDTO,
  TeilnehmerStatusType,
  TeilnehmerWithFilesEntityListResponseDTO,
  TeilnehmerWithFilesEntityResponseDTO,
} from '@plateg/rest-api/models/';
import { CommonRow, ErrorController, GlobalDI, TableComponent } from '@plateg/theme';

import { getAbstimmungTableVals, getTeilnehmerCall, sortInital } from './controller.react';

export interface TeilnehmerWithFilesEntityResponseDTORow extends TeilnehmerWithFilesEntityResponseDTO {
  id: string;
}

interface VoteDetailsBodyProps {
  abstimmung?: AbstimmungEntityResponseDTO;
  finished: boolean;
}

export interface TableProps<T extends CommonRow> {
  columns: ColumnsType<T>;
  expandable: boolean;
  expandedRowRenderer: (record: T) => ReactElement;
  expandableCondition?: (record: T) => boolean;
  rowClassName: (record: TeilnehmerWithFilesEntityResponseDTORow, index: number) => string;
}
export function VoteDetailsBody(props: VoteDetailsBodyProps): React.ReactElement {
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const abstimmung = props.abstimmung;
  const finished = props.finished;
  const [content, setContent] = useState<TeilnehmerWithFilesEntityResponseDTORow[]>([]);

  const refreshContent = () => {
    if (abstimmung) {
      getTeilnehmerCall(abstimmung.base.id).subscribe({
        next: (vals: TeilnehmerWithFilesEntityListResponseDTO) => {
          const tableVals: TeilnehmerWithFilesEntityResponseDTORow[] = vals.dtos
            .filter(
              (tn) =>
                tn.dto.status !== TeilnehmerStatusType.TeilnahmeZurueckgezogen &&
                tn.dto.status !== TeilnehmerStatusType.TeilnahmeAbgelehnt &&
                tn.dto.status !== TeilnehmerStatusType.TeilnahmeAngefragt,
            )
            .map((val) => {
              return { ...val, id: val.base.id };
            });
          setContent(sortInital(tableVals));
          seperateWithComma(tableVals);
        },
        error: (error: AjaxError) => {
          errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
        },
      });
    }
  };

  const seperateWithComma = (memberList: TeilnehmerWithFilesEntityResponseDTORow[]) => {
    memberList.forEach((m) => {
      if (m.dto.user.dto.fachreferat !== null) {
        m.dto.user.dto.abteilung = m.dto.user.dto.abteilung + ',';
      }
    });
  };

  const tableProps: TableProps<TeilnehmerWithFilesEntityResponseDTORow> = getAbstimmungTableVals(
    refreshContent,
    finished,
    abstimmung,
  );

  useEffect(() => {
    refreshContent();
  }, [abstimmung]);

  return (
    <div className="abstimmung-body">
      {content?.length > 0 && (
        <TableComponent
          id="hra-abstimmung-body-table"
          columns={tableProps.columns}
          content={content}
          expandableCondition={tableProps.expandableCondition}
          expandable={tableProps.expandable}
          rowClassName={tableProps.rowClassName}
          expandedRowRender={tableProps.expandedRowRenderer}
        ></TableComponent>
      )}
    </div>
  );
}
