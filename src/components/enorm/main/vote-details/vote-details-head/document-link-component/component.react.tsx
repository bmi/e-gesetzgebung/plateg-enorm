// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import { DokumentDTO, DokumentenmappeDTO } from '@plateg/rest-api';

import { EnormDownloadLink } from '../../../../../../shares/download-link/component.react';
import { getFileLink } from '../../../../../../utils/controller';
import { FileDownloadProps } from '../component.react';

interface DocumentLinkComponentProps {
  regelungsentwurf?: DokumentenmappeDTO;
  fileProps?: FileDownloadProps;
  noAnswer: React.ReactElement;
}

export function DocumentLinkComponent(props: DocumentLinkComponentProps): React.ReactElement {
  const { t } = useTranslation();
  let content;

  if (props.regelungsentwurf) {
    content = (
      <div>
        <span>Stammgesetz: {props.regelungsentwurf?.titel}</span>
        <table>
          {(props.regelungsentwurf.dokumente as DokumentDTO[]).map((document, index) => {
            return (
              <tbody key={index}>
                <tr>
                  <td>{t(`enorm.votingRound.header.documentTypeName.${document.typ}`)}:</td>
                  <td>
                    <a
                      id={`enorm-regelungsentwurf-link${index}`}
                      target="_blank"
                      href={`#/editor/document/${document.id}`}
                    >
                      {document.titel}
                    </a>
                  </td>
                </tr>
              </tbody>
            );
          })}
        </table>
      </div>
    );
  } else if (props.fileProps) {
    content = <EnormDownloadLink link={getFileLink(props.fileProps?.id)} name={props.fileProps?.name} isfile />;
  } else {
    content = props.noAnswer;
  }

  return (
    <div className={props.regelungsentwurf ? 'editor-entwurf' : ''}>
      <dt>{t(`enorm.votingRound.header.${props.regelungsentwurf ? 'labelEntwurfEditor' : 'labelDownload'}`)}</dt>
      <dd>{content}</dd>
    </div>
  );
}
