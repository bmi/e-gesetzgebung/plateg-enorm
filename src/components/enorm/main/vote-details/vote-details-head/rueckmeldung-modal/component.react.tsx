// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './rueckmeldungModal.less';

import { Button, Form, Radio } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { GeneralFormWrapper, HinweisComponent, ModalWrapper } from '@plateg/theme';

export interface RueckmeldungOption<T> {
  id: string;
  value: T;
  label: string;
}

export interface ModalInfo {
  modalTitel: string;
  modalSubTitel: string;
  labelMain: string;
  hinweisTitle: string;
  hinweisContent: string;
}

interface Props<T> {
  setREModalIsVisible: (value: boolean) => void;
  reModalIsVisible: boolean;
  rueckmeldung?: T;
  updateRueckmeldung: (rueckmeldung: T) => void;
  rueckmeldungOptions: RueckmeldungOption<T>[];
  formName: string;
  modalInfo: ModalInfo;
}

export function RueckmeldungModal<T>(props: Props<T>): React.ReactElement {
  const { t } = useTranslation();
  const [rueckmeldungForm] = Form.useForm();
  useEffect(() => {
    if (props.rueckmeldung) {
      rueckmeldungForm.setFieldValue(props.formName, props.rueckmeldung);
    }
  }, [props.rueckmeldung]);

  const radioGroupOptions = props.rueckmeldungOptions.map((option) => {
    return (
      <Radio key={option.id} name={props.formName} id={option.id} value={option.value}>
        {option.label}
      </Radio>
    );
  });

  return (
    <ModalWrapper
      className="rueckmeldungModal"
      open={props.reModalIsVisible}
      width={600}
      centered={true}
      closable={true}
      title={<h3>{props.modalInfo.modalTitel}</h3>}
      onCancel={() => props.setREModalIsVisible(false)}
      footer={[
        <div key="footer-left" className="plateg-modal-footer-left">
          <Button
            className="ant-btn"
            onClick={() => {
              props.setREModalIsVisible(false);
            }}
          >
            {t('enorm.votingRound.modalReImInternet.modalCancel')}
          </Button>
        </div>,
        <div className="plateg-modal-footer-spacer"></div>,
        <div key="footer-right" className="plateg-modal-footer-right">
          <Button
            className="ant-btn ant-btn-primary"
            onClick={() => {
              rueckmeldungForm
                .validateFields()
                .then(() => {
                  props.updateRueckmeldung(rueckmeldungForm.getFieldValue(props.formName) as T);
                })
                .catch((error) => {
                  console.error(error);
                });
            }}
          >
            {t('enorm.votingRound.modalReImInternet.modalConfirm')}
          </Button>
        </div>,
      ]}
    >
      <Title level={2}>{props.modalInfo.modalSubTitel}</Title>
      <p>{t('enorm.votingRound.modalReImInternet.modalPflichtfelderParagraph')}</p>
      <GeneralFormWrapper
        form={rueckmeldungForm}
        layout="vertical"
        scrollToFirstError={true}
        noValidate={false}
        onChange={() => {}}
      >
        <fieldset className="radio ">
          <Form.Item
            name={props.formName}
            label={<>{props.modalInfo.labelMain}</>}
            rules={[{ required: true, whitespace: true, message: t('enorm.beantwortenVote.mitzeichnung.error') }]}
          >
            <Radio.Group className="horizontal-radios" name="statusREInternet" defaultValue={props.rueckmeldung}>
              {radioGroupOptions}
            </Radio.Group>
          </Form.Item>
        </fieldset>
      </GeneralFormWrapper>
      <HinweisComponent
        mode="warning"
        title={props.modalInfo.hinweisTitle}
        content={<span>{props.modalInfo.hinweisContent}</span>}
      />
    </ModalWrapper>
  );
}
