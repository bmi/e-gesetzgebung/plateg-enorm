// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Text from 'antd/lib/typography/Text';
import React from 'react';

import { AbstimmungEntityResponseDTO } from '@plateg/rest-api';

import { getAdditionalDeadlines } from '../../../../../../utils/controller';

interface AdditionalDeadlineComponentProps {
  abstimmung?: AbstimmungEntityResponseDTO;
}
export function AdditionalDeadlineComponent(props: AdditionalDeadlineComponentProps): React.ReactElement {
  return (
    <>
      {getAdditionalDeadlines(props.abstimmung?.dto.fristen, true).map((frist, index) => {
        return (
          <li key={index} className="additional-fristen-list">
            <Text>
              {frist} Uhr
              {index < getAdditionalDeadlines(props.abstimmung?.dto.fristen).length - 1 ? ', ' : ''}
            </Text>
          </li>
        );
      })}
    </>
  );
}
