// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Col, Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { AjaxError } from 'rxjs/ajax';

import {
  AbstimmungControllerApi,
  AbstimmungEntityResponseDTO,
  AbstimmungstypType,
  AnfragetypType,
  BASE_PATH,
  DokumentenmappeDTO,
  FileControllerApi,
  FileEntityListResponseDTO,
  FristtypType,
  ModifyRueckmeldungBkAmtRequest,
  ModifyRueckmeldungBmjRequest,
  RueckmeldungBkAmtType,
  RueckmeldungBmjType,
  UserEntityResponseDTO,
} from '@plateg/rest-api';
import { BtnBackComponent, displayMessage, DownloadOutlined, ErrorController, GlobalDI } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';
import { openFile } from '@plateg/theme/src/controllers/RestConfigController';

import { routes } from '../../../../../shares/routes';
import {
  checkDeadlineExceededString,
  getAbstimmungsbezeichner,
  getAbstimmungsrundeName,
  getAdditionalDeadlines,
  getDateString,
  getDeadline,
  getGeschlossenString,
  getTyp,
} from '../../../../../utils/controller';
import { AbstimmungDetailsController } from '../../mitzeichnung-main/abstimmung-details/controller';
import { BeantwortenController } from '../../mitzeichnung-main/controller';
import { compareOrValues } from '../../vote-main/vote-base/component.react';
import { AnfragenComponent } from '../anfragen/component.react';
import { VerlaufComponent } from '../anfragen/verlauf/verlauf-component/component.react';
import { AdditionalDeadlineComponent } from './additional-deadline-component/component.react';
import { AdditionalDocumentLinkComponent } from './additional-documents-link-component/component.react';
import { DocumentLinkComponent } from './document-link-component/component.react';
import { RueckmeldungModal, RueckmeldungOption } from './rueckmeldung-modal/component.react';

interface VoteDetailsHeadProps {
  abstimmung?: AbstimmungEntityResponseDTO;
  action: string;
  tabName: string;
  isTeilnehmerAccepted: boolean | null;
}
export interface FileDownloadProps {
  id: string;
  name: string;
}

// helper function to reduce complexity
const checkDefinedValue = (arg1: unknown, arg2: React.ReactNode) => arg1 && arg2;

export function VoteDetailsHead(props: VoteDetailsHeadProps): React.ReactElement {
  const { t } = useTranslation();
  const ctrl = GlobalDI.getOrRegister('enormBeantwortenController', () => new BeantwortenController());
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const voteDetailCtrl = new AbstimmungDetailsController();
  const abstimmung = props.abstimmung;
  const regelungsvorhaben = abstimmung?.dto.regelungsvorhaben;
  const [userInfo, setUserInfo] = useState<UserEntityResponseDTO>();
  const [fileProps, setFileProps] = useState<FileDownloadProps>();
  const [additionalFileProps, setAdditionalFileProps] = useState<FileDownloadProps[]>([]);
  const [mailVisible, setMailVisible] = useState(true);
  const [regelungsentwurf, setRegelungsentwurf] = useState<DokumentenmappeDTO>();
  const [rueckmeldungBkAmt, setRueckmeldungBkAmt] = useState<RueckmeldungBkAmtType | undefined>();
  const [rueckmeldungBmj, setRueckmeldungBmj] = useState<RueckmeldungBmjType | undefined>();
  const [reModalIsVisible, setREModalIsVisible] = useState<boolean>(false);
  const [bmjModalIsVisible, setBMJModalIsVisible] = useState<boolean>(false);
  const [userIsErsteller, setUserIsErsteller] = useState<boolean>();

  const appStore = useAppSelector((state) => state.user);

  const newFrist = voteDetailCtrl.getAbstimmungDetailsValues(abstimmung).neueFrist;

  const updateRueckmeldungBkAmt = (rueckmeldung: ModifyRueckmeldungBkAmtRequest) => {
    const abstimmungController = GlobalDI.getOrRegister<AbstimmungControllerApi>(
      'enormController',
      () => new AbstimmungControllerApi(),
    );
    abstimmungController.modifyRueckmeldungBkAmt(rueckmeldung).subscribe({
      next: () => {
        setRueckmeldungBkAmt(rueckmeldung.rueckmeldungType);
        setREModalIsVisible(false);
      },
      error: (error: string) => {
        setREModalIsVisible(false);
        displayMessage(t(`enorm.votingRound.modalReImInternet.saveError`) + ' ' + error, error);
      },
    });
  };

  const updateRueckmeldungBMJ = (rueckmeldung: ModifyRueckmeldungBmjRequest) => {
    const abstimmungController = GlobalDI.getOrRegister<AbstimmungControllerApi>(
      'enormController',
      () => new AbstimmungControllerApi(),
    );
    abstimmungController.modifyRueckmeldungBmj(rueckmeldung).subscribe({
      next: () => {
        setRueckmeldungBmj(rueckmeldung.rueckmeldungType);
        setBMJModalIsVisible(false);
      },
      error: (error: string) => {
        setBMJModalIsVisible(false);
        displayMessage(t(`enorm.votingRound.modalReImInternet.saveError`) + ' ' + error, error);
      },
    });
  };

  useEffect(() => {
    const abstimmungId = abstimmung?.base.id;
    if (abstimmungId) {
      const fileController = GlobalDI.get<FileControllerApi>('fileController');
      fileController.getErstellerFiles({ abstimmungId }).subscribe(
        (vals: FileEntityListResponseDTO) => {
          ctrl.setFiles(vals, setFileProps, setAdditionalFileProps);
        },
        (error: AjaxError) => {
          errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
        },
      );
      if (abstimmung.dto.typ === AbstimmungstypType.Schlussabstimmung) {
        const abstimmungController = GlobalDI.get<AbstimmungControllerApi>('enormController');
        abstimmungController.getErsteller({ id: abstimmungId }).subscribe({
          next: (data) => {
            setUserIsErsteller(data.base.id === appStore.user?.base.id);
          },
          error: (error: AjaxError) => {
            errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
            console.error(error);
          },
        });
      }
    }
    if (abstimmung?.dto.dokumentenmappe) {
      setRegelungsentwurf(abstimmung?.dto.dokumentenmappe);
    }
    setRueckmeldungBkAmt(abstimmung?.dto.rueckmeldungBkAmt);
    setRueckmeldungBmj(abstimmung?.dto.rueckmeldungBmj);
  }, [abstimmung]);

  useEffect(() => {
    if (appStore.user) {
      setUserInfo(appStore.user);
    }
  }, [appStore.user]);

  let route = routes.MEINE_ABSTIMMUNG;
  let action = routes.ABSTIMMUNGSRUNDE;

  if (props.abstimmung?.dto.oberabstimmung?.erstellerId !== userInfo?.base.id) {
    route = routes.BITTE_UM_MITZEICHNUNG;
    action = routes.ABSTIMMUNG_BEANTWORTEN_PRUEFEN;
  }

  const openMailText = () => {
    setMailVisible(!mailVisible);
  };
  const noAnswer = <em>{t('enorm.vote.noAnswer')}</em>;
  const getInitialFristElement = (abstimmung: AbstimmungEntityResponseDTO | undefined): React.ReactElement | null => {
    const frist = getDeadline(FristtypType.Abstimmungsende, abstimmung?.dto.fristen, true);
    return frist ? (
      <>
        <dt>{t('enorm.votingRound.header.labelDeadline')}</dt>
        <dd>
          <strong>{`${frist} Uhr`}</strong>
          <span className="text-light">
            {getGeschlossenString(abstimmung?.dto.oberabstimmung) ?? checkDeadlineExceededString(abstimmung)}
          </span>
        </dd>
        <dt>{t('enorm.votingRound.header.labelVerschweigensfrist')}</dt>
        <dd>
          {t(`enorm.votingRound.header.optionsVerschweigensfrist.${abstimmung?.dto.verschweigensfristGekennzeichnet}`)}
        </dd>
      </>
    ) : (
      <dd>{noAnswer}</dd>
    );
  };

  const ruckmeldeOptionsBKAmt: RueckmeldungOption<RueckmeldungBkAmtType>[] = [
    {
      id: 'enorm-beantwortenVoteMitzeichnen-radio',
      value: RueckmeldungBkAmtType.RueckmeldungOffen,
      label: t('enorm.votingRound.modalReImInternet.RUECKMELDUNG_OFFEN'),
    },
    {
      id: 'enorm-beantwortenVoteMitzeichnenUnterVorbehalt-radio',
      value: RueckmeldungBkAmtType.EinvernehmenErteilt,
      label: t('enorm.votingRound.modalReImInternet.EINVERNEHMEN_ERTEILT'),
    },
    {
      id: 'enorm-beantwortenVoteKeineMitzeichnung-radio',
      value: RueckmeldungBkAmtType.EinvernehmenAbgelehnt,
      label: t('enorm.votingRound.modalReImInternet.EINVERNEHMEN_ABGELEHNT'),
    },
  ];

  const modalinfoBKAmt = {
    modalTitel: t('enorm.votingRound.modalReImInternet.modalTitel'),
    modalSubTitel: t('enorm.votingRound.modalReImInternet.modalSubTitel'),
    labelMain: t('enorm.votingRound.modalReImInternet.labelMain'),
    hinweisTitle: t('enorm.votingRound.modalReImInternet.hinweisTitle'),
    hinweisContent: t('enorm.votingRound.modalReImInternet.hinweisContent'),
  };

  const ruckmeldeOptionsBMJ: RueckmeldungOption<RueckmeldungBmjType>[] = [
    {
      id: 'enorm-bmj-rueckmeldung-offen-radio',
      value: RueckmeldungBmjType.RueckmeldungOffen,
      label: t('enorm.votingRound.modalSABestaetigungDurchfuehrung.RUECKMELDUNG_OFFEN'),
    },
    {
      id: 'enorm-bmj-best-erteilt-radio',
      value: RueckmeldungBmjType.EinvernehmenErteilt,
      label: t('enorm.votingRound.modalSABestaetigungDurchfuehrung.EINVERNEHMEN_ERTEILT'),
    },
    {
      id: 'enorm-bmj-best-abgeleht-radio',
      value: RueckmeldungBmjType.EinvernehmenAbgelehnt,
      label: t('enorm.votingRound.modalSABestaetigungDurchfuehrung.EINVERNEHMEN_ABGELEHNT'),
    },
  ];

  const modalinfoBMJ = {
    modalTitel: t('enorm.votingRound.modalSABestaetigungDurchfuehrung.modalTitel'),
    modalSubTitel: t('enorm.votingRound.modalSABestaetigungDurchfuehrung.modalSubTitel'),
    labelMain: t('enorm.votingRound.modalSABestaetigungDurchfuehrung.labelMain'),
    hinweisTitle: t('enorm.votingRound.modalSABestaetigungDurchfuehrung.hinweisTitle'),
    hinweisContent: t('enorm.votingRound.modalSABestaetigungDurchfuehrung.hinweisContent'),
  };
  return (
    <>
      <Row>
        <Col>
          <Title level={1}>{getAbstimmungsbezeichner(props.abstimmung?.dto)}</Title>
        </Col>
      </Row>

      <dl>
        <>
          {checkDefinedValue(
            abstimmung?.dto?.titel,
            <>
              <dt>{t('enorm.votingRound.header.labelRegelungsvorhaben')}</dt>
              <dd>
                {regelungsvorhaben?.dto.abkuerzung} - {regelungsvorhaben?.dto.kurzbezeichnung}
              </dd>
            </>,
          )}
          {getTyp(abstimmung?.dto.typ) &&
          abstimmung?.dto.typ === AbstimmungstypType.Unterabstimmung &&
          abstimmung?.dto.oberabstimmung !== null ? (
            <>
              <dt>{t('enorm.votingRound.header.labelAbstimmungsschritt')}</dt>
              <dd>
                {t('enorm.votingRound.header.unterabstimmungDetails', {
                  unterabstimmung: getTyp(abstimmung?.dto.typ),
                  hauptabstimmung: getTyp(abstimmung?.dto.oberabstimmung?.typ),
                })}
                {
                  <Link
                    to={`/hra/${route}/${props.abstimmung?.dto?.oberabstimmung?.abstimmungId as string}/${action}`}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {getAbstimmungsrundeName(
                      compareOrValues(abstimmung?.dto.version, 1) as number,
                      abstimmung?.dto.titel,
                    )}
                  </Link>
                }
              </dd>
            </>
          ) : (
            (
              <>
                <dt>{t('enorm.votingRound.header.labelAbstimmungsschritt')}</dt>
                <dd>{getTyp(abstimmung?.dto.typ)}</dd>
              </>
            ) || null
          )}
          {abstimmung?.dto.eingeschraenkt !== undefined && (
            <>
              <dt>{t('enorm.votingRound.header.labelZugang')}</dt>
              {abstimmung?.dto.eingeschraenkt ? (
                <dd>{t('enorm.votingRound.header.optionsZugang.eingeschraenkt')}</dd>
              ) : (
                <dd>{t('enorm.votingRound.header.optionsZugang.offen')}</dd>
              )}
            </>
          )}
          {getInitialFristElement(abstimmung)}
          {checkDefinedValue(
            newFrist,
            <>
              <dt className="dt-new-deadline">{t('enorm.votingRound.header.labelNewDeadline')} </dt>
              {newFrist}
            </>,
          )}
          {compareOrValues(abstimmung?.dto.bkAmtUmEinvernehmenBitten, abstimmung?.dto.teilnehmendeUmBenehmenBitten) &&
            props.isTeilnehmerAccepted === null && (
              <>
                <dt>{t('enorm.votingRound.modalReImInternet.modalTitel')}:</dt>
                <dd>
                  {rueckmeldungBkAmt && (
                    <span className="span-ruckmeldung-modal-info">
                      {t(`enorm.votingRound.modalReImInternet.${rueckmeldungBkAmt}`)}
                    </span>
                  )}
                  <a
                    href="#"
                    onClick={(e) => {
                      e.preventDefault();
                      setREModalIsVisible(true);
                    }}
                  >
                    {t('enorm.votingRound.modalReImInternet.modalConfirm')}
                  </a>
                </dd>
                <RueckmeldungModal
                  modalInfo={modalinfoBKAmt}
                  formName="rueckmeldungBkAmt"
                  rueckmeldungOptions={ruckmeldeOptionsBKAmt}
                  setREModalIsVisible={setREModalIsVisible}
                  reModalIsVisible={reModalIsVisible}
                  rueckmeldung={rueckmeldungBkAmt}
                  updateRueckmeldung={(rueckmeldung: RueckmeldungBkAmtType): void => {
                    if (abstimmung?.base.id) {
                      updateRueckmeldungBkAmt({ id: abstimmung?.base.id, rueckmeldungType: rueckmeldung });
                    }
                  }}
                />
              </>
            )}
          {checkDefinedValue(
            abstimmung?.dto.typ === AbstimmungstypType.Schlussabstimmung && userIsErsteller,
            <>
              <dt>{t('enorm.votingRound.modalSABestaetigungDurchfuehrung.modalTitel')}:</dt>
              <dd>
                {rueckmeldungBmj && (
                  <span className="span-ruckmeldung-modal-info">
                    {t(`enorm.votingRound.modalSABestaetigungDurchfuehrung.${rueckmeldungBmj}`)}
                  </span>
                )}
                <a
                  href="#"
                  onClick={(e) => {
                    e.preventDefault();
                    setBMJModalIsVisible(true);
                  }}
                >
                  {t('enorm.votingRound.modalReImInternet.modalConfirm')}
                </a>
              </dd>
              <RueckmeldungModal
                modalInfo={modalinfoBMJ}
                formName="rueckmeldungBMJ"
                rueckmeldungOptions={ruckmeldeOptionsBMJ}
                setREModalIsVisible={setBMJModalIsVisible}
                reModalIsVisible={bmjModalIsVisible}
                rueckmeldung={rueckmeldungBmj}
                updateRueckmeldung={(rueckmeldung: RueckmeldungBmjType): void => {
                  if (abstimmung?.base.id) {
                    updateRueckmeldungBMJ({ id: abstimmung?.base.id, rueckmeldungType: rueckmeldung });
                  }
                }}
              />
            </>,
          )}
          {props.isTeilnehmerAccepted === null &&
            checkDefinedValue(
              getDeadline(FristtypType.VeroeffentlichungImInternet, abstimmung?.dto.fristen),
              <>
                <dt>{t('enorm.votingRound.header.labelInternetDeadlineEarlyReply')}</dt>
                <dd>{getDeadline(FristtypType.VeroeffentlichungImInternet, abstimmung?.dto.fristen)} Uhr</dd>
              </>,
            )}
          {checkDefinedValue(
            getDeadline(FristtypType.ZuleitungAnLaenderUndVerbaende, abstimmung?.dto.fristen),
            <>
              <dt>{t('enorm.votingRound.header.labelDeadlineEarlyReply')}</dt>
              <dd>{getDeadline(FristtypType.ZuleitungAnLaenderUndVerbaende, abstimmung?.dto.fristen)} Uhr</dd>
            </>,
          )}
          {getAdditionalDeadlines(abstimmung?.dto.fristen).length > 0 && (
            <>
              <dt>{t('enorm.votingRound.header.labelDeadlineAdditional')}</dt>
              <dd>
                <AdditionalDeadlineComponent abstimmung={abstimmung} />
              </dd>
            </>
          )}
          {getTyp(abstimmung?.dto.typ) && abstimmung?.dto.typ === AbstimmungstypType.Ressortabstimmung && (
            <>
              <dt>{t('enorm.votingRound.header.labelCoordinationBkAmt')}</dt>
              {abstimmung?.dto?.fruehkoordinierungBkAmtErfolgtAm &&
              abstimmung?.dto.fruehkoordinierungBkAmtErfolgtAm !== null ? (
                <dd>
                  {t('enorm.votingRound.header.optionsBkAmt.erfolgt', {
                    date: getDateString(abstimmung?.dto.fruehkoordinierungBkAmtErfolgtAm),
                  })}
                </dd>
              ) : (
                <dd>{t('enorm.votingRound.header.optionsBkAmt.nichtErfolgt')}</dd>
              )}
            </>
          )}
          {props.isTeilnehmerAccepted || props.isTeilnehmerAccepted === null ? (
            <DocumentLinkComponent fileProps={fileProps} regelungsentwurf={regelungsentwurf} noAnswer={noAnswer} />
          ) : (
            <>
              <dt>{t(`enorm.votingRound.header.labelDownload`)}</dt>
              <dd>{t(`enorm.votingRound.header.teilnehmerNotAccepted`)}</dd>
            </>
          )}
          {props.isTeilnehmerAccepted || props.isTeilnehmerAccepted === null ? (
            <AdditionalDocumentLinkComponent additionalFileProps={additionalFileProps} noAnswer={noAnswer} />
          ) : (
            <>
              <dt>{t(`enorm.votingRound.header.labelAdditionalDownload`)}</dt>
              <dd>{t(`enorm.votingRound.header.teilnehmerNotAccepted`)}</dd>
            </>
          )}
          {(props.isTeilnehmerAccepted || props.isTeilnehmerAccepted === null) && (
            <>
              <dt>{t('enorm.votingRound.header.labelInvitation')}</dt>
              <dd>
                <Button id="abstimmung-mail" className="show-mail-button" onClick={openMailText}>
                  {mailVisible
                    ? t('enorm.votingRound.header.labelInvitationButtonClosed')
                    : t('enorm.votingRound.header.labelInvitationButtonOpened')}
                </Button>
                <p hidden={mailVisible} offset={6} className="ant-typography p-no-style header-values">
                  {abstimmung?.dto.einladungsMailText}
                </p>
              </dd>
            </>
          )}
        </>
      </dl>

      {props.isTeilnehmerAccepted || props.isTeilnehmerAccepted === null ? (
        <AnfragenComponent tabName={props.tabName} action={props.action} anfragen={abstimmung?.dto.anfragen} />
      ) : (
        <dl>
          <VerlaufComponent
            data={[
              {
                type: AnfragetypType.Abstimmungsteilnahme,
                title: t('enorm.votingRound.header.anfragen.verlauf.types.teilnahme.title'),
                anfragen: abstimmung?.dto.anfragen?.filter((anfrage) => {
                  return anfrage.dto.typ === AnfragetypType.Abstimmungsteilnahme;
                }),
              },
            ]}
          />
        </dl>
      )}

      <Row>
        <Col className="header-labels" span={24}>
          <div className="vertical-seperator" />
        </Col>
      </Row>
      <Row className="btn-row">
        <Col span={16}>
          <BtnBackComponent
            styleBtn={{ marginTop: '6px' }}
            id="hra-VotingRound-btn-back"
            url={`/hra/${props.tabName}`}
          />
        </Col>

        <Col span={8}>
          {props.isTeilnehmerAccepted === null && (
            <Button
              id="enorm-teilnehmerExport-btn"
              type="primary"
              onClick={
                abstimmung
                  ? () =>
                      openFile(`${BASE_PATH}/file/teilnehmerexport/${abstimmung.base.id.toString()}`, {
                        fileName: `${abstimmung?.dto.titel ?? 'Abstimmung'}_runde_${abstimmung?.dto.version ?? 0}.zip`,
                        onError: (error) => {
                          console.error(error);
                        },
                      })
                  : undefined
              }
            >
              <DownloadOutlined />
              {t('enorm.votingRound.mid.downloadAll')}
            </Button>
          )}
        </Col>
      </Row>
    </>
  );
}
