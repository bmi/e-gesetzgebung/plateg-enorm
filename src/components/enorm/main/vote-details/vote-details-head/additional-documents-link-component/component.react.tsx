// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import { EnormDownloadLink } from '../../../../../../shares/download-link/component.react';
import { getFileLink } from '../../../../../../utils/controller';
import { FileDownloadProps } from '../component.react';

interface AdditionalDocumentLinkComponentProps {
  additionalFileProps?: FileDownloadProps[];
  noAnswer: React.ReactElement;
}
export function AdditionalDocumentLinkComponent(props: AdditionalDocumentLinkComponentProps): React.ReactElement {
  const { t } = useTranslation();
  let content;

  if (props.additionalFileProps && props.additionalFileProps.length > 0) {
    content = (
      <ul className="additional-file-list">
        {props.additionalFileProps.map((file) => {
          return (
            <li key={`${file.name}-${file.id}`}>
              <EnormDownloadLink isfile link={getFileLink(file.id)} name={file.name} />
            </li>
          );
        })}
      </ul>
    );
  } else {
    content = props.noAnswer;
  }

  return (
    <>
      <dt>{t('enorm.votingRound.header.labelAdditionalDownload')}</dt>
      <dd>{content}</dd>
    </>
  );
}
