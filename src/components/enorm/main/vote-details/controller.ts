// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Observable } from 'rxjs';

import { AbstimmungControllerApi, AbstimmungEntityResponseDTO } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

export function getMeineAbstimmungCall(id: string): Observable<AbstimmungEntityResponseDTO> {
  const abstimmungController = GlobalDI.get<AbstimmungControllerApi>('enormController');
  return abstimmungController.getAbstimmung({ id });
}
