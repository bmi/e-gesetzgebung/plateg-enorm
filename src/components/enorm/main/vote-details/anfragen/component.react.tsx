// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './anfragen-component.less';

import { Collapse, CollapseProps } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { AnfrageEntityResponseDTO, AnfragetypType } from '@plateg/rest-api';
import { DirectionDownOutlined } from '@plateg/theme/src/components/icons/DirectionDownOutlined';
import { DirectionRightOutlined } from '@plateg/theme/src/components/icons/DirectionRightOutlined';

import { routes } from '../../../../../shares/routes';
import { AnfragenSortType, VerlaufComponent } from './verlauf/verlauf-component/component.react';

interface AnfragenComponentProps {
  anfragen?: AnfrageEntityResponseDTO[];
  action: string;
  tabName: string;
}

export function AnfragenComponent(props: AnfragenComponentProps): React.ReactElement {
  const { t } = useTranslation();

  if (!props.anfragen?.length) {
    return <></>;
  }

  const getAnfragenType: AnfragenSortType[] = [
    {
      type: AnfragetypType.Fristverlaengerung,
      title: t('enorm.votingRound.header.anfragen.verlauf.types.frist.title'),
      anfragen: props.anfragen?.filter((item) => item.dto.typ === AnfragetypType.Fristverlaengerung),
    },
    {
      type: AnfragetypType.Abstimmungsteilnahme,
      title: t('enorm.votingRound.header.anfragen.verlauf.types.teilnahme.title'),
      anfragen: props.anfragen?.filter((item) => item.dto.typ === AnfragetypType.Abstimmungsteilnahme),
    },
  ];

  const filterAnfragen = (anfragenType?: AnfragetypType) =>
    getAnfragenType.filter((anfrage) => anfrage.type === anfragenType);

  const getAnfragenOnRoutes = (action: string, tabName: string) => {
    if (tabName === routes.MEINE_ABSTIMMUNG) {
      return getAnfragenType;
    }
    if (tabName === routes.ANFRAGEN) {
      return action === routes.ANFRAGE_TEILNAHME_DETAILS
        ? filterAnfragen(AnfragetypType.Abstimmungsteilnahme)
        : filterAnfragen(AnfragetypType.Fristverlaengerung);
    } else {
      // default case for ts
      return getAnfragenType;
    }
  };

  const collItems: CollapseProps['items'] = [
    {
      key: 1,
      label: t('enorm.votingRound.header.anfragen.title'),
      children: (
        <dl>
          <VerlaufComponent data={getAnfragenOnRoutes(props.action, props.tabName)} />
        </dl>
      ),
    },
  ];

  return (
    <Collapse
      items={collItems}
      className="anfragen-component"
      expandIcon={({ isActive }) => {
        if (isActive) {
          return <DirectionDownOutlined />;
        } else {
          return <DirectionRightOutlined />;
        }
      }}
      ghost
    />
  );
}
