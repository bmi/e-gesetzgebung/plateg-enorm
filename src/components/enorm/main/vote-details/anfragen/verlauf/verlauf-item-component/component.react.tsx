// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './verlauf-item.less';

import React from 'react';
import { useTranslation } from 'react-i18next';

import { AnfrageEntityResponseDTO, AnfrageStatusType } from '@plateg/rest-api';
import { ContactPerson, InfoComponent } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

interface VerlaufItemComponentProps {
  anfrage: AnfrageEntityResponseDTO;
}

export function VerlaufItemComponent(props: VerlaufItemComponentProps): React.ReactElement {
  const { t } = useTranslation();

  const status =
    props.anfrage?.dto.status === AnfrageStatusType.Offen ? (
      <b>{t(`enorm.votingRound.header.anfragen.verlauf.verlaufItem.status.status.${props.anfrage?.dto.status}`)}</b>
    ) : (
      <>
        <b>{t(`enorm.votingRound.header.anfragen.verlauf.verlaufItem.status.status.${props.anfrage?.dto.status}`)}</b>
        &nbsp;
        {t('enorm.votingRound.header.anfragen.verlauf.verlaufItem.status.content', {
          date: Filters.dateTimeFromString(props.anfrage?.base.bearbeitetAm),
        })}
      </>
    );

  if (!props.anfrage.dto.erstelltVon) {
    return <></>;
  }
  return (
    <>
      <dl className="verlauf-item-dl">
        <dt>{t('enorm.votingRound.header.anfragen.verlauf.verlaufItem.von.vonTitle')}</dt>
        <dd>
          <InfoComponent
            id={`${props.anfrage.dto.erstelltVon?.base.id}-${props.anfrage.base.id}`}
            isContactPerson={true}
            title={t('enorm.votingRound.header.anfragen.verlauf.verlaufItem.von.drawerTitle')}
            buttonText={props.anfrage?.dto.erstelltVon?.dto.name}
          >
            <ContactPerson user={props.anfrage?.dto.erstelltVon?.dto} />
          </InfoComponent>{' '}
          {t('enorm.votingRound.header.anfragen.verlauf.verlaufItem.von.content', {
            date: Filters.dateTimeFromString(props.anfrage?.base.erstelltAm),
          })}
        </dd>
        <dt>{t('enorm.votingRound.header.anfragen.verlauf.verlaufItem.status.statusTitle')}</dt>
        <dd>{status}</dd>
      </dl>
    </>
  );
}
