// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { Fragment } from 'react';

import { AnfrageEntityResponseDTO, AnfragetypType } from '@plateg/rest-api';

import { VerlaufItemComponent } from '../verlauf-item-component/component.react';

export interface AnfragenSortType {
  type: AnfragetypType;
  title: string;
  anfragen?: AnfrageEntityResponseDTO[];
}

interface VerlaufComponentProps {
  data: AnfragenSortType[];
  additionalContent?: React.ReactElement;
}

export function VerlaufComponent(props: VerlaufComponentProps): React.ReactElement {
  return (
    <>
      {props.data
        .filter((item) => item.anfragen?.length)
        .map((item) => {
          return (
            <Fragment key={item.type}>
              <dt>{item.title}</dt>
              <dd>
                {item.anfragen?.map((anfrage) => {
                  return <VerlaufItemComponent key={anfrage.base.id} anfrage={anfrage} />;
                })}
              </dd>
            </Fragment>
          );
        })}
      {props.additionalContent}
    </>
  );
}
