// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useRouteMatch } from 'react-router-dom';
import { AjaxError } from 'rxjs/ajax';

import { GlobalDI, LoadingStatusController } from '@plateg/theme';

import { getAbstimmungsInfoCall } from '../../home-tabs/controller.react';
import { NoParticipantAbgelaufenComponent } from './abgelaufen/component.react';
import { NoParticipantEingeschraenktComponent } from './eingeschraenkt/component.react';
import { NoParticipantOffenComponent } from './offen/component.react';

interface NoParticipantErrorPageProps {
  loadData: () => void;
}

export function NoParticipantErrorPageComponent(props: NoParticipantErrorPageProps): React.ReactElement {
  const routeMatcherVorbereitung = useRouteMatch<{ tabName: string; id: string; action: string }>(
    '/hra/:tabName/:id/:action/',
  );
  const abstimmungId = routeMatcherVorbereitung?.params.id as string;
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const [showRequestButton, setShowRequestButton] = useState(false);
  const [isEingeschraenkt, setIsEingeschraenkt] = useState<boolean>();
  const [errorCode, setErrorCode] = useState<number>();
  useEffect(() => {
    loadingStatusController.setLoadingStatus(true);
    const sub = getAbstimmungsInfoCall(abstimmungId).subscribe({
      next: (abstimmungInfo) => {
        setErrorCode(undefined);
        setIsEingeschraenkt(abstimmungInfo.eingeschraenkt);
        setShowRequestButton(true);
        loadingStatusController.setLoadingStatus(false);
      },
      error: (err: AjaxError) => {
        setErrorCode(err.response.status);
        setShowRequestButton(false);
        loadingStatusController.setLoadingStatus(false);
      },
    });
    return () => {
      sub?.unsubscribe();
    };
  }, []);
  if (errorCode === 1012) {
    return <NoParticipantAbgelaufenComponent />;
  }
  if (isEingeschraenkt === undefined) {
    return <></>;
  }
  return isEingeschraenkt ? (
    <NoParticipantEingeschraenktComponent abstimmungId={abstimmungId} showRequestButton={showRequestButton} />
  ) : (
    <NoParticipantOffenComponent
      abstimmungId={abstimmungId}
      loadData={props.loadData}
      showRequestButton={showRequestButton}
    />
  );
}
