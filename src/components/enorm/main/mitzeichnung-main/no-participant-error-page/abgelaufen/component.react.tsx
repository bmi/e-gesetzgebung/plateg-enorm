// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import { ErrorPage } from '@plateg/theme';

export function NoParticipantAbgelaufenComponent(): React.ReactElement {
  const { t } = useTranslation();
  return (
    <ErrorPage
      title={t(`enorm.reviewVoteAnswer.noParticipant.abgelaufen.title`)}
      text={t(`enorm.reviewVoteAnswer.noParticipant.abgelaufen.text`)}
      link={`/cockpit`}
      linkText={t(`enorm.reviewVoteAnswer.noParticipant.abgelaufen.linkText`)}
    />
  );
}
