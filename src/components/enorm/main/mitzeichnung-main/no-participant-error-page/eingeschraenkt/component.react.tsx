// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

import { NotificationPage } from '@plateg/theme/src/components/notifications/component.react';

import { routes } from '../../../../../../shares/routes';

interface EingeschraenktProps {
  abstimmungId: string;
  showRequestButton: boolean;
}

export function NoParticipantEingeschraenktComponent(props: EingeschraenktProps): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  return (
    <NotificationPage
      title={t(`enorm.reviewVoteAnswer.noParticipant.eingeschraenkt.title`)}
      text={t(`enorm.reviewVoteAnswer.noParticipant.eingeschraenkt.text`)}
      type="locked"
      additionalButton={
        props.showRequestButton ? (
          <Button
            id="hra-request-participation-btn"
            type="primary"
            onClick={() => {
              history.push(`/hra/${routes.ANFRAGEN}/${props.abstimmungId}/${routes.ANFRAGEN_ANLEGEN}`);
            }}
          >
            {t('enorm.reviewVoteAnswer.noParticipant.eingeschraenkt.buttonText')}
          </Button>
        ) : (
          <></>
        )
      }
    />
  );
}
