// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { AjaxError } from 'rxjs/ajax';

import { ErrorController, GlobalDI, LoadingStatusController } from '@plateg/theme';
import { NotificationPage } from '@plateg/theme/src/components/notifications/component.react';

import { addTeilnehmerCall } from '../../../home-tabs/controller.react';

interface OffenProps {
  abstimmungId: string;
  loadData: () => void;
  showRequestButton: boolean;
}

export function NoParticipantOffenComponent(props: OffenProps): React.ReactElement {
  const { t } = useTranslation();
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());

  const onClick = () => {
    loadingStatusController.setLoadingStatus(true);
    const sub = addTeilnehmerCall(props.abstimmungId).subscribe({
      next: () => {
        loadingStatusController.setLoadingStatus(false);
        props.loadData();
        sub?.unsubscribe();
      },
      error: (error: AjaxError) => {
        loadingStatusController.setLoadingStatus(false);
        errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
        sub?.unsubscribe();
      },
    });
  };

  return (
    <NotificationPage
      title={t(`enorm.reviewVoteAnswer.noParticipant.offen.title`)}
      text={t(`enorm.reviewVoteAnswer.noParticipant.offen.text`)}
      type="unlocked"
      additionalButton={
        props.showRequestButton ? (
          <Button id="hra-request-participation-btn" type="primary" onClick={onClick}>
            {t('enorm.reviewVoteAnswer.noParticipant.offen.buttonText')}
          </Button>
        ) : (
          <></>
        )
      }
    />
  );
}
