// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Collapse } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { AbstimmungEntityResponseDTO } from '@plateg/rest-api';
import { CombinedTitle } from '@plateg/theme';
import { DirectionDownOutlined } from '@plateg/theme/src/components/icons/DirectionDownOutlined';
import { DirectionRightOutlined } from '@plateg/theme/src/components/icons/DirectionRightOutlined';

import { getAbstimmungsbezeichner } from '../../../../../utils/controller';
import { AbstimmungDetailsComponent } from '../abstimmung-details/component.react';

interface HeadSectionReviewComponentProps {
  modify: boolean;
  abstimmung?: AbstimmungEntityResponseDTO;
}

export function HeadSectionReviewComponent(props: HeadSectionReviewComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const { Panel } = Collapse;

  const mainTitle = props.modify
    ? t('enorm.beantwortenVote.modify.maintTitleReview')
    : t('enorm.beantwortenVote.maintTitleReview');

  return (
    <>
      <CombinedTitle title={getAbstimmungsbezeichner(props.abstimmung?.dto)} suffix={mainTitle} />
      <Collapse
        expandIcon={({ isActive }) => {
          if (isActive) {
            return <DirectionDownOutlined />;
          } else {
            return <DirectionRightOutlined />;
          }
        }}
        ghost
      >
        <Panel header={t('enorm.beantwortenVote.modify.generalInfoTitle')} key="1">
          <AbstimmungDetailsComponent abstimmung={props.abstimmung} />
        </Panel>
      </Collapse>
    </>
  );
}
