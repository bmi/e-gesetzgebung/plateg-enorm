// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../beantworten-vote.less';

import { Form } from 'antd';
import { UploadFile } from 'antd/lib/upload/interface';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory, useRouteMatch } from 'react-router-dom';
import { AjaxError } from 'rxjs/ajax';

import {
  AbstimmungEntityResponseDTO,
  FileEntityListResponseDTO,
  TeilnehmerEntityModifyDTO,
  TeilnehmerFreigabeType,
  TeilnehmerStatusType,
  TeilnehmerWithFilesEntityResponseDTO,
} from '@plateg/rest-api';
import {
  BreadcrumbComponent,
  displayMessage,
  ErrorController,
  GlobalDI,
  HeaderController,
  LoadingStatusController,
  RouteLeavingGuard,
} from '@plateg/theme';
import { GeneralFormWrapper } from '@plateg/theme/src/components';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { ContinueLaterComponent } from '../../../../../shares/continue-later/component.react';
import { routes } from '../../../../../shares/routes';
import { getAbstimmungsbezeichner } from '../../../../../utils/controller';
import { Files, ManageFilesController } from '../../../../../utils/controllerManageFiles';
import { BeantwortenController, FetchVoteDataInterface } from '../controller';
import { HeadSectionComponent } from '../head-section/component.react';
import { NoParticipantErrorPageComponent } from '../no-participant-error-page/component.react';
import { ReviewSectionComponent } from '../review-section/component.react';
import { FormControlComponent } from '../review-section/form-control/component.react';

interface MitzeichnungAntwortProps {
  abstimmung?: AbstimmungEntityResponseDTO | null;
  setAbstimmung: (abstimmung: AbstimmungEntityResponseDTO | null) => void;
  fileList: UploadFile[];
  setFileList: (fileList: UploadFile[]) => void;
  additionalFileList: UploadFile[];
  setAdditionalFileList: (additionalFileList: UploadFile[]) => void;
  initalFileList: UploadFile[];
  setInitalFileList: (fileList: UploadFile[]) => void;
  initalAdditionalFileList: UploadFile[];
  setAdditionalInititalFileList: (additionalFileList: UploadFile[]) => void;
  formValues?: TeilnehmerEntityModifyDTO;
  setFormValues: (formValues: TeilnehmerEntityModifyDTO | undefined) => void;
}

export function MitzeichnungAntwort(props: MitzeichnungAntwortProps): React.ReactElement {
  const [form] = Form.useForm();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; tabName: string; action: string }>(
    '/hra/:tabName/:id/:action',
  );
  const antwortId = routeMatcherVorbereitung?.params.id as string;
  const tabName = routeMatcherVorbereitung?.params.tabName as string;
  const action = routeMatcherVorbereitung?.params.action as string;
  const [isLeavingPageBlocked, setIsLeavingPageBlocked] = useState(false);
  const [modify, setModify] = useState<boolean>();
  const ctrl = GlobalDI.getOrRegister('enormBeantwortenController', () => new BeantwortenController());
  const manageFilesController = GlobalDI.getOrRegister('enormManageFilesController', () => new ManageFilesController());
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const headerController = GlobalDI.get<HeaderController>('enormHeadercontroller');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const [readOnlyAccess, setReadOnlyAccess] = useState(true);
  const { t } = useTranslation();
  const history = useHistory();
  const appStore = useAppSelector((state) => state.user);

  const loadData = () => {
    if (appStore.user) {
      loadingStatusController.setLoadingStatus(true);
      fetchData(appStore.user.base.id);
    }
  };

  useEffect(() => {
    loadData();
  }, [appStore.user]);

  useEffect(() => {
    if (props.abstimmung) {
      setBreadcrumb(props.abstimmung);
    }
  }, [modify, props.abstimmung]);

  const fetchData = (userId: string) => {
    const abstimmungSubscription = ctrl.fetchVoteData(antwortId, userId).subscribe({
      next: ({ dataAbstimmung, teilnehmerData }: FetchVoteDataInterface) => {
        props.setAbstimmung(dataAbstimmung);
        initFormValues(teilnehmerData);
        loadingStatusController.setLoadingStatus(false);
      },
      error: (error: AjaxError) => {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
        if (error.response.status !== 1006) {
          errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
        }
        props.setAbstimmung(null);
        console.error('error fetching initial data', error);
        loadingStatusController.setLoadingStatus(false);
      },
    });

    return function cleanup() {
      abstimmungSubscription.unsubscribe();
    };
  };

  const setBreadcrumb = (vote: AbstimmungEntityResponseDTO) => {
    const tabLink = (
      <Link id="enorm-beantwortenVote-breadcrumbsTabNavigation-link" key={`enorm-${tabName}`} to={`/hra/${tabName}`}>
        {t(`enorm.breadcrumbs.projectName`)} - {t(`enorm.breadcrumbs.tabName.${tabName}`)}
      </Link>
    );

    const abstimmungLink = (
      <Link
        id="enorm-beantwortenVote-breadcrumbsTabNavigation-abstimmung-link"
        key={`enorm-${tabName}-abstimmung-link`}
        to={`/hra/${tabName}/${vote.base.id}/${routes.ABSTIMMUNG_BEANTWORTEN_PRUEFEN}`}
      >
        {vote?.dto.regelungsvorhaben ? getAbstimmungsbezeichner(vote.dto) : ''}
      </Link>
    );

    const breadcrumbTitle = (
      <span key={`vote-${action}`}>
        {t(`enorm.breadcrumbs.action.${modify ? 'abstimmungBeantwortenAendern' : action}`)}{' '}
      </span>
    );
    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[tabLink, abstimmungLink, breadcrumbTitle]} />],
    });
  };

  const saveCurrentBeantwortenState = () => {
    setFormValues();
    const fileProps = {
      fileList: props.fileList,
      initalFileList: props.initalFileList,
      additionalFileList: props.additionalFileList,
      initalAdditionalFileList: props.initalAdditionalFileList,
    };
    ctrl.saveBeantworten(
      form.getFieldsValue(true) as TeilnehmerEntityModifyDTO,
      fileProps,
      antwortId,
      t('enorm.beantwortenVote.successSaveAnswerMsg'),
      TeilnehmerFreigabeType.Privat,
      actionOnAnswer,
    );
  };

  const setFormValues = () => {
    props.setFormValues(form.getFieldsValue(true) as TeilnehmerEntityModifyDTO);
  };

  const getFiles = (filesDTO: FileEntityListResponseDTO): void => {
    const files = manageFilesController.prepareFilesList(filesDTO);
    setFilesState(files);
  };

  const setFilesState = (result: Files) => {
    props.setInitalFileList(result.files);
    props.setAdditionalInititalFileList(result.additionalFiles);
    props.setFileList(result.files.map((item) => ({ ...item })));
    props.setAdditionalFileList(result.additionalFiles.map((item) => ({ ...item })));
  };

  const actionOnAnswer = (successMsg: string, shouldGotoEnorm = false) => {
    displayMessage(successMsg, 'success');
    setIsLeavingPageBlocked(false);
    loadingStatusController.setLoadingStatus(false);
    if (shouldGotoEnorm) {
      history.push(`/hra/${routes.BITTE_UM_MITZEICHNUNG}`);
    } else {
      props.setFormValues(undefined);
      fetchData();
    }
  };

  const onFormValuesChanged = () => {
    setIsLeavingPageBlocked(true);
  };

  const prepateFormValues = (teilnehmerData: TeilnehmerWithFilesEntityResponseDTO) => {
    const formValues = teilnehmerData.dto;
    formValues.status = formValues.status === TeilnehmerStatusType.NichtBearbeitet ? '' : formValues.status;
    getFiles(teilnehmerData.dto.files);
    return formValues;
  };

  const initFormValues = (teilnehmerData: TeilnehmerWithFilesEntityResponseDTO) => {
    let formValues;
    if (props.formValues) {
      formValues = props.formValues;
    } else {
      formValues = prepateFormValues(teilnehmerData);
    }
    setReadOnlyAccess(teilnehmerData.dto.readOnlyAccess);
    setModify(formValues.freigabe !== TeilnehmerFreigabeType.Privat);
    form.setFieldsValue(formValues);
  };

  const shouldBlockNavigation = (pathname: string) => {
    return pathname.includes(`/${routes.ABSTIMMUNG_BEANTWORTEN}/${routes.ABSTIMMUNG_BEANTWORTEN_REVIEW}`)
      ? false
      : isLeavingPageBlocked;
  };

  const onFinish = () => {
    setIsLeavingPageBlocked(false);
    setFormValues();
    history.push({
      pathname: `/hra/${routes.BITTE_UM_MITZEICHNUNG}/${antwortId}/${routes.ABSTIMMUNG_BEANTWORTEN}/${routes.ABSTIMMUNG_BEANTWORTEN_REVIEW}`,
    });
  };

  if (!routeMatcherVorbereitung?.isExact && modify === undefined && readOnlyAccess) {
    displayMessage(t('enorm.generalErrorMsg'), 'error');
    console.error('error route params');
    loadingStatusController.setLoadingStatus(false);
    return <></>;
  }

  if (props.abstimmung === null) {
    return <NoParticipantErrorPageComponent loadData={loadData} />;
  }

  return (
    <div className="beantworten-vote-page">
      <RouteLeavingGuard
        navigate={(path: string) => history.push(path)}
        shouldBlockNavigation={(location) => shouldBlockNavigation(location.pathname)}
        title={t('enorm.beantwortenVote.confirmModal.title')}
        btnOk={t('enorm.beantwortenVote.confirmModal.btnCancelConfirmOk')}
        btnCancel={t('enorm.beantwortenVote.confirmModal.btnCancelConfirmNo')}
      />
      {props.abstimmung && (
        <>
          <HeadSectionComponent abstimmung={props.abstimmung} modify={modify} />
          <GeneralFormWrapper
            form={form}
            layout="vertical"
            onFinish={onFinish}
            onFinishFailed={() => document?.getElementById('errorBox')?.focus()}
            onValuesChange={onFormValuesChanged}
            scrollToFirstError={true}
          >
            <ReviewSectionComponent
              additionalFileList={props.additionalFileList}
              fileList={props.fileList}
              setAdditionalFileList={props.setAdditionalFileList}
              setFileList={props.setFileList}
              abstimmung={props.abstimmung}
              modify={modify}
              form={form}
            />
            <FormControlComponent modify={modify} />
            <ContinueLaterComponent
              isLeavingPageBlocked={isLeavingPageBlocked}
              saveCurrentState={saveCurrentBeantwortenState}
              modify={modify}
              translationKey={'beantwortenVote'}
            />
          </GeneralFormWrapper>
        </>
      )}
    </div>
  );
}
