// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { UploadFile } from 'antd/lib/upload/interface';
import React, { useState } from 'react';
import { Route, Switch } from 'react-router';

import { AbstimmungEntityResponseDTO, TeilnehmerEntityModifyDTO } from '@plateg/rest-api';

import { routes } from '../../../../shares/routes';
import { MitzeichnungAntwort } from './mitzeichnung-antwort/component.react';
import { MitzeichnungDetails } from './mitzeichnung-details/component.react';
import { ReviewMitzeichnungAntwort } from './mitzeichnung-details/review-mitzeichnung-antwort/component.react';

export function MitzeichnungRoutes(): React.ReactElement {
  const [fileList, setFileList] = useState<UploadFile[]>([]);
  const [additionalFileList, setAdditionalFileList] = useState<UploadFile[]>([]);
  const [initalFileList, setInititalFileList] = useState<UploadFile[]>([]);
  const [initalAdditionalFileList, setAdditionalInititalFileList] = useState<UploadFile[]>([]);
  const [formValues, setFormValues] = useState<TeilnehmerEntityModifyDTO | undefined>();
  const [abstimmung, setAbstimmung] = useState<AbstimmungEntityResponseDTO>();

  return (
    <Switch>
      <Route exact path={[`/hra/:tabName/:id/${routes.ABSTIMMUNG_BEANTWORTEN_PRUEFEN}`]}>
        <MitzeichnungDetails />
      </Route>
      <Route exact path={[`/hra/:tabName/:id/${routes.ABSTIMMUNG_BEANTWORTEN}`]}>
        <MitzeichnungAntwort
          fileList={fileList}
          setFileList={setFileList}
          additionalFileList={additionalFileList}
          setAdditionalFileList={setAdditionalFileList}
          initalFileList={initalFileList}
          setInitalFileList={setInititalFileList}
          initalAdditionalFileList={initalAdditionalFileList}
          setAdditionalInititalFileList={setAdditionalInititalFileList}
          abstimmung={abstimmung}
          setAbstimmung={setAbstimmung}
          formValues={formValues}
          setFormValues={setFormValues}
        />
      </Route>
      <Route
        exact
        path={[`/hra/:tabName/:id/${routes.ABSTIMMUNG_BEANTWORTEN}/${routes.ABSTIMMUNG_BEANTWORTEN_REVIEW}`]}
      >
        {formValues ? (
          <ReviewMitzeichnungAntwort
            fileList={fileList}
            initalFileList={initalFileList}
            additionalFileList={additionalFileList}
            initalAdditionalFileList={initalAdditionalFileList}
            abstimmung={abstimmung}
            formValues={formValues}
          />
        ) : (
          <></>
        )}
      </Route>
    </Switch>
  );
}
