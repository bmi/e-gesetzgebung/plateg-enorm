// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './sent-answer.less';

import { Typography } from 'antd';
import Text from 'antd/lib/typography/Text';
import React from 'react';
import { useTranslation } from 'react-i18next';

interface CommentsComponentProps {
  comment?: string;
  isNKR?: boolean;
}

export function CommentsComponent(props: CommentsComponentProps): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const comment = props.comment;

  return (
    <section className="comment-component">
      <Title level={3}>
        {t('enorm.reviewVoteAnswer.comments.title', {
          document: props.isNKR ? 'zur Stellungnahme' : 'zum Änderungsvorschlag',
        })}
      </Title>
      <dl>
        <dt>{t('enorm.reviewVoteAnswer.comments.yourComments')}</dt>
        {comment ? (
          <dd>
            <Text className="comment-val">{comment}</Text>
          </dd>
        ) : (
          <em>{t('enorm.vote.noAnswer')}</em>
        )}
      </dl>
    </section>
  );
}
