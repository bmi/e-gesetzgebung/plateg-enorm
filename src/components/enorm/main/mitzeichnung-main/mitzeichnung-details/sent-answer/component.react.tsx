// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './sent-answer.less';

import { Typography } from 'antd';
import Text from 'antd/lib/typography/Text';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { TeilnehmerFreigabeType, TeilnehmerStatusType } from '@plateg/rest-api';

interface SentAnswerComponentProps {
  status?: TeilnehmerStatusType;
  finished?: boolean;
  modify?: boolean;
  readOnly?: boolean;
  freigabe?: TeilnehmerFreigabeType;
}

export function SentAnswerComponent(props: SentAnswerComponentProps): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const finished = props.finished;
  const modify = props.modify;
  const freigabe = props.freigabe;
  const status: TeilnehmerStatusType | undefined =
    !modify && freigabe === TeilnehmerFreigabeType.Privat ? TeilnehmerStatusType.NichtBearbeitet : props.status;
  const readOnly = props.readOnly;
  const showStatus = status && !(status === TeilnehmerStatusType.NichtBearbeitet && finished) && !readOnly;

  const statusText = status ? (
    <Text className="status-val">{t(`enorm.myVotes.tabs.sign.table.status.${status}`)}</Text>
  ) : (
    <></>
  );

  const title = () => {
    if (readOnly) {
      return 'enorm.reviewVoteAnswer.sentAnswer.noParticipant';
    } else if (status && status === TeilnehmerStatusType.NichtBearbeitet && finished) {
      return 'enorm.reviewVoteAnswer.sentAnswer.noParticipation';
    } else if (!modify) {
      return 'enorm.reviewVoteAnswer.sentAnswer.title';
    }
    return 'enorm.reviewVoteAnswer.sentAnswer.titleModify';
  };

  return (
    <section className="sent-answer-component">
      <Title level={2}>{t(`${title()}`)}</Title>
      {showStatus && (
        <>
          <Title level={3}>{t('enorm.reviewVoteAnswer.sentAnswer.subTitle')}</Title>
          <dl>
            <dt>{t('enorm.reviewVoteAnswer.sentAnswer.choice')}</dt>
            <dd>{statusText}</dd>
          </dl>
        </>
      )}
    </section>
  );
}
