// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { EntwurfstypType, FileEntityListResponseDTO } from '@plateg/rest-api';

import { EnormDownloadLink } from '../../../../../../shares/download-link/component.react';
import { getFileLink } from '../../../../../../utils/controller';

interface UploadFilesComponentProps {
  files?: FileEntityListResponseDTO;
  isNKR?: boolean;
}

export function UploadedFilesComponent(props: UploadFilesComponentProps): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const files = props.files;

  const uploadedRegelungsentwurf = files?.dtos.find((file) => {
    return file.dto.entwurfStatusType === EntwurfstypType.Regelungsentwurf;
  });

  const uploadedNKRFiles = files?.dtos.filter((file) => {
    return file.dto.entwurfStatusType === EntwurfstypType.Stellungnahme;
  });

  const uploadedAttachments = files?.dtos.filter((file) => file.dto.entwurfStatusType === EntwurfstypType.Zusatzinfo);
  const noAnswer = <em>{t('enorm.vote.noAnswer')}</em>;

  return (
    <section className="uploaded-files-component">
      <Title level={3}>{t('enorm.reviewVoteAnswer.files.title')}</Title>
      <dl>
        <dt>{t(`enorm.reviewVoteAnswer.files.uploadChangeTitle${props.isNKR ? 'NKR' : ''}`)}</dt>
        {!props.isNKR && (
          <dd>
            {uploadedRegelungsentwurf ? (
              <EnormDownloadLink
                isfile
                name={uploadedRegelungsentwurf.dto.fileName}
                link={getFileLink(uploadedRegelungsentwurf.base.id)}
              />
            ) : (
              noAnswer
            )}
          </dd>
        )}
        {props.isNKR && (
          <dd>
            {uploadedNKRFiles && uploadedNKRFiles.length > 0
              ? uploadedNKRFiles.map((nkrFile, index) => {
                  return (
                    <EnormDownloadLink
                      isfile
                      key={`download-${nkrFile.dto.fileName}-${index.toString()}`}
                      name={nkrFile.dto.fileName}
                      link={getFileLink(nkrFile.base.id)}
                    />
                  );
                })
              : noAnswer}
          </dd>
        )}
        <dt>{t('enorm.reviewVoteAnswer.files.uploadAttachmentsTitle')}</dt>
        <dd>
          {uploadedAttachments && uploadedAttachments.length > 0
            ? uploadedAttachments.map((attachment, index) => {
                return (
                  <EnormDownloadLink
                    isfile
                    key={`download-${attachment.dto.fileName}-${index.toString()}`}
                    name={attachment.dto.fileName}
                    link={getFileLink(attachment.base.id)}
                  />
                );
              })
            : noAnswer}
        </dd>
      </dl>
    </section>
  );
}
