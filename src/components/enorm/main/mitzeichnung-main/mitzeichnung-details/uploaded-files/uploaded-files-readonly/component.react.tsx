// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './uploaded-files-readonly.less';

import { Typography } from 'antd';
import { UploadFile } from 'antd/lib/upload/interface';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { FileListComponent } from '../../../../vote-main/review-vote/file-list/component.react';

interface UploadedFilesReadonlyComponentProps {
  fileList?: UploadFile[];
  additionalFileList?: UploadFile[];
  isEnormFormat: boolean;
  isNKR?: boolean;
}

export function UploadedFilesReadonlyComponent(props: UploadedFilesReadonlyComponentProps): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const fileList = props.fileList;
  const additionalFileList = props.additionalFileList;
  const noAnswer = <em>{t('enorm.vote.noAnswer')}</em>;
  return (
    <section className="uploaded-files-readonly-component">
      <Title level={3}>{t('enorm.reviewVoteAnswer.files.title')}</Title>
      <dl>
        {props.isEnormFormat && !props.isNKR && (
          <>
            <dt>{t('enorm.reviewVoteAnswer.files.uploadChangeTitle')}</dt>
            <dd>{fileList && fileList.length > 0 ? <FileListComponent files={fileList} /> : noAnswer}</dd>
          </>
        )}
        {props.isNKR && (
          <>
            <dt>{t('enorm.reviewVoteAnswer.files.uploadChangeTitleNKR')}</dt>
            <dd>{fileList && fileList.length > 0 ? <FileListComponent files={fileList} /> : noAnswer}</dd>
          </>
        )}
        <dt>{t('enorm.reviewVoteAnswer.files.uploadAttachmentsTitle')}</dt>
        <dd>
          {additionalFileList && additionalFileList.length > 0 ? (
            <FileListComponent files={additionalFileList} />
          ) : (
            noAnswer
          )}
        </dd>
      </dl>
    </section>
  );
}
