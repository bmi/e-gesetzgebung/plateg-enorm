// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../review-antwort.less';

import { Button } from 'antd';
import { UploadFile } from 'antd/lib/upload/interface';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useRouteMatch } from 'react-router';
import { Link } from 'react-router-dom';

import {
  AbstimmungEntityResponseDTO,
  DocumentType,
  TeilnehmerEntityModifyDTO,
  TeilnehmerFreigabeType,
  TeilnehmerStatusType,
} from '@plateg/rest-api';
import {
  BreadcrumbComponent,
  displayMessage,
  GlobalDI,
  HeaderController,
  LoadingStatusController,
  RouteLeavingGuard,
} from '@plateg/theme';

import { routes } from '../../../../../../shares/routes';
import { getAbstimmungsbezeichner } from '../../../../../../utils/controller';
import { BeantwortenController } from '../../controller';
import { HeadSectionReviewComponent } from '../../head-section-review/component.react';
import { CommentsComponent } from '../comments/component.react';
import { SentAnswerComponent } from '../sent-answer/component.react';
import { UploadedFilesReadonlyComponent } from '../uploaded-files/uploaded-files-readonly/component.react';

interface ReviewMitzeichnungAntwortProps {
  abstimmung?: AbstimmungEntityResponseDTO;
  fileList: UploadFile[];
  initalFileList: UploadFile[];
  additionalFileList: UploadFile[];
  initalAdditionalFileList: UploadFile[];
  formValues?: TeilnehmerEntityModifyDTO;
}

export function ReviewMitzeichnungAntwort(props: ReviewMitzeichnungAntwortProps): React.ReactElement {
  const ctrl = GlobalDI.getOrRegister('enormBeantwortenController', () => new BeantwortenController());
  const { t } = useTranslation();
  const history = useHistory();
  const routeMatcher = useRouteMatch<{ id: string; tabName: string; action: string }>('/hra/:tabName/:id/:action');
  const antwortId = routeMatcher?.params.id as string;
  const tabName = routeMatcher?.params.tabName as string;
  const action = routeMatcher?.params.action as string;
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const headerController = GlobalDI.get<HeaderController>('enormHeadercontroller');
  const [isLeavingPageBlocked, setIsLeavingPageBlocked] = useState(true);
  const modify = props?.formValues?.freigabe === TeilnehmerFreigabeType.Oeffentlich;

  useEffect(() => {
    setBreadcrumb(props.abstimmung as AbstimmungEntityResponseDTO);
  }, []);

  const setBreadcrumb = (vote: AbstimmungEntityResponseDTO) => {
    const tabLink = (
      <Link
        id="enorm-reviewModifiedAntwort-breadcrumbsTabNavigation-link"
        key={`enorm-${tabName}`}
        to={`/hra/${tabName}`}
      >
        {t(`enorm.breadcrumbs.projectName`)} - {t(`enorm.breadcrumbs.tabName.${tabName}`)}
      </Link>
    );
    const abstimmungLink = (
      <Link
        id="enorm-beantwortenVote-breadcrumbsTabNavigation-abstimmung-link"
        key={`enorm-${tabName}-abstimmung-link`}
        to={`/hra/${tabName}/${vote.base.id}/${routes.ABSTIMMUNG_BEANTWORTEN_PRUEFEN}`}
      >
        {vote?.dto.regelungsvorhaben ? getAbstimmungsbezeichner(vote.dto) : ''}
      </Link>
    );
    const actionLink = (
      <Link
        id="enorm-beantwortenVote-breadcrumbsTabNavigation-action-link"
        key={`enorm-${tabName}-action-link`}
        to={`/hra/${tabName}/${vote.base.id}/${routes.ABSTIMMUNG_BEANTWORTEN}`}
      >
        {t(`enorm.beantwortenVote.mainTitle`)}
      </Link>
    );
    const title = modify ? 'enorm.beantwortenVote.modify.maintTitleReview' : 'enorm.beantwortenVote.maintTitleReview';
    const breadcrumbTitle = <span key={`vote-${action}`}>{t(title)}</span>;
    headerController.setHeaderProps({
      headerLeft: [
        <BreadcrumbComponent key="breadcrumb" items={[tabLink, abstimmungLink, actionLink, breadcrumbTitle]} />,
      ],
    });
  };

  const actionOnAnswer = (successMsg: string, shouldGotoEnorm = false) => {
    displayMessage(successMsg, 'success');
    loadingStatusController.setLoadingStatus(false);
    if (shouldGotoEnorm) {
      history.push(`/hra/bitteUmMitzeichnung`);
    }
  };

  const onFinish = () => {
    setIsLeavingPageBlocked(false);
    if (props.formValues) {
      const fileProps = {
        fileList: props.fileList,
        initalFileList: props.initalFileList,
        additionalFileList: props.additionalFileList,
        initalAdditionalFileList: props.initalAdditionalFileList,
      };
      ctrl.saveBeantworten(
        props.formValues,
        fileProps,
        antwortId,
        modify ? t('enorm.beantwortenVote.successAnswerModifyMsg') : t('enorm.beantwortenVote.successAnswerMsg'),
        TeilnehmerFreigabeType.Oeffentlich,
        actionOnAnswer,
      );
    }
  };

  return (
    <div className="review-antwort-vote-page beantworten-vote-page">
      <RouteLeavingGuard
        navigate={(path: string) => history.push(path)}
        shouldBlockNavigation={(location) => {
          if (location.pathname.includes(`/${routes.ABSTIMMUNG_BEANTWORTEN}`)) {
            return false;
          }
          return isLeavingPageBlocked;
        }}
        title={t('enorm.beantwortenVote.confirmModal.title')}
        btnOk={t('enorm.beantwortenVote.confirmModal.btnCancelConfirmOk')}
        btnCancel={t('enorm.beantwortenVote.confirmModal.btnCancelConfirmNo')}
      />
      <HeadSectionReviewComponent modify={modify} abstimmung={props?.abstimmung} />
      <SentAnswerComponent modify={modify} finished={true} status={props.formValues?.status} />
      <CommentsComponent
        comment={props.formValues?.kommentar}
        isNKR={
          props.formValues.status === TeilnehmerStatusType.KeineMitzeichnungStellungnahmeDesNationalenNormenkontrollrats
        }
      />
      <UploadedFilesReadonlyComponent
        fileList={props?.fileList}
        additionalFileList={props?.additionalFileList}
        isEnormFormat={props.abstimmung?.dto.documentTyp === DocumentType.Enorm}
        isNKR={
          props.formValues.status === TeilnehmerStatusType.KeineMitzeichnungStellungnahmeDesNationalenNormenkontrollrats
        }
      />
      <div className="btn-container">
        <Button id="enorm-reviewVoteAnswerSubmit-btn" type="primary" onClick={onFinish} size={'large'}>
          {modify
            ? t('enorm.reviewVoteAnswer.sentAnswer.submitModified')
            : t('enorm.reviewVoteAnswer.sentAnswer.submit')}
        </Button>
        <Button
          size={'large'}
          onClick={() => {
            setIsLeavingPageBlocked(false);
            history.push({
              pathname: `/hra/${routes.BITTE_UM_MITZEICHNUNG}/${antwortId}/${routes.ABSTIMMUNG_BEANTWORTEN}`,
            });
          }}
        >
          {t('enorm.reviewVoteAnswer.sentAnswer.btnCancelModified')}
        </Button>
      </div>
    </div>
  );
}
