// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';

import {
  AbstimmungEntityResponseDTO,
  TeilnehmerFreigabeType,
  TeilnehmerStatusType,
  TeilnehmerWithFilesEntityResponseDTO,
} from '@plateg/rest-api';
import { BtnBackComponent } from '@plateg/theme';

import { routes } from '../../../../../../shares/routes';
import { UnterabstimmungHintModal } from '../../../home-tabs/bitte-um-mitzeichnung-tab/unterabstimmung-hint-modal/component.react';
import { CommentsComponent } from '../comments/component.react';
import { SentAnswerComponent } from '../sent-answer/component.react';
import { UploadedFilesComponent } from '../uploaded-files/component.react';

interface ContentSectionComponentProps {
  abstimmung: AbstimmungEntityResponseDTO;
  teilnehmer: TeilnehmerWithFilesEntityResponseDTO;
  finished: boolean;
  archived: boolean;
}

export function ContentSectionComponent(props: ContentSectionComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  const abstimmung = props.abstimmung;
  const [beantwortenModalIsVisible, setBeantwortenModalIsVisible] = useState<boolean>(false);
  const teilnahmeAngefragtStatus =
    props.teilnehmer.dto.status === TeilnehmerStatusType.TeilnahmeAngefragt ||
    props.teilnehmer.dto.status === TeilnehmerStatusType.TeilnahmeZurueckgezogen ||
    props.teilnehmer.dto.status === TeilnehmerStatusType.TeilnahmeAbgelehnt;

  return (
    <>
      <UnterabstimmungHintModal
        abstimmungTitel={abstimmung.dto.titel}
        abstimmungVersion={abstimmung.dto.version}
        route={`/hra/${routes.BITTE_UM_MITZEICHNUNG}/${abstimmung.base.id}/${routes.ABSTIMMUNG_BEANTWORTEN}`}
        beantwortenModalIsVisible={beantwortenModalIsVisible}
        setBeantwortenModalIsVisible={setBeantwortenModalIsVisible}
      />
      {!teilnahmeAngefragtStatus && (
        <>
          <SentAnswerComponent
            readOnly={props.teilnehmer.dto.readOnlyAccess}
            finished={props.finished}
            status={props.teilnehmer?.dto.status}
            freigabe={props.teilnehmer.dto.freigabe}
          />
          {!props.teilnehmer.dto.readOnlyAccess && (
            <>
              {props.teilnehmer.dto.status !== TeilnehmerStatusType.NichtBearbeitet && (
                <>
                  <CommentsComponent
                    comment={props.teilnehmer?.dto.kommentar}
                    isNKR={
                      props.teilnehmer.dto.status ===
                      TeilnehmerStatusType.KeineMitzeichnungStellungnahmeDesNationalenNormenkontrollrats
                    }
                  />
                  <UploadedFilesComponent
                    files={props.teilnehmer?.dto.files}
                    isNKR={
                      props.teilnehmer.dto.status ===
                      TeilnehmerStatusType.KeineMitzeichnungStellungnahmeDesNationalenNormenkontrollrats
                    }
                  />
                </>
              )}
            </>
          )}
        </>
      )}

      <div className="btn-container">
        <BtnBackComponent
          styleBtn={{ marginTop: '6px' }}
          id={`hra-${props.archived ? 'archiv' : 'mitzeichnung'}-details-btn-back`}
          url={`/hra/${props.archived ? routes.ARCHIV : routes.BITTE_UM_MITZEICHNUNG}`}
        />
        {!props.finished && !props.teilnehmer.dto.readOnlyAccess && !teilnahmeAngefragtStatus && (
          <>
            <Button
              id="enorm-abstimmungBeantworten-btn"
              className="ant-btn-secondary"
              size={'large'}
              onClick={() => {
                if (abstimmung.dto.eingeleiteteUnterabstimmungenVorhanden) {
                  setBeantwortenModalIsVisible(true);
                } else {
                  history.push(
                    `/hra/${routes.BITTE_UM_MITZEICHNUNG}/${abstimmung.base.id}/${routes.ABSTIMMUNG_BEANTWORTEN}/`,
                  );
                }
              }}
            >
              {t(
                props.teilnehmer.dto.status !== TeilnehmerStatusType.NichtBearbeitet &&
                  props.teilnehmer?.dto.freigabe !== TeilnehmerFreigabeType.Privat
                  ? 'enorm.reviewVoteAnswer.changeAnswerBtn'
                  : 'enorm.reviewVoteAnswer.answerBtn',
              )}
            </Button>
          </>
        )}
      </div>
    </>
  );
}
