// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../beantworten-vote.less';
import './review-antwort.less';

import { Button, Typography } from 'antd';
import isPast from 'date-fns/isPast';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Redirect, useHistory, useRouteMatch } from 'react-router';
import { Link } from 'react-router-dom';
import { forkJoin, Subscription } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  AbstimmungEntityResponseDTO,
  BASE_PATH,
  FristtypType,
  TeilnehmerStatusType,
  TeilnehmerWithFilesEntityResponseDTO,
  UserEntityWithStellvertreterResponseDTO,
} from '@plateg/rest-api';
import {
  BreadcrumbComponent,
  DropdownMenu,
  ErrorController,
  GlobalDI,
  HeaderController,
  LoadingStatusController,
} from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';
import { openFile } from '@plateg/theme/src/controllers/RestConfigController';

import { RegelungsvorhabenDetailsDrawer } from '../../../../../shares/regelungsvorhaben-details-drawer/component.react';
import { routes } from '../../../../../shares/routes';
import { getAbstimmungsbezeichner } from '../../../../../utils/controller';
import { AbstimmungDetailsComponent } from '../abstimmung-details/component.react';
import { BeantwortenController } from '../controller';
import { NoParticipantErrorPageComponent } from '../no-participant-error-page/component.react';
import { ContentSectionComponent } from './content-section/component.react';

export function MitzeichnungDetails(): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const history = useHistory();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; tabName: string; action: string }>(
    `/hra/:tabName/:id/:action`,
  );
  const antwortId = routeMatcherVorbereitung?.params.id as string;
  const tabName = routeMatcherVorbereitung?.params.tabName as string;
  const action = routeMatcherVorbereitung?.params.action as string;

  const ctrl = GlobalDI.getOrRegister('enormBeantwortenController', () => new BeantwortenController());
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const headerController = GlobalDI.get<HeaderController>('enormHeadercontroller');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const [abstimmung, setAbstimmung] = useState<AbstimmungEntityResponseDTO | undefined>();
  const [finished, setFinished] = useState(false);
  const [teilnehmer, setTeilnehmer] = useState<TeilnehmerWithFilesEntityResponseDTO | null>();
  const [fristExceeded, setFristExceeded] = useState<boolean>(false);
  const [loggedUser, setLoggedUser] = useState<UserEntityWithStellvertreterResponseDTO>();
  const appStore = useAppSelector((state) => state.user);
  const [shouldRedirectTN, setShouldRedirectTN] = useState(false);

  const errorCallback = (error: AjaxError) => {
    loadingStatusController.setLoadingStatus(false);
    console.error(error, error);
    setTeilnehmer(null);
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    if (error.response.status !== 1006 && error.response.status !== 1016) {
      errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
    }
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    if (error.response.status === 1016) {
      history.push(
        `/hra/${routes.MEINE_ABSTIMMUNG}/${antwortId}/${routes.ABSTIMMUNGSRUNDE}/${routes.ABSTIMMUNGSRUNDE_TEILNAHMER}`,
      );
    }
  };

  const fetchData = (userId: string) => {
    loadingStatusController.setLoadingStatus(true);
    return forkJoin({
      dataAbstimmung: ctrl.getMeineAbstimmungCall(antwortId),
      dataTeilnehmer: ctrl.getTeilnehmerCall(antwortId, userId),
    }).subscribe({
      next: ({ dataAbstimmung, dataTeilnehmer }) => {
        const fExceeded = isPast(
          new Date(
            dataAbstimmung?.dto.fristen.find((frist) => {
              return frist.fristTyp === FristtypType.Abstimmungsende;
            })?.fristablauf || '',
          ),
        );
        setFristExceeded(fExceeded);
        setAbstimmung(dataAbstimmung);
        setFinished(dataAbstimmung.dto.oberabstimmung?.geschlossen || fExceeded);
        setTeilnehmer(dataTeilnehmer);
        checkShoudlRedirectTeilnehmer(dataTeilnehmer.dto.status);
        loadingStatusController.setLoadingStatus(false);
      },
      error: errorCallback,
    });
  };

  const checkShoudlRedirectTeilnehmer = (status: TeilnehmerStatusType) => {
    if (status === TeilnehmerStatusType.TeilnahmeAngefragt) {
      setShouldRedirectTN(true);
    }
    if (status === TeilnehmerStatusType.TeilnahmeAbgelehnt || status === TeilnehmerStatusType.TeilnahmeZurueckgezogen) {
      setTeilnehmer(null);
    }
  };

  useEffect(() => {
    if (appStore.user) {
      setLoggedUser(appStore.user);
    }
  }, [appStore.user]);
  useEffect(() => {
    if (abstimmung && abstimmung.dto.regelungsvorhaben && teilnehmer) {
      setBreadcrumb(
        abstimmung,
        abstimmung.dto.oberabstimmung?.geschlossen || fristExceeded,
        loggedUser?.dto.email || '',
      );
    }
  }, [abstimmung, fristExceeded, teilnehmer, setLoggedUser]);
  const loadData = () => {
    let sub: Subscription;
    if (routeMatcherVorbereitung?.isExact && appStore.user) {
      sub = fetchData(appStore.user.base.id);
    }
    return function cleanup() {
      sub?.unsubscribe();
    };
  };
  useEffect(() => {
    loadData();
  }, [antwortId, appStore.user]);

  const setBreadcrumb = (vote: AbstimmungEntityResponseDTO, fristExceeded: boolean, loggedInUserEmail: string) => {
    const tabLink = (
      <Link id="enorm-reviewAntwort-breadcrumbsTabNavigation-link" key={`enorm-${tabName}`} to={`/hra/${tabName}`}>
        {t(`enorm.breadcrumbs.projectName`)} - {t(`enorm.breadcrumbs.tabName.${tabName}`)}
      </Link>
    );

    const breadcrumbTitle = <span key={`vote-${action}`}>{getAbstimmungsbezeichner(abstimmung?.dto)}</span>;
    const rv = abstimmung?.dto.regelungsvorhaben;
    const rvInfoDrawer = (
      <RegelungsvorhabenDetailsDrawer
        key="rv-detail-drawer"
        regelungsvorhaben={rv}
        isCreator={rv?.dto.erstelltVon?.dto.email === loggedInUserEmail}
      />
    );
    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[tabLink, breadcrumbTitle]} />],

      headerRight: [
        <Button
          id="enorm-teilnehmerantwortexport-btn"
          key="export-button"
          type="primary"
          onClick={() =>
            openFile(`${BASE_PATH}/file/teilnehmerantwortexport/${vote.base.id}`, {
              fileName: `${abstimmung?.dto.titel ?? 'Abstimmung'}_${abstimmung?.dto.version ?? 0}.zip`,
              onError: (error) => {
                console.error(error);
              },
            })
          }
          disabled={!fristExceeded}
        >
          {t('enorm.reviewVoteAnswer.header.exportButton')}
        </Button>,
        rvInfoDrawer,
        <DropdownMenu
          key="reviewVoteAnswer_drobdown"
          items={[
            {
              element: t('enorm.reviewVoteAnswer.header.exportButton'),
              onClick: () => {
                openFile(`${BASE_PATH}/file/teilnehmerantwortexport/${vote.base.id}`, {
                  fileName: `${abstimmung?.dto.titel ?? 'Abstimmung'}_${abstimmung?.dto.version ?? 0}.zip`,
                  onError: (error) => {
                    console.error(error);
                  },
                });
              },
              disabled: () => !fristExceeded,
            },
            {
              element: rvInfoDrawer,
            },
          ]}
          elementId={'headerRightAlternative'}
          overlayClass={'headerRightAlternative-overlay'}
        />,
      ],
    });
  };

  if (shouldRedirectTN) {
    return <Redirect to={`/hra/${routes.ANFRAGEN}/${antwortId}/${routes.ANFRAGE_TEILNAHME_DETAILS}`} />;
  }
  if (teilnehmer === null) {
    return <NoParticipantErrorPageComponent loadData={loadData} />;
  }
  return (
    <div className="review-antwort-vote-page beantworten-vote-page">
      <Title level={1}>{getAbstimmungsbezeichner(abstimmung?.dto)}</Title>
      {abstimmung && teilnehmer && (
        <>
          <AbstimmungDetailsComponent abstimmung={abstimmung} />
          {abstimmung && (
            <ContentSectionComponent
              abstimmung={abstimmung}
              teilnehmer={teilnehmer}
              finished={finished}
              archived={routeMatcherVorbereitung?.params.tabName === 'archiv'}
            />
          )}
        </>
      )}
    </div>
  );
}
