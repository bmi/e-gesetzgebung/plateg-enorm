// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { forkJoin, of, Subscription } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  AbstimmungEntityResponseDTO,
  AbstimmungstypType,
  FileEntityListResponseDTO,
  UserEntityDTO,
  UserEntityResponseDTO,
  UserEntityWithStellvertreterResponseDTO,
} from '@plateg/rest-api';
import { ErrorController, GlobalDI, LoadingStatusController, UserFormattingController } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { FileDownloadProps } from '../../vote-details/vote-details-head/component.react';
import { BeantwortenController } from '../controller';
import { AbstimmungDetailsController, AbstimmungDetailsValues } from './controller';

interface AbstimmungDetailComponentProps {
  abstimmung?: AbstimmungEntityResponseDTO;
  loggedUser?: UserEntityWithStellvertreterResponseDTO;
}
export function AbstimmungDetailsComponent(props: AbstimmungDetailComponentProps): React.ReactElement {
  const abstimmung = props.abstimmung;
  const [showEinladungsText, setShowEinladungsText] = useState(false);
  const [showTeilnehmerList, setShowTeilnehmerList] = useState(false);
  const [fileProps, setFileProps] = useState<FileDownloadProps>();
  const [additionalFileProps, setAdditionalFileProps] = useState<FileDownloadProps[]>([]);
  const [ersteller, setErsteller] = useState<UserEntityDTO>();
  const [detailValues, setDetailValues] = useState<AbstimmungDetailsValues>();
  const [teilnehmer, setTeilnehmer] = useState('');
  const [loggedUser, setLoggedUser] = useState<UserEntityWithStellvertreterResponseDTO>();

  const appStore = useAppSelector((state) => state.user);
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const ctrl = GlobalDI.getOrRegister('enormBeantwortenController', () => new BeantwortenController());
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const abstimmungDetailsController = GlobalDI.getOrRegister(
    'enormAbstimmungDetailsController',
    () => new AbstimmungDetailsController(),
  );
  const userFormattingController = GlobalDI.getOrRegister<UserFormattingController>(
    'userFormattingController',
    () => new UserFormattingController(),
  );
  useEffect(() => {
    if (appStore.user) {
      setLoggedUser(appStore.user);
    }
  }, [appStore.user]);

  const { t } = useTranslation();

  useEffect(() => {
    let sub: Subscription;
    if (abstimmung?.base.id) {
      loadingStatusController.setLoadingStatus(true);
      sub = forkJoin({
        erstellerFiles: ctrl.getErstellerFiles(abstimmung?.base.id),
        erstellerCall: ctrl.getErstellerCall(abstimmung?.base.id),
        teilnehmer: abstimmung?.dto.ressortTeilnehmer.length
          ? userFormattingController.getUsersByEmailCall(abstimmung?.dto.ressortTeilnehmer)
          : of(''),
      }).subscribe({
        next: (data: {
          erstellerFiles: FileEntityListResponseDTO;
          erstellerCall: UserEntityResponseDTO;
          teilnehmer: string;
        }) => {
          ctrl.setFiles(data.erstellerFiles, setFileProps, setAdditionalFileProps);
          setErsteller(data.erstellerCall.dto);
          setTeilnehmer(data.teilnehmer);
          loadingStatusController.setLoadingStatus(false);
        },
        error: (error: AjaxError) => {
          errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
          loadingStatusController.setLoadingStatus(false);
        },
      });
    }
    return () => {
      sub?.unsubscribe();
    };
  }, [abstimmung]);

  useEffect(() => {
    setDetailValues(
      abstimmungDetailsController.getAbstimmungDetailsValues(abstimmung, fileProps, additionalFileProps, ersteller),
    );
  }, [fileProps, additionalFileProps, ersteller]);

  const showMore = () => {
    setShowEinladungsText(!showEinladungsText);
  };

  return (
    <section className="review-section">
      {detailValues?.title}
      <dl>
        <dt>{t('enorm.beantwortenVote.generalInfo.regelungsvorhaben')}</dt>
        {detailValues?.regelungsvorhaben}

        <dt>{t('enorm.beantwortenVote.generalInfo.abstimmungsschritt')}</dt>
        {detailValues?.abstimmungsschritt}

        <dt>{t('enorm.beantwortenVote.generalInfo.fristDerAbstimmungsrunde')}</dt>
        {detailValues?.fristVotingRound}

        <dt>{t('enorm.beantwortenVote.generalInfo.verschweigensfrist')}</dt>
        {detailValues?.verschweigensfrist}
      </dl>
      <dl
        className="dt-new-deadline"
        hidden={detailValues?.neueFrist === null || detailValues?.neueFrist === undefined}
      >
        <dt>{t('enorm.beantwortenVote.generalInfo.neueFristDerAbstimmungsrunde')}</dt>
        {detailValues?.neueFrist}
      </dl>
      <dl hidden={abstimmung?.dto.typ === AbstimmungstypType.AbstimmungDerVorlageFuerDenRegierungsentwurf}>
        <dt>{t('enorm.beantwortenVote.generalInfo.fristZurRuckmeldungInternet')}</dt>
        {detailValues?.fristInternetFeedback}
      </dl>
      <dl>
        <dt hidden={abstimmung?.dto.typ === AbstimmungstypType.AbstimmungDerVorlageFuerDenRegierungsentwurf}>
          {t('enorm.beantwortenVote.generalInfo.fristZurRuckmeldung')}
        </dt>
        {detailValues?.fristFeedback}
      </dl>
      <dl hidden={abstimmung?.dto.typ !== AbstimmungstypType.Ressortabstimmung}>
        <dt>{t('enorm.beantwortenVote.generalInfo.fruehkoordinierungBKAmt')}</dt>
        {detailValues?.fruehKoordinierungBkamt}
      </dl>
      <dl>
        <dt>{t('enorm.beantwortenVote.generalInfo.additionalDeadlines')}</dt>
        <dd>{detailValues?.additionalFristen}</dd>
      </dl>
      <dl>
        <dt>{t('enorm.beantwortenVote.generalInfo.einleiter')}</dt>
        {detailValues?.einleiter}

        <dt>{t('enorm.beantwortenVote.generalInfo.email')}</dt>
        {detailValues?.mailto}

        <dt>{t('enorm.beantwortenVote.generalInfo.phone')}</dt>
        {detailValues?.phone}
        <dt>
          {t('enorm.beantwortenVote.generalInfo.ressortTeilnehmer', {
            ressort: loggedUser?.dto.ressort?.kurzbezeichnung,
          })}
        </dt>
        <dd>
          {teilnehmer.length ? (
            <>
              <Button
                id="enorm-toggleTeilnehmerList-btn"
                className="btn-show-more"
                onClick={() => setShowTeilnehmerList(!showTeilnehmerList)}
              >
                {t(`enorm.beantwortenVote.generalInfo.${showTeilnehmerList ? 'hide' : 'show'}TeilnehmerList`)}
              </Button>

              <p hidden={!showTeilnehmerList} className="ant-typography p-no-style teilnehmer-text">
                {teilnehmer.replace(/; /g, '\n')}
              </p>
            </>
          ) : (
            t('enorm.vote.noAnswer')
          )}
        </dd>
        <dt>{t('enorm.beantwortenVote.generalInfo.draft')}</dt>
        {detailValues?.draft}

        <dt>{t('enorm.beantwortenVote.generalInfo.labelAdditionalDownload')}</dt>
        {detailValues?.additionalFiles}

        <dt>{t('enorm.beantwortenVote.generalInfo.einladungstext')}</dt>
        <dd className="email-text">
          <Button id="enorm-toggleEinladungText-btn" className="btn-show-more" onClick={showMore}>
            {showEinladungsText
              ? t('enorm.beantwortenVote.generalInfo.hideText')
              : t('enorm.beantwortenVote.generalInfo.showText')}
          </Button>

          <p hidden={!showEinladungsText} className="ant-typography p-no-style email-text">
            {abstimmung?.dto.einladungsMailText}
          </p>
        </dd>
      </dl>
      {detailValues?.unterabstimmung}
      <div
        className="review-item-bg-white review-anfrage-verlauf"
        hidden={detailValues?.verlauf === null || detailValues === undefined}
      >
        <Title level={3}>{t('enorm.beantwortenVote.generalInfo.verlauf.title')}</Title>
        {detailValues?.verlauf}
      </div>
    </section>
  );
}
