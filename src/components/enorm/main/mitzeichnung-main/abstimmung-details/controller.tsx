// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Title from 'antd/lib/typography/Title';
import { isBefore } from 'date-fns';
import i18n, { t } from 'i18next';
import React from 'react';

import {
  AbstimmungEntityResponseDTO,
  AnfrageEntityResponseDTO,
  AnfragetypType,
  DokumentDTO,
  FristtypType,
  RegelungsvorhabenEntityWithDetailsResponseDTO,
  UserEntityDTO,
} from '@plateg/rest-api';
import { ContactPerson, getDateTimeString, InfoComponent } from '@plateg/theme';

import { EnormDownloadLink } from '../../../../../shares/download-link/component.react';
import { routes } from '../../../../../shares/routes';
import {
  checkDeadlineExceededString,
  getDateString,
  getDeadline,
  getDeadlineExceededStringFromString,
  getFileLink,
  getGeschlossenString,
  getTyp,
} from '../../../../../utils/controller';
import { FileDownloadProps } from '../../vote-details/vote-details-head/component.react';

export interface AbstimmungDetailsValues {
  title: React.ReactElement | null;
  unterabstimmung: React.ReactElement | null;
  regelungsvorhaben: React.ReactElement | null;
  abstimmungsschritt: React.ReactElement | null;
  fristVotingRound: React.ReactElement | null;
  verschweigensfrist: React.ReactElement | null;
  neueFrist: React.ReactElement | null;
  fristInternetFeedback: React.ReactElement | null;
  fristFeedback: React.ReactElement | null;
  einleiter: React.ReactElement | null;
  mailto: React.ReactElement | null;
  phone: React.ReactElement | null;
  draft: React.ReactElement | null;
  additionalFiles: React.ReactElement | null;
  verlauf: React.ReactElement | null;
  fruehKoordinierungBkamt: React.ReactElement | null;
  additionalFristen: React.JSX.Element | null;
}
export class AbstimmungDetailsController {
  public getAbstimmungDetailsValues(
    abstimmung?: AbstimmungEntityResponseDTO,
    fileProps?: FileDownloadProps,
    additionalFileProps?: FileDownloadProps[],
    ersteller?: UserEntityDTO,
  ): AbstimmungDetailsValues {
    const noAnswer = <em>{String(i18n.t('enorm.vote.noAnswer'))}</em>;
    return {
      title: this.getTitleElement(abstimmung, noAnswer),
      unterabstimmung: this.getUnterabstimmungData(abstimmung),
      regelungsvorhaben: this.getRvElement(abstimmung?.dto.regelungsvorhaben, noAnswer),
      abstimmungsschritt: abstimmung?.dto.typ ? <dd>{getTyp(abstimmung?.dto.typ)}</dd> : noAnswer,
      fristVotingRound: this.getFristVotingRoundElement(abstimmung, noAnswer),
      verschweigensfrist: this.getVerschweigensfristElement(abstimmung),
      neueFrist: this.getNewFristVotingRoundElement(abstimmung),
      fristInternetFeedback: this.getFristInternetFeedbackElement(abstimmung, noAnswer),
      fristFeedback: this.getFirstFeedbackElement(abstimmung, noAnswer),
      einleiter: this.getEinleiterElement(ersteller, noAnswer, abstimmung?.dto.erstellerStellvertreter?.dto),
      mailto: this.getMailToElement(ersteller, noAnswer),
      phone: ersteller?.telefon ? <dd>{ersteller?.telefon}</dd> : <dd>{noAnswer}</dd>,
      draft: this.getDraftElement(fileProps, noAnswer, abstimmung),
      additionalFiles: this.getAdditionalFilesElement(additionalFileProps, noAnswer),
      verlauf: this.getVerlaufElement(abstimmung?.dto.anfragen),
      fruehKoordinierungBkamt: this.getFruehKooridinierungBkamt(abstimmung),
      additionalFristen: this.getAdditionalFristen(abstimmung, noAnswer),
    };
  }

  private getAdditionalFilesElement(
    additionalFileProps: FileDownloadProps[] | undefined,
    noAnswer: React.ReactElement,
  ): React.ReactElement | null {
    return additionalFileProps && additionalFileProps.length > 0 ? (
      <dd>
        <ul className="additional-file-list">
          {additionalFileProps.map((file) => {
            return (
              <li key={`${file.name}-${file.id}`}>
                <EnormDownloadLink link={getFileLink(file.id)} name={file.name} isfile />
              </li>
            );
          })}
        </ul>
      </dd>
    ) : (
      <dd>{noAnswer}</dd>
    );
  }

  private getDraftElement(
    fileProps: FileDownloadProps | undefined,
    noAnswer: React.ReactElement,
    abstimmung?: AbstimmungEntityResponseDTO,
  ): React.ReactElement | null {
    let draftElement: React.ReactElement;
    if (abstimmung?.dto.dokumentenmappe) {
      draftElement = (
        <dd>
          <span className="document-title">Stammgesetz: {abstimmung?.dto.dokumentenmappe.titel}</span>
          <table>
            {(abstimmung?.dto.dokumentenmappe.dokumente as DokumentDTO[]).map((document, index) => {
              return (
                <tbody key={index}>
                  <tr>
                    <td>{i18n.t(`enorm.votingRound.header.documentTypeName.${document.typ}`).toString()}:</td>
                    <td className="document-link">
                      <a id="enorm-regelungsentwurf-link" target="_blank" href={`#/editor/document/${document.id}`}>
                        {document.titel}
                      </a>
                    </td>
                  </tr>
                </tbody>
              );
            })}
          </table>
        </dd>
      );
    } else if (fileProps) {
      draftElement = <dd>{<EnormDownloadLink link={getFileLink(fileProps.id)} name={fileProps?.name} isfile />}</dd>;
    } else {
      draftElement = <dd>{noAnswer}</dd>;
    }
    return draftElement;
  }

  private getMailToElement(
    ersteller: UserEntityDTO | undefined,
    noAnswer: React.ReactElement,
  ): React.ReactElement | null {
    return ersteller?.email ? (
      <dd>
        <a id="enorm-mailToErsteller-link" href={`mailto:${ersteller?.email}`}>
          {ersteller?.email}
        </a>
      </dd>
    ) : (
      <dd>{noAnswer}</dd>
    );
  }

  private getEinleiterElement(
    ersteller: UserEntityDTO | undefined,
    noAnswer: React.ReactElement,
    stellvertreter: UserEntityDTO | undefined,
  ): React.ReactElement | null {
    const isStellvertretung = !!stellvertreter;
    const user = stellvertreter || ersteller;
    return user?.name ? (
      <strong>
        <dd>
          {isStellvertretung && (
            <span className="stellvertretung-label">
              {i18n.t('enorm.beantwortenVote.generalInfo.stellvertretungLabel').toString()}
            </span>
          )}
          <InfoComponent
            isContactPerson={true}
            title={i18n.t('enorm.beantwortenVote.generalInfo.contactPersonTitle')}
            buttonText={user?.name}
          >
            <ContactPerson user={user} />
          </InfoComponent>
        </dd>
      </strong>
    ) : (
      <dd>{noAnswer}</dd>
    );
  }

  private getFristInternetFeedbackElement(
    abstimmung: AbstimmungEntityResponseDTO | undefined,
    noAnswer: React.ReactElement,
  ): React.ReactElement | null {
    return getDeadline(FristtypType.VeroeffentlichungImInternet, abstimmung?.dto.fristen) ? (
      <dd>{getDeadline(FristtypType.VeroeffentlichungImInternet, abstimmung?.dto.fristen)} Uhr</dd>
    ) : (
      <dd>{noAnswer}</dd>
    );
  }

  private getFirstFeedbackElement(
    abstimmung: AbstimmungEntityResponseDTO | undefined,
    noAnswer: React.ReactElement,
  ): React.ReactElement | null {
    return getDeadline(FristtypType.ZuleitungAnLaenderUndVerbaende, abstimmung?.dto.fristen) ? (
      <dd>{getDeadline(FristtypType.ZuleitungAnLaenderUndVerbaende, abstimmung?.dto.fristen)} Uhr</dd>
    ) : (
      <dd>{noAnswer}</dd>
    );
  }

  private getFristVotingRoundElement(
    abstimmung: AbstimmungEntityResponseDTO | undefined,
    noAnswer: React.ReactElement,
  ): React.ReactElement | null {
    const deadline = getDeadline(FristtypType.Abstimmungsende, abstimmung?.dto.fristen, true);

    return deadline ? (
      <dd>
        <strong>{`${deadline} Uhr`}</strong>
        <span className="text-light">
          {getGeschlossenString(abstimmung?.dto.oberabstimmung) ?? checkDeadlineExceededString(abstimmung)}
        </span>
      </dd>
    ) : (
      <dd>{noAnswer}</dd>
    );
  }

  private getVerschweigensfristElement(abstimmung: AbstimmungEntityResponseDTO | undefined): React.ReactElement | null {
    return (
      <dd>
        {String(
          i18n.t(
            `enorm.votingRound.header.optionsVerschweigensfrist.${abstimmung?.dto.verschweigensfristGekennzeichnet}`,
          ),
        )}
      </dd>
    );
  }

  private getNewFristVotingRoundElement(
    abstimmung: AbstimmungEntityResponseDTO | undefined,
  ): React.ReactElement | null {
    const newFrist = abstimmung?.dto.fristen.find(
      (frist) => frist.fristTyp === FristtypType.Abstimmungsende && frist.deadlineChanged,
    )?.fristablauf;

    return newFrist ? (
      <dd className="dd-new-deadline">
        <strong>{getDateTimeString(newFrist)} Uhr</strong>
        <span className="text-light">
          {getGeschlossenString(abstimmung?.dto.oberabstimmung) ?? getDeadlineExceededStringFromString(newFrist)}
        </span>
      </dd>
    ) : null;
  }

  private getRvElement(
    regelungsvorhaben: RegelungsvorhabenEntityWithDetailsResponseDTO | undefined,
    noAnswer: React.ReactElement,
  ): React.ReactElement | null {
    return regelungsvorhaben ? (
      <dd>
        {regelungsvorhaben?.dto.kurzbezeichnung} ({regelungsvorhaben?.dto.abkuerzung})
      </dd>
    ) : (
      <dd>{noAnswer}</dd>
    );
  }

  private getTitleElement(
    abstimmung: AbstimmungEntityResponseDTO | undefined,
    noAnswer: React.ReactElement,
  ): React.ReactElement | null {
    return abstimmung?.dto.titel ? (
      <Title level={2}>
        {abstimmung?.dto.titel} #{abstimmung?.dto.version} -{' '}
        {i18n.t('enorm.beantwortenVote.generalInfo.title').toString()}
      </Title>
    ) : (
      <dd>{noAnswer}</dd>
    );
  }

  private getVerlaufElement(anfragen: AnfrageEntityResponseDTO[] | undefined): React.ReactElement | null {
    return anfragen?.length ? (
      <ul>
        {anfragen.map((anfrage: AnfrageEntityResponseDTO) => {
          const typ = anfrage.dto.typ === AnfragetypType.Abstimmungsteilnahme ? 'ABSTIMMUNGSTEILNAHME' : 'content';
          return (
            <li key={anfrage.base.id}>
              {i18n
                .t(`enorm.beantwortenVote.generalInfo.verlauf.${typ}.${anfrage.dto.status}`, {
                  date: anfrage.dto.geantwortetAm
                    ? getDateTimeString(anfrage.dto.geantwortetAm)
                    : getDateTimeString(anfrage.base.erstelltAm),
                })
                .toString()}
            </li>
          );
        })}
      </ul>
    ) : null;
  }

  private getUnterabstimmungData(abstimmung: AbstimmungEntityResponseDTO): React.ReactElement | null {
    if (!abstimmung.dto.meineUnterabstimmungen?.length) {
      return null;
    } else {
      const meineUnterabstimmungen = abstimmung.dto.meineUnterabstimmungen;
      return meineUnterabstimmungen ? (
        <div className="review-item-bg-white review-anfrage-verlauf">
          <Title level={3}>{i18n.t('enorm.beantwortenVote.generalInfo.verlauf.title').toString()}</Title>
          {meineUnterabstimmungen.map((vote) => {
            const result = isBefore(Date.parse(vote.fristablauf as string), new Date());
            const unterabstimmungKey = result ? 'unterabstimmungExpired' : 'unterabstimmungOngoing';
            return (
              <p
                key={vote.abstimmungId}
                dangerouslySetInnerHTML={{
                  __html: t(`enorm.beantwortenVote.generalInfo.verlauf.${unterabstimmungKey}`, {
                    date: getDateTimeString(vote.erstelltAm as string),
                    dateEnd: getDateTimeString(vote.fristablauf as string),
                    link: `#/hra/${routes.MEINE_ABSTIMMUNG}/${vote.abstimmungId}/${routes.ABSTIMMUNGSRUNDE}`,
                  }),
                }}
              ></p>
            );
          })}
        </div>
      ) : null;
    }
  }

  private getFruehKooridinierungBkamt(abstimmung: AbstimmungEntityResponseDTO): React.ReactElement | null {
    if (abstimmung.dto.fruehkoordinierungBkAmtErfolgtAm !== null && abstimmung?.dto?.fruehkoordinierungBkAmtErfolgtAm) {
      return (
        <dd>
          {t('enorm.votingRound.header.optionsBkAmt.erfolgt', {
            date: getDateString(abstimmung?.dto.fruehkoordinierungBkAmtErfolgtAm),
          }).toString()}
        </dd>
      );
    } else {
      return <dd>{t('enorm.votingRound.header.optionsBkAmt.nichtErfolgt').toString()}</dd>;
    }
  }

  private getAdditionalFristen(
    abstimmung: AbstimmungEntityResponseDTO,
    noAnswer: React.JSX.Element,
  ): React.JSX.Element | null {
    const additionalDeadlines = abstimmung.dto.fristen.filter(
      (frist) => frist.fristTyp === FristtypType.ZusaetzlicheFrist,
    );
    return additionalDeadlines.length > 0 ? (
      <ul>
        {additionalDeadlines.map((addDeadlineItem, index) => {
          return (
            <li className="additional-fristen-list" key={index}>
              {addDeadlineItem.titel} · {getDateTimeString(addDeadlineItem.fristablauf as string)} Uhr
            </li>
          );
        })}
      </ul>
    ) : (
      noAnswer
    );
  }
}
