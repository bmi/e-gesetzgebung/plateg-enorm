// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Radio } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import TextArea from 'antd/lib/input/TextArea';
import Title from 'antd/lib/typography/Title';
import { UploadFile } from 'antd/lib/upload/interface';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AjaxError } from 'rxjs/ajax';

import { AbstimmungEntityResponseDTO, DocumentType, DokumentenmappeDTO, TeilnehmerStatusType } from '@plateg/rest-api';
import {
  Constants,
  ErrorController,
  GlobalDI,
  HinweisComponent,
  InfoComponent,
  MainContentSelectWrapper,
} from '@plateg/theme';
import { SelectDown } from '@plateg/theme/src/components/icons/SelectDown';
import { Filters } from '@plateg/theme/src/shares/filters';

import { AdditionalUploadComponent } from '../../../../../shares/additionalUploadComponent/component.react';
import { standardMaximumSize, UploadComponent } from '../../../../../shares/uploadComponent/component.react';
import { BeantwortenController } from '../controller';

interface ReviewSectionComponentProps {
  modify: boolean;
  fileList: UploadFile[];
  setFileList: (fileList: UploadFile[]) => void;
  additionalFileList: UploadFile[];
  setAdditionalFileList: (fileList: UploadFile[]) => void;
  abstimmung?: AbstimmungEntityResponseDTO;
  form?: FormInstance;
}

export function ReviewSectionComponent(props: ReviewSectionComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const uploadMessagesConfig = {
    uploadErrorFileType: t('enorm.vote.uploadErrorFileType'),
    uploadErrorFileSize: t('enorm.vote.uploadErrorFileSize'),
    uploadError: t('enorm.vote.regelungsentwurfUpload.error'),
    uploadBtn: t('enorm.vote.uploadButton'),
    changeFileNameTitle: t('enorm.vote.uploadChangeFileNameModalTitel'),
    changeFileNameCancelText: t('enorm.vote.uploadChangeFileNameCancelTextButton'),
    changeFileNameItemLabel: t('enorm.vote.uploadChangeFileNameLabel'),
    changeFileNameErrorMsg: t('enorm.vote.uploadChangeFileNameError'),
  };
  const ctrl = GlobalDI.getOrRegister('enormBeantwortenController', () => new BeantwortenController());
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const [isNkr, setIsNkr] = useState(false);

  const [fileFormat, setFileFormat] = useState('.doc,.docx');
  const [localFilesList, setLocalFilesList] = useState<UploadFile<any>[]>(props.fileList);
  const [dokumentenmappen, setDokumentenmappen] = useState<DokumentenmappeDTO[]>([]);

  const commentBox = (
    <>
      <Title level={2}>{t(`enorm.beantwortenVote.commentBox.title${isNkr ? 'Stellungnahme' : ''}`)}</Title>
      <Form.Item
        name="kommentar"
        label={
          <>
            {t('enorm.beantwortenVote.commentBox.label')}{' '}
            <span className="hint">
              {t('enorm.beantwortenVote.commentBox.hint', {
                maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
              })}
            </span>
          </>
        }
        rules={[
          {
            max: Constants.TEXT_AREA_LENGTH,
            message: t('enorm.beantwortenVote.commentBox.error', {
              maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
            }),
          },
        ]}
      >
        <TextArea rows={8} />
      </Form.Item>
    </>
  );

  useEffect(() => {
    setFileFormat(isNkr ? '.pdf' : '.doc,.docx');
  }, [isNkr]);

  useEffect(() => {
    setIsNkr(
      props.form?.getFieldValue('status') ===
        TeilnehmerStatusType.KeineMitzeichnungStellungnahmeDesNationalenNormenkontrollrats,
    );
  }, [props.form?.getFieldValue('status')]);

  useEffect(() => {
    setLocalFilesList(props.fileList);
  }, [props.fileList]);
  useEffect(() => {
    if (props.abstimmung?.dto.documentTyp === DocumentType.Editor) {
      ctrl.getDokumentenmappen(props.abstimmung.base.id).subscribe({
        next: (data) => {
          setDokumentenmappen(data);
        },
        error: (error: AjaxError) => {
          console.log(error);
          errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
        },
      });
    }
  }, [props.abstimmung]);

  return (
    <>
      <div className="review-section">
        <Title level={2}>
          {t('enorm.beantwortenVote.mitzeichnung.title')}
          <InfoComponent title={t('enorm.beantwortenVote.mitzeichnung.mitzeichnungDrawerTitle')}>
            <p
              dangerouslySetInnerHTML={{
                __html: t('enorm.beantwortenVote.mitzeichnung.mitzeichnungDrawerText', {
                  interpolation: { escapeValue: false },
                }),
              }}
            ></p>
          </InfoComponent>
        </Title>
        {props.abstimmung?.dto.documentTyp === DocumentType.Editor && (
          <>
            {dokumentenmappen.length ? (
              <fieldset className="fieldset-form-items">
                <legend className="seo">{t('enorm.beantwortenVote.mitzeichnung.regelungsentwurf')}</legend>
                <Form.Item
                  className="no-colons"
                  name="dokumentenmappeId"
                  label={
                    <span>
                      {t('enorm.beantwortenVote.mitzeichnung.regelungsentwurf')}
                      <InfoComponent title={t('enorm.beantwortenVote.mitzeichnung.regelungsentwurfHinWeisTitle')}>
                        <p
                          dangerouslySetInnerHTML={{
                            __html: t('enorm.beantwortenVote.mitzeichnung.regelungsentwurfDrawerText'),
                          }}
                        ></p>
                      </InfoComponent>
                    </span>
                  }
                >
                  <MainContentSelectWrapper
                    placeholder={t('enorm.vote.regelungsvorhaben.placeholder')}
                    style={{ width: '100%' }}
                    suffixIcon={<SelectDown />}
                    options={dokumentenmappen.map((item, index) => ({
                      label: (
                        <span key={item.id + index.toString()} aria-label={item.titel}>
                          {item.titel}
                        </span>
                      ),
                      value: item.id,
                      title: item.titel,
                    }))}
                  />
                </Form.Item>
              </fieldset>
            ) : (
              <HinweisComponent
                title={t('enorm.beantwortenVote.mitzeichnung.regelungsentwurfHinWeisTitle')}
                mode="warning"
                content={<p>{t('enorm.beantwortenVote.mitzeichnung.regelungsentwurfHinWeisText')}</p>}
              />
            )}
          </>
        )}

        <fieldset className="fieldset-form-items">
          <legend className="seo">{t('enorm.beantwortenVote.mitzeichnung.labelMain')}</legend>
          <Form.Item
            name="status"
            label={<span>{t('enorm.beantwortenVote.mitzeichnung.labelMain')}</span>}
            rules={[{ required: true, whitespace: true, message: t('enorm.beantwortenVote.mitzeichnung.error') }]}
          >
            <Radio.Group
              className="horizontal-radios"
              name="status"
              onChange={(e) => {
                setLocalFilesList([]);
                props.setFileList([]);
                setIsNkr(
                  (e.target.value as TeilnehmerStatusType) ===
                    TeilnehmerStatusType.KeineMitzeichnungStellungnahmeDesNationalenNormenkontrollrats,
                );
              }}
            >
              <Radio id="enorm-beantwortenVoteMitzeichnen-radio" value={TeilnehmerStatusType.Mitzeichnung}>
                {t('enorm.beantwortenVote.mitzeichnung.mitzeichnenLabel')}
              </Radio>
              <Radio
                id="enorm-beantwortenVoteMitzeichnenUnterVorbehalt-radio"
                value={TeilnehmerStatusType.MitzeichnungUnterVorbehalt}
              >
                {t('enorm.beantwortenVote.mitzeichnung.mitzeichnenUnterVorbehaltLabel')}
              </Radio>
              <Radio id="enorm-beantwortenVoteKeineMitzeichnung-radio" value={TeilnehmerStatusType.KeineMitzeichnung}>
                {t('enorm.beantwortenVote.mitzeichnung.mitzeichnungAblehnenLabel')}
              </Radio>
              <Radio
                id="enorm-beantwortenVoteKeineMitzeichnungNKR-radio"
                value={TeilnehmerStatusType.KeineMitzeichnungStellungnahmeDesNationalenNormenkontrollrats}
              >
                {t('enorm.beantwortenVote.mitzeichnung.keineMitzeichnungStellungnahmeNKR')}
              </Radio>
            </Radio.Group>
          </Form.Item>
        </fieldset>

        {!isNkr && commentBox}

        <Title level={2}>
          {t('enorm.beantwortenVote.upload.mainTitle')}
          <InfoComponent title={t('enorm.vote.uploadFilesTitle')}>
            <p
              dangerouslySetInnerHTML={{
                __html: t('enorm.beantwortenVote.upload.uploadDrawerText', {
                  interpolation: { escapeValue: false },
                }),
              }}
            ></p>
          </InfoComponent>
        </Title>

        {props.abstimmung?.dto.documentTyp === DocumentType.Enorm && (
          <Form.Item shouldUpdate>
            {() => (
              <UploadComponent
                fileList={localFilesList}
                setFileList={props.setFileList}
                fileFormat={fileFormat}
                fileSize={standardMaximumSize}
                onlySingleFileAllowed={true}
                restrictRenaming={false}
                messagesConfig={{
                  title: <>{t(`enorm.beantwortenVote.uploadFileTitle${isNkr ? 'NKR' : ''}`)}</>,
                  ...uploadMessagesConfig,
                }}
                version={props.abstimmung?.dto.version}
                isBearbeiten={false}
                errorProps={{
                  errorTitle: t('enorm.beantwortenVote.uploadFileTitle'),
                  formatString: t('enorm.beantwortenVote.uploadFileFormatShort'),
                }}
              />
            )}
          </Form.Item>
        )}
        <AdditionalUploadComponent
          fileList={props.additionalFileList}
          setFileList={props.setAdditionalFileList}
          fileFormat=".doc,.docx,.pdf"
          fileSize={standardMaximumSize}
          singleFile={false}
          messagesConfig={{
            title: t('enorm.vote.uploadAttachmentsTitle'),
            ...uploadMessagesConfig,
          }}
        />

        {isNkr && commentBox}
      </div>
    </>
  );
}
