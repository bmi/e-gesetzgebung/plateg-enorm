// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import { AbstimmungEntityResponseDTO } from '@plateg/rest-api';
import { CombinedTitle, InfoComponent } from '@plateg/theme';

import { getAbstimmungsbezeichner } from '../../../../../utils/controller';
import { AbstimmungDetailsComponent } from '../abstimmung-details/component.react';

interface HeadSectionComponentProps {
  modify: boolean;
  abstimmung?: AbstimmungEntityResponseDTO;
}

export function HeadSectionComponent(props: HeadSectionComponentProps): React.ReactElement {
  const { t } = useTranslation();

  const mainTitle = props.modify ? t('enorm.beantwortenVote.modify.mainTitle') : t('enorm.beantwortenVote.mainTitle');

  return (
    <>
      <CombinedTitle
        title={getAbstimmungsbezeichner(props.abstimmung?.dto)}
        suffix={mainTitle}
        additionalContent={
          <InfoComponent title={t(`enorm.beantwortenVote.info.title`)}>
            <p>{t(`enorm.beantwortenVote.info.text`)}</p>
          </InfoComponent>
        }
      />
      <p className="ant-typography p-no-style">
        Pflichtfelder sind mit einem <sup>*</sup> gekennzeichnet.
      </p>
      <AbstimmungDetailsComponent abstimmung={props.abstimmung} />
    </>
  );
}
