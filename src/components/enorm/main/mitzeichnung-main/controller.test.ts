// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { of, throwError } from 'rxjs';
import sinon from 'sinon';

import {
  AbstimmungControllerApi,
  DokumentenmappeDTOStatusEnum,
  DokumentenmappeDTOTypEnum,
  EditorRemoteControllerApi,
  EntwurfstypType,
  TeilnehmerEntityModifyDTO,
  TeilnehmerFreigabeType,
  TeilnehmerStatusType,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { displayErrorMsgStub, loadingStatusSetStub } from '../../../../general.test';
import { ManageFilesController } from '../../../../utils/controllerManageFiles';
import { FileDownloadProps } from '../vote-details/vote-details-head/component.react';
import { BeantwortenController } from './controller';

describe('Test BeantwortenVoteController', () => {
  const beantwortenCtrl = new BeantwortenController();
  const abstimmungController = GlobalDI.get<AbstimmungControllerApi>('enormController');
  const manageFilesController = GlobalDI.getOrRegister('enormManageFilesController', () => new ManageFilesController());
  const editorRemoteControllerApi = GlobalDI.getOrRegister<EditorRemoteControllerApi>(
    'editorRemoteControllerApi',
    () => new EditorRemoteControllerApi(),
  );
  const modifyTeilnehmerStub = sinon.stub(abstimmungController, 'modifyTeilnehmer');
  describe('Test simple methods', () => {
    it('Test modifyTeilnehmerCall', (done) => {
      const teilnehmer = {
        status: TeilnehmerStatusType.KeineMitzeichnung,
        freigabe: TeilnehmerFreigabeType.Oeffentlich,
      };
      beantwortenCtrl.modifyTeilnehmerCall('123', teilnehmer);
      setTimeout(() => {
        sinon.assert.calledOnce(modifyTeilnehmerStub);
        sinon.assert.calledWith(modifyTeilnehmerStub, { id: '123', teilnehmerEntityModifyDTO: teilnehmer });
        modifyTeilnehmerStub.reset();
        done();
      }, 20);
    });
    it('Test getTeilnehmerCall', (done) => {
      const getTeilnehmerStub = sinon.stub(abstimmungController, 'getTeilnehmer1');
      beantwortenCtrl.getTeilnehmerCall('123', 'test');
      setTimeout(() => {
        sinon.assert.calledOnce(getTeilnehmerStub);
        sinon.assert.calledWith(getTeilnehmerStub, { id: '123', userId: 'test' });
        getTeilnehmerStub.restore();
        done();
      }, 20);
    });
  });
  describe('Test fetchVoteData', () => {
    it('Success case', (done) => {
      const getTeilnehmerStub = sinon.stub(beantwortenCtrl, 'getTeilnehmerCall');
      const getAbstimmungStub = sinon.stub(abstimmungController, 'getAbstimmung');
      const userId = 'userId-123';
      const antwortId = 'antwortId-123';

      beantwortenCtrl.fetchVoteData(antwortId, userId);
      setTimeout(() => {
        sinon.assert.calledOnce(getTeilnehmerStub);
        sinon.assert.calledOnce(getAbstimmungStub);
        getTeilnehmerStub.restore();
        getAbstimmungStub.restore();
        done();
      }, 200);
    });
  });
  describe('Test fetchDokumentenmappen', () => {
    it('Success case', (done) => {
      const editorRemoteControllerApiStub = sinon
        .stub(editorRemoteControllerApi, 'getDokumentenmappenFuerAbstimmung')
        .returns(
          of([
            {
              id: '1',
              titel: '1',
              version: '1',
              typ: DokumentenmappeDTOTypEnum.Stammgesetz,
              status: DokumentenmappeDTOStatusEnum.Draft,
              erstelltAm: '1',
              bearbeitetAm: '1',
              dokumente: [],
            },
          ]),
        );

      beantwortenCtrl.getDokumentenmappen('1').subscribe({
        next: (data) => {
          expect(data[0].id).to.eql('1');
          editorRemoteControllerApiStub.restore();
          done();
        },
      });
    });
  });
  describe('Test setFiles', () => {
    it('Success Case', () => {
      const setFilesStub = sinon.stub();
      const setAddFilesStub = sinon.stub();
      const inputVals = {
        dtos: [
          { base: { id: '3' }, dto: { entwurfStatusType: EntwurfstypType.Regelungsentwurf, fileName: 'Hello' } },
          { base: { id: '4' }, dto: { entwurfStatusType: EntwurfstypType.Regelungsentwurf, fileName: 'Miami' } },
          { base: { id: '42' }, dto: { entwurfStatusType: EntwurfstypType.Stellungnahme, fileName: 'NKR' } },
          { base: { id: '7' }, dto: { entwurfStatusType: EntwurfstypType.Zusatzinfo, fileName: 'World' } },
          { base: { id: '9' }, dto: { entwurfStatusType: EntwurfstypType.Zusatzinfo, fileName: 'Vice' } },
        ],
      };
      const expectedFiles = { id: '3', name: 'Hello' };
      const expectedAddFiles = [
        { id: '7', name: 'World' },
        { id: '9', name: 'Vice' },
      ];
      beantwortenCtrl.setFiles(inputVals, setFilesStub, setAddFilesStub);
      sinon.assert.calledOnce(setFilesStub);
      sinon.assert.calledWith(setFilesStub, expectedFiles);
      sinon.assert.calledOnce(setAddFilesStub);
      sinon.assert.calledWith(setAddFilesStub, expectedAddFiles);
    });
    it('Empty Case', () => {
      const setFilesStub = sinon.stub();
      const setAddFilesStub = sinon.stub();
      const inputVals = {
        dtos: [],
      };
      const expectedAddFiles = [] as FileDownloadProps[];
      beantwortenCtrl.setFiles(inputVals, setFilesStub, setAddFilesStub);
      sinon.assert.notCalled(setFilesStub);
      sinon.assert.calledOnce(setAddFilesStub);
      sinon.assert.calledWith(setAddFilesStub, expectedAddFiles);
    });
  });
  describe('Test saveBeantworten', () => {
    const prepareListStub = sinon.stub(manageFilesController, 'prepareListOfuploadFilesObservables').returns([]);
    beforeEach(() => {
      modifyTeilnehmerStub.reset();
      displayErrorMsgStub.resetHistory();
      loadingStatusSetStub.resetHistory();
    });
    after(() => {
      prepareListStub.restore();
      modifyTeilnehmerStub.reset();
    });
    it('Success Case', (done) => {
      prepareListStub.returns([]);
      modifyTeilnehmerStub.returns(of({ base: { id: '3' }, dto: {} }));
      const actionOnAnswerStub = sinon.stub();
      beantwortenCtrl.saveBeantworten(
        { status: TeilnehmerStatusType.KeineMitzeichnung } as TeilnehmerEntityModifyDTO,
        {},
        '123',
        'Hello World',
        TeilnehmerFreigabeType.Oeffentlich,
        actionOnAnswerStub,
      );
      setTimeout(() => {
        sinon.assert.calledOnce(prepareListStub);
        sinon.assert.calledOnce(actionOnAnswerStub);
        sinon.assert.calledOnce(modifyTeilnehmerStub);
        sinon.assert.calledOnce(loadingStatusSetStub);
        loadingStatusSetStub.resetHistory();
        prepareListStub.resetHistory();
        done();
      }, 20);
    });
    it('Error listofUploadedFileObs', (done) => {
      prepareListStub.returns([throwError({ status: 500 })]);
      modifyTeilnehmerStub.returns(of({ base: { id: '3' }, dto: {} }));
      const actionOnAnswerStub = sinon.stub();
      beantwortenCtrl.saveBeantworten(
        { status: TeilnehmerStatusType.KeineMitzeichnung } as TeilnehmerEntityModifyDTO,
        {},
        '123',
        'Hello World',
        TeilnehmerFreigabeType.Oeffentlich,
        actionOnAnswerStub,
      );
      setTimeout(() => {
        sinon.assert.calledOnce(prepareListStub);
        sinon.assert.notCalled(actionOnAnswerStub);
        sinon.assert.calledOnce(modifyTeilnehmerStub);
        sinon.assert.calledTwice(loadingStatusSetStub);
        sinon.assert.calledOnce(displayErrorMsgStub);
        loadingStatusSetStub.resetHistory();
        displayErrorMsgStub.resetHistory();
        done();
      }, 20);
    });
    it('Error modify Teilnehmer', (done) => {
      modifyTeilnehmerStub.returns(throwError({ status: 500 }));
      beantwortenCtrl.saveBeantworten({}, {}, '123', 'Hello World', TeilnehmerFreigabeType.Oeffentlich, () => {});
      setTimeout(() => {
        sinon.assert.calledOnce(modifyTeilnehmerStub);
        sinon.assert.calledOnce(displayErrorMsgStub);
        sinon.assert.calledTwice(loadingStatusSetStub);
        loadingStatusSetStub.resetHistory();
        displayErrorMsgStub.resetHistory();
        done();
      }, 20);
    });
  });
});
