// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { UploadFile } from 'antd/lib/upload/interface';
import { forkJoin, Observable } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  AbstimmungControllerApi,
  AbstimmungEntityResponseDTO,
  DokumentenmappeDTO,
  EditorRemoteControllerApi,
  EinstellungenControllerApi,
  EinstellungenDTO,
  EntwurfstypType,
  FileControllerApi,
  FileEntityListResponseDTO,
  TeilnehmerEntityModifyDTO,
  TeilnehmerEntityResponseDTO,
  TeilnehmerFreigabeType,
  TeilnehmerStatusType,
  TeilnehmerWithFilesEntityResponseDTO,
  UserEntityResponseDTO,
} from '@plateg/rest-api';
import { ErrorController, GlobalDI, LoadingStatusController } from '@plateg/theme';

import { ManageFilesController } from '../../../../utils/controllerManageFiles';
import { FileDownloadProps } from '../vote-details/vote-details-head/component.react';

export interface FetchVoteDataInterface {
  dataAbstimmung: AbstimmungEntityResponseDTO;
  teilnehmerData: TeilnehmerWithFilesEntityResponseDTO;
}

export class BeantwortenController {
  private readonly abstimmungController = GlobalDI.getOrRegister<AbstimmungControllerApi>(
    'enormController',
    () => new AbstimmungControllerApi(),
  );
  private readonly fileController = GlobalDI.getOrRegister<FileControllerApi>(
    'fileController',
    () => new FileControllerApi(),
  );
  private readonly manageFilesController = GlobalDI.getOrRegister(
    'enormManageFilesController',
    () => new ManageFilesController(),
  );

  private readonly einstellungenController = GlobalDI.getOrRegister<EinstellungenControllerApi>(
    'einstellungenController',
    () => new EinstellungenControllerApi(),
  );
  private readonly editorRemoteControllerApi = GlobalDI.getOrRegister<EditorRemoteControllerApi>(
    'editorRemoteControllerApi',
    () => new EditorRemoteControllerApi(),
  );
  private readonly loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  private readonly errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());

  public getMeineAbstimmungCall(id: string): Observable<AbstimmungEntityResponseDTO> {
    return this.abstimmungController.getAbstimmung({ id });
  }

  public getErstellerCall(id: string): Observable<UserEntityResponseDTO> {
    return this.abstimmungController.getErsteller({ id });
  }

  public getErstellerFiles(abstimmungId: string): Observable<FileEntityListResponseDTO> {
    return this.fileController.getErstellerFiles({ abstimmungId });
  }
  public getDokumentenmappen(abstimmungId: string): Observable<DokumentenmappeDTO[]> {
    return this.editorRemoteControllerApi.getDokumentenmappenFuerAbstimmung({ abstimmungId });
  }

  public modifyTeilnehmerCall(
    id: string,
    teilnehmer: TeilnehmerEntityModifyDTO,
  ): Observable<TeilnehmerEntityResponseDTO> {
    const body = {
      id,
      teilnehmerEntityModifyDTO: teilnehmer,
    };

    return this.abstimmungController.modifyTeilnehmer(body);
  }

  public getTeilnehmerCall(id: string, userId: string): Observable<TeilnehmerWithFilesEntityResponseDTO> {
    const body = {
      id,
      userId,
    };
    return this.abstimmungController.getTeilnehmer1(body);
  }

  public getEinstellungenCall(): Observable<EinstellungenDTO> {
    return this.einstellungenController.getEinstellungen();
  }

  public fetchVoteData(antwortId: string, userId: string): Observable<FetchVoteDataInterface> {
    return forkJoin({
      dataAbstimmung: this.getMeineAbstimmungCall(antwortId),
      teilnehmerData: this.getTeilnehmerCall(antwortId, userId),
    });
  }

  public saveBeantworten(
    formValues: TeilnehmerEntityModifyDTO,
    fileProps: {
      fileList: UploadFile[];
      initalFileList: UploadFile[];
      additionalFileList: UploadFile[];
      initalAdditionalFileList: UploadFile[];
    },
    antwortId: string,
    successMsg: string,
    freigabeType: TeilnehmerFreigabeType,
    actionOnAnswer: (successMsg: string, shouldGotoEnorm: boolean) => void,
  ): void {
    const isActionSubmit = freigabeType === TeilnehmerFreigabeType.Oeffentlich;
    formValues.freigabe = freigabeType;
    formValues.status = formValues.status || TeilnehmerStatusType.NichtBearbeitet;
    this.loadingStatusController.setLoadingStatus(true);
    this.modifyTeilnehmerCall(antwortId, formValues).subscribe({
      next: (data: TeilnehmerEntityResponseDTO) => {
        const entwurfstypType =
          formValues.status === TeilnehmerStatusType.KeineMitzeichnungStellungnahmeDesNationalenNormenkontrollrats
            ? EntwurfstypType.Stellungnahme
            : EntwurfstypType.Regelungsentwurf;
        const listOfuploadFilesObservables: Observable<any>[] =
          this.manageFilesController.prepareListOfuploadFilesObservables(
            antwortId,
            fileProps.fileList,
            fileProps.initalFileList,
            fileProps.additionalFileList,
            fileProps.initalAdditionalFileList,
            entwurfstypType as EntwurfstypType,
          );

        if (!listOfuploadFilesObservables.length) {
          actionOnAnswer(successMsg, isActionSubmit);
        } else {
          forkJoin(listOfuploadFilesObservables).subscribe({
            next: () => {
              actionOnAnswer(successMsg, isActionSubmit);
            },
            error: (error: AjaxError) => {
              this.loadingStatusController.setLoadingStatus(false);
              this.errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
              console.error('error vote upload Files', error);
            },
          });
        }
      },
      error: (error: AjaxError) => {
        this.loadingStatusController.setLoadingStatus(false);
        console.error('error modifyTeilnehmer', error);
        this.errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
      },
    });
  }

  public setFiles = (
    vals: FileEntityListResponseDTO,
    setFileProps: (props: FileDownloadProps) => void,
    setAdditionalFileProps: (props: FileDownloadProps[]) => void,
  ): void => {
    const additionalFiles: FileDownloadProps[] = [];
    const regFiles = vals.dtos.filter((dto) => {
      return dto.dto.entwurfStatusType !== EntwurfstypType.Zusatzinfo;
    });
    const addFiles = vals.dtos.filter((dto) => {
      return dto.dto.entwurfStatusType === EntwurfstypType.Zusatzinfo;
    });
    addFiles?.forEach((dto) => {
      additionalFiles.push({ id: dto.base.id, name: dto.dto.fileName });
    });
    if (regFiles.length) {
      setFileProps({ id: regFiles[0].base.id, name: regFiles[0].dto.fileName });
    }
    setAdditionalFileProps(additionalFiles);
  };
}
