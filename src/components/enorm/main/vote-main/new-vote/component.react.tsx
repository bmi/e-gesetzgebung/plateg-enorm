// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useRouteMatch } from 'react-router-dom';

import { AbstimmungEntityDTO, AbstimmungstypType, RegelungsvorhabenEntityResponseShortDTO } from '@plateg/rest-api';

import { routes } from '../../../../../shares/routes';
import { Files } from '../../../../../utils/controllerManageFiles';
import { VoteBase } from '../vote-base/component.react';

interface NewVoteComponentProps {
  setVote: (vote: AbstimmungEntityDTO) => void;
  setVoteFiles: (voteFiles: Files) => void;
  setVoteInitialFiles: (voteFiles: Files) => void;
  setVoteId: (voteId: string | null) => void;
  voteForEditing?: AbstimmungEntityDTO;
  voteFiles: Files;
  setSelectedRV: (rv: RegelungsvorhabenEntityResponseShortDTO) => void;
  setSendRVanPKP: (val: boolean) => void;
  sendRVanPKP: boolean;
}

function getAbstimmungType(key: string | undefined): AbstimmungstypType | undefined {
  if (key === routes.HAUSABSTIMMUNG) {
    return AbstimmungstypType.Hausabstimmung;
  }
  if (key === routes.RESSORTABSTIMMUNG) {
    return AbstimmungstypType.Ressortabstimmung;
  }
  if (key === routes.SCHLUSSABSTIMMUNG) {
    return AbstimmungstypType.Schlussabstimmung;
  }
  if (key === routes.ABSTIMMUNG_HAUSLEITUNG) {
    return AbstimmungstypType.AbstimmungDerVorlageFuerDenRegierungsentwurf;
  }
  if (key === routes.VORHABENCLEARING) {
    return AbstimmungstypType.Vorhabenclearing;
  }
  return undefined;
}

export function NewVoteComponent(props: NewVoteComponentProps): React.ReactElement {
  const routeMatcherVorbereitung = useRouteMatch<{ tabName: string; action: string; voteType: string; rvId: string }>(
    '/hra/:tabName/:action/:voteType/:rvId',
  );

  return (
    <VoteBase
      setSendRVanPKP={props.setSendRVanPKP}
      sendRVanPKP={props.sendRVanPKP}
      setSelectedRV={props.setSelectedRV}
      voteId={null}
      setVote={props.setVote}
      setVoteFiles={props.setVoteFiles}
      setVoteInitialFiles={props.setVoteInitialFiles}
      setVoteId={props.setVoteId}
      voteForEditing={props.voteForEditing}
      voteFiles={props.voteFiles}
      tabName={routeMatcherVorbereitung?.params?.tabName || routes.MEINE_ABSTIMMUNG}
      action={routeMatcherVorbereitung?.params?.action || routes.ABSTIMMUNG_ANLEGEN}
      rvId={routeMatcherVorbereitung?.params.rvId}
      abstimmungType={getAbstimmungType(routeMatcherVorbereitung?.params.voteType)}
    />
  );
}
