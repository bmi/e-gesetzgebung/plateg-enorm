// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useRouteMatch } from 'react-router-dom';

import { AbstimmungEntityDTO, RegelungsvorhabenEntityResponseShortDTO } from '@plateg/rest-api';

import { Files } from '../../../../../utils/controllerManageFiles';
import { VoteBase } from '../vote-base/component.react';

interface EditVoteComponentProps {
  setVote: (vote: AbstimmungEntityDTO) => void;
  setVoteFiles: (voteFiles: Files) => void;
  setVoteInitialFiles: (voteFiles: Files) => void;
  setVoteId: (voteId: string | null) => void;
  voteForEditing?: AbstimmungEntityDTO;
  voteFiles: Files;
  setSelectedRV: (rv: RegelungsvorhabenEntityResponseShortDTO) => void;
  setSendRVanPKP: (val: boolean) => void;
  sendRVanPKP: boolean;
}
export function EditVoteComponent(props: EditVoteComponentProps): React.ReactElement {
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; tabName: string; action: string }>(
    '/hra/:tabName/:id/:action',
  );
  const tabName = routeMatcherVorbereitung?.params?.tabName != null ? routeMatcherVorbereitung?.params?.tabName : '';
  const voteId = routeMatcherVorbereitung?.params?.id != null ? routeMatcherVorbereitung?.params?.id : null;
  const action = routeMatcherVorbereitung?.params?.action != null ? routeMatcherVorbereitung?.params?.action : '';
  return (
    <VoteBase
      setSendRVanPKP={props.setSendRVanPKP}
      sendRVanPKP={props.sendRVanPKP}
      setSelectedRV={props.setSelectedRV}
      setVote={props.setVote}
      setVoteFiles={props.setVoteFiles}
      setVoteInitialFiles={props.setVoteInitialFiles}
      setVoteId={props.setVoteId}
      voteForEditing={props.voteForEditing}
      voteFiles={props.voteFiles}
      voteId={voteId}
      tabName={tabName}
      action={action}
    />
  );
}
