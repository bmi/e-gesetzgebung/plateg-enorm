// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './file-list.less';

import { UploadFile } from 'antd/lib/upload/interface';
import { saveAs } from 'file-saver';
import React from 'react';

import { EnormDownloadLink } from '../../../../../../shares/download-link/component.react';
import { getFileLink } from '../../../../../../utils/controller';

interface FileListComponentProps {
  files: UploadFile<any>[];
}

export function FileListComponent(props: FileListComponentProps): React.ReactElement {
  const entries = props.files.map((file: UploadFile) => {
    const uploadedFileExists = file.originFileObj !== undefined;
    const onClick = () => {
      saveAs(file.originFileObj as Blob, file.name);
    };
    const target = uploadedFileExists ? { onClick: onClick } : { link: getFileLink(file.uid || ''), isfile: true };
    return (
      <li key={file.uid} className="file-name">
        <EnormDownloadLink name={file.name} {...target} />
      </li>
    );
  });

  return <ul className="review-file-list">{entries}</ul>;
}
