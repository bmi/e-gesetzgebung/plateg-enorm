// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import lightFormat from 'date-fns/lightFormat';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { FristEntityDTO } from '@plateg/rest-api';

interface FristenViewProps {
  fristen: FristEntityDTO[];
}
export function FristenView(props: FristenViewProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const deadlineDateLabel = t('enorm.reviewVote.deadlines.deadlineData');
  const deadlineTimeLabel = t('enorm.reviewVote.deadlines.deadlineTime');
  return (
    <div>
      {props.fristen &&
        props.fristen.map((item, index) => {
          const fristMoment = new Date(item.fristablauf || '');
          return (
            <div key={index}>
              <Title level={4}>{item.titel}</Title>
              <dl>
                <dt>{deadlineDateLabel}</dt>
                <dd>{lightFormat(fristMoment, 'dd.MM.yyyy')}</dd>
                <dt>{deadlineTimeLabel}</dt>
                <dd>{lightFormat(fristMoment, 'HH:mm')} Uhr</dd>
              </dl>
            </div>
          );
        })}
    </div>
  );
}
