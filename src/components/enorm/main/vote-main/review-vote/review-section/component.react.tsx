// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FC } from 'react';

import { AbstimmungEntityDTO } from '@plateg/rest-api';

interface Props {
  children: React.ReactElement;
  type: string;
  voteDraft: AbstimmungEntityDTO;
}

export const ReviewVoteSection: FC<Props> = ({ children, voteDraft, type }) => {
  const value = voteDraft[type as keyof AbstimmungEntityDTO];

  return <>{value && children}</>;
};
