// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './review-vote.less';

import { Button, Typography } from 'antd';
import { lightFormat } from 'date-fns';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory, useRouteMatch } from 'react-router-dom';
import { AjaxError } from 'rxjs/ajax';

import {
  AbstimmungEntityDTO,
  AbstimmungStatusType,
  AbstimmungstypType,
  DokumentenmappeDTO,
  PkpGesendetStatusType,
  RegelungsvorhabenEntityResponseShortDTO,
} from '@plateg/rest-api';
import {
  BreadcrumbComponent,
  CombinedTitle,
  displayMessage,
  ErrorController,
  GlobalDI,
  HeaderController,
  InfoComponent,
  LoadingStatusController,
} from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { getAbstimmungsbezeichner, getAbstimmungsrundeName } from '../../../../../utils/controller';
import { EnormHelpBreadcrumb } from '../../help/component.react';
import { Files } from '../vote-base/controller';
import { ReviewVoteController } from './controller';
import { FileListComponent } from './file-list/component.react';
import { FristenView } from './fristen-view/component.react';
import { ReviewVoteSection } from './review-section/component.react';

interface ReviewVoteComponentProps {
  vote: AbstimmungEntityDTO;
  voteFiles: Files;
  voteId: string | null;
  voteInitialFiles: Files;
  selectedRv?: RegelungsvorhabenEntityResponseShortDTO;
  sendRVanPKP: boolean;
  setSendRVanPKP: (val: boolean) => void;
}

export function ReviewVoteComponent(props: ReviewVoteComponentProps): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const history = useHistory();

  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const ctrl = GlobalDI.getOrRegister('enormReviewVoteController', () => new ReviewVoteController());
  const headerController = GlobalDI.get<HeaderController>('enormHeadercontroller');

  const [existingVoteId, setExistingVoteId] = useState<string | null>(props.voteId);
  const [regelungsentwurf, setRegelungsentwurf] = useState<DokumentenmappeDTO>();
  const [voteDraft, setVoteDraft] = useState<AbstimmungEntityDTO>({} as AbstimmungEntityDTO);
  const [isBearbeiten, setIsBearbeiten] = useState(false);
  const routeMatcherVorbereitung = useRouteMatch<{ tabName: string; action: string }>('/hra/:tabName/:action');
  const tabName = routeMatcherVorbereitung?.params?.tabName as string;
  const action = routeMatcherVorbereitung?.params?.action as string;

  const [teilnehmer, setTeilnehmer] = useState('');
  const [teilnehmerInCc, setTeilnehmerInCc] = useState('');

  useEffect(() => {
    loadingStatusController.setLoadingStatus(true);
    setExistingVoteId(props.voteId);
    setIsBearbeiten(props.vote.status === AbstimmungStatusType.InAbstimmung);

    const subFetchInitData = ctrl.fetchInitialData(props.vote).subscribe({
      next: (data) => {
        loadingStatusController.setLoadingStatus(false);
        setVoteDraft(props.vote);
        setRegelungsentwurf(data.regelungsentwurfDTO);
        setTeilnehmer(data.teilnehmerNames);
        setTeilnehmerInCc(data.teilnehmerInCCNames);
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
        loadingStatusController.setLoadingStatus(false);
      },
    });

    return () => {
      loadingStatusController.setLoadingStatus(false);
      subFetchInitData.unsubscribe();
    };
  }, []);

  useEffect(() => {
    setBreadcrumb(props.vote);
  }, [voteDraft]);

  const setBreadcrumb = (vote: AbstimmungEntityDTO) => {
    const tabLink = (
      <Link id="enorm-reviewVote-breadcrumbsTabNavigation-link" key={`enorm-${tabName}`} to={`/hra/${tabName}`}>
        {t(`enorm.breadcrumbs.projectName`)} - {t(`enorm.breadcrumbs.tabName.${tabName}`)}
      </Link>
    );

    const actionLinkUrl = ctrl.prepareEditUrl(existingVoteId, voteDraft, isBearbeiten, tabName, action);
    const actionLinkLabel = ctrl.prepareEditAction(existingVoteId, voteDraft, isBearbeiten, tabName, action);
    const actionLink = (
      <Link id="enorm-reviewVote-breadcrumbsTabNavigation-mainAction-link" key={`enorm-${tabName}`} to={actionLinkUrl}>
        {vote?.regelungsvorhaben?.dto.abkuerzung ? getAbstimmungsbezeichner(vote) + ': ' : undefined}
        {t(`enorm.breadcrumbs.action.${actionLinkLabel}`)}
      </Link>
    );

    const breadcrumbTitle = <span key={`vote-${action}`}>{t(`enorm.breadcrumbs.action.${action}`)}</span>;
    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[tabLink, actionLink, breadcrumbTitle]} />],
      headerLast: [<EnormHelpBreadcrumb key="enormhelp-breadcrumb" />],
      headerCenter: [],
      headerRight: [],
    });
  };

  const editVote = () => {
    const url = ctrl.prepareEditUrl(existingVoteId, voteDraft, isBearbeiten, tabName, action);
    history.push(url);
  };

  const errorHandler = (error: AjaxError) => {
    if ((error.response as { status: number })?.status === 1008) {
      displayMessage(t('enorm.vote.errorRvInactive'), 'error');
    } else {
      errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
    }
    console.error('Unable to create vote', error);
    loadingStatusController.setLoadingStatus(false);
    const url = ctrl.prepareErrorUrl(existingVoteId, voteDraft, tabName, action);
    history.push(url);
  };
  const sendVote = (status: AbstimmungStatusType | undefined, erfolgRoute = routes.ERFOLG) => {
    ctrl.saveVote(voteDraft, existingVoteId, props.voteFiles, props.voteInitialFiles, status).subscribe({
      next: (id) => {
        setExistingVoteId(id);
        loadingStatusController.setLoadingStatus(false);
        const isUnterabstimmung = voteDraft.typ === AbstimmungstypType.Unterabstimmung;
        if (isUnterabstimmung) {
          isBearbeiten
            ? history.push(`/hra/${id}/${routes.ABSTIMMUNG_PRUEFEN}/${routes.ERFOLG_UA_UPD}`)
            : history.push(`/hra/${id}/${routes.ABSTIMMUNG_PRUEFEN}/${routes.ERFOLG_UA}`);
        } else {
          isBearbeiten
            ? history.push(`/hra/${id}/${routes.ABSTIMMUNG_PRUEFEN}/${routes.ERFOLGUPD}`)
            : history.push(`/hra/${id}/${routes.ABSTIMMUNG_PRUEFEN}/${erfolgRoute}`);
        }
      },
      error: (error: AjaxError) => errorHandler(error),
      complete: () => {
        props.setSendRVanPKP(false);
      },
    });
  };
  const sendToPKP = (fehlerRoute: string, saveAs: AbstimmungStatusType | undefined, rvId: string) => {
    ctrl.sendRvToPkp(rvId).subscribe({
      next: (result: PkpGesendetStatusType) => {
        if (result !== PkpGesendetStatusType.Erstellt && result !== PkpGesendetStatusType.Aktualisiert) {
          sendVote(saveAs, fehlerRoute);
        } else {
          sendVote(undefined, routes.ERFOLGABSTIMMUNGPKP);
        }
      },
      error: (error: AjaxError) => {
        sendVote(saveAs, fehlerRoute);
      },
    });
  };
  const saveVotePKP = () => {
    loadingStatusController.setLoadingStatus(true);
    if (voteDraft.status === AbstimmungStatusType.Entwurf && voteDraft.regelungsvorhabenId) {
      if (voteDraft.typ === AbstimmungstypType.Ressortabstimmung) {
        sendToPKP(routes.FEHLERPKP, AbstimmungStatusType.Entwurf, voteDraft.regelungsvorhabenId);
        return;
      }
      if (voteDraft.typ === AbstimmungstypType.Hausabstimmung) {
        if (props.sendRVanPKP) {
          sendToPKP(routes.ERFOLGNURABSTIMMUNG, undefined, voteDraft.regelungsvorhabenId);
          return;
        } else {
          sendVote(undefined);
          return;
        }
      }
      sendVote(undefined);
    } else {
      sendVote(undefined);
    }
  };
  return (
    <div className="review-vote-page">
      <div className="heading-holder">
        <CombinedTitle
          title={getAbstimmungsrundeName(props.vote.version || 1, props.vote.titel)}
          suffix={t('enorm.reviewVote.mainTitle')}
        />
        &nbsp;
        <InfoComponent title={t(`enorm.reviewVote.mainTitle`)}>
          <p>{t('enorm.reviewVote.mainTitleInfo')}</p>
        </InfoComponent>
      </div>
      <div className="review-section">
        <Title level={2}>{t('enorm.reviewVote.generalInfo.title')}</Title>
        <dl>
          <ReviewVoteSection type="typ" voteDraft={voteDraft}>
            <>
              <dt>{t('enorm.reviewVote.generalInfo.type')}</dt>
              <dd>{t(`enorm.reviewVote.generalInfo.type${voteDraft.typ as AbstimmungstypType}`)}</dd>
            </>
          </ReviewVoteSection>

          {voteDraft.typ === AbstimmungstypType.Ressortabstimmung && (
            <>
              <dt>{t('enorm.reviewVote.generalInfo.earlyVoteBundeskanzleramt')}</dt>
              <dd>
                {voteDraft.fruehkoordinierungBkAmtErfolgtAm
                  ? t('enorm.reviewVote.generalInfo.optionsBkAmt.erfolgt', {
                      date: lightFormat(new Date(voteDraft.fruehkoordinierungBkAmtErfolgtAm), 'dd.MM.yyyy'),
                    })
                  : t('enorm.reviewVote.generalInfo.optionsBkAmt.nichtErfolgt')}
              </dd>
            </>
          )}

          {props.selectedRv?.dto && (
            <>
              <dt>{t('enorm.reviewVote.generalInfo.regulationPlan')}</dt>
              <dd>{`${props.selectedRv?.dto.abkuerzung} - ${props.selectedRv?.dto.kurzbezeichnung}`}</dd>
            </>
          )}
          <ReviewVoteSection type="titel" voteDraft={voteDraft}>
            <>
              <dt>{t('enorm.reviewVote.generalInfo.voteTitle')}</dt>
              <dd> {getAbstimmungsrundeName(voteDraft.version, voteDraft.titel)}</dd>
            </>
          </ReviewVoteSection>
          {(voteDraft.typ === AbstimmungstypType.Hausabstimmung ||
            voteDraft.typ === AbstimmungstypType.Ressortabstimmung) &&
            voteDraft.status === AbstimmungStatusType.Entwurf && (
              <>
                <dt>
                  {t('enorm.reviewVote.generalInfo.anPKPSenden')}
                  <InfoComponent title={t('enorm.reviewVote.generalInfo.anPKPSendenDrawerTitle')}>
                    <p>{t('enorm.reviewVote.generalInfo.anPKPSendenDrawerText')}</p>
                  </InfoComponent>
                </dt>
                {voteDraft.typ === AbstimmungstypType.Hausabstimmung && (
                  <dd>
                    {t(
                      `enorm.reviewVote.generalInfo.anPKPSenden${voteDraft.typ || ''}_${
                        props.sendRVanPKP ? 'yes' : 'no'
                      }`,
                    )}
                  </dd>
                )}
                {voteDraft.typ === AbstimmungstypType.Ressortabstimmung && (
                  <dd>{t(`enorm.reviewVote.generalInfo.anPKPSendenRESSORTABSTIMMUNG_yes`)}</dd>
                )}
              </>
            )}
        </dl>
      </div>
      <div className="review-section">
        <Title level={2}>{t('enorm.reviewVote.uploadedFiles.title')}</Title>
        <dl>
          <>
            <dt>{t('enorm.reviewVote.documentType.label')}</dt>
            <dd>{t(`enorm.reviewVote.documentType.${String(voteDraft.documentTyp)}`)}</dd>
          </>
          {voteDraft.documentTyp === DocumentType.Editor ? (
            <>
              <dt>{t('enorm.reviewVote.uploadedFiles.selectedRE')}</dt>
              <dd>{regelungsentwurf?.titel}</dd>
            </>
          ) : (
            <>
              <dt>{t('enorm.reviewVote.uploadedFiles.uploadedDraft')}</dt>
              <dd>
                <FileListComponent files={props.voteFiles?.files} />
              </dd>
            </>
          )}
          {props.voteFiles?.additionalFiles.length ? (
            <>
              <dt>{t('enorm.reviewVote.uploadedFiles.uploadedAttachments')}</dt>
              <dd>
                <FileListComponent files={props.voteFiles?.additionalFiles} />
              </dd>
            </>
          ) : null}
        </dl>
      </div>
      {props.vote.typ !== AbstimmungstypType.AbstimmungDerVorlageFuerDenRegierungsentwurf && (
        <div className="review-section">
          <Title level={2}>{t('enorm.vote.internetIntranetTitle')}</Title>
          <dl>
            <dt>{t('enorm.vote.bkAmtUmEinvernehmenBittenText')}:</dt>
            <dd>
              {t(`enorm.vote.${voteDraft.bkAmtUmEinvernehmenBitten ? 'internetIntranetJa' : 'internetIntranetNein'}`)}
            </dd>
            <dt>{t('enorm.vote.teilnehmendeUmBenehmenBittenText')}:</dt>
            <dd>
              {t(
                `enorm.vote.${voteDraft.teilnehmendeUmBenehmenBitten ? 'internetIntranetJa' : 'internetIntranetNein'}`,
              )}
            </dd>

            <dt>{t('enorm.vote.veroffentlichungInternetText')}</dt>
            <dd>
              {t(`enorm.vote.${voteDraft.veroeffentlichungImInternet ? 'internetIntranetJa' : 'internetIntranetNein'}`)}
            </dd>
            <dt>{t('enorm.vote.veroffentlichungIntranetText')}</dt>
            <dd>
              {t(`enorm.vote.${voteDraft.veroeffentlichungImIntranet ? 'internetIntranetJa' : 'internetIntranetNein'}`)}
            </dd>
          </dl>
        </div>
      )}
      <div className="review-section">
        <Title level={2}>{t('enorm.reviewVote.deadlines.title')}</Title>
        <FristenView fristen={voteDraft?.fristen} />
      </div>
      <div className="review-section">
        <Title level={2}>{t('enorm.reviewVote.invitationEmail.title')}</Title>
        <dl>
          <ReviewVoteSection type="teilnehmer" voteDraft={voteDraft}>
            <>
              <dt>{t('enorm.reviewVote.invitationEmail.emails')}</dt>
              <dd>{teilnehmer}</dd>
            </>
          </ReviewVoteSection>

          {voteDraft.teilnehmerInCc && voteDraft.teilnehmerInCc.length > 0 ? (
            <>
              <dt>{t('enorm.reviewVote.invitationEmail.cc')}</dt>
              <dd>{teilnehmerInCc}</dd>
            </>
          ) : null}

          <ReviewVoteSection type="einladungsMailBetreff" voteDraft={voteDraft}>
            <>
              <dt>{t('enorm.reviewVote.invitationEmail.subject')}</dt>
              <dd>{voteDraft.einladungsMailBetreff}</dd>
            </>
          </ReviewVoteSection>
          <ReviewVoteSection type="einladungsMailText" voteDraft={voteDraft}>
            <>
              <dt>{t('enorm.reviewVote.invitationEmail.emailText')}</dt>
              <dd style={{ whiteSpace: 'pre-line' }}>{voteDraft.einladungsMailText}</dd>
            </>
          </ReviewVoteSection>
        </dl>
      </div>
      <div className="form-control-buttons">
        <Button id="enorm-EinleitenUndAktualisierung-btn" type="primary" onClick={saveVotePKP} size="large">
          {t(`enorm.reviewVote.${isBearbeiten ? 'btnAktualisierung' : 'btnStart'}`)}
        </Button>
        <Button id="enorm-angabeBearbeiten-btn" size="large" onClick={editVote}>
          {t('enorm.reviewVote.btnEdit')}
        </Button>
      </div>
    </div>
  );
}
