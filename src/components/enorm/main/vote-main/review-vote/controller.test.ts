// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai, { expect } from 'chai';
import { Observable, of } from 'rxjs';
import sinon from 'sinon';

import {
  AbstimmungEntityDTO,
  AbstimmungEntityResponseDTO,
  PkpGesendetStatusType,
  RegelungsvorhabenControllerApi,
} from '@plateg/rest-api';
import { GlobalDI, UserFormattingController } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { ManageFilesController } from '../../../../../utils/controllerManageFiles';
import { Files, VoteBaseController } from '../vote-base/controller';
import { ReviewVoteController } from './controller';

describe('Test ReviewVoteController', () => {
  const ctrl = new ReviewVoteController();
  const voteCtrl = GlobalDI.getOrRegister('enormVoteController', () => new VoteBaseController());
  const manageFilesCtrl = GlobalDI.getOrRegister('enormManageFilesController', () => new ManageFilesController());
  const userFormattingController = GlobalDI.getOrRegister<UserFormattingController>(
    'userFormattingController',
    () => new UserFormattingController(),
  );
  const regelungsvorhabenController = GlobalDI.getOrRegister<RegelungsvorhabenControllerApi>(
    'regelungsvorhabenController',
    () => new RegelungsvorhabenControllerApi(),
  );

  it('Test prepareFilesNames', () => {
    const uploadFiles = [
      { uid: '1', name: 'Hello' },
      { uid: '2', name: 'World' },
    ];
    const expectedOutput = ['Hello', 'World'];
    expect(ctrl.prepareFilesNames(uploadFiles)).eql(expectedOutput);
  });
  it('Test prepareEditUrl - first case', () => {
    const result1 = ctrl.prepareEditUrl(
      null,
      { abstimmungMetadataId: '1' } as AbstimmungEntityDTO,
      true,
      'a',
      routes.ABSTIMMUNGSRUNDE_PRUEFEN,
    );
    expect(result1).eql(`/hra/a/1/${routes.NACHSTE_ABSTIMMUNGSRUNDE}`);
  });
  it('Test prepareEditUrl - second case', () => {
    const result2 = ctrl.prepareEditUrl(null, {} as AbstimmungEntityDTO, true, 'b', 'a');
    expect(result2).eql(`/hra/b/${routes.ABSTIMMUNG_ANLEGEN}`);
  });
  it('Test prepareEditUrl - third case', () => {
    const result3 = ctrl.prepareEditUrl('a', { version: 1 } as AbstimmungEntityDTO, true, 'b', 'a');
    expect(result3).eql(`/hra/b/a/${routes.ABSTIMMUNG_BEARBEITEN}`);
  });
  it('Test prepareEditUrl - fourth case', () => {
    const result4 = ctrl.prepareEditUrl('a', { version: 0 } as AbstimmungEntityDTO, true, 'b', 'a');
    expect(result4).eql(`/hra/b/a/${routes.ABSTIMMUNG_BEARBEITEN}`);
  });
  it('Test prepareEditUrl - fifth case', () => {
    const result5 = ctrl.prepareEditUrl(
      'a',
      { version: 0, abstimmungMetadataId: '1' } as AbstimmungEntityDTO,
      false,
      'b',
      'a',
    );
    expect(result5).eql(`/hra/b/1/${routes.NACHSTE_ABSTIMMUNGSRUNDE}`);
  });

  it('Test prepareEditAction - first case', () => {
    const result1 = ctrl.prepareEditAction(
      null,
      { abstimmungMetadataId: '1' } as AbstimmungEntityDTO,
      true,
      'a',
      routes.ABSTIMMUNGSRUNDE_PRUEFEN,
    );
    expect(result1).eql(routes.NACHSTE_ABSTIMMUNGSRUNDE);
  });
  it('Test prepareEditAction - second case', () => {
    const result2 = ctrl.prepareEditAction(null, {} as AbstimmungEntityDTO, true, 'b', 'a');
    expect(result2).eql(routes.ABSTIMMUNG_ANLEGEN);
  });
  it('Test prepareEditAction - third case', () => {
    const result3 = ctrl.prepareEditAction('a', { version: 1 } as AbstimmungEntityDTO, true, 'b', 'a');
    expect(result3).eql(routes.ABSTIMMUNG_BEARBEITEN);
  });
  it('Test prepareEditAction - fourth case', () => {
    const result4 = ctrl.prepareEditAction('a', { version: 0 } as AbstimmungEntityDTO, true, 'b', 'a');
    expect(result4).eql(routes.ABSTIMMUNG_BEARBEITEN);
  });
  it('Test prepareEditUrl - fifth case', () => {
    const result5 = ctrl.prepareEditAction(
      'a',
      { version: 0, abstimmungMetadataId: '1' } as AbstimmungEntityDTO,
      false,
      'b',
      'a',
    );
    expect(result5).eql(routes.NACHSTE_ABSTIMMUNGSRUNDE);
  });

  it('Test prepareErrorUrl - first case', () => {
    const result1 = ctrl.prepareErrorUrl(null, {} as AbstimmungEntityDTO, 'b', routes.ABSTIMMUNGSRUNDE_PRUEFEN);
    expect(result1).eql(`/hra/b/0/${routes.ABSTIMMUNG_PRUEFEN}/${routes.FEHLER}`);
  });
  it('Test prepareErrorUrl - second case', () => {
    const result2 = ctrl.prepareErrorUrl('a', { version: 1 } as AbstimmungEntityDTO, 'b', 'test');
    expect(result2).eql(`/hra/b/a/${routes.ABSTIMMUNG_PRUEFEN}/${routes.FEHLER}`);
  });
  it('Test prepareErrorUrl - third case', () => {
    const result3 = ctrl.prepareErrorUrl('a', { version: 0 } as AbstimmungEntityDTO, 'b', routes.ABSTIMMUNG_PRUEFEN);
    expect(result3).eql(`/hra/b/a/${routes.ABSTIMMUNG_PRUEFEN}/${routes.FEHLER}`);
  });
  it('Test prepareErrorUrl - fourth case', () => {
    const result4 = ctrl.prepareErrorUrl(
      'a',
      { abstimmungMetadataId: '1', version: 0 } as AbstimmungEntityDTO,
      'b',
      'test',
    );
    expect(result4).eql(`/hra/b/1/${routes.ABSTIMMUNGSRUNDE_PRUEFEN}/${routes.FEHLER}`);
  });
  it('Test getMailText', () => {
    const voteDto = { einladungsMailText: 'Hello World /cockpit/#/hra/?/' };
    const expected_output = 'Hello World /cockpit/#/hra/123/';
    const result = ctrl.getMailtext('123', voteDto as AbstimmungEntityDTO);
    expect(result).eql(expected_output);
  });
  it('Test uploadFiles', (done) => {
    const prepareListStub = sinon.stub(manageFilesCtrl, 'prepareListOfuploadFilesObservables');
    ctrl.uploadFiles('123', { files: [], additionalFiles: [] } as Files, { files: [], additionalFiles: [] } as Files);
    setTimeout(() => {
      sinon.assert.calledOnce(prepareListStub);
      prepareListStub.restore();
      done();
    }, 20);
  });
  it('Test fetchInitialData', (done) => {
    const vote = {
      regelungsentwurfEditorId: '1',
      regelungsvorhabenId: '2',
      teilnehmer: ['horst'],
      teilnehmerInCc: ['maria', 'evelyn'],
    };
    const getReCallStub = sinon.stub(voteCtrl, 'getRegelungsentwurfCall');
    const getUsersCallStub = sinon.stub(userFormattingController, 'getUsersByEmailCall');

    ctrl.fetchInitialData(vote as AbstimmungEntityDTO);
    setTimeout(() => {
      sinon.assert.calledOnce(getReCallStub);
      sinon.assert.calledTwice(getUsersCallStub);
      getReCallStub.restore();
      getUsersCallStub.restore();
      done();
    }, 20);
  });
  it('Test saveVote with create vote', (done) => {
    const dummyObs = of({ base: { id: '123' }, dto: {} });
    const createVoteStub = sinon
      .stub(voteCtrl, 'createVote')
      .returns(dummyObs as Observable<AbstimmungEntityResponseDTO>);
    ctrl.saveVote(
      {} as AbstimmungEntityDTO,
      null,
      { files: [], additionalFiles: [] } as Files,
      { files: [], additionalFiles: [] } as Files,
    );
    setTimeout(() => {
      sinon.assert.calledOnce(createVoteStub);
      sinon.assert.calledWith(createVoteStub, {} as AbstimmungEntityDTO);
      createVoteStub.restore();
      done();
    }, 20);
  });
  it('Test saveVote with modify vote', (done) => {
    const dummyObs = of({ base: { id: '123' }, dto: {} });
    const modifyVoteStub = sinon
      .stub(voteCtrl, 'modifyVote')
      .returns(dummyObs as Observable<AbstimmungEntityResponseDTO>);
    ctrl.saveVote(
      {} as AbstimmungEntityDTO,
      '1',
      { files: [], additionalFiles: [] } as Files,
      { files: [], additionalFiles: [] } as Files,
    );
    setTimeout(() => {
      sinon.assert.calledOnce(modifyVoteStub);
      sinon.assert.calledWith(modifyVoteStub, '1', {} as AbstimmungEntityDTO);
      modifyVoteStub.restore();
      done();
    }, 20);
  });
  it('call send to pkp and get right values', (done) => {
    const expected_output = of('ERSTELLT');
    const regelungsvorhabenControllerStub = sinon
      .stub(regelungsvorhabenController, 'sendRvToPkp')
      .returns(expected_output as Observable<PkpGesendetStatusType>);
    ctrl.sendRvToPkp('1');
    setTimeout(() => {
      sinon.assert.calledOnce(regelungsvorhabenControllerStub);
      sinon.assert.calledWith(regelungsvorhabenControllerStub, { id: '1' });
      regelungsvorhabenControllerStub.restore();
      done();
    }, 20);
  });
});
