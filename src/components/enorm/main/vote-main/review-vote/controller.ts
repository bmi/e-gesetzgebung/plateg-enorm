// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { UploadFile } from 'antd/lib/upload/interface';
import { forkJoin, Observable, of } from 'rxjs';
import { concatMap, map } from 'rxjs/operators';

import {
  AbstimmungEntityDTO,
  AbstimmungEntityResponseDTO,
  AbstimmungStatusType,
  DokumentenmappeDTO,
  PkpGesendetStatusType,
  RegelungsvorhabenControllerApi,
} from '@plateg/rest-api';
import { GlobalDI, UserFormattingController } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { Files, ManageFilesController } from '../../../../../utils/controllerManageFiles';
import { VoteBaseController } from '../vote-base/controller';

interface FetchInitialData {
  regelungsentwurfDTO: DokumentenmappeDTO | undefined;
  teilnehmerNames: string;
  teilnehmerInCCNames: string;
}

export class ReviewVoteController {
  private readonly manageFilesController = GlobalDI.getOrRegister(
    'enormManageFilesController',
    () => new ManageFilesController(),
  );
  private readonly regelungsvorhabenController = GlobalDI.getOrRegister<RegelungsvorhabenControllerApi>(
    'regelungsvorhabenController',
    () => new RegelungsvorhabenControllerApi(),
  );
  private readonly voteController = GlobalDI.getOrRegister('enormVoteController', () => new VoteBaseController());
  private readonly userFormattingController = GlobalDI.getOrRegister<UserFormattingController>(
    'userFormattingController',
    () => new UserFormattingController(),
  );

  public prepareFilesNames(filesList: UploadFile[]): string[] {
    return filesList.map((file) => file.name);
  }

  public prepareEditUrl(
    existingVoteId: string | null,
    voteDraft: AbstimmungEntityDTO,
    isBearbeiten: boolean,
    tabName: string,
    action: string,
  ): string {
    if (action === routes.ABSTIMMUNGSRUNDE_PRUEFEN) {
      return `/hra/${tabName}/${voteDraft.abstimmungMetadataId as string}/${routes.NACHSTE_ABSTIMMUNGSRUNDE}`;
    }

    if (!existingVoteId) {
      return `/hra/${tabName}/${routes.ABSTIMMUNG_ANLEGEN}`;
    }

    if (voteDraft.version === 1 || isBearbeiten) {
      return `/hra/${tabName}/${existingVoteId}/${routes.ABSTIMMUNG_BEARBEITEN}`;
    } else {
      return `/hra/${tabName}/${voteDraft.abstimmungMetadataId as string}/${routes.NACHSTE_ABSTIMMUNGSRUNDE}`;
    }
  }

  public prepareEditAction(
    existingVoteId: string | null,
    voteDraft: AbstimmungEntityDTO,
    isBearbeiten: boolean,
    tabName: string,
    action: string,
  ): string {
    if (action === routes.ABSTIMMUNGSRUNDE_PRUEFEN) {
      return routes.NACHSTE_ABSTIMMUNGSRUNDE;
    }

    if (!existingVoteId) {
      return routes.ABSTIMMUNG_ANLEGEN;
    }

    if (voteDraft.version === 1 || isBearbeiten) {
      return routes.ABSTIMMUNG_BEARBEITEN;
    } else {
      return routes.NACHSTE_ABSTIMMUNGSRUNDE;
    }
  }

  public prepareErrorUrl(
    existingVoteId: string | null,
    voteDraft: AbstimmungEntityDTO,
    tabName: string,
    action?: string,
  ): string {
    if (!existingVoteId) {
      return `/hra/${tabName}/0/${routes.ABSTIMMUNG_PRUEFEN}/${routes.FEHLER}`;
    }

    if (voteDraft.version === 1 || action === routes.ABSTIMMUNG_PRUEFEN) {
      return `/hra/${tabName}/${existingVoteId}/${routes.ABSTIMMUNG_PRUEFEN}/${routes.FEHLER}`;
    } else {
      return `/hra/${tabName}/${voteDraft.abstimmungMetadataId as string}/${routes.ABSTIMMUNGSRUNDE_PRUEFEN}/${
        routes.FEHLER
      }`;
    }
  }
  public sendRvToPkp(rvId: string): Observable<PkpGesendetStatusType> {
    return this.regelungsvorhabenController.sendRvToPkp({ id: rvId });
  }
  public saveVote(
    dto: AbstimmungEntityDTO,
    voteId: string | null,
    voteFiles: Files,
    voteInitialFiles: Files,
    status = AbstimmungStatusType.InAbstimmung,
  ): Observable<string> {
    let observable: Observable<AbstimmungEntityResponseDTO>;
    if (voteId != null) {
      // Update exisiting vote

      observable = this.voteController.modifyVote(voteId, dto);
    } else {
      // Create a new vote
      observable = this.voteController.createVote(dto);
    }

    return observable.pipe(
      map((data: AbstimmungEntityResponseDTO): string => {
        return data.base.id;
      }),
      concatMap(
        // Now we concatenate uploadFiles with main stream,
        // to be sure what uploadFiles will be started only after previous request (create or modify) is finished
        (voteID: string) => {
          return this.uploadFiles(voteID, voteFiles, voteInitialFiles).pipe(
            map((): string => {
              return voteID;
            }),
          );
        },
      ),
      concatMap(
        // After uploading files we modify vote second time to set correct url in mail and status in case it has status Entwurf after creation
        // or just return ID of modifed vote.
        (voteID: string): Observable<string> => {
          if (dto.status === AbstimmungStatusType.Entwurf) {
            const updatedDTO: AbstimmungEntityDTO = {
              ...dto,
              einladungsMailText: this.getMailtext(voteID, dto),
              status: status,
            };
            return this.voteController.modifyVote(voteID, updatedDTO).pipe(
              map((data: AbstimmungEntityResponseDTO): string => {
                return data.base.id;
              }),
            );
          } else {
            return of(voteID);
          }
        },
      ),
    );
  }

  public uploadFiles(id: string, voteFiles: Files, voteInitialFiles: Files): Observable<any> {
    const listOfuploadFilesObservables: Observable<any>[] =
      this.manageFilesController.prepareListOfuploadFilesObservables(
        id,
        voteFiles.files,
        voteInitialFiles.files,
        voteFiles.additionalFiles,
        voteInitialFiles.additionalFiles,
      );

    return forkJoin(listOfuploadFilesObservables);
  }

  public fetchInitialData(vote: AbstimmungEntityDTO): Observable<FetchInitialData> {
    return forkJoin({
      regelungsentwurfDTO: this.voteController.getRegelungsentwurfCall(vote.dokumentenmappeId),
      teilnehmerNames: this.userFormattingController.getUsersByEmailCall(vote.teilnehmer),
      teilnehmerInCCNames:
        vote.teilnehmerInCc && vote.teilnehmerInCc.length
          ? this.userFormattingController.getUsersByEmailCall(vote.teilnehmerInCc)
          : of(''),
    });
  }

  public getMailtext = (voteId: string, voteDto: AbstimmungEntityDTO): string | undefined => {
    return voteDto.einladungsMailText?.replace('/cockpit/#/hra/?/', `/cockpit/#/hra/${voteId}/`);
  };
}
