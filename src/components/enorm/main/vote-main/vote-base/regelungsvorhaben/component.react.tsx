// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import { RegelungsvorhabenEntityResponseShortDTO } from '@plateg/rest-api';
import { FormItemWithInfo, MainContentSelectWrapper } from '@plateg/theme';
import { SelectDown } from '@plateg/theme/src/components/icons/SelectDown';

export interface RegelungsvorhabenComponentProps {
  regelungsvorhaben: RegelungsvorhabenEntityResponseShortDTO[];
  setAutofill: (autofill: boolean) => void;
  disabled?: boolean;
}

export function RegelungsvorhabenComponent(props: RegelungsvorhabenComponentProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <FormItemWithInfo
      name="regelungsvorhabenId"
      label={<span>{t('enorm.vote.regelungsvorhaben.label')}</span>}
      rules={[{ required: true, message: t('enorm.vote.regelungsvorhaben.error') }]}
    >
      <MainContentSelectWrapper
        id="regelungsvorhabenId"
        disabled={props.disabled}
        onChange={() => props.setAutofill(true)}
        placeholder={t('enorm.vote.regelungsvorhaben.placeholder')}
        style={{ width: '100%' }}
        suffixIcon={<SelectDown />}
        options={props.regelungsvorhaben
          .filter((item) => item !== null)
          .map((item, index) => ({
            label: (
              <span key={item?.dto.kurzbezeichnung + index.toString()} aria-label={item?.dto.kurzbezeichnung}>
                {item?.dto.abkuerzung} - {item?.dto.kurzbezeichnung}
              </span>
            ),
            value: item?.base.id,
            title: `${item?.dto.abkuerzung} - ${item?.dto.kurzbezeichnung}`,
          }))}
      />
    </FormItemWithInfo>
  );
}
