// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Radio } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { InfoComponent } from '@plateg/theme';

interface PKPAnfrageProps {
  setSendRVanPKP: (val: boolean) => void;
  sendRVanPKP: boolean;
}

export default function PKPAnfrage(props: PKPAnfrageProps) {
  const { t } = useTranslation();
  return (
    <fieldset className="fieldset-form-items">
      <legend className="seo">{t('enorm.vote.datenBlattAnPkp.label')}</legend>
      <Form.Item
        name="sendRVanPKP"
        label={
          <>
            <span>{t('enorm.vote.datenBlattAnPkp.label')}</span>
            <InfoComponent title={t('enorm.vote.datenBlattAnPkp.label')}>
              <span
                dangerouslySetInnerHTML={{
                  __html: t('enorm.vote.datenBlattAnPkp.infoText', {
                    interpolation: { escapeValue: false },
                  }),
                }}
              />
            </InfoComponent>
          </>
        }
        initialValue={false}
      >
        <Radio.Group
          className="horizontal-radios"
          name="PKP"
          value={!!props.sendRVanPKP}
          onChange={(e) => {
            props.setSendRVanPKP?.(!!e.target.value);
          }}
        >
          <Radio id="enorm-send-an-PKP-radio" value={true}>
            {t('enorm.vote.datenBlattAnPkp.sendToPkp')}
          </Radio>
          <Radio id="enorm-not-send-an-PKP-radio" value={false}>
            {t('enorm.vote.datenBlattAnPkp.doNotSendAnPkP')}
          </Radio>
        </Radio.Group>
      </Form.Item>
    </fieldset>
  );
}
