// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form } from 'antd';
import { FormInstance } from 'antd/lib/form';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { BASE_PATH } from '@plateg/rest-api';
import { CombinedTitle, ErrorBox, InfoComponent } from '@plateg/theme';

interface FormHeaderComponentProps {
  action: string;
  form: FormInstance;
  itemName?: string;
}

export function FormHeaderComponent(props: FormHeaderComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const GGO_LINK = BASE_PATH + '/arbeitshilfen/download/34#page=';
  return (
    <>
      <div className="heading-holder" style={{ marginTop: 30 }}>
        <CombinedTitle
          title={props.itemName}
          suffix={t(`enorm.${props.action}.mainTitle`)}
          additionalContent={
            props.action !== 'unterabstimmungAnlegen' ? (
              <InfoComponent title={t(`enorm.${props.action}.mainTitleDrawer`)}>
                <p
                  dangerouslySetInnerHTML={{
                    __html: t(`enorm.${props.action}.mainTitleDrawerText`, {
                      interpolation: { escapeValue: false },
                      link15: GGO_LINK + '14',
                      link45: GGO_LINK + '36',
                    }),
                  }}
                ></p>
              </InfoComponent>
            ) : undefined
          }
        />
      </div>
      <p className="ant-typography p-no-style">
        Pflichtfelder sind mit einem <sup>*</sup> gekennzeichnet.
      </p>
      <Form.Item shouldUpdate noStyle>
        {() => <ErrorBox fieldsErrors={props.form.getFieldsError()} />}
      </Form.Item>
    </>
  );
}
