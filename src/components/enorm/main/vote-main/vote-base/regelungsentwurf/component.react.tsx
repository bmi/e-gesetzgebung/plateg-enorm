// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { DokumentenmappeDTO, EditorRemoteControllerApi } from '@plateg/rest-api';
import {
  FormItemWithInfo,
  GlobalDI,
  HinweisComponent,
  LoadingStatusController,
  MainContentSelectWrapper,
} from '@plateg/theme';
import { SelectDown } from '@plateg/theme/src/components/icons/SelectDown';

export interface RVCompProps {
  isBearbeiten: boolean;
  selectedRvId: string | undefined;
}

export function RegelungsentwurfComponent(props: RVCompProps): React.ReactElement {
  const { t } = useTranslation();
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const [regelungsentwuerfe, setRegelungsentwuerfe] = useState<DokumentenmappeDTO[]>([]);
  const editorCtrl = GlobalDI.get<EditorRemoteControllerApi>('editorRemoteControllerApi');

  useEffect(() => {
    if (props.selectedRvId) {
      loadingStatusController.setLoadingStatus(false);
      const obs = editorCtrl.getDokumentenmappen({ rvId: props.selectedRvId });
      obs.subscribe({
        next: (data) => {
          setRegelungsentwuerfe(data);
          loadingStatusController.setLoadingStatus(false);
        },
        error: (error) => {
          console.error('Fetching Regelungsentwürfe was not succesful', error);
          loadingStatusController.setLoadingStatus(false);
        },
      });
    }
  }, [props.selectedRvId]);

  return (
    <div role="note" aria-describedby={'regelungsentwurf-hinweis'}>
      <FormItemWithInfo
        name="dokumentenmappeId"
        label={<span>{t('enorm.vote.regelungsentwurf.label')}</span>}
        rules={[{ required: true, message: t('enorm.vote.regelungsentwurf.error') }]}
      >
        {props.selectedRvId === undefined || regelungsentwuerfe?.length > 0 ? (
          <MainContentSelectWrapper
            id="dokumentenmappeId"
            disabled={props.selectedRvId === undefined || props.isBearbeiten}
            placeholder={t('enorm.vote.regelungsentwurf.placeholder')}
            style={{ width: '100%' }}
            suffixIcon={<SelectDown />}
            options={regelungsentwuerfe.map((item) => ({
              label: (
                <span key={item.id} aria-label={item.titel}>
                  {item.titel}
                </span>
              ),
              value: item.id,
              title: item.titel,
            }))}
          />
        ) : (
          <HinweisComponent
            title={t('enorm.vote.regelungsentwurf.hinweisTitle')}
            content={<p>{t('enorm.vote.regelungsentwurf.hinweisContent')}</p>}
          />
        )}
      </FormItemWithInfo>
      {props.selectedRvId === undefined || regelungsentwuerfe?.length > 0 ? (
        <HinweisComponent
          mode="warning"
          title={t('enorm.vote.regelungsentwurf.editorDoc.hinweisTitle')}
          content={<p>{t('enorm.vote.regelungsentwurf.editorDoc.hinweisContent')}</p>}
          hinweisId="regelungsentwurf-hinweis"
        />
      ) : null}
    </div>
  );
}
