// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Typography } from 'antd';
import { UploadFile } from 'antd/lib/upload/interface';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { DocumentType } from '@plateg/rest-api';
import { InfoComponent } from '@plateg/theme';

import { AdditionalUploadComponent } from '../../../../../../shares/additionalUploadComponent/component.react';
import { standardMaximumSize, UploadComponent } from '../../../../../../shares/uploadComponent/component.react';
import { FormTypePartial } from '../component.react';
import { DocumentTypeComponent } from '../document-type/component.react';
import { RegelungsentwurfComponent } from '../regelungsentwurf/component.react';

interface FormButtonsComponentProps {
  version: number;
  disabled: boolean;
  isBearbeiten: boolean;
  fileList?: UploadFile[];
  setFileList: React.Dispatch<React.SetStateAction<UploadFile[]>>;
  additionalFileList?: UploadFile[];
  setAdditionalFileList: React.Dispatch<React.SetStateAction<UploadFile[]>>;
  documentType?: DocumentType;
  setDocumentType?: (type: DocumentType) => void;
  editorDocIsDisabled?: boolean;
  onFormChange?: () => void;
}

export function FileUploadComponent(props: FormButtonsComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const uploadMessagesConfig = {
    uploadErrorFileType: t('enorm.vote.uploadErrorFileType'),
    uploadErrorFileSize: t('enorm.vote.uploadErrorFileSize'),
    uploadErrorFileNameLength: t('enorm.vote.uploadErrorFileNameLength'),
    uploadError: t('enorm.vote.regelungsentwurfUpload.error'),
    uploadBtnRegelungsentwurf: t('enorm.vote.uploadButtonRegelungsentwurf'),
    uploadBtnAttachments: t('enorm.vote.uploadButtonAttachments'),
    uploadHintRegelungsentwurf: t('enorm.vote.uploadHintRegelungsentwurf'),
    uploadHintAttachments: t('enorm.vote.uploadHintAttachments'),
    changeFileNameTitle: t('enorm.vote.uploadChangeFileNameModalTitel'),
    changeFileNameCancelText: t('enorm.vote.uploadChangeFileNameCancelTextButton'),
    changeFileNameItemLabel: t('enorm.vote.uploadChangeFileNameLabel'),
    changeFileNameErrorMsg: t('enorm.vote.uploadChangeFileNameError'),
  };

  return (
    <>
      <div className="heading-holder">
        <Title level={2}>{t('enorm.vote.uploadFilesTitle')}</Title>
        <InfoComponent title={t('enorm.vote.uploadFilesTitle')}>
          <p
            dangerouslySetInnerHTML={{
              __html: t('enorm.vote.uploadDraftTitleDrawerText', {
                interpolation: { escapeValue: false },
              }),
            }}
          />
        </InfoComponent>
      </div>
      <DocumentTypeComponent
        disabled={props.editorDocIsDisabled || props.disabled}
        setDocumentType={props.setDocumentType}
        documentType={props.documentType}
      />
      {props.documentType === DocumentType.Editor && (
        <Form.Item
          shouldUpdate={(prevVal: FormTypePartial, curVal: FormTypePartial) =>
            prevVal.regelungsvorhabenId !== curVal.regelungsvorhabenId
          }
        >
          {({ getFieldValue }) => {
            return (
              <RegelungsentwurfComponent
                selectedRvId={getFieldValue('regelungsvorhabenId') as string}
                isBearbeiten={props.isBearbeiten}
              />
            );
          }}
        </Form.Item>
      )}
      {props.documentType === DocumentType.Enorm && (
        <UploadComponent
          messagesConfig={{
            title: (
              <>
                {t('enorm.vote.uploadDraftTitle')}*
                <InfoComponent title={t('enorm.vote.uploadDraftTitleDrawer')}>
                  {t('enorm.vote.uploadDraftDrawerTooltip')}
                </InfoComponent>
              </>
            ),
            subtitle: <>{t('enorm.vote.uploadedDraftTitle')}</>,
            ...uploadMessagesConfig,
          }}
          fileList={props.fileList}
          setFileList={props.setFileList}
          fileFormat=".doc,.docx,.pdf"
          fileSize={standardMaximumSize}
          onlySingleFileAllowed={true}
          restrictRenaming={false}
          required={true}
          version={props.version}
          isBearbeiten={props.isBearbeiten}
          errorProps={{
            errorTitle: t('enorm.vote.uploadDraftErrorTitle'),
            formatString: t('enorm.vote.uploadDraftFormatShort'),
          }}
          onFormChange={props.onFormChange}
        />
      )}
      <AdditionalUploadComponent
        onFormChange={props.onFormChange}
        fileList={props.additionalFileList}
        setFileList={props.setAdditionalFileList}
        fileFormat=".doc,.docx,.pdf"
        fileSize={standardMaximumSize}
        singleFile={false}
        messagesConfig={{
          title: t('enorm.vote.uploadAttachmentsTitle'),
          subtitle: <>{t('enorm.vote.uploadedAttachmentsTitle')}</>,
          ...uploadMessagesConfig,
        }}
      />
    </>
  );
}
