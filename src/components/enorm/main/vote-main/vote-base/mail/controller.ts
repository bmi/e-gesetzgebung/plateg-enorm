// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import lightFormat from 'date-fns/lightFormat';
import i18n from 'i18next';

import {
  AbstimmungstypType,
  OberabstimmungEntityDTO,
  RegelungsvorhabenEntityResponseShortDTO,
  UserEntityResponseDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { routes } from '../../../../../../shares/routes';
import {
  createMailBodyFooter,
  createMailBodyHeader,
  createMailSubject,
  getTyp,
} from '../../../../../../utils/controller';
import { VoteBaseController } from '../controller';
import { AdditionalFrist } from '../deadlines/component.react';

export class MailController {
  private readonly ctrl = GlobalDI.getOrRegister('enormVoteController', () => new VoteBaseController());

  public getMailSubject(
    typeOfVoteKey?: string,
    regulatoryProject?: RegelungsvorhabenEntityResponseShortDTO,
    oberabstimmung?: OberabstimmungEntityDTO,
    version?: number,
  ): string {
    const typeOfVote = getTyp(typeOfVoteKey as AbstimmungstypType);
    if (typeOfVoteKey !== undefined && (regulatoryProject !== undefined || oberabstimmung !== undefined)) {
      return createMailSubject({
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        abbreviationOfVote: regulatoryProject?.dto.abkuerzung || oberabstimmung?.titel,
        typeOfVote,
        roundOfVote: version,
      });
    } else if (typeOfVoteKey === undefined && regulatoryProject !== undefined) {
      return i18n.t('enorm.vote.emailSubject.placeholder2');
    } else if (typeOfVoteKey !== undefined && regulatoryProject === undefined) {
      return i18n.t('enorm.vote.emailSubject.placeholder3');
    } else if (typeOfVoteKey === undefined && regulatoryProject === undefined) {
      return i18n.t('enorm.vote.emailSubject.placeholder');
    }
    throw new Error('Unreachable combination');
  }

  public getMailBodies(
    fristen: {
      mainDeadlineDate?: string;
      replyInternetDate?: string;
      replyDate?: string;
      additionalFristenString?: string;
      verschweigensfristGekennzeichnet: boolean;
    },
    votedata: {
      typeOfVoteKey?: string;
      titleOfVote?: string;
      voteId?: string | null;
      eingeschraenkt: boolean;
    },
    regulatoryProject?: RegelungsvorhabenEntityResponseShortDTO,
    user?: UserEntityResponseDTO,
    fileName?: string,
  ): {
    mailBodyHeader: string;
    mailBodyFooter: string;
  } {
    const typeOfVote = getTyp(votedata.typeOfVoteKey as AbstimmungstypType);
    const id = votedata.voteId ? votedata.voteId : '?';
    const linkToVote = `${window.location.origin}/cockpit/#/hra/${routes.BITTE_UM_MITZEICHNUNG}/${id}/${routes.ABSTIMMUNG_BEANTWORTEN_PRUEFEN}`;

    const mailBodyHeader = createMailBodyHeader({
      titleOfVote: votedata.titleOfVote,
      nameOfReferent: user?.dto?.name,
      ressortOfReferent: user?.dto?.ressort?.kurzbezeichnung,
      titleOfAssociatedRegulatoryProject: regulatoryProject?.dto.kurzbezeichnung,
      typeOfVote,
      mainDeadlineDate: fristen.mainDeadlineDate,
      replyInternetDate: fristen.replyInternetDate,
      replyDate: fristen.replyDate,
      additionalFristenString: fristen.additionalFristenString,
      verschweigensfristGekennzeichnet: fristen.verschweigensfristGekennzeichnet,
    });
    const mailBodyFooter = createMailBodyFooter({
      fileName,
      linkToVote,
      mailOfReferent: user?.dto?.email,
      phoneOfReferent: user?.dto?.telefon,
      eingeschraenkt: votedata.eingeschraenkt,
    });

    return { mailBodyHeader, mailBodyFooter };
  }

  public getMailDateString(dateTimeString: string | undefined): string | undefined {
    if (!dateTimeString) {
      return undefined;
    }
    return lightFormat(new Date(dateTimeString), 'dd.MM.yyyy, HH:mm') + ' Uhr';
  }

  public getAdditionalFristenString = (additionalFristList?: AdditionalFrist[]): string | undefined => {
    if (!additionalFristList) {
      return undefined;
    }
    return additionalFristList
      .map((frist) => {
        return `- ${frist.additionalDeadlineTitle || ''}, ${
          this.getMailDateString(
            this.ctrl.generateFristenString(frist.additionalDeadlineDate, frist.additionalDeadlineTime),
          ) || ''
        }\n`;
      })
      .join('');
  };
}
