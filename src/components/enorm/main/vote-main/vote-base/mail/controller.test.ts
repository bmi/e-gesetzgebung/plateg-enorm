// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai, { expect } from 'chai';

import { AbstimmungstypType, FristtypType } from '@plateg/rest-api';

import i18n from '../../../../../../shares/i18n';
import { MailController } from './controller';

describe('Validate additional fristen string generation', () => {
  let mailController: MailController;
  before(() => {
    mailController = new MailController();
  });
  it('DateTime fristen are valid', () => {
    const additionalFristList = [
      {
        id: '1',
        additionalDeadlineTitle: 'Hello',
        additionalDeadlineDate: new Date('1995-12-17T03:24:00'),
        additionalDeadlineTime: new Date('1995-12-14T06:00:00'),
        fristTyp: FristtypType.ZusaetzlicheFrist,
      },
      {
        id: '2',
        additionalDeadlineTitle: 'World',
        additionalDeadlineDate: new Date('December 17, 2000 03:24:00'),
        additionalDeadlineTime: new Date('December 17, 1995 17:59:23'),
        fristTyp: FristtypType.ZuleitungAnLaenderUndVerbaende,
      },
    ];
    const expected_output = '- Hello, 17.12.1995, 05:00 Uhr\n- World, 17.12.2000, 16:59 Uhr\n';
    const result = mailController.getAdditionalFristenString(additionalFristList);
    expect(result).to.eql(expected_output);
  });
  it('DateTime fristen are undefined', () => {
    const result = mailController.getAdditionalFristenString();
    expect(result).to.eql(undefined);
  });
});

describe('Test: getMailSubject', () => {
  let mailController: MailController;
  before(() => {
    mailController = new MailController();
  });
  it('both arguments != undefined', () => {
    const expected_output1 = 'Hausabstimmung abc (#1): Bitte um Mitzeichnung';
    const result1 = mailController.getMailSubject(AbstimmungstypType.Hausabstimmung, { dto: { abkuerzung: 'abc' } });
    expect(result1).to.eql(expected_output1);
    const expected_output2 = 'Ressortabstimmung xyz (#1): Bitte um Mitzeichnung';
    const result2 = mailController.getMailSubject(AbstimmungstypType.Ressortabstimmung, { dto: { abkuerzung: 'xyz' } });
    expect(result2).to.eql(expected_output2);
    const expected_output3 = '(die Person hat keine Angabe gemacht) xyz (#1): Bitte um Mitzeichnung';
    const result3 = mailController.getMailSubject('komischer Wert', { dto: { abkuerzung: 'xyz' } });
    expect(result3).to.eql(expected_output3);
  });
  it('first arguments == undefined', () => {
    const expected_output = i18n.t('enorm.vote.emailSubject.placeholder2');
    const result = mailController.getMailSubject(undefined, {});
    expect(result).to.eql(expected_output);
  });
  it('second arguments == undefined', () => {
    const expected_output = i18n.t('enorm.vote.emailSubject.placeholder3');
    const result = mailController.getMailSubject('test', undefined);
    expect(result).to.eql(expected_output);
  });
  it('both arguments == undefined', () => {
    const expected_output = i18n.t('enorm.vote.emailSubject.placeholder');
    const result = mailController.getMailSubject();
    expect(result).to.eql(expected_output);
  });
});

describe('Test: getMailBodies', () => {
  let mailController: MailController;
  before(() => {
    mailController = new MailController();
  });
  it('no input arguments eingeschraenkt', () => {
    const expected_output1 = {
      mailBodyFooter:
        '\nBitte beachten Sie bei Ihrer Antwort auch die neuen Anforderungen des § 43 Abs. 1 Nr. 13 GGO (“Exekutiver Fußabdruck”).\n\nHier können Sie den Regelungsentwurf und die Anlagen aufrufen und Ihre Änderungsvorschläge direkt im Dokument einfügen:\n\nnull/cockpit/#/hra/bitteUmMitzeichnung/?/abstimmungBeantwortenPruefen \n\nDiese E-Mail darf an weitere Personen weiterleitet werden, die aus Ihrer Sicht an der Abstimmung teilnehmen sollten. Diese Personen dürfen über den obigen Link zum Regelungsentwurf die "Teilnahme anfragen". Auch eine Weiterleitung an Postfächer ist möglich, die Teilnahmeanfrage allerdings nur für Personen.\n\nDies ist eine automatisch generierte Nachricht. Bitte antworten Sie nicht auf diese E-Mail. Bei Fragen wenden Sie sich an (die Person hat keine Angabe gemacht), (Telefonnummer: die Person hat keine Angabe gemacht).',
      mailBodyHeader:
        'Sehr geehrte Nutzerin, sehr geehrter Nutzer,\n\nSie wurden von (die Person hat keine Angabe gemacht), (die Person hat keine Angabe gemacht) zu der (die Person hat keine Angabe gemacht) (die Person hat keine Angabe gemacht) bezüglich des Vorhabens (die Person hat keine Angabe gemacht) hinzugefügt.\n\nFolgende Frist/en ist/sind zu beachten:\n \n- Ende der Abstimmungsrunde, (die Person hat keine Angabe gemacht)\n \n \n\nEs wurde folgender Kommentar hinzugefügt:\n',
    };
    const result1 = mailController.getMailBodies({ verschweigensfristGekennzeichnet: false }, { eingeschraenkt: true });
    expect(result1).to.eql(expected_output1);
  });
  it('no input arguments offen', () => {
    const expected_output1 = {
      mailBodyFooter:
        '\nBitte beachten Sie bei Ihrer Antwort auch die neuen Anforderungen des § 43 Abs. 1 Nr. 13 GGO (“Exekutiver Fußabdruck”).\n\nHier können Sie den Regelungsentwurf und die Anlagen aufrufen und Ihre Änderungsvorschläge direkt im Dokument einfügen:\n\nnull/cockpit/#/hra/bitteUmMitzeichnung/?/abstimmungBeantwortenPruefen \n\nDiese E-Mail darf an weitere Personen weiterleitet werden, die aus Ihrer Sicht an der Abstimmung teilnehmen sollten. Über den obigen Link können diese direkt an der Abstimmung teilnehmen. Auch eine Weiterleitung an Postfächer ist möglich, die Teilnahme allerdings nur für Personen.\n\nDies ist eine automatisch generierte Nachricht. Bitte antworten Sie nicht auf diese E-Mail. Bei Fragen wenden Sie sich an (die Person hat keine Angabe gemacht), (Telefonnummer: die Person hat keine Angabe gemacht).',
      mailBodyHeader:
        'Sehr geehrte Nutzerin, sehr geehrter Nutzer,\n\nSie wurden von (die Person hat keine Angabe gemacht), (die Person hat keine Angabe gemacht) zu der (die Person hat keine Angabe gemacht) (die Person hat keine Angabe gemacht) bezüglich des Vorhabens (die Person hat keine Angabe gemacht) hinzugefügt.\n\nFolgende Frist/en ist/sind zu beachten:\n \n- Ende der Abstimmungsrunde, (die Person hat keine Angabe gemacht)\n \n \n\nEs wurde folgender Kommentar hinzugefügt:\n',
    };
    const result1 = mailController.getMailBodies(
      { verschweigensfristGekennzeichnet: false },
      { eingeschraenkt: false },
    );
    expect(result1).to.eql(expected_output1);
  });
  it('no input arguments verschweigensfrist', () => {
    const expected_output1 = {
      mailBodyFooter:
        '\nBitte beachten Sie bei Ihrer Antwort auch die neuen Anforderungen des § 43 Abs. 1 Nr. 13 GGO (“Exekutiver Fußabdruck”).\n\nHier können Sie den Regelungsentwurf und die Anlagen aufrufen und Ihre Änderungsvorschläge direkt im Dokument einfügen:\n\nnull/cockpit/#/hra/bitteUmMitzeichnung/?/abstimmungBeantwortenPruefen \n\nDiese E-Mail darf an weitere Personen weiterleitet werden, die aus Ihrer Sicht an der Abstimmung teilnehmen sollten. Über den obigen Link können diese direkt an der Abstimmung teilnehmen. Auch eine Weiterleitung an Postfächer ist möglich, die Teilnahme allerdings nur für Personen.\n\nDies ist eine automatisch generierte Nachricht. Bitte antworten Sie nicht auf diese E-Mail. Bei Fragen wenden Sie sich an (die Person hat keine Angabe gemacht), (Telefonnummer: die Person hat keine Angabe gemacht).',
      mailBodyHeader:
        'Sehr geehrte Nutzerin, sehr geehrter Nutzer,\n\nSie wurden von (die Person hat keine Angabe gemacht), (die Person hat keine Angabe gemacht) zu der (die Person hat keine Angabe gemacht) (die Person hat keine Angabe gemacht) bezüglich des Vorhabens (die Person hat keine Angabe gemacht) hinzugefügt.\n\nFolgende Frist/en ist/sind zu beachten:\n \n- Ende der Abstimmungsrunde, (die Person hat keine Angabe gemacht)\nDiese Frist wurde als Verschweigensfrist gekennzeichnet.\n \n \n\nEs wurde folgender Kommentar hinzugefügt:\n',
    };
    const result1 = mailController.getMailBodies({ verschweigensfristGekennzeichnet: true }, { eingeschraenkt: false });
    expect(result1).to.eql(expected_output1);
  });
});
