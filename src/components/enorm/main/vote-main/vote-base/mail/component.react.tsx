// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, FormInstance, Input } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';

import {
  AbstimmungstypType,
  BASE_PATH,
  OberabstimmungEntityDTO,
  RegelungsvorhabenEntityResponseShortDTO,
  UserEntityResponseDTO,
} from '@plateg/rest-api';
import { Constants, EmailsSearchComponent, GlobalDI, HinweisComponent } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

import { createMailBody } from '../../../../../../utils/controller';
import { AdditionalFrist } from '../deadlines/component.react';
import { MailController } from './controller';

interface MailProps {
  form: FormInstance;
  regelungsvorhaben?: RegelungsvorhabenEntityResponseShortDTO;
  currentUser?: UserEntityResponseDTO;
  autofill: boolean;
  mainDeadlineDate?: string;
  replyInternetDate?: string;
  replyDate?: string;
  isBearbeiten: boolean;
  fileName: string;
  voteId?: string | null;
  oberabstimmung?: OberabstimmungEntityDTO;
  isEditingVote: boolean;
  version?: number;
  isVorhabenclearing?: boolean;
  isDraft?: boolean;
  onFormChange?: () => void;
}

export function MailComponent(props: Readonly<MailProps>): React.ReactElement {
  const { t } = useTranslation();
  const { TextArea } = Input;

  const ctrl = GlobalDI.getOrRegister('enormMailController', () => new MailController());
  const typ = props.form.getFieldValue('typ') as string;
  const titel = props.form.getFieldValue('titel') as string;
  const verschweigensfristGekennzeichnet = props.form.getFieldValue('verschweigensfristGekennzeichnet') as boolean;
  const eingeschraenkt = props.form.getFieldValue('eingeschraenkt') as boolean;
  const einladungsMailText = props.form.getFieldValue('einladungsMailText') as string;
  const einladungsMailKommentar = props.form.getFieldValue('einladungsMailKommentar') as string;
  const setEinladungsMailBetreff = (value: string) => props.form.setFieldsValue({ einladungsMailBetreff: value });
  const setEinladungsMailText = (value: string) => props.form.setFieldsValue({ einladungsMailText: value });
  const additionalFristList = (props.form.getFieldValue('additionalFristList') as AdditionalFrist[]) || [];
  const isFirstRender = useRef(true);
  const isFirstRenderTyp = useRef(true);

  const updateMailSubject = (): void => {
    setEinladungsMailBetreff(ctrl.getMailSubject(typ, props.regelungsvorhaben, props.oberabstimmung, props.version));
  };

  const updateMailText = (): void => {
    const body = ctrl.getMailBodies(
      {
        mainDeadlineDate: ctrl.getMailDateString(props.mainDeadlineDate),
        replyInternetDate: ctrl.getMailDateString(props.replyInternetDate),
        replyDate: ctrl.getMailDateString(props.replyDate),
        additionalFristenString: ctrl.getAdditionalFristenString(additionalFristList),
        verschweigensfristGekennzeichnet,
      },
      {
        typeOfVoteKey: typ,
        titleOfVote: titel,
        voteId: props.voteId,
        eingeschraenkt,
      },
      props.regelungsvorhaben,
      props.currentUser,
      props.fileName,
    );
    setEinladungsMailText(createMailBody(body.mailBodyHeader, body.mailBodyFooter, einladungsMailKommentar));
  };

  useEffect(() => {
    if (props.autofill) {
      updateMailSubject();
    }
  }, [typ, props.regelungsvorhaben, props.oberabstimmung, props.autofill]);

  useEffect(() => {
    updateMailText();
  }, [
    typ,
    props.regelungsvorhaben,
    titel,
    verschweigensfristGekennzeichnet,
    eingeschraenkt,
    props.currentUser,
    props.mainDeadlineDate,
    props.replyInternetDate,
    props.replyDate,
    ...additionalFristList.map((frist) => {
      return frist.additionalDeadlineTime;
    }),
    ...additionalFristList.map((frist) => {
      return frist.additionalDeadlineDate;
    }),
    ...additionalFristList.map((frist) => {
      return frist.additionalDeadlineTitle;
    }),
    additionalFristList?.length,
    props.fileName,
    props.voteId,
  ]);

  useEffect(() => {
    if (isFirstRender.current && !props.isVorhabenclearing) {
      isFirstRender.current = false;
      return;
    }
    if (props.isVorhabenclearing) {
      const vorhabenClearingEmails = t('enorm.vote.emailAddress.vorhabenclearing').split(';');
      props.form.setFieldValue('teilnehmer', vorhabenClearingEmails);
    } else {
      props.form.setFields([
        {
          name: 'teilnehmer',
          value: [],
          touched: false,
        },
      ]);
    }
  }, [props.isVorhabenclearing]);

  useEffect(() => {
    if (isFirstRenderTyp.current && props.isDraft) {
      isFirstRenderTyp.current = false;
      return;
    }
    fillObligatoryEmails(
      typ === AbstimmungstypType.Ressortabstimmung.toString() ||
        typ === AbstimmungstypType.Schlussabstimmung.toString(),
    );
  }, [typ]);

  const mailValidator = (values: any, msg: string) => {
    return props.currentUser?.dto.email && (values as string[]).includes(props.currentUser?.dto.email)
      ? Promise.reject(msg)
      : Promise.resolve();
  };

  const mailIntersection = (values: string[]) => {
    const teilnehmer: string[] = props.form.getFieldValue('teilnehmer') as string[];
    const teilnehmerIntersections = teilnehmer.filter((value) => values.includes(value));
    return teilnehmerIntersections.length > 0;
  };

  const fillObligatoryEmails = (shouldFill: boolean) => {
    const obligatoryEmails = t('enorm.vote.emailAddress.obligatoryEmails').split(';');
    const emailAddress = props.form.getFieldValue('teilnehmer') as string[];
    let preparedEmailsList: string[] = [];
    if (shouldFill) {
      preparedEmailsList = [...new Set([...emailAddress, ...obligatoryEmails])];
    } else {
      // Remove obligatory emails
      preparedEmailsList = [...emailAddress.filter((email) => !obligatoryEmails.includes(email))];
    }

    if (props.isEditingVote) {
      preparedEmailsList = [...emailAddress];
    }
    if (!props.isVorhabenclearing) {
      props.form.setFields([
        {
          name: 'teilnehmer',
          value: preparedEmailsList,
          touched: preparedEmailsList.length !== 0,
        },
      ]);
    }
  };

  // Trigger validate teilnehmerInCc manually because setFieldsValue doesn't triger onChange
  const updateFormField = (fieldName: string, value: string[]) => {
    props.form.setFieldsValue({
      [fieldName]: value,
    });
    void props.form.validateFields(['teilnehmer']);
    void props.form.validateFields(['teilnehmerInCc']);
    props.onFormChange?.();
  };

  const getMailSearchText = (name: string) => {
    const GGO_LINK = BASE_PATH + '/arbeitshilfen/download/34#page=';
    return {
      searchLabel: t(`enorm.vote.emailAddress.${name}.search.label`),
      searchDrawer: {
        searchDrawerTitle: t(`enorm.vote.emailAddress.${name}.search.drawer.title`),
        searchDrawerText: t(`enorm.vote.emailAddress.${name}.search.drawer.text`),
      },
      searchHint: t(`enorm.vote.emailAddress.${name}.search.hint`),
      addressLabel: t(`enorm.vote.emailAddress.${name}.label`),
      addressDrawer: {
        addressDrawerTitle: t(`enorm.vote.emailAddress.${name}.drawer.title`),
        addressDrawerText: t(`enorm.vote.emailAddress.${name}.drawer.text`, {
          interpolation: { escapeValue: false },
          link45: GGO_LINK + '36',
          link74: GGO_LINK + '55',
          link21: GGO_LINK + '17',
          link24: GGO_LINK + '19',
        }),
      },
    };
  };

  return (
    <>
      <Title level={2} style={{ marginTop: '30px' }}>
        {t('enorm.vote.emailTitle')}
      </Title>

      {typ && typ !== AbstimmungstypType.Unterabstimmung.toString() && (
        <HinweisComponent
          title={t('enorm.vote.leserechte.hinweisTitle')}
          content={<p>{t('enorm.vote.leserechte.hinweisContent')}</p>}
        />
      )}
      {/* Email addresses (To) */}
      <EmailsSearchComponent
        isVorhabenclearing={props.isVorhabenclearing}
        name="teilnehmer"
        form={props.form}
        currentUserEmail={props.currentUser?.dto.email ?? ''}
        texts={getMailSearchText('teilnehmer')}
        rules={[
          { required: true, message: t('enorm.vote.emailAddress.error') },
          {
            pattern: Constants.EMAIL_REGEXP_PATTERN,
            message: t('enorm.vote.emailAddress.errorShouldBeEmail'),
          },
          {
            validator: (_rule, values) => {
              return mailValidator(values, t('enorm.vote.errorParticipant'));
            },
          },
        ]}
        updateFormField={updateFormField}
        allowUnknownEmail={true}
        ccMode={{
          name: 'teilnehmerInCc',
          texts: getMailSearchText('teilnehmerInCc'),
          rules: [
            {
              pattern: Constants.EMAIL_REGEXP_PATTERN,
              message: t('enorm.vote.emailAddressInCC.errorShouldBeEmail'),
            },
            {
              validator: (_rule, values) => {
                return mailValidator(values, t('enorm.vote.errorParticipantCC'));
              },
            },
            {
              validator: (_rule, values) => {
                return mailIntersection(values as string[])
                  ? Promise.reject(t('enorm.vote.errorParticipantCCDuplication'))
                  : Promise.resolve();
              },
            },
          ],
          dependencies: ['teilnehmer'],
          checkIntersection: mailIntersection,
          className: 'has-hinweis',
        }}
      />
      {/* Email Subject */}
      <Form.Item
        name="einladungsMailBetreff"
        label={<span>{t('enorm.vote.emailSubject.label')}</span>}
        rules={[
          { required: true, message: t('enorm.vote.emailSubject.error') },
          {
            max: Constants.TEXT_AREA_LENGTH,
            message: t('enorm.vote.emailSubject.errorSubjectLength', {
              maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
            }),
          },
        ]}
      >
        <Input
          disabled={props.regelungsvorhaben === undefined || typ === undefined || props.isBearbeiten}
          required
          placeholder={t('enorm.vote.emailSubject.placeholder')}
        />
      </Form.Item>
      <Form.Item
        name="einladungsMailKommentar"
        label={
          <span>
            {t('enorm.vote.emailComment.label')}{' '}
            <span className="hint">
              {t('enorm.vote.emailComment.hint', {
                maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
              })}
            </span>
          </span>
        }
        rules={[
          {
            max: Constants.TEXT_AREA_LENGTH,
            message: t('enorm.vote.emailComment.error', {
              maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
            }),
          },
        ]}
      >
        <TextArea rows={8} onBlur={updateMailText} disabled={props.isBearbeiten} />
      </Form.Item>
      {/*  Email text */}
      <Title level={4}>{t('enorm.vote.emailText.label')}</Title>
      <p className="ant-typography p-no-style">{t('enorm.vote.emailText.hint')}</p>
      <hr />
      <p className="ant-typography p-no-style" id="mail_body">
        {einladungsMailText}
      </p>
      <hr />
    </>
  );
}
