// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import { FormInstance } from 'antd/lib/form/Form';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import { AbstimmungstypType, FristEntityDTO, FristtypType } from '@plateg/rest-api';
import { InfoComponent, RepeatableFormComponent } from '@plateg/theme';

import { AdditionalDeadlineComponent } from '../additionalDeadline/component.react';
import { GGO_LINK, VoteFormValues } from '../component.react';
import { DateTimeComponent } from '../dateTimeComponent/component.react';

export interface AdditionalFrist {
  additionalDeadlineTitle?: string;
  additionalDeadlineDate?: Date;
  additionalDeadlineTime?: Date;
  fristTyp: FristtypType;
  id: string;
}
export interface DeadlinesComponentProps {
  form: FormInstance;
  maxDate?: string;
  internetDateIsRequired?: boolean;
  isBearbeiten: boolean;
  onFormChange?: () => void;
  votingType?: AbstimmungstypType;
}

export function DeadlinesComponent(props: DeadlinesComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const getAdditionalDeadlines = () => {
    return (props.form.getFieldsValue(true) as VoteFormValues).additionalFristList || [];
  };

  const setAdditionalDeadlines = (additionalFristList: AdditionalFrist[]) => {
    const currentValues = props.form.getFieldsValue(true) as VoteFormValues;
    currentValues.additionalFristList = additionalFristList;
    props.form.setFieldsValue(currentValues);
  };

  return (
    <>
      {/* Main deadline */}
      <div className="heading-holder">
        <Title level={2}>{t('enorm.vote.deadlinesTitle')}</Title>
        <InfoComponent title={t('enorm.vote.deadlinesTitleDrawerTitle')}>
          <p
            dangerouslySetInnerHTML={{
              __html: t('enorm.vote.deadlinesTitleDrawerText', {
                interpolation: { escapeValue: false },
                link50: GGO_LINK + '39',
                link47: GGO_LINK + '37',
              }),
            }}
          />
        </InfoComponent>
      </div>
      <div className="white-holder deadline-holder">
        <fieldset>
          <legend style={{ all: 'unset' }}>
            <strong className="form-label">{t('enorm.vote.deadlineVote.title')}</strong>
          </legend>
          <DateTimeComponent
            aliasDate="mainDeadlineDate"
            aliasTime="mainDeadlineTime"
            required={true}
            form={props.form}
            identifier="deadlineVote"
            isBearbeiten={props.isBearbeiten}
            maxDate={props.maxDate}
            onFormChange={props.onFormChange}
          />
        </fieldset>
      </div>
      {!!props.internetDateIsRequired && (
        <div className="white-holder deadline-holder">
          <fieldset>
            <legend style={{ all: 'unset' }}>
              <strong className="form-label">{t('enorm.vote.replyInternetDate.title')}</strong>
            </legend>
            <DateTimeComponent
              aliasDate="replyInternetDate"
              aliasTime="replyInternetTime"
              required={!!props.internetDateIsRequired}
              form={props.form}
              identifier="replyInternetDate"
              isBearbeiten={props.isBearbeiten}
              onFormChange={props.onFormChange}
            />
          </fieldset>
        </div>
      )}
      {props.votingType !== AbstimmungstypType.AbstimmungDerVorlageFuerDenRegierungsentwurf && (
        <div className="white-holder deadline-holder">
          <fieldset>
            <legend style={{ all: 'unset' }}>
              <strong className="form-label">{t('enorm.vote.replyDate.title')}</strong>
            </legend>
            <DateTimeComponent
              aliasDate="replyDate"
              aliasTime="replyTime"
              required={false}
              form={props.form}
              identifier="replyDate"
              isBearbeiten={props.isBearbeiten}
              onFormChange={props.onFormChange}
            />
          </fieldset>
        </div>
      )}
      {/* Additional deadlines */}
      <RepeatableFormComponent
        getRepeatableItem={getAdditionalDeadlines}
        setRepeatableItem={setAdditionalDeadlines}
        emptyItem={() => {
          return {
            id: uuidv4(),
            fristTyp: FristtypType.ZusaetzlicheFrist,
          };
        }}
        textAddItem={t('enorm.vote.additionalDeadline.addNewDeadline')}
        repeatableComponent={(item: FristEntityDTO, index: number, deleteItem: (index: number) => void) => (
          <AdditionalDeadlineComponent
            onFormChange={props.onFormChange}
            key={index}
            index={index}
            deleteDeadline={deleteItem}
            form={props.form}
          />
        )}
      />
    </>
  );
}
