// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './vote.less';

import { Checkbox, Form, Switch, Typography } from 'antd';
import { UploadFile } from 'antd/lib/upload/interface';
import i18n from 'i18next';
import React, { useEffect, useState } from 'react';
import { flushSync } from 'react-dom';
import { useTranslation } from 'react-i18next';
import { Link, Redirect, useHistory } from 'react-router-dom';
import { Subscription } from 'rxjs';

import {
  AbstimmungEntityDTO,
  AbstimmungEntityResponseDTO,
  AbstimmungStatusType,
  AbstimmungstypType,
  BASE_PATH,
  DocumentType,
  FristtypType,
  RegelungsvorhabenEntityResponseShortDTO,
  UserEntityResponseDTO,
} from '@plateg/rest-api';
import {
  BreadcrumbComponent,
  displayMessage,
  DropdownMenu,
  GeneralFormWrapper,
  GlobalDI,
  HeaderController,
  HinweisComponent,
  IconLocked,
  InfoComponent,
  LoadingStatusController,
  RouteLeavingGuard,
} from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { ContinueLaterButton, ContinueLaterComponent } from '../../../../../shares/continue-later/component.react';
import { routes } from '../../../../../shares/routes';
import { getAbstimmungsbezeichner } from '../../../../../utils/controller';
import { ManageFilesController } from '../../../../../utils/controllerManageFiles';
import { EnormHelpBreadcrumb } from '../../help/component.react';
import { Files, SaveVoteResponseInterface, VoteBaseController, VoteInitialDataInterface } from './controller';
import { AdditionalFrist, DeadlinesComponent } from './deadlines/component.react';
import { EarlyVoteBundeskanzleramtComponent } from './earlyVoteBundeskanzleramt/component.react';
import { FileUploadComponent } from './fileUpload/component.react';
import { FormButtonsComponent } from './formButtons/component.react';
import { FormHeaderComponent } from './formHeader/component.react';
import { LayoutVoteComponent } from './layout-vote/component.react';
import { MailComponent } from './mail/component.react';
import { RegelungsvorhabenComponent } from './regelungsvorhaben/component.react';
import PKPAnfrage from './sendToPKP/component.react';
import { TitleComponent } from './title-component/component.react';
import { VoteTypeComponent } from './voteType/component.react';

export interface VoteFormValues extends AbstimmungEntityDTO {
  additionalFristList: AdditionalFrist[];
  mainDeadlineDate: Date;
  mainDeadlineTime: Date;
  replyInternetDate?: Date;
  replyInternetTime?: Date;
  replyDate: Date;
  replyTime: Date;
  earlyVoteBundeskanzleramtDate?: Date;
  ressortTeilnehmer: string[];
}

export interface VoteBaseProps {
  setVote: (vote: AbstimmungEntityDTO) => void;
  setVoteFiles: (voteFiles: Files) => void;
  setVoteInitialFiles: (voteFiles: Files) => void;
  setVoteId: (voteId: string | null) => void;
  voteForEditing?: AbstimmungEntityDTO;
  voteFiles: Files;
  voteId: string | null;
  tabName: string;
  action: string;
  rvId?: string;
  abstimmungType?: AbstimmungstypType;
  setSelectedRV: (rv: RegelungsvorhabenEntityResponseShortDTO) => void;
  setSendRVanPKP: (val: boolean) => void;
  sendRVanPKP: boolean;
}

export const GGO_LINK = BASE_PATH + '/arbeitshilfen/download/34#page=';

export type FormTypePartial = { regelungsvorhabenId: string };
/**
 * helper function to replace multiple 'or' statements to reduce cognitive complexity in sonarcube
 */
export const compareOrValues = (arg1: unknown, arg2: unknown) => arg1 || arg2;

export function VoteBase(props: VoteBaseProps): React.ReactElement {
  const ctrl = GlobalDI.getOrRegister('enormVoteController', () => new VoteBaseController());
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const manageFilesController = GlobalDI.getOrRegister('enormManageFilesController', () => new ManageFilesController());
  const { t } = useTranslation();
  const history = useHistory();
  const { Title } = Typography;
  const [form] = Form.useForm();
  const [fileList, setFileList] = useState<UploadFile[]>([]);
  const [initalFileList, setInititalFileList] = useState<UploadFile[]>([]);
  const [additionalFileList, setAdditionalFileList] = useState<UploadFile[]>([]);
  const [initalAdditionalFileList, setAdditionalInititalFileList] = useState<UploadFile[]>([]);
  const [isLeavingPageBlocked, setIsLeavingPageBlocked] = useState(false);
  const [regelungsvorhabenList, setRegelungsvorhabenList] = useState<RegelungsvorhabenEntityResponseShortDTO[]>();
  const [currentUser, setCurrentUser] = useState<UserEntityResponseDTO>();
  const [deadlineExceeded, setDeadlineExceeded] = useState(false);
  const [status, setStatus] = useState<AbstimmungStatusType>();
  const [version, setVersion] = useState(1);
  const [existingVoteId, setExistingVoteId] = useState(props.voteId);
  const [isBearbeiten, setIsBearbeiten] = useState(false);
  const [documentType, setDocumentType] = useState<DocumentType>(DocumentType.Editor);
  const [vote, setVote] = useState<AbstimmungEntityDTO>();
  const [currentUserIsStellvertreter, setCurrentUserIsStellvertreter] = useState(false);
  const [savedDraft, setSavedDraft] = useState(false);

  const actionKey = props.action;
  const voteCtrl = GlobalDI.getOrRegister('enormVoteController', () => new VoteBaseController());

  const { initialValues, votingRoundAndIsBearbeiten, votingRound, isUnterabstimmung } = ctrl.getInitialValues(
    props.action,
    isBearbeiten,
    vote,
  );

  const [autofill, setAutofill] = useState(isUnterabstimmung);
  const [votingType, setVotingType] = useState<AbstimmungstypType>();

  const [editorDocIsDisabled, setEditorDocIsDisabled] = useState(false);
  const [internetValues, setInternetValues] = useState<{
    bkAmtUmEinvernehmenBitten: boolean;
    teilnehmendeUmBenehmenBitten: boolean;
    veroeffentlichungImInternet: boolean;
    veroeffentlichungImIntranet: boolean;
  }>({
    bkAmtUmEinvernehmenBitten: false,
    teilnehmendeUmBenehmenBitten: false,
    veroeffentlichungImInternet: false,
    veroeffentlichungImIntranet: false,
  });
  const [isEingeschraenkt, setIsEingeschraenkt] = useState<boolean>();
  const appStore = useAppSelector((state) => state.user);

  const loadFilesToState = (filesData: Files): void => {
    setFileList(filesData.files.map((item) => ({ ...item })));
    setAdditionalFileList(filesData.additionalFiles.map((item) => ({ ...item })));
  };
  const loadInitalFilesToState = (filesData: Files): void => {
    setInititalFileList(filesData.files);
    setAdditionalInititalFileList(filesData.additionalFiles);
  };

  useEffect(() => {
    if (documentType === DocumentType.Editor) {
      setFileList([]);
    } else {
      const currentFormValues: VoteFormValues = form.getFieldsValue(true) as VoteFormValues;
      currentFormValues.dokumentenmappeId = undefined;
      form.setFieldsValue(currentFormValues);
    }
  }, [documentType]);
  useEffect(() => {
    let subInitialData: Subscription;
    if (appStore.user) {
      if (appStore.user?.dto.stellvertreter) {
        setCurrentUserIsStellvertreter(true);
      }

      loadingStatusController.setLoadingStatus(true);
      subInitialData = ctrl
        .fetchInitialData(
          existingVoteId,
          votingRound,
          existingVoteId,
          appStore.user,
          isUnterabstimmung,
          props.voteForEditing?.status === AbstimmungStatusType.Entwurf || props.tabName === 'entwuerfe',
        )
        .subscribe({
          next: ({ voteData, filesData }) => {
            // If we have a vote for editing from Review page we use it
            // If not - we take vote from BE.
            const actualVote = compareOrValues(props.voteForEditing, voteData.voteInfo.vote) as AbstimmungEntityDTO;
            if (actualVote && !actualVote.regelungsvorhabenId && actualVote.regelungsvorhaben) {
              actualVote.regelungsvorhabenId = actualVote.regelungsvorhaben.base.id;
            }
            if (voteData.regelungsvorhabenList) {
              setRegelungsvorhabenList(
                [...voteData.regelungsvorhabenList].filter((item) => {
                  return item !== null && item !== undefined;
                }),
              );
            } else if (actualVote.regelungsvorhaben) {
              setRegelungsvorhabenList([actualVote.regelungsvorhaben] as RegelungsvorhabenEntityResponseShortDTO[]);
            } else if (actualVote?.oberabstimmung?.abstimmungId) {
              voteCtrl.getVote(actualVote?.oberabstimmung?.abstimmungId).subscribe({
                next: (oberabstimmung: AbstimmungEntityResponseDTO) => {
                  form.setFieldValue('regelungsvorhaben', oberabstimmung.dto.regelungsvorhaben);
                  form.setFieldValue('regelungsvorhabenId', oberabstimmung.dto.regelungsvorhabenId);
                  setRegelungsvorhabenList([
                    oberabstimmung.dto.regelungsvorhaben,
                  ] as RegelungsvorhabenEntityResponseShortDTO[]);
                },
              });
            }

            setValuesActualVote(actualVote, voteData, filesData);
            loadingStatusController.setLoadingStatus(false);
          },
        });
    }
    return () => {
      subInitialData?.unsubscribe();
      loadingStatusController.setLoadingStatus(false);
    };
  }, [appStore.user]);

  useEffect(() => {
    if (props.rvId && regelungsvorhabenList?.some((item) => item?.base?.id === props.rvId)) {
      form.setFieldValue('regelungsvorhabenId', props.rvId);
      setAutofill(true);
    }
  }, [props.rvId, currentUserIsStellvertreter, regelungsvorhabenList]);

  useEffect(() => {
    if (props.abstimmungType) {
      setVotingType(props.abstimmungType);
      form.setFieldValue('typ', props.abstimmungType);
    }
  }, [props.abstimmungType]);

  useEffect(() => {
    const preparedForm = form.getFieldsValue(true) as VoteFormValues;
    if (!preparedForm.bkAmtUmEinvernehmenBitten && !preparedForm.teilnehmendeUmBenehmenBitten) {
      preparedForm.fristen = preparedForm.fristen?.filter(
        (item) => item.fristTyp !== FristtypType.VeroeffentlichungImInternet,
      );
      const preparedFristen = ctrl.prepareFristen(form.getFieldsValue(true) as VoteFormValues, preparedForm);
      preparedFristen.replyInternetDate = undefined;
      preparedFristen.replyInternetTime = undefined;
      form.setFieldsValue(preparedFristen);
      form.resetFields([
        'replyInternetDate',
        'replyInternetTime',
        'replyInternetTime-placeholder',
        'replyInternetDate-date-placeholder',
        'replyInternetDate-calendar-popup',
      ]);
    } else {
      const preparedFristen = ctrl.prepareFristen(form.getFieldsValue(true) as VoteFormValues, preparedForm);
      form.setFieldsValue(preparedFristen);
    }
  }, [internetValues.bkAmtUmEinvernehmenBitten, internetValues.teilnehmendeUmBenehmenBitten]);

  useEffect(() => {
    if (props.voteForEditing) {
      setBreadcrumb(props, props.voteForEditing);
    }
  }, [props.voteForEditing, props.tabName]);

  const setValuesActualVote = (
    actualVote: AbstimmungEntityDTO,
    voteData: VoteInitialDataInterface,
    filesData: Files,
  ) => {
    if (actualVote) {
      const isUnterabstimmungForUnauthorizedRV =
        props.action === routes.UNTERABSTIMMUNG_ANLEGEN &&
        actualVote.regelungsvorhaben?.dto?.erstelltVon?.base?.id !== voteData.currentUser.base.id;
      setStatus(actualVote.status);
      setIsBearbeiten(actualVote.status === AbstimmungStatusType.InAbstimmung);
      setVersion(actualVote.version);
      setDocumentType(
        compareOrValues(isUnterabstimmungForUnauthorizedRV, isUnterabstimmung)
          ? DocumentType.Enorm
          : actualVote.documentTyp,
      );
      setEditorDocIsDisabled(isUnterabstimmungForUnauthorizedRV);
      setVotingType(actualVote.typ);
      actualVote.documentTyp = compareOrValues(isUnterabstimmungForUnauthorizedRV, isUnterabstimmung)
        ? DocumentType.Enorm
        : actualVote.documentTyp;
      form.setFieldsValue(actualVote);
      const preparedFristen = ctrl.prepareFristen(form.getFieldsValue(true) as VoteFormValues, actualVote);
      form.setFieldsValue(preparedFristen);
      setVote(actualVote);
      setInternetValues({
        bkAmtUmEinvernehmenBitten: actualVote.bkAmtUmEinvernehmenBitten,
        teilnehmendeUmBenehmenBitten: actualVote.teilnehmendeUmBenehmenBitten,
        veroeffentlichungImInternet: actualVote.veroeffentlichungImInternet,
        veroeffentlichungImIntranet: actualVote.veroeffentlichungImIntranet,
      });
      setIsEingeschraenkt(actualVote.eingeschraenkt);
    }

    setCurrentUser(voteData.currentUser);
    setExistingVoteId(voteData.voteInfo.existingVoteId);
    setDeadlineExceeded(voteData.voteInfo.deadlineExceeded);
    loadFilesToState(compareOrValues(props.voteFiles, filesData) as Files);
    loadInitalFilesToState(filesData);
    setBreadcrumb({ ...props, action: actionKey }, actualVote);
  };

  const onFinish = () => {
    flushSync(() => {
      setIsLeavingPageBlocked(false);
    });
    const formValues = form.getFieldsValue(true) as VoteFormValues;
    const dto = ctrl.prepareVoteForSubmit(formValues);
    props.setVote(dto);
    props.setVoteFiles({
      files: fileList,
      additionalFiles: additionalFileList,
    });
    props.setVoteInitialFiles({
      files: initalFileList,
      additionalFiles: initalAdditionalFileList,
    });
    props.setVoteId(existingVoteId);
    const selectedRVItem = getSelectedRegelungsvorhaben();
    if (selectedRVItem) {
      props.setSelectedRV(selectedRVItem);
    }

    const url = ctrl.prepareFinishUrl(actionKey, props.tabName);
    history.push(url);
  };

  const saveVote = (successMsg: string) => {
    const formValues = form.getFieldsValue(true) as VoteFormValues;
    const dto = ctrl.prepareVoteForSubmit(formValues);
    const filesData = {
      fileList,
      initalFileList,
      additionalFileList,
      initalAdditionalFileList,
    };

    loadingStatusController.setLoadingStatus(true);
    ctrl.saveVote(dto, existingVoteId, filesData).subscribe({
      next: (data: SaveVoteResponseInterface) => {
        setExistingVoteId(data.voteId);
        manageFilesController.getFilesCall(data.voteId).subscribe({
          next: (result: Files) => {
            loadFilesToState(result);
            loadInitalFilesToState(result);
            loadingStatusController.setLoadingStatus(false);
          },
        });
        props.setVote(dto);
        displayMessage(successMsg, 'success');
        flushSync(() => {
          setIsLeavingPageBlocked(false);
        });
        setSavedDraft(true);
        loadingStatusController.setLoadingStatus(false);
        if (dto.version > 1) {
          return history.push(`/hra/${routes.ENTWUERFE}/${data.voteId}/${routes.NACHSTE_ABSTIMMUNGSRUNDE}`);
        } else {
          return <Redirect to={`/hra/${routes.ENTWUERFE}/${data.voteId}/${routes.ABSTIMMUNG_ANLEGEN}`} />;
        }
      },
    });
  };

  const saveCurrentVoteState = () => {
    saveVote(t('enorm.vote.successSaveCurrentVoteMsg'));
  };

  const onFinishFailed = () => {
    document?.getElementById('errorBox')?.focus();
  };
  const onFormValuesChanged = () => {
    setIsLeavingPageBlocked(true);
  };

  const getSelectedRegelungsvorhaben = (): RegelungsvorhabenEntityResponseShortDTO | undefined => {
    const selectedRegulatoryProjectId = form.getFieldValue('regelungsvorhabenId') as string;
    return regelungsvorhabenList?.find((item) => item?.base?.id === selectedRegulatoryProjectId);
  };

  if (votingRound && !deadlineExceeded && status !== AbstimmungStatusType.Entwurf) {
    return <></>;
  }

  const checkIfbkAmtUmEinvernehmenBittenIsChecked =
    form.getFieldValue('bkAmtUmEinvernehmenBitten') === true &&
    form.getFieldValue('veroeffentlichungImIntranet') !== true &&
    form.getFieldValue('veroeffentlichungImInternet') !== true;

  const checkIfteilnehmendeUmBenehmenBittenIsChecked =
    form.getFieldValue('teilnehmendeUmBenehmenBitten') === true &&
    form.getFieldValue('veroeffentlichungImIntranet') !== true &&
    form.getFieldValue('veroeffentlichungImInternet') !== true;

  const isPublicationInternetIntranet =
    props.voteForEditing &&
    compareOrValues(checkIfbkAmtUmEinvernehmenBittenIsChecked, checkIfteilnehmendeUmBenehmenBittenIsChecked);

  const checkboxValidationMessage = t('enorm.vote.internetIntranetVeroffentlichungDescription');
  const publicationInternetIntranetRule = () => {
    if (isPublicationInternetIntranet) {
      return Promise.reject(checkboxValidationMessage);
    } else {
      return Promise.resolve();
    }
  };

  const headerController = GlobalDI.get<HeaderController>('enormHeadercontroller');
  headerController.setHeaderProps({
    headerRight: [
      <ContinueLaterButton
        key="continue-later-btn"
        isLeavingPageBlocked={isLeavingPageBlocked}
        modify={isBearbeiten}
        translationKey={'vote'}
        saveCurrentState={saveCurrentVoteState}
      />,
      <DropdownMenu
        items={[
          {
            element: t('enorm.header.btnContinueLater.btnText'),
            onClick: () => {
              saveCurrentVoteState();
            },
            disabled: () => !isLeavingPageBlocked,
          },
        ]}
        elementId={'headerRightAlternative'}
        overlayClass={'headerRightAlternative-overlay'}
      />,
    ],
  });

  if (!currentUser) {
    return <></>;
  }
  return (
    <LayoutVoteComponent>
      <RouteLeavingGuard
        navigate={(path: string) => history.push(path)}
        shouldBlockNavigation={(location) => {
          if (location.pathname.includes(`/${routes.ABSTIMMUNG_ANLEGEN}`)) {
            return false;
          }
          return isLeavingPageBlocked;
        }}
        title={t('enorm.vote.confirmModal.title')}
        btnOk={t('enorm.vote.confirmModal.btnCancelConfirmOk')}
        btnCancel={t('enorm.vote.confirmModal.btnCancelConfirmNo')}
      />
      <GeneralFormWrapper
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        className="vote-form"
        onChange={onFormValuesChanged}
        initialValues={initialValues}
        noValidate
      >
        <FormHeaderComponent
          action={actionKey}
          form={form}
          itemName={vote?.regelungsvorhaben?.dto.abkuerzung ? getAbstimmungsbezeichner(vote) : undefined}
        />
        <Title level={2}>{t('enorm.vote.generalInfoTitle')}</Title>

        {/* Associated projects */}
        {regelungsvorhabenList && (
          <RegelungsvorhabenComponent
            disabled={compareOrValues(isUnterabstimmung, votingRoundAndIsBearbeiten) as boolean}
            setAutofill={setAutofill}
            regelungsvorhaben={regelungsvorhabenList}
          />
        )}

        {/* Type of vote */}
        <Form.Item
          shouldUpdate={(prevVal: FormTypePartial, curVal: FormTypePartial) =>
            prevVal.regelungsvorhabenId !== curVal.regelungsvorhabenId
          }
        >
          {() => (
            <VoteTypeComponent
              setVotingType={setVotingType}
              disabled={votingRoundAndIsBearbeiten}
              setAutofill={setAutofill}
              isUnterabstimmung={isUnterabstimmung}
              regelungsvorhaben={getSelectedRegelungsvorhaben()}
              votingType={votingType}
            />
          )}
        </Form.Item>

        {/* Frühabstimmung mit dem Bundeskanzleramt */}
        {votingType === AbstimmungstypType.Ressortabstimmung && (
          <EarlyVoteBundeskanzleramtComponent onFormChange={onFormValuesChanged} vote={vote} form={form} />
        )}

        {/* Vote title */}
        <Form.Item
          shouldUpdate={(prevVal: FormTypePartial, curVal: FormTypePartial) =>
            prevVal.regelungsvorhabenId !== curVal.regelungsvorhabenId
          }
        >
          {() => (
            <TitleComponent
              regelungsvorhaben={getSelectedRegelungsvorhaben()}
              setTitle={(value) => form.setFieldsValue({ titel: value })}
              autofill={autofill}
              oberabstimmung={vote?.oberabstimmung}
            />
          )}
        </Form.Item>
        {/* vote eingeschraenkt */}
        <Form.Item
          label={
            <span className="label-text">
              {t('enorm.vote.eingeschraenkt.title')}
              <InfoComponent title={t('enorm.vote.eingeschraenkt.title')}>
                <p
                  dangerouslySetInnerHTML={{
                    __html: t('enorm.vote.eingeschraenkt.drawerText', {
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
              </InfoComponent>
            </span>
          }
        >
          <div className="switch-with-label">
            <Switch
              checked={isEingeschraenkt}
              onChange={(e) => {
                form.setFieldValue('eingeschraenkt', e);
                setIsEingeschraenkt(e);
                onFormValuesChanged();
              }}
            />
            <span>{t('enorm.vote.eingeschraenkt.label')}</span>
            <IconLocked />
          </div>
        </Form.Item>
        {isEingeschraenkt && (
          <div className="warning-div">
            <HinweisComponent
              title={t('enorm.vote.eingeschraenkt.title')}
              mode="warning"
              content={
                <p
                  dangerouslySetInnerHTML={{
                    __html: t('enorm.vote.eingeschraenkt.hinweis', {
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
              }
            />
          </div>
        )}
        {(!vote || vote.status === AbstimmungStatusType.Entwurf) && (
          <>
            {votingType === AbstimmungstypType.Ressortabstimmung && (
              <div className="warning-div">
                <Title level={3}>{t('enorm.vote.ressortAbstimmungWarningMessage.title1')}</Title>
                <HinweisComponent
                  title={t('enorm.vote.ressortAbstimmungWarningMessage.title2')}
                  mode="warning"
                  content={
                    <p
                      dangerouslySetInnerHTML={{
                        __html: t('enorm.vote.ressortAbstimmungWarningMessage.body', {
                          link: '/egesetzgebung-platform-backend/arbeitshilfen/download/34',
                          linkTitle: '§ 40 GGO',
                          interpolation: { escapeValue: false },
                        }),
                      }}
                    />
                  }
                />
              </div>
            )}

            {votingType === AbstimmungstypType.Hausabstimmung && !currentUserIsStellvertreter && (
              <PKPAnfrage sendRVanPKP={props.sendRVanPKP} setSendRVanPKP={props.setSendRVanPKP} />
            )}
          </>
        )}

        <FileUploadComponent
          documentType={documentType}
          setDocumentType={setDocumentType}
          disabled={votingRoundAndIsBearbeiten}
          version={version}
          isBearbeiten={isBearbeiten}
          fileList={fileList}
          setFileList={setFileList}
          additionalFileList={additionalFileList}
          setAdditionalFileList={setAdditionalFileList}
          editorDocIsDisabled={editorDocIsDisabled}
          onFormChange={() => {
            setIsLeavingPageBlocked(true);
          }}
        />
        {/* publication on the Internet */}
        {votingType !== AbstimmungstypType.AbstimmungDerVorlageFuerDenRegierungsentwurf && (
          <div className="internetIntranetSection">
            <div className="heading-holder">
              <Title level={2}>{t('enorm.vote.internetIntranetTitle')}</Title>
              <InfoComponent title={t('enorm.vote.internetIntranetTitle')}>
                <p
                  dangerouslySetInnerHTML={{
                    __html: t('enorm.vote.internetIntranetDrawerText', {
                      link: `${BASE_PATH}/arbeitshilfen/download/34#page=38`,
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
              </InfoComponent>
            </div>
            <p className="ant-typography p-no-style">{t('enorm.vote.internetIntranetDescription')}</p>
            <Form.Item>
              <Checkbox
                disabled={votingRoundAndIsBearbeiten}
                checked={internetValues.bkAmtUmEinvernehmenBitten}
                onChange={(e) => {
                  form.setFieldValue('bkAmtUmEinvernehmenBitten', e.target.checked);
                  setInternetValues({ ...internetValues, bkAmtUmEinvernehmenBitten: e.target.checked });
                }}
              >
                {t('enorm.vote.bkAmtUmEinvernehmenBittenText')}
                <InfoComponent title={t('enorm.vote.bkAmtUmEinvernehmenBittenText')}>
                  <p
                    dangerouslySetInnerHTML={{
                      __html: t('enorm.vote.bkAmtUmEinvernehmenBittenDrawerText', {
                        interpolation: { escapeValue: false },
                      }),
                    }}
                  />
                </InfoComponent>
              </Checkbox>
            </Form.Item>
            <Form.Item>
              <Checkbox
                disabled={votingRoundAndIsBearbeiten}
                checked={internetValues.teilnehmendeUmBenehmenBitten}
                onChange={(e) => {
                  form.setFieldValue('teilnehmendeUmBenehmenBitten', e.target.checked);
                  setInternetValues({ ...internetValues, teilnehmendeUmBenehmenBitten: e.target.checked });
                }}
              >
                {t('enorm.vote.teilnehmendeUmBenehmenBittenText')}
                <InfoComponent title={t('enorm.vote.teilnehmendeUmBenehmenBittenText')}>
                  <p
                    dangerouslySetInnerHTML={{
                      __html: t('enorm.vote.teilnehmendeUmBenehmenBittenDrawerText', {
                        interpolation: { escapeValue: false },
                      }),
                    }}
                  />
                </InfoComponent>
              </Checkbox>
            </Form.Item>

            {(compareOrValues(
              internetValues.bkAmtUmEinvernehmenBitten,
              internetValues.teilnehmendeUmBenehmenBitten,
            ) as boolean) && (
              <>
                <p className="ant-typography p-no-style">
                  {t('enorm.vote.internetIntranetVeroffentlichungDescription')}
                </p>
                <Form.Item
                  id="veroeffentlichungImInternet"
                  name="veroeffentlichungImInternet"
                  rules={[
                    {
                      required:
                        (internetValues.bkAmtUmEinvernehmenBitten === true &&
                          internetValues.veroeffentlichungImIntranet === false &&
                          internetValues.veroeffentlichungImInternet === false) ||
                        (internetValues.teilnehmendeUmBenehmenBitten === true &&
                          internetValues.veroeffentlichungImIntranet === false &&
                          internetValues.veroeffentlichungImInternet === false),
                      message: checkboxValidationMessage,
                    },
                    {
                      validator: publicationInternetIntranetRule,
                      validateTrigger: 'submit',
                    },
                  ]}
                >
                  <Checkbox
                    disabled={votingRoundAndIsBearbeiten}
                    checked={internetValues.veroeffentlichungImInternet}
                    onChange={(e) => {
                      form.setFieldValue('veroeffentlichungImInternet', e.target.checked);
                      setInternetValues({ ...internetValues, veroeffentlichungImInternet: e.target.checked });
                    }}
                  >
                    {t('enorm.vote.veroffentlichungInternetText')}
                  </Checkbox>
                </Form.Item>

                <Form.Item
                  name="veroeffentlichungImIntranet"
                  id="veroeffentlichungImIntranet"
                  rules={[
                    {
                      required:
                        (internetValues.bkAmtUmEinvernehmenBitten === true &&
                          internetValues.veroeffentlichungImInternet === false &&
                          internetValues.veroeffentlichungImIntranet === false) ||
                        (internetValues.teilnehmendeUmBenehmenBitten === true &&
                          internetValues.veroeffentlichungImInternet === false &&
                          internetValues.veroeffentlichungImIntranet === false),
                      message: checkboxValidationMessage,
                    },
                    {
                      validator: publicationInternetIntranetRule,
                      validateTrigger: 'submit',
                    },
                  ]}
                >
                  <Checkbox
                    disabled={votingRoundAndIsBearbeiten}
                    checked={internetValues.veroeffentlichungImIntranet}
                    onChange={(e) => {
                      form.setFieldValue('veroeffentlichungImIntranet', e.target.checked);
                      setInternetValues({ ...internetValues, veroeffentlichungImIntranet: e.target.checked });
                    }}
                  >
                    {t('enorm.vote.veroffentlichungIntranetText')}
                  </Checkbox>
                </Form.Item>
              </>
            )}
          </div>
        )}
        {/* Deadlines */}
        <DeadlinesComponent
          votingType={votingType}
          internetDateIsRequired={
            internetValues.bkAmtUmEinvernehmenBitten || internetValues.teilnehmendeUmBenehmenBitten
          }
          form={form}
          isBearbeiten={isBearbeiten}
          maxDate={vote?.oberabstimmung?.fristablauf}
          onFormChange={() => {
            setIsLeavingPageBlocked(true);
          }}
        />

        <Form.Item shouldUpdate>
          {() => (
            <MailComponent
              form={form}
              regelungsvorhaben={getSelectedRegelungsvorhaben()}
              oberabstimmung={vote?.oberabstimmung}
              currentUser={currentUser}
              autofill={autofill}
              mainDeadlineDate={ctrl.generateFristenString(
                form.getFieldValue('mainDeadlineDate') as Date,
                form.getFieldValue('mainDeadlineTime') as Date,
              )}
              replyInternetDate={ctrl.generateFristenString(
                form.getFieldValue('replyInternetDate') as Date,
                form.getFieldValue('replyInternetTime') as Date,
              )}
              replyDate={ctrl.generateFristenString(
                form.getFieldValue('replyDate') as Date,
                form.getFieldValue('replyTime') as Date,
              )}
              isBearbeiten={isBearbeiten}
              fileName={fileList[0]?.name}
              voteId={existingVoteId}
              isEditingVote={!!props.voteForEditing}
              version={vote?.version}
              isVorhabenclearing={votingType === AbstimmungstypType.Vorhabenclearing}
              isDraft={props.tabName === routes.ENTWUERFE}
              onFormChange={onFormValuesChanged}
            />
          )}
        </Form.Item>
        <FormButtonsComponent tabName={props.tabName} disableSubmit={!isLeavingPageBlocked && !savedDraft} />
        <ContinueLaterComponent
          isLeavingPageBlocked={isLeavingPageBlocked}
          modify={isBearbeiten}
          translationKey={'vote'}
          saveCurrentState={saveCurrentVoteState}
        />
      </GeneralFormWrapper>
    </LayoutVoteComponent>
  );
}

function setBreadcrumb(props: VoteBaseProps, vote: AbstimmungEntityDTO) {
  const headerController = GlobalDI.get<HeaderController>('enormHeadercontroller');
  const tabLink = (
    <Link id="enorm-vote-breadcrumbsTabNavigation-link" key={`enorm-${props.tabName}`} to={`/hra/${props.tabName}`}>
      <>
        {i18n.t(`enorm.breadcrumbs.projectName`)} - {i18n.t(`enorm.breadcrumbs.tabName.${props.tabName}`)}
      </>
    </Link>
  );
  const breadcrumbTitle = (
    <span key={`vote-${props.action}`}>
      <>
        {vote ? getAbstimmungsbezeichner(vote) + ': ' : undefined}
        {i18n.t(`enorm.breadcrumbs.action.${props.action}`)}
      </>
    </span>
  );
  headerController.setHeaderProps({
    headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[tabLink, breadcrumbTitle]} />],
    headerLast: [<EnormHelpBreadcrumb key="enormhelp-breadcrumb" />],
    headerCenter: [],
    headerRight: [],
  });
}
