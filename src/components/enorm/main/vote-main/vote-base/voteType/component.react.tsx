// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Radio } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Subscription } from 'rxjs';

import {
  AbstimmungControllerApi,
  AbstimmungstypType,
  BASE_PATH,
  LetzteHausabstimmungDTO,
  RegelungsvorhabenEntityResponseShortDTO,
} from '@plateg/rest-api';
import { GlobalDI, InfoComponent } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

import { GGO_LINK } from '../component.react';

export interface VoteTypeComponentProps {
  setAutofill: (autofill: boolean) => void;
  disabled?: boolean;
  isUnterabstimmung: boolean;
  setVotingType: (value: AbstimmungstypType) => void;
  regelungsvorhaben?: RegelungsvorhabenEntityResponseShortDTO;
  votingType?: AbstimmungstypType;
}

export function VoteTypeComponent(props: VoteTypeComponentProps): React.ReactElement {
  const { t } = useTranslation();

  const abstimmungController = GlobalDI.getOrRegister<AbstimmungControllerApi>(
    'enormController',
    () => new AbstimmungControllerApi(),
  );
  const [letzteHausabstimmung, setLetzteHausabstimmung] = useState<LetzteHausabstimmungDTO | null>();

  useEffect(() => {
    let sub: Subscription;
    if (props.regelungsvorhaben) {
      sub = abstimmungController.getLetzteHausabstimmung({ rvId: props.regelungsvorhaben?.base.id }).subscribe({
        next: (data) => {
          setLetzteHausabstimmung(data);
        },
      });
    }
    return () => {
      sub?.unsubscribe();
    };
  }, [props.regelungsvorhaben]);
  return (
    <Form.Item
      className="form-item-vote-type"
      hidden={props.isUnterabstimmung}
      name="typ"
      label={<span>{t('enorm.vote.typeOfVote.label')}</span>}
      rules={[{ required: true, whitespace: true, message: t('enorm.vote.typeOfVote.error') }]}
    >
      <fieldset>
        <Radio.Group
          disabled={props.disabled}
          onChange={(e) => {
            props.setAutofill(true);
            props.setVotingType(e.target.value as AbstimmungstypType);
          }}
          className="horizontal-radios"
          name="typ"
          defaultValue={props.votingType}
        >
          <div className="radio-info-item-div">
            <Radio
              id="enorm-typeOfVoteHomeVote-radio"
              value={AbstimmungstypType.Hausabstimmung}
              disabled={props.isUnterabstimmung || props.disabled}
              aria-hidden={props.isUnterabstimmung}
            >
              {t('enorm.vote.typeOfVote.typeHomeVote')}
              {letzteHausabstimmung && (
                <>
                  <br />
                  <span className="ha-ended-info-div">
                    {`${t('enorm.vote.typeOfVote.homeVoteInfo.endingInfo')}  
            ${Filters.dateFromString(letzteHausabstimmung?.fristablauf)} · 
            ${Filters.timeFromString(letzteHausabstimmung?.fristablauf)}
            `}
                  </span>
                </>
              )}
            </Radio>
            <span className={letzteHausabstimmung ? 'ha-info-component' : ''}>
              <InfoComponent
                title={t(`enorm.vote.typeOfVote.homeVoteInfo.title`)}
                disabled={props.isUnterabstimmung || props.disabled}
              >
                <p>{t(`enorm.vote.typeOfVote.homeVoteInfo.text`)}</p>
              </InfoComponent>
            </span>
          </div>

          <div className="radio-info-item-div">
            <Radio
              value={AbstimmungstypType.Vorhabenclearing}
              disabled={props.isUnterabstimmung || props.disabled}
              aria-hidden={props.isUnterabstimmung}
            >
              {t('enorm.vote.typeOfVote.typeVorhabenclearing')}
            </Radio>
            <InfoComponent
              title={t(`enorm.vote.typeOfVote.vorhabenClearingInfo.title`)}
              disabled={props.isUnterabstimmung || props.disabled}
            >
              <p>{t(`enorm.vote.typeOfVote.vorhabenClearingInfo.text1`)}</p>
              <p>{t(`enorm.vote.typeOfVote.vorhabenClearingInfo.text2`)}</p>
            </InfoComponent>
          </div>

          <div className="radio-info-item-div">
            <Radio
              id="enorm-typeOfVoteDepartmentVote-radio"
              value={AbstimmungstypType.Ressortabstimmung}
              disabled={props.isUnterabstimmung || props.disabled}
              aria-hidden={props.isUnterabstimmung}
            >
              {t('enorm.vote.typeOfVote.typeDepartmentVote')}
            </Radio>
            <InfoComponent
              title={t(`enorm.vote.typeOfVote.departmentInfo.title`)}
              disabled={props.isUnterabstimmung || props.disabled}
            >
              <p
                dangerouslySetInnerHTML={{
                  __html: t('enorm.vote.typeOfVote.departmentInfo.text1', {
                    linkGGO_45_1: `${BASE_PATH}/arbeitshilfen/download/34#page=36`,
                    linkGGO_45_3: `${BASE_PATH}/arbeitshilfen/download/34#page=36`,
                    linkGGO_74_5: `${BASE_PATH}/arbeitshilfen/download/34#page=55`,
                    linkGGO_21_1: `${BASE_PATH}/arbeitshilfen/download/34#page=17`,
                    linkGGO_24_1: `${BASE_PATH}/arbeitshilfen/download/34#page=19`,
                    interpolation: { escapeValue: false },
                  }),
                }}
              ></p>
              <p>{t(`enorm.vote.typeOfVote.departmentInfo.text2`)}</p>
            </InfoComponent>
          </div>
          <div className="radio-info-item-div">
            <Radio
              value={AbstimmungstypType.Schlussabstimmung}
              disabled={props.isUnterabstimmung || props.disabled}
              aria-hidden={props.isUnterabstimmung}
            >
              {t('enorm.vote.typeOfVote.typeFinalVote')}
            </Radio>
            <InfoComponent
              title={t(`enorm.vote.typeOfVote.finalVoteInfo.title`)}
              disabled={props.isUnterabstimmung || props.disabled}
            >
              <p
                dangerouslySetInnerHTML={{
                  __html: t('enorm.vote.typeOfVote.finalVoteInfo.text1', {
                    interpolation: { escapeValue: false },
                    link: GGO_LINK + '37',
                  }),
                }}
              />

              <p>{t(`enorm.vote.typeOfVote.finalVoteInfo.text2`)}</p>
            </InfoComponent>
          </div>
          <div className="radio-info-item-div">
            <Radio
              value={AbstimmungstypType.AbstimmungDerVorlageFuerDenRegierungsentwurf}
              disabled={props.isUnterabstimmung || props.disabled}
              aria-hidden={props.isUnterabstimmung}
            >
              {t('enorm.vote.typeOfVote.typeHouseManagementVote')}
            </Radio>
            <InfoComponent
              title={t(`enorm.vote.typeOfVote.houseManagementVoteInfo.title`)}
              disabled={props.isUnterabstimmung || props.disabled}
            >
              <p>{t(`enorm.vote.typeOfVote.houseManagementVoteInfo.text1`)}</p>
            </InfoComponent>
          </div>
          {props.isUnterabstimmung && (
            <Radio
              id="enorm-typeOfVoteSubVote-radio"
              value={AbstimmungstypType.Unterabstimmung}
              disabled={!props.isUnterabstimmung || props.disabled}
              aria-hidden={!props.isUnterabstimmung}
            >
              {t('enorm.vote.typeOfVote.typeNestedVote')}
              <InfoComponent
                title={t(`enorm.vote.typeOfVote.nestedVoteInfo.title`)}
                disabled={!props.isUnterabstimmung || props.disabled}
              >
                <p
                  dangerouslySetInnerHTML={{
                    __html: t('enorm.vote.typeOfVote.nestedVoteInfo.text', {
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
                <p
                  dangerouslySetInnerHTML={{
                    __html: t('enorm.vote.typeOfDocument.UnterabstimmungInfo', {
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
              </InfoComponent>
            </Radio>
          )}
          <div className="radio-info-item-div">
            <Radio
              id="enorm-typeOfVoteOtherVote-radio"
              value={AbstimmungstypType.SonstigeAbstimmung}
              disabled={props.isUnterabstimmung || props.disabled}
              aria-hidden={props.isUnterabstimmung}
            >
              {t('enorm.vote.typeOfVote.typeOtherVote')}
            </Radio>
            <InfoComponent title={t(`enorm.vote.typeOfVote.otherVoteInfo.title`)} disabled={props.disabled}>
              <p
                dangerouslySetInnerHTML={{
                  __html: t('enorm.vote.typeOfVote.otherVoteInfo.text', {
                    interpolation: { escapeValue: false },
                  }),
                }}
              />
            </InfoComponent>
          </div>
        </Radio.Group>
      </fieldset>
    </Form.Item>
  );
}
