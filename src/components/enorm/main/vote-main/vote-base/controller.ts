// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { UploadFile } from 'antd/lib/upload/interface';
import { zonedTimeToUtc } from 'date-fns-tz';
import lightFormat from 'date-fns/lightFormat';
import i18n from 'i18next';
import { forkJoin, Observable, of } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';
import { map, shareReplay, switchMap } from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';

import {
  AbstimmungControllerApi,
  AbstimmungEntityDTO,
  AbstimmungEntityResponseDTO,
  EditorRemoteControllerApi,
  RegelungsvorhabenControllerApi,
} from '@plateg/rest-api';
import {
  AbstimmungStatusType,
  AbstimmungstypType,
  DokumentenmappeDTO,
  FristEntityDTO,
  FristtypType,
  RegelungsvorhabenEntityListResponseShortDTO,
  RegelungsvorhabenEntityResponseShortDTO,
  UserEntityResponseDTO,
  UserEntityWithStellvertreterResponseDTO,
} from '@plateg/rest-api/models';
import { displayMessage, ErrorController, GlobalDI, LoadingStatusController } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { ManageFilesController } from '../../../../../utils/controllerManageFiles';
import { VoteFormValues } from './component.react';

export interface Files {
  files: UploadFile[];
  additionalFiles: UploadFile[];
}

interface VoteInfoInterface {
  vote: AbstimmungEntityDTO;
  existingVoteId: string | null;
  deadlineExceeded: boolean;
  idForLoadingFiles?: string;
}
export interface VoteInitialDataInterface {
  regelungsvorhabenList: Array<RegelungsvorhabenEntityResponseShortDTO>;
  currentUser: UserEntityResponseDTO;
  voteInfo: VoteInfoInterface;
}

interface InitalDataResponseInterface {
  voteData: VoteInitialDataInterface;
  filesData: Files;
}

interface FilesDataInterface {
  fileList: UploadFile[];
  initalFileList: UploadFile[];
  additionalFileList: UploadFile[];
  initalAdditionalFileList: UploadFile[];
}

export interface SaveVoteResponseInterface {
  voteId: string;
  filesData: any;
}

export class VoteBaseController {
  private readonly regelungsvorhabenController = GlobalDI.getOrRegister<RegelungsvorhabenControllerApi>(
    'regelungsvorhabenController',
    () => new RegelungsvorhabenControllerApi(),
  );
  private readonly enormController = GlobalDI.getOrRegister<AbstimmungControllerApi>(
    'enormController',
    () => new AbstimmungControllerApi(),
  );
  private readonly loadingStatusController = GlobalDI.getOrRegister<LoadingStatusController>(
    'loadingStatusController',
    () => new LoadingStatusController(),
  );

  private readonly manageFilesController = GlobalDI.getOrRegister(
    'enormManageFilesController',
    () => new ManageFilesController(),
  );
  private readonly errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());

  private readonly editorCtrl = GlobalDI.getOrRegister<EditorRemoteControllerApi>(
    'editorRemoteControllerApi',
    () => new EditorRemoteControllerApi(),
  );

  public getVoteDTO(
    voteId: string | null,
    isVotingRound: boolean,
    isUnterabstimmung: boolean,
  ): Observable<VoteInfoInterface> {
    // if don't have an id - this is a new vote
    if (!voteId) {
      return of({} as VoteInfoInterface);
    }

    const voteObservable: Observable<AbstimmungEntityResponseDTO> = (() => {
      if (isVotingRound) {
        return this.enormController.getAbstimmungsentwurf({ metadataId: voteId });
      }
      if (isUnterabstimmung) {
        return this.enormController.getUnterabstimmungsentwurf({ metadataId: voteId });
      }
      return this.enormController.getAbstimmung({ id: voteId });
    })();
    return voteObservable.pipe(
      map((voteDTO: AbstimmungEntityResponseDTO) => {
        return this.prepareVoteData(voteDTO, voteId, isVotingRound, isUnterabstimmung);
      }),
    );
  }

  public prepareVoteData(
    voteDTO: AbstimmungEntityResponseDTO,
    voteId: string,
    isVotingRound: boolean,
    isUnterabstimmung: boolean,
  ): VoteInfoInterface {
    const voteData: AbstimmungEntityDTO = voteDTO.dto;
    const initialData: VoteInfoInterface = {} as VoteInfoInterface;

    initialData['vote'] = voteData;

    if (isVotingRound || isUnterabstimmung) {
      initialData['existingVoteId'] = voteDTO?.base.id;
      if (isVotingRound) {
        initialData['deadlineExceeded'] =
          new Date(
            voteData?.fristen?.find((frist) => frist.fristTyp === FristtypType.Abstimmungsende)?.fristablauf || '',
          ) < new Date(Date.now());
      }
    } else {
      initialData['existingVoteId'] = voteId;
    }

    if ((isVotingRound || isUnterabstimmung) && voteDTO?.base.id) {
      initialData['idForLoadingFiles'] = voteDTO?.base.id;
    }
    if (!isVotingRound && !isUnterabstimmung) {
      initialData['idForLoadingFiles'] = voteId;
    }
    return initialData;
  }

  public fetchInitialData(
    voteId: string | null,
    isVotingRound: boolean,
    existingVoteId: string | null,
    currentUser: UserEntityWithStellvertreterResponseDTO,
    isUnterabstimmung?: boolean,
    isDraft?: boolean,
  ): Observable<InitalDataResponseInterface> {
    const getRequestList = (exisitingVoteId: string | null, isDraft?: boolean) => ({
      voteData: this.getVoteDTO(voteId, isVotingRound, isUnterabstimmung ?? false),
      ...(exisitingVoteId && !isDraft ? {} : { regelungsvorhabenListDTO: this.getRegelungsvorhabenListCall() }),
    });

    const preparedData: VoteInitialDataInterface = {} as VoteInitialDataInterface;
    const observerFetch = forkJoin(getRequestList(existingVoteId, isDraft)).pipe(
      map(({ voteData, regelungsvorhabenListDTO }) => {
        if (regelungsvorhabenListDTO) {
          preparedData['regelungsvorhabenList'] = [...regelungsvorhabenListDTO.dtos].sort(
            (o1, o2) => new Date(o2.dto.bearbeitetAm).valueOf() - new Date(o1.dto.bearbeitetAm).valueOf(),
          );
        }
        preparedData['currentUser'] = currentUser;
        preparedData['voteInfo'] = voteData;
        return preparedData;
      }),
      switchMap((data) => {
        return forkJoin({
          voteData: of(data),
          filesData: data.voteInfo.idForLoadingFiles
            ? this.manageFilesController.getFilesCall(data.voteInfo.idForLoadingFiles)
            : of({ files: [], additionalFiles: [] }),
        });
      }),
      shareReplay(1),
    );
    observerFetch.subscribe({ error: this.errorCb });
    return observerFetch;
  }

  public saveVote(
    dto: AbstimmungEntityDTO,
    existingVoteId: string | null,
    filesData: FilesDataInterface,
  ): Observable<SaveVoteResponseInterface> {
    let observable: Observable<AbstimmungEntityResponseDTO>;
    if (existingVoteId != null) {
      // Update exisiting vote
      observable = this.modifyVote(existingVoteId, dto);
    } else {
      // Create a new vote
      observable = this.createVote(dto);
    }

    const saveVoteObservable = observable.pipe(
      map((data: AbstimmungEntityResponseDTO) => {
        return data.base.id;
      }),
      switchMap((voteId) => {
        const listOfuploadFilesObservables: Observable<any>[] =
          this.manageFilesController.prepareListOfuploadFilesObservables(
            voteId,
            filesData.fileList,
            filesData.initalFileList,
            filesData.additionalFileList,
            filesData.initalAdditionalFileList,
          );

        const requestsList = {
          voteId: of(voteId),
          filesData: listOfuploadFilesObservables.length ? forkJoin(listOfuploadFilesObservables) : of(null),
        };

        return forkJoin(requestsList);
      }),
      shareReplay(1),
    );
    saveVoteObservable.subscribe({
      error: (error: AjaxError) => {
        const response = error.response as { status: number };
        if (response.status === 1022) {
          displayMessage(i18n.t('enorm.vote.errorParticipant'), 'error');
        } else if (response.status === 1023) {
          displayMessage(i18n.t('enorm.vote.errorParticipantCC'), 'error');
        } else if (response.status === 400) {
          displayMessage(i18n.t('enorm.vote.emailAddress.errorShouldBeEmail'), 'error');
        } else if (response.status === 1002) {
          displayMessage(i18n.t('enorm.vote.errorDraftDeleted'), 'error');
        } else if (response.status === 1008) {
          displayMessage(i18n.t('enorm.vote.errorRvInactive'), 'error');
        } else if (response.status > 0) {
          this.errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
        }
        console.error('error vote creating', error);
        this.loadingStatusController.setLoadingStatus(false);
      },
    });
    return saveVoteObservable;
  }

  public prepareVoteForSubmit(formValues: VoteFormValues): AbstimmungEntityDTO {
    formValues.fristen = this.generateFristen(formValues);
    formValues.fruehkoordinierungBkAmtErfolgtAm = formValues.earlyVoteBundeskanzleramtDate
      ? zonedTimeToUtc(formValues.earlyVoteBundeskanzleramtDate, 'Europe/Berlin').toISOString()
      : undefined;
    formValues.ressortTeilnehmer = formValues.ressortTeilnehmer || [];
    return { ...(formValues as AbstimmungEntityDTO) };
  }

  public loadFilesById(id: string): Observable<Files> {
    return this.manageFilesController.getFilesCall(id);
  }

  public createVote(vote: AbstimmungEntityDTO): Observable<AbstimmungEntityResponseDTO> {
    const body = { abstimmungEntityDTO: vote };
    return this.enormController.createAbstimmung(body);
  }

  public getVote(id: string): Observable<AbstimmungEntityResponseDTO> {
    return this.enormController.getAbstimmung({ id });
  }

  public getVotingRound(id: string): Observable<AbstimmungEntityResponseDTO> {
    return this.enormController.getAbstimmungsentwurf({ metadataId: id });
  }

  public getRegelungsvorhabenListCall(): Observable<RegelungsvorhabenEntityListResponseShortDTO> {
    return this.regelungsvorhabenController.getRegelungsvorhabenShortList();
  }

  public getRegelungsentwurfCall(dokumentenmappeId: string | undefined): Observable<DokumentenmappeDTO | undefined> {
    let obs: Observable<DokumentenmappeDTO | undefined>;
    if (dokumentenmappeId) {
      obs = this.editorCtrl.getDokumentenmappeById({ id: dokumentenmappeId });
    } else {
      obs = of(undefined);
    }
    return obs;
  }

  public modifyVote(id: string, vote: AbstimmungEntityDTO): Observable<AbstimmungEntityResponseDTO> {
    const body = {
      id,
      abstimmungEntityDTO: vote,
    };
    return this.enormController.modifyAbstimmung(body);
  }

  public generateFristen = (formValues: VoteFormValues): FristEntityDTO[] => {
    const fristen = [];
    fristen.push({
      fristTyp: FristtypType.Abstimmungsende,
      titel: 'Frist Ende der Abstimmungsrunde',
      fristablauf: this.generateFristenString(formValues?.mainDeadlineDate, formValues?.mainDeadlineTime),
    });
    if (
      formValues?.replyInternetDate &&
      formValues?.replyInternetTime &&
      (formValues?.bkAmtUmEinvernehmenBitten || formValues?.teilnehmendeUmBenehmenBitten)
    ) {
      fristen.push({
        fristTyp: FristtypType.VeroeffentlichungImInternet,
        titel: 'Frist Rückmeldung zur Veröffentlichung im Internet/Intranet',
        fristablauf: this.generateFristenString(formValues?.replyInternetDate, formValues?.replyInternetTime),
      });
    }
    if (formValues?.replyDate && formValues?.replyTime) {
      fristen.push({
        fristTyp: FristtypType.ZuleitungAnLaenderUndVerbaende,
        titel: 'Frist Rückmeldung zur frühzeitigen Beteiligung der Länder und Verbände',
        fristablauf: this.generateFristenString(formValues?.replyDate, formValues?.replyTime),
      });
    }
    formValues?.additionalFristList?.forEach((item) => {
      fristen.push({
        fristTyp: FristtypType.ZusaetzlicheFrist,
        titel: item.additionalDeadlineTitle,
        fristablauf: this.generateFristenString(item.additionalDeadlineDate, item.additionalDeadlineTime),
      });
    });
    return fristen as FristEntityDTO[];
  };

  public generateFristenString = (date?: Date, time?: Date): string | undefined => {
    if (!date && !time) {
      return undefined;
    }
    let utcDate;
    try {
      const dateFormatted = lightFormat(date || new Date(), 'yyyy-MM-dd');
      const timeFormatted = lightFormat(time || new Date(1900, 0, 1, 18), 'HH:mm:ss');
      const localDateTime = `${dateFormatted} ${timeFormatted}`;
      utcDate = zonedTimeToUtc(localDateTime, 'Europe/Berlin').toISOString();
    } catch (e) {
      return undefined;
    }
    return utcDate;
  };

  public prepareFristen = (formValues: VoteFormValues, dto: AbstimmungEntityDTO): VoteFormValues => {
    formValues.additionalFristList = [];
    dto?.fristen?.forEach((item) => {
      if (item.fristTyp == FristtypType.Abstimmungsende && item.fristablauf) {
        const fristMoment = new Date(item.fristablauf);
        formValues.mainDeadlineDate = fristMoment;
        formValues.mainDeadlineTime = fristMoment;
      }
      if (
        item.fristTyp == FristtypType.VeroeffentlichungImInternet &&
        item.fristablauf &&
        (dto.bkAmtUmEinvernehmenBitten || dto.teilnehmendeUmBenehmenBitten)
      ) {
        const fristMoment = new Date(item.fristablauf);
        formValues.replyInternetDate = fristMoment;
        formValues.replyInternetTime = fristMoment;
      }
      if (item.fristTyp == FristtypType.ZuleitungAnLaenderUndVerbaende && item.fristablauf) {
        const fristMoment = new Date(item.fristablauf);
        formValues.replyDate = fristMoment;
        formValues.replyTime = fristMoment;
      }
      if (item.fristTyp == FristtypType.ZusaetzlicheFrist) {
        let dateTime;
        if (item.fristablauf) {
          dateTime = new Date(item.fristablauf);
        }
        formValues.additionalFristList.push({
          id: uuidv4(),
          additionalDeadlineTitle: item.titel || undefined,
          additionalDeadlineDate: dateTime,
          additionalDeadlineTime: dateTime,
          fristTyp: FristtypType.ZusaetzlicheFrist,
        });
      }
    });
    formValues.earlyVoteBundeskanzleramtDate = dto.fruehkoordinierungBkAmtErfolgtAm
      ? new Date(dto.fruehkoordinierungBkAmtErfolgtAm)
      : formValues.earlyVoteBundeskanzleramtDate;
    return formValues;
  };

  public prepareFinishUrl(action: string, tabName: string): string {
    const url =
      action === routes.NACHSTE_ABSTIMMUNGSRUNDE ? routes.ABSTIMMUNGSRUNDE_PRUEFEN : routes.ABSTIMMUNG_PRUEFEN;

    return `/hra/${tabName}/${url}`;
  }

  public getInitialValues(
    action: string,
    isBearbeiten: boolean,
    vote?: AbstimmungEntityDTO,
  ): {
    initialValues: AbstimmungEntityDTO;
    votingRound: boolean;
    votingRoundAndIsBearbeiten: boolean;
    isUnterabstimmung: boolean;
  } {
    const votingRound = action === routes.NACHSTE_ABSTIMMUNGSRUNDE;
    const isUnterabstimmung =
      action === routes.UNTERABSTIMMUNG_ANLEGEN || vote?.typ === AbstimmungstypType.Unterabstimmung;
    return {
      votingRound,
      isUnterabstimmung,
      initialValues: {
        teilnehmer: [],
        teilnehmerInCc: [],
        fristen: [],
        status: AbstimmungStatusType.Entwurf,
        version: 1,
        verschweigensfristGekennzeichnet: false,
        eingeschraenkt: false,
      },
      votingRoundAndIsBearbeiten: votingRound || isBearbeiten,
    };
  }

  private readonly errorCb = (error: AjaxError) => {
    console.error('Error sub', error);
    this.loadingStatusController.setLoadingStatus(false);
    if ((error.response as { status: number })?.status === 1008) {
      displayMessage(i18n.t('enorm.vote.errorRvInactive'), 'error');
    } else {
      this.errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
    }
  };
}
