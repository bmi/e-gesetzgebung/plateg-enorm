// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Radio } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { DocumentType } from '@plateg/rest-api';
import { FormItemWithInfo, InfoComponent } from '@plateg/theme';

interface DocumentTypeProps {
  setDocumentType?: (type: DocumentType) => void;
  disabled: boolean;
  documentType?: DocumentType;
}
export function DocumentTypeComponent(props: DocumentTypeProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <FormItemWithInfo
      name="documentTyp"
      label={
        <span>
          {t('enorm.vote.typeOfDocument.label')}
          <InfoComponent withLabelRequired title={t('enorm.vote.typeOfDocument.label')}>
            <p>{t('enorm.vote.typeOfDocument.UnterabstimmungInfo')}</p>
          </InfoComponent>
        </span>
      }
      initialValue={DocumentType.Editor}
      rules={[{ required: true, message: t('enorm.vote.typeOfDocument.error') }]}
    >
      <fieldset>
        <Radio.Group
          disabled={props.disabled}
          onChange={(event) => {
            if (props.setDocumentType) {
              props.setDocumentType(event?.target.value as DocumentType);
            }
          }}
          className="horizontal-radios"
          name="documentTyp"
          defaultValue={props.documentType}
        >
          <Radio id="enorm-editorDocument-radio" value={DocumentType.Editor}>
            {t('enorm.vote.typeOfDocument.typeEditorDocument')}
          </Radio>
          <Radio id="enorm-enormDocument-radio" value={DocumentType.Enorm}>
            {t('enorm.vote.typeOfDocument.typeEnormDocument')}
          </Radio>
        </Radio.Group>
      </fieldset>
    </FormItemWithInfo>
  );
}
