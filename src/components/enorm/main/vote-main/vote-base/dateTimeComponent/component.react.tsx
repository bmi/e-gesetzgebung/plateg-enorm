// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Checkbox, Col, Form, Input, Row } from 'antd';
import { FormInstance } from 'antd/lib/form';
import { format, isAfter, set } from 'date-fns';
import endOfDay from 'date-fns/endOfDay';
import isBefore from 'date-fns/isBefore';
import isSameDay from 'date-fns/isSameDay';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { CustomDatepicker, getDateTimeString, InfoComponent } from '@plateg/theme';

import { VoteFormValues } from '../component.react';

interface DateTimeComponentProps {
  aliasDate: string[] | string;
  aliasTime: string[] | string;
  required: boolean;
  form: FormInstance;
  identifier: string;
  isBearbeiten: boolean;
  index?: number;
  maxDate?: string;
  minDate?: string;
  onFormChange?: () => void;
}

const TIME_REGEX = /^([01]\d|2[0-3]):([0-5]\d)$/;

export function DateTimeComponent(props: DateTimeComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [date, setDate] = useState<Date | null>();
  const [isDienstschlussSelected, setIsDienstschlussSelected] = useState<boolean>(false);
  const [isVerschweigensfrist, setIsVerschweigensfrist] = useState<boolean>(
    props.form.getFieldValue('verschweigensfristGekennzeichnet'),
  );
  const ref = useRef<HTMLDivElement>(null);
  const timePlaceholderName =
    typeof props.aliasTime === 'string' ? `${props.aliasTime}-placeholder` : `${props.aliasTime.join('-')}-placeholder`;

  // Prefill time field
  useEffect(() => {
    if (props.form.getFieldValue(props.aliasTime)) {
      const values = props.form.getFieldsValue(true) as VoteFormValues;
      const time = props.form.getFieldValue(props.aliasTime) as Date;
      // eslint-disable-next-line
      values[timePlaceholderName] = format(time, 'HH:mm');
      props.form.setFieldsValue(values);
    }
  }, [props.form.getFieldValue(props.aliasTime)]);

  useEffect(() => {
    if (isDienstschlussSelected) {
      const timeDienstSchluss = new Date();
      timeDienstSchluss.setHours(18, 0);
      const onlyTime = format(timeDienstSchluss, 'HH:mm');
      props.form.setFieldValue(props.aliasTime, prepareSelectedTime(onlyTime));
      props.form.setFieldValue(timePlaceholderName, onlyTime);
    }
  }, [isDienstschlussSelected]);

  // Gets value that should be checked and returns boolean
  const disabledDate = (possibleDate: Date) => {
    const current = new Date();
    return (
      isBefore(endOfDay(possibleDate), endOfDay(current)) ||
      (props.minDate && isBefore(endOfDay(possibleDate), endOfDay(new Date(props.minDate))))
    );
  };

  const errorTextDate = t('enorm.vote.replyDate.errorDate');
  const errorTextTime = t('enorm.vote.replyDate.errorTime');
  const errorTextMax = t(`enorm.vote.${props.identifier}.errorTimeAfterMax`, {
    date: props.maxDate ? getDateTimeString(props.maxDate) : '',
  });
  const errorTextMin = t(`enorm.vote.${props.identifier}.errorTimeBeforeMin`, {
    date: props.minDate ? getDateTimeString(props.minDate) : '',
  });
  const errorTextTimeFormat = t(`enorm.vote.${props.identifier}.errorTimeFormat`, { index: props.index });
  const errorTextDateFormat = t(`enorm.vote.${props.identifier}.errorDateFormat`, { index: props.index });
  const errorTextTimeFuture = t(`enorm.vote.${props.identifier}.errorTimeFuture`, { index: props.index });

  const manageTimeFields = (value: string) => {
    if (TIME_REGEX.test(value)) {
      props.form.setFields([{ name: props.aliasTime, value: prepareSelectedTime(value) }]);
    } else {
      props.form.resetFields([props.aliasTime]);
    }
  };

  const isTimeDisabled = (value: string) => {
    const current = new Date();
    const result = prepareSelectedTime(value);
    if (!date) {
      return false;
    }
    if (
      (isSameDay(date, current) && isBefore(result, current)) ||
      isBefore(date.setHours(0, 0, 0, 0), current.setHours(0, 0, 0, 0))
    ) {
      return true;
    } else {
      return false;
    }
  };

  const isAfterCustomEnd = (value: string, isTimeValue: boolean, customEnd?: string) => {
    const result = isTimeValue
      ? prepareSelectedTime(value)
      : new Date(
          (props.form.getFieldsValue(true) as VoteFormValues)[props.aliasTime as keyof VoteFormValues] as string,
        );
    return customEnd && date
      ? isAfter(set(date, { hours: result.getHours(), minutes: result.getMinutes() }), new Date(customEnd))
      : false;
  };

  const isBeforeCustomMin = (value: string, customMin?: string) => {
    const time = prepareSelectedTime(value);
    return customMin && date
      ? isBefore(set(date, { hours: time.getHours(), minutes: time.getMinutes() }), new Date(customMin))
      : false;
  };

  const prepareSelectedTime = (value: string) => {
    const parsedDate = value.split(':');
    return set(new Date(), { hours: Number(parsedDate[0]), minutes: Number(parsedDate[1]), seconds: 0 });
  };

  return (
    <Row ref={ref} gutter={8} className="date-time-holder">
      <Col span={12}>
        <CustomDatepicker
          onFormChange={props.onFormChange}
          disabledDate={disabledDate}
          setDate={setDate}
          getPopupContainer={() => ref.current as HTMLElement}
          aliasDate={props.aliasDate}
          aliasTime={props.aliasTime}
          form={props.form}
          required={props.required}
          timePlaceholderName={timePlaceholderName}
          additionalValidators={[
            //check if val is before maxDate
            {
              // eslint-disable-next-line @typescript-eslint/no-misused-promises
              validator: (_rule, value: string) => {
                if (value && isAfterCustomEnd(value, false, props.maxDate)) {
                  return Promise.reject(errorTextDate);
                } else {
                  return Promise.resolve();
                }
              },
              message: errorTextMax,
              validateTrigger: 'submit',
            },
          ]}
          errorRequired={t(`enorm.vote.${props.identifier}.errorDate`, { index: props.index })}
          errorDateTime={errorTextDate}
          errorInvalidFormat={errorTextDateFormat}
          errorDisabledDate={errorTextTimeFuture}
        />
      </Col>
      <Col span={12}>
        <Form.Item
          shouldUpdate
          className="time-field"
          name={timePlaceholderName}
          dependencies={[props.aliasDate]}
          label={<span>{t('enorm.vote.deadlineVote.timeLabel')}</span>}
          rules={[
            {
              required: props.required,
              message: t(`enorm.vote.${props.identifier}.errorTime`, { index: props.index }),
              validateTrigger: 'submit',
            },
            {
              // Überprüft, dass auch eine Uhrzeit eingegeben wird, wenn ein Datum eingegeben wurde
              validator: () => {
                if (
                  !props.required && // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
                  props.form.getFieldValue(`${props.aliasDate}-date-placeholder`) !== undefined &&
                  // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
                  props.form.getFieldValue(`${props.aliasDate}-date-placeholder`) !== '' &&
                  (props.form.getFieldValue(timePlaceholderName) === '' ||
                    props.form.getFieldValue(timePlaceholderName) === undefined)
                ) {
                  return Promise.reject(errorTextTime);
                } else {
                  return Promise.resolve();
                }
              },
              message: errorTextTime,
              validateTrigger: 'submit',
            },
            {
              // Validate format on submit
              validator: (_rule, value: string) => {
                if (value && !TIME_REGEX.test(value)) {
                  return Promise.reject(errorTextTimeFormat);
                } else {
                  return Promise.resolve();
                }
              },
              validateTrigger: 'submit',
            },
            {
              // Validate time should be in future
              validator: (_rule, value: string) => {
                if (value && value.length > 4 && isTimeDisabled(value)) {
                  return Promise.reject(errorTextTimeFuture);
                } else {
                  return Promise.resolve();
                }
              },
              validateTrigger: 'submit',
            },
            //check if val is before maxDate
            {
              validator: (_rule, value: string) => {
                if (value && value.length > 4 && isAfterCustomEnd(value, true, props.maxDate)) {
                  return Promise.reject(errorTextDate);
                } else {
                  return Promise.resolve();
                }
              },
              message: errorTextMax,
              validateTrigger: 'submit',
            },
            //check if val is after end of min date
            {
              validator: (_rule, value: string) => {
                if (value && value.length > 4 && props.minDate && isBeforeCustomMin(value, props.minDate)) {
                  return Promise.reject();
                } else {
                  return Promise.resolve();
                }
              },
              message: errorTextMin,
              validateTrigger: 'submit',
            },
          ]}
        >
          <Input
            type="text"
            onChange={(e) => {
              //need to manually validate because currently bidirectional dependency doesnt work
              manageTimeFields(e.target.value);
            }}
            placeholder={isDienstschlussSelected ? '18:00' : 'Zeit eingeben'}
            disabled={isDienstschlussSelected}
          />
        </Form.Item>
      </Col>
      {(props.aliasTime === 'mainDeadlineTime' || props.aliasTime === 'fristTime') && (
        <>
          <Form.Item>
            <Checkbox onChange={() => setIsDienstschlussSelected(!isDienstschlussSelected)}>
              {t('enorm.vote.deadlineVote.checkboxTimeLabel')}
            </Checkbox>
          </Form.Item>
          <Form.Item>
            <Checkbox
              checked={isVerschweigensfrist}
              disabled={props.isBearbeiten}
              onChange={(e) => {
                props.form.setFieldValue('verschweigensfristGekennzeichnet', e.target.checked);
                setIsVerschweigensfrist(e.target.checked);
              }}
            >
              {t('enorm.vote.deadlineVote.checkboxVerschweigensfristLabel')}
              <InfoComponent title={t('enorm.vote.deadlineVote.checkboxVerschweigensfristLabel')}>
                <p
                  dangerouslySetInnerHTML={{
                    __html: t('enorm.vote.deadlineVote.checkboxVerschweigensfristDrawerText', {
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
              </InfoComponent>
            </Checkbox>
          </Form.Item>
        </>
      )}
    </Row>
  );
}
