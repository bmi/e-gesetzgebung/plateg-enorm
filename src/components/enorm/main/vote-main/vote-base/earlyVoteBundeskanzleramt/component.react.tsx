// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './earlyVoteBundeskanzleramt.less';

import { Col, FormInstance, Radio, Row } from 'antd';
import { endOfDay, isAfter } from 'date-fns';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { AbstimmungEntityDTO, BASE_PATH } from '@plateg/rest-api';
import { CustomDatepicker, InfoComponent } from '@plateg/theme';

export interface EarlyVoteBundeskanzleramtComponentProps {
  form: FormInstance;
  vote?: AbstimmungEntityDTO;
  onFormChange?: () => void;
}

export function EarlyVoteBundeskanzleramtComponent(props: EarlyVoteBundeskanzleramtComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [isErfolgt, setIsErfolgt] = useState(false);
  const [_, setDate] = useState<Date | null>();
  const ref = useRef<HTMLDivElement>(null);
  useEffect(() => {
    if (props.vote) {
      setIsErfolgt(!!props.vote.fruehkoordinierungBkAmtErfolgtAm);
    }
  }, [props.vote]);

  useEffect(() => {
    if (!isErfolgt) {
      props.form.setFieldsValue({
        earlyVoteBundeskanzleramtDate: undefined,
        'earlyVoteBundeskanzleramtDate-date-placeholder': undefined,
      });
    }
  }, [isErfolgt]);

  // Gets value that should be checked and returns boolean
  const disabledDate = (possibleDate: Date) => {
    const current = new Date();
    return isAfter(endOfDay(possibleDate), endOfDay(current));
  };

  return (
    <fieldset className="earlyVoteBundeskanzleramt-holder">
      <Radio.Group
        onChange={(e) => {
          setIsErfolgt(e.target.value as boolean);
        }}
        className="horizontal-radios"
        name="earlyVoteBundeskanzleramtRadio"
        value={isErfolgt}
      >
        <label htmlFor="earlyVoteBundeskanzleramtRadio" className="earlyVoteBundeskanzleramt-title">
          <strong>{t('enorm.vote.earlyVoteBundeskanzleramt.label')} *</strong>
          <InfoComponent title={t('enorm.vote.earlyVoteBundeskanzleramt.label')}>
            <ol
              className="earlyVoteBundeskanzleramt-info"
              dangerouslySetInnerHTML={{
                __html: t(`enorm.vote.earlyVoteBundeskanzleramt.info`, {
                  linkGGO_24: `${BASE_PATH}/arbeitshilfen/download/34#page=19`,
                  linkGGO_40: `${BASE_PATH}/arbeitshilfen/download/34#page=30`,
                }),
              }}
            ></ol>
          </InfoComponent>
        </label>
        <Radio id="enorm-earlyVoteBundeskanzleramtRadio-radio-ja" value={true}>
          {t('enorm.vote.earlyVoteBundeskanzleramt.answerYes')}
        </Radio>
        {isErfolgt && (
          <Row gutter={8} ref={ref} className="date-time-holder">
            <Col span={13} offset={1}>
              <CustomDatepicker
                disabledDate={disabledDate}
                setDate={setDate}
                aliasDate={'earlyVoteBundeskanzleramtDate'}
                aliasTime={'earlyVoteBundeskanzleramtDate'}
                form={props.form}
                required={true}
                timePlaceholderName={'timePlaceholderName'}
                additionalValidators={[]}
                errorRequired={t('enorm.vote.earlyVoteBundeskanzleramt.errorRequired')}
                errorDateTime={''}
                errorInvalidFormat={''}
                errorDisabledDate={''}
                customLabel={t('enorm.vote.earlyVoteBundeskanzleramt.labelDate')}
                onFormChange={props.onFormChange}
              />
            </Col>
          </Row>
        )}
        <Radio id="enorm-earlyVoteBundeskanzleramtRadio-radio-nein" value={false}>
          {t('enorm.vote.earlyVoteBundeskanzleramt.answerNo')}
        </Radio>
      </Radio.Group>
    </fieldset>
  );
}
