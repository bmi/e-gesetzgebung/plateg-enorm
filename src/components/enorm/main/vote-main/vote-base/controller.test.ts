// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { of } from 'rxjs';
import sinon from 'sinon';

import { AbstimmungEntityDTO, EditorRemoteControllerApi, FristEntityDTO, FristtypType } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { VoteFormValues } from './component.react';
import { VoteBaseController } from './controller';

describe('Test VoteBaseController', () => {
  describe('Test fetchInitialData neue Abstimmung vs Abstimmung Bearbeiten RV Api Call', () => {
    let voteCtr: VoteBaseController;
    const userData = {
      base: { id: '1', erstelltAm: '', bearbeitetAm: '' },
      dto: { email: 'test@test.test', ungeleseneNeuigkeiten: 5, abteilung: '' },
    };
    before(() => {
      voteCtr = new VoteBaseController();
    });

    it('Test case neue Abstimmung', (done) => {
      const voteId = '1';
      const isVotingRound = true;
      const existingVoteId = null;

      const getRvListCallStub = sinon.stub(voteCtr, 'getRegelungsvorhabenListCall');

      voteCtr.fetchInitialData(voteId, isVotingRound, existingVoteId, userData);
      setTimeout(() => {
        sinon.assert.calledOnce(getRvListCallStub);
        getRvListCallStub.restore();
        done();
      }, 20);
    });

    it('Test case Abstimmung bearbeiten', (done) => {
      const voteId = '1';
      const isVotingRound = true;
      const existingVoteId = '1';

      const getRvListCallStub = sinon.stub(voteCtr, 'getRegelungsvorhabenListCall');

      voteCtr.fetchInitialData(voteId, isVotingRound, existingVoteId, userData);
      setTimeout(() => {
        sinon.assert.notCalled(getRvListCallStub);
        getRvListCallStub.restore();
        done();
      }, 30);
    });
  });

  describe('Validate getRegelungsentwurfCall', () => {
    let voteController: VoteBaseController;
    let editorController: EditorRemoteControllerApi;

    before(() => {
      voteController = new VoteBaseController();
      editorController = GlobalDI.getOrRegister<EditorRemoteControllerApi>(
        'editorRemoteControllerApi',
        () => new EditorRemoteControllerApi(),
      );
    });
    it('Check valid id', (done) => {
      const expected_output = of({ titel: 'Hello', id: '123', version: 1 });
      const editorControllerStub = sinon.stub(editorController, 'getDokumentenmappeById').returns(expected_output);
      const result = voteController.getRegelungsentwurfCall('123');
      sinon.assert.calledOnce(editorControllerStub);
      result.subscribe({
        next: (data) => {
          expect(data).to.eql({ titel: 'Hello', id: '123', version: 1 });
          done();
        },
      });
      editorControllerStub.restore();
    });
    it('Check undefined id', (done) => {
      const editorControllerStub = sinon.stub(editorController, 'getDokumentenmappeById');
      const result = voteController.getRegelungsentwurfCall(undefined);
      sinon.assert.notCalled(editorControllerStub);
      result.subscribe({
        next: (data) => {
          expect(data).to.eql(undefined);
          done();
        },
      });
      editorControllerStub.restore();
    });
  });

  describe('generateFristen', () => {
    let voteController: VoteBaseController;
    before(() => {
      voteController = new VoteBaseController();
    });
    it('check fruehkoordinierungBkAmtErfolgtAm exist', () => {
      const formValues = {
        mainDeadlineDate: new Date('2022-08-05T10:48:25Z'),
        mainDeadlineTime: new Date('2022-08-05T10:48:25Z'),
        replyInternetDate: new Date('2022-08-05T10:48:25Z'),
        replyInternetTime: new Date('2022-08-05T10:48:25Z'),
        replyDate: new Date('2022-08-05T10:48:25Z'),
        replyTime: new Date('2022-08-05T10:48:25Z'),
        earlyVoteBundeskanzleramtDate: new Date('2022-08-05T10:48:25Z'),
        teilnehmendeUmBenehmenBitten: true,
      } as VoteFormValues;

      const data = voteController.generateFristen(formValues);
      expect(data.length).to.eql(3);
    });
  });

  describe('prepareFristen', () => {
    let voteController: VoteBaseController;
    before(() => {
      voteController = new VoteBaseController();
    });
    it('check fruehkoordinierungBkAmtErfolgtAm exist', () => {
      const dto = { fruehkoordinierungBkAmtErfolgtAm: '2022-08-05T10:48:25Z' } as AbstimmungEntityDTO;
      const data = voteController.prepareFristen({} as VoteFormValues, dto);
      expect(data.earlyVoteBundeskanzleramtDate).to.eql(new Date('2022-08-05T10:48:25Z'));
    });
    it('check fruehkoordinierungBkAmtErfolgtAm undefined', () => {
      const dto = { fruehkoordinierungBkAmtErfolgtAm: undefined } as AbstimmungEntityDTO;
      const data = voteController.prepareFristen({} as VoteFormValues, dto);
      expect(data.earlyVoteBundeskanzleramtDate).to.eql(undefined);
    });
    it('check DTO fristen exist', () => {
      const formValues = {
        mainDeadlineDate: new Date('2022-08-05T16:00:00Z'),
        mainDeadlineTime: new Date('2022-08-05T16:00:00Z'),
        replyInternetDate: new Date('2022-08-05T16:00:00Z'),
        replyInternetTime: new Date('2022-08-05T16:00:00Z'),
        replyDate: new Date('2022-08-05T16:00:00Z'),
        replyTime: new Date('2022-08-05T16:00:00Z'),
        earlyVoteBundeskanzleramtDate: new Date('2022-08-05T16:00:00Z'),
        teilnehmendeUmBenehmenBitten: true,
      } as VoteFormValues;
      const fristValues: FristEntityDTO[] = [
        { fristTyp: FristtypType.Abstimmungsende, fristablauf: '2022-08-05T16:00:00.000Z' },
        { fristTyp: FristtypType.VeroeffentlichungImInternet, fristablauf: '2022-08-05T16:00:00.000Z' },
        { fristTyp: FristtypType.ZuleitungAnLaenderUndVerbaende, fristablauf: '2022-08-05T16:00:00.000Z' },
      ];
      const dto = { fruehkoordinierungBkAmtErfolgtAm: undefined, fristen: fristValues } as AbstimmungEntityDTO;
      const data = voteController.prepareFristen(formValues, dto);
      expect(dto.fristen.length).to.eql(3);
      expect(voteController.generateFristenString(data.mainDeadlineDate)).to.eql(dto?.fristen[0].fristablauf);
    });
  });

  describe('prepareVoteForSubmit', () => {
    let voteController: VoteBaseController;
    before(() => {
      voteController = new VoteBaseController();
    });
    it('check earlyVoteBundeskanzleramtDate exist', () => {
      // Test failed on local env due different system time on local and remote
      const formValues = { earlyVoteBundeskanzleramtDate: new Date('2022-08-05T10:48:25Z') } as VoteFormValues;
      const data = voteController.prepareVoteForSubmit(formValues);
      expect(data.fruehkoordinierungBkAmtErfolgtAm).to.eql('2022-08-05T08:48:25.000Z');
    });
    it('check earlyVoteBundeskanzleramtDate undefined', () => {
      const formValues = { earlyVoteBundeskanzleramtDate: undefined } as VoteFormValues;
      const data = voteController.prepareVoteForSubmit(formValues);
      expect(data.fruehkoordinierungBkAmtErfolgtAm).to.eql(undefined);
    });
  });
});
