// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input } from 'antd';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { OberabstimmungEntityDTO, RegelungsvorhabenEntityResponseShortDTO } from '@plateg/rest-api';
import { Constants } from '@plateg/theme';

export interface TitleComponentProps {
  regelungsvorhaben?: RegelungsvorhabenEntityResponseShortDTO;
  setTitle: (value: string) => void;
  autofill: boolean;
  oberabstimmung?: OberabstimmungEntityDTO;
}

export function TitleComponent(props: TitleComponentProps): React.ReactElement {
  const { t } = useTranslation();

  useEffect(() => {
    if (props.autofill) {
      props.setTitle(
        props.regelungsvorhaben?.dto.abkuerzung ||
          // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
          `Unterabstimmung: ${props.oberabstimmung?.titel}` ||
          'Unterabstimmung Titel',
      );
    }
  }, [props.regelungsvorhaben, props.oberabstimmung, props.autofill]);

  return (
    <Form.Item shouldUpdate>
      {() => (
        <Form.Item
          className="with-custom-label"
          name="titel"
          label={
            <span>
              <span className="label-text">{t('enorm.vote.titleVote.label')}</span>
              <span className="hint">{t('enorm.vote.titleVote.additionalLabel')}</span>
            </span>
          }
          rules={[
            { required: true, message: t('enorm.vote.titleVote.error.noInput') },
            {
              max: Constants.TEXT_BOX_LENGTH,
              message: t('enorm.vote.titleVote.error.inputTooLong', {
                maxChars: Constants.TEXT_BOX_LENGTH,
              }),
            },
          ]}
        >
          <Input
            disabled={props.regelungsvorhaben === undefined}
            required
            placeholder={t('enorm.vote.titleVote.placeholder')}
          />
        </Form.Item>
      )}
    </Form.Item>
  );
}
