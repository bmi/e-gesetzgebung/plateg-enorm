// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Form, Input, InputRef } from 'antd';
import { FormInstance } from 'antd/lib/form';
import React, { useLayoutEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';

import { Constants } from '@plateg/theme';
import { DeleteOutlined } from '@plateg/theme/src/components/icons/DeleteOutlined';

import { DateTimeComponent } from '../dateTimeComponent/component.react';

interface AdditionalDeadlineComponentProps {
  index: number;
  deleteDeadline: (index: number) => void;
  form: FormInstance;
  onFormChange?: () => void;
}
export function AdditionalDeadlineComponent(props: AdditionalDeadlineComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const deadlineInputRef = useRef<InputRef>(null);

  useLayoutEffect(() => {
    deadlineInputRef.current?.focus();
  }, []);

  return (
    <>
      <div
        className="white-holder deadline-holder"
        role="group"
        aria-labelledby={`${t('enorm.vote.additionalDeadline.title')} ${props.index + 1}`}
      >
        <fieldset>
          <legend style={{ all: 'unset' }}>
            <strong className="form-label">{`${t('enorm.vote.additionalDeadline.title')} ${props.index + 1}`}</strong>
          </legend>
          <Form.Item
            htmlFor="enorm-voteAdditionalDeadline-txtBox"
            name={['additionalFristList', props.index, 'additionalDeadlineTitle']}
            label={`${t('enorm.vote.additionalDeadline.label')} ${props.index + 1}`}
            rules={[
              {
                required: true,
                whitespace: true,
                message: t('enorm.vote.additionalDeadline.errorTitle', { index: props.index + 1 }),
              },
              {
                max: Constants.TEXT_BOX_LENGTH,
                message: t('enorm.vote.additionalDeadline.errorTitleLength', {
                  maxChars: Constants.TEXT_BOX_LENGTH,
                }),
              },
            ]}
          >
            <Input
              id="enorm-voteAdditionalDeadline-txtBox"
              ref={deadlineInputRef}
              placeholder={t('enorm.vote.additionalDeadline.placeholder')}
              required
            />
          </Form.Item>
          <DateTimeComponent
            aliasDate={['additionalFristList', props.index, 'additionalDeadlineDate']}
            aliasTime={['additionalFristList', props.index, 'additionalDeadlineTime']}
            required={true}
            form={props.form}
            identifier="additionalDeadline"
            index={props.index + 1}
            onFormChange={props.onFormChange}
          />
        </fieldset>
      </div>
      <div style={{ textAlign: 'right', marginTop: '-16px' }}>
        <Button
          id="enorm-fristEntfernen-btn"
          size={'small'}
          icon={<DeleteOutlined />}
          type="text"
          onClick={() => {
            props.deleteDeadline(props.index);
            props.onFormChange?.();
          }}
          className="blue-text-button"
        >
          {t('enorm.vote.additionalDeadline.deleteDeadline')}
        </Button>
      </div>
    </>
  );
}
