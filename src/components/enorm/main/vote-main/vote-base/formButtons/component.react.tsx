// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';

interface FormButtonsComponentProps {
  tabName: string;
  disableSubmit?: boolean;
}

export function FormButtonsComponent(props: FormButtonsComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();

  return (
    <div className="form-control-buttons">
      <Button
        id="enorm-abstimmungsAngabenPruefen-btn"
        disabled={!!props.disableSubmit}
        type="primary"
        htmlType="submit"
        size="large"
      >
        {t('enorm.vote.btnSubmit')}
      </Button>

      <Button id="enorm-abstimmungAbbrechen-btn" size="large" onClick={() => history.push(`/hra/${props.tabName}`)}>
        {t('enorm.vote.btnCancel')}
      </Button>
    </div>
  );
}
