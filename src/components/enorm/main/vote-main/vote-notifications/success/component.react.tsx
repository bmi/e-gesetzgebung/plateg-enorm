// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './success.less';

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useRouteMatch } from 'react-router';

import { SuccessPage } from '@plateg/theme';

import { routes } from '../../../../../../shares/routes';

export function EnormInfoSuccessComponent(): React.ReactElement {
  const { t } = useTranslation();
  const routeMatcher = useRouteMatch<{ action: string }>(`/hra/:id/${routes.ABSTIMMUNG_PRUEFEN}/:action`);
  const isUnterabstimmung =
    routeMatcher?.params.action === routes.ERFOLG_UA || routeMatcher?.params.action === routes.ERFOLG_UA_UPD;
  const pkpText = (): { title: string; text: string } => {
    if (routeMatcher?.params.action === routes.ERFOLGABSTIMMUNGPKP) {
      return {
        title: t(`enorm.notificationPages.success.title`, {
          type: 'eingeleitet',
          abstimmung: 'Abstimmung',
        }),
        text: t(`enorm.notificationPages.success.pkp`),
      };
    }
    if (routeMatcher?.params.action === routes.ERFOLGNURABSTIMMUNG) {
      return { title: t(`enorm.notificationPages.error.pkpTitle`), text: t(`enorm.notificationPages.error.pkp`) };
    }

    return {
      title: t(`enorm.notificationPages.success.title`, {
        type:
          routeMatcher?.params.action === routes.ERFOLGUPD || routeMatcher?.params.action === routes.ERFOLG_UA_UPD
            ? 'aktualisiert'
            : 'eingeleitet',
        abstimmung: isUnterabstimmung ? 'Unterabstimmung' : 'Abstimmung',
      }),
      text: t(`enorm.notificationPages.success.text`),
    };
  };
  return (
    <SuccessPage
      title={pkpText().title}
      text={<p className="sussess-messages" dangerouslySetInnerHTML={{ __html: pkpText().text }} />}
      link={isUnterabstimmung ? `/hra/meineUnterabstimmung` : `/hra`}
      linkText={t(`enorm.notificationPages.success.buttonText`, {
        abstimmungen: isUnterabstimmung ? 'Unterabstimmungen' : 'Abstimmungen',
      })}
    />
  );
}
