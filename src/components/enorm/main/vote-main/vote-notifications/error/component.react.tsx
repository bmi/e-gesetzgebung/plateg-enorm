// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './error.less';

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useRouteMatch } from 'react-router-dom';
import { validate } from 'uuid';

import { RegelungsvorhabenEntityResponseShortDTO } from '@plateg/rest-api';
import { ErrorPage } from '@plateg/theme';

import { routes } from '../../../../../../shares/routes';

interface EnormInfoErrorComponentInterface {
  selectedRv?: RegelungsvorhabenEntityResponseShortDTO;
}

export function EnormInfoErrorComponent(props: Readonly<EnormInfoErrorComponentInterface>): React.ReactElement {
  const { t } = useTranslation();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; tabName: string; action: string }>(
    '/hra/:tabName/:id/:action',
  );

  const hasValidId = routeMatcherVorbereitung?.params?.id && validate(routeMatcherVorbereitung.params.id);
  const tabName = routeMatcherVorbereitung?.params?.tabName ?? '';
  const action = routeMatcherVorbereitung?.params?.action ?? '';

  const getVoteId = () => {
    if (hasValidId) {
      return `${routeMatcherVorbereitung?.params?.id}/`;
    } else {
      return '';
    }
  };

  const getEditUrl = () => {
    if (!hasValidId) {
      return routes.ABSTIMMUNG_ANLEGEN;
    } else if (action === routes.ABSTIMMUNGSRUNDE_PRUEFEN) {
      return routes.NACHSTE_ABSTIMMUNGSRUNDE;
    } else {
      return routes.ABSTIMMUNG_BEARBEITEN;
    }
  };
  let text: string | React.ReactElement = t(`enorm.notificationPages.error.text`);
  let link = `/hra/${tabName}/${getVoteId()}${getEditUrl()}`;
  let linkText = t(`enorm.notificationPages.error.buttonText`);

  if (action === routes.FEHLERPKP) {
    text = (
      <p
        className="error-messages"
        dangerouslySetInnerHTML={{ __html: t(`enorm.notificationPages.error.pkpRessort`) }}
      />
    );
    link = `/regelungsvorhaben/meineRegelungsvorhaben/${props.selectedRv?.base.id}/datenblatt`;
    linkText = t(`enorm.notificationPages.error.pkpRessortBtnText`);
  }
  return (
    <ErrorPage
      title={
        action === routes.FEHLERPKP
          ? t(`enorm.notificationPages.error.pkpRessortTitle`)
          : t(`enorm.notificationPages.error.title`)
      }
      text={text}
      link={link}
      linkText={linkText}
    />
  );
}
