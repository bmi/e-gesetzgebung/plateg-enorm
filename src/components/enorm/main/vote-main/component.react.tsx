// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useState } from 'react';
import { Route, Switch } from 'react-router';

import { AbstimmungEntityDTO, RegelungsvorhabenEntityResponseShortDTO } from '@plateg/rest-api';

import { routes } from '../../../../shares/routes';
import { Files } from '../../../../utils/controllerManageFiles';
import { EditVoteComponent } from './edit-vote/component.react';
import { NewVoteComponent } from './new-vote/component.react';
import { ReviewVoteComponent } from './review-vote/component.react';
import { EnormInfoErrorComponent } from './vote-notifications/error/component.react';
import { EnormInfoSuccessComponent } from './vote-notifications/success/component.react';

export function VoteRoutes(): React.ReactElement {
  const [vote, setVote] = useState<AbstimmungEntityDTO>();
  const [voteFiles, setVoteFiles] = useState<Files>();
  const [voteInitialFiles, setVoteInitialFiles] = useState<Files>();
  const [voteId, setVoteId] = useState<string | null>(null);
  const [selectedRV, setSelectedRV] = useState<RegelungsvorhabenEntityResponseShortDTO>();
  const [sendRVanPKP, setSendRVanPKP] = useState<boolean>(false);

  return (
    <Switch>
      <Route
        exact
        path={[
          `/hra/:tabName/${routes.ABSTIMMUNG_ANLEGEN}`,
          `/hra/:tabName/${routes.ABSTIMMUNG_ANLEGEN}/${routes.HAUSABSTIMMUNG}/:id`,
          `/hra/:tabName/${routes.ABSTIMMUNG_ANLEGEN}/${routes.RESSORTABSTIMMUNG}/:id`,
          `/hra/:tabName/${routes.ABSTIMMUNG_ANLEGEN}/${routes.SCHLUSSABSTIMMUNG}/:id`,
          `/hra/:tabName/${routes.ABSTIMMUNG_ANLEGEN}/${routes.ABSTIMMUNG_HAUSLEITUNG}/:id`,
          `/hra/:tabName/${routes.ABSTIMMUNG_ANLEGEN}/${routes.VORHABENCLEARING}/:id`,
        ]}
      >
        <NewVoteComponent
          setSelectedRV={setSelectedRV}
          setVote={setVote}
          setVoteFiles={setVoteFiles}
          setVoteInitialFiles={setVoteInitialFiles}
          setVoteId={setVoteId}
          voteForEditing={vote}
          voteFiles={voteFiles as Files}
          setSendRVanPKP={setSendRVanPKP}
          sendRVanPKP={sendRVanPKP}
        />
      </Route>
      <Route
        exact
        path={[
          `/hra/:tabName/:id/${routes.ABSTIMMUNG_ANLEGEN}`,
          `/hra/:tabName/:id/${routes.ABSTIMMUNG_BEARBEITEN}`,
          `/hra/:tabName/:id/${routes.NACHSTE_ABSTIMMUNGSRUNDE}`,
          `/hra/:tabName/:id/${routes.UNTERABSTIMMUNG_ANLEGEN}`,
        ]}
      >
        <EditVoteComponent
          setSelectedRV={setSelectedRV}
          setVote={setVote}
          setVoteFiles={setVoteFiles}
          setVoteInitialFiles={setVoteInitialFiles}
          setVoteId={setVoteId}
          voteForEditing={vote}
          voteFiles={voteFiles as Files}
          setSendRVanPKP={setSendRVanPKP}
          sendRVanPKP={sendRVanPKP}
        />
      </Route>

      <Route
        exact
        path={[`/hra/:tabName/${routes.ABSTIMMUNG_PRUEFEN}`, `/hra/:tabName/${routes.ABSTIMMUNGSRUNDE_PRUEFEN}`]}
      >
        <ReviewVoteComponent
          vote={vote as AbstimmungEntityDTO}
          voteFiles={voteFiles as Files}
          voteId={voteId}
          voteInitialFiles={voteInitialFiles as Files}
          selectedRv={selectedRV}
          sendRVanPKP={sendRVanPKP}
          setSendRVanPKP={setSendRVanPKP}
        />
      </Route>

      <Route
        exact
        path={[
          `/hra/:id/${routes.ABSTIMMUNG_PRUEFEN}/${routes.ERFOLG}`,
          `/hra/:id/${routes.ABSTIMMUNG_PRUEFEN}/${routes.ERFOLGNURABSTIMMUNG}`,
          `/hra/:id/${routes.ABSTIMMUNG_PRUEFEN}/${routes.ERFOLGABSTIMMUNGPKP}`,
          `/hra/:id/${routes.ABSTIMMUNG_PRUEFEN}/${routes.ERFOLGUPD}`,
          `/hra/:id/${routes.ABSTIMMUNG_PRUEFEN}/${routes.ERFOLG_UA}`,
          `/hra/:id/${routes.ABSTIMMUNG_PRUEFEN}/${routes.ERFOLG_UA_UPD}`,
        ]}
      >
        <EnormInfoSuccessComponent />
      </Route>

      <Route
        exact
        path={[
          `/hra/:tabName/${routes.ABSTIMMUNG_PRUEFEN}/${routes.FEHLER}`,
          `/hra/:tabName/:id/${routes.ABSTIMMUNG_PRUEFEN}/${routes.FEHLER}`,
          `/hra/:id/${routes.ABSTIMMUNG_PRUEFEN}/${routes.FEHLERPKP}`,
          `/hra/:tabName/:id/${routes.ABSTIMMUNGSRUNDE_PRUEFEN}/${routes.FEHLER}`,
        ]}
      >
        <EnormInfoErrorComponent selectedRv={selectedRV} />
      </Route>
    </Switch>
  );
}
