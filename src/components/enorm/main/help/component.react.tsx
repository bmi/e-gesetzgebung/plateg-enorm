// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { InfoComponent } from '@plateg/theme';

export function EnormHelpBreadcrumb(): React.ReactElement {
  return (
    <InfoComponent
      key="enorm-help"
      title={i18n.t('enorm.help.drawerTitle')}
      buttonText={i18n.t('enorm.header.linkHelp')}
    >
      <EnormHelpComponent />
    </InfoComponent>
  );
}
export function EnormHelpComponent(): React.ReactElement {
  const { t } = useTranslation();

  return (
    <div
      className="enorm-help-content"
      dangerouslySetInnerHTML={{
        __html: t('enorm.help.section1'),
      }}
    />
  );
}
