// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

import { routes } from '../../../../../shares/routes';

interface FormControlProps {
  abstimmungId: string;
  modify?: boolean;
}

export function TeilnahmeAnfragenAnlegenFormControlComponent(props: FormControlProps): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  const submit = (
    <Button id="enorm-abstimmungPruefen-btn" type="primary" htmlType="submit" size={'large'}>
      {!props.modify
        ? t('enorm.teilnahmeAnfragen.formControls.btnSubmit')
        : t('enorm.teilnahmeAnfragen.formControls.modify.btnSubmit')}
    </Button>
  );

  return (
    <div className="form-control-buttons" style={{ marginBottom: '20px' }}>
      {submit}
      {props.modify && (
        <Button
          size={'large'}
          onClick={() => {
            history.push({
              pathname: `/hra/${routes.ANFRAGEN}/${props.abstimmungId}/${routes.ANFRAGEN_ANLEGEN}`,
            });
          }}
        >
          {t('enorm.teilnahmeAnfragen.formControls.btnEdit')}
        </Button>
      )}
      <Button
        size={'large'}
        onClick={() => {
          history.push({
            pathname: `/hra/${routes.BITTE_UM_MITZEICHNUNG}/${props.abstimmungId}/${routes.ABSTIMMUNG_BEANTWORTEN_PRUEFEN}`,
          });
        }}
        className={props.modify ? 'align-right' : ''}
      >
        {t('enorm.teilnahmeAnfragen.formControls.btnCancel')}
      </Button>
    </div>
  );
}
