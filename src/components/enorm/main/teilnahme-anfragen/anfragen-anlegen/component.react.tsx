// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../teilnahme-anfragen.less';

import { Form } from 'antd';
import TextArea from 'antd/es/input/TextArea';
import Title from 'antd/es/typography/Title';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory, useRouteMatch } from 'react-router-dom';
import { Subscription } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import { AbstimmungInfoDTO, AbstimmungsanfrageDTO } from '@plateg/rest-api';
import {
  BreadcrumbComponent,
  Constants,
  ErrorController,
  GeneralFormWrapper,
  GlobalDI,
  HeaderController,
  LoadingStatusController,
} from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

import { routes } from '../../../../../shares/routes';
import { getAbstimmungsInfoCall } from '../../home-tabs/controller.react';
import { AbstimmungInfoComponent } from '../abstimmung-info/component.react';
import { TeilnahmeAnfragenAnlegenFormControlComponent } from '../form-control/component.react';

interface TeilnahmeAnfragenAnlegenProps {
  abstimmung?: AbstimmungInfoDTO;
  setAbstimmung: (abstimmung: AbstimmungInfoDTO) => void;
  formValues?: AbstimmungsanfrageDTO;
  setFormValues: (formValues: AbstimmungsanfrageDTO | undefined) => void;
}

export function TeilnahmeAnfragenAnlegenComponent(props: TeilnahmeAnfragenAnlegenProps): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const history = useHistory();
  const headerController = GlobalDI.get<HeaderController>('enormHeadercontroller');
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const routeMatcherVorbereitung = useRouteMatch<{ tabName: string; id: string; action: string }>(
    '/hra/:tabName/:id/:action/',
  );
  const tabName = routeMatcherVorbereitung?.params?.tabName as string;
  const action = routeMatcherVorbereitung?.params.action as string;
  const abstimmungId = routeMatcherVorbereitung?.params.id as string;

  useEffect(() => {
    if (props.formValues) {
      form.setFieldsValue(props.formValues);
    }
    if (props.abstimmung) {
      setBreadcrumb(`${props.abstimmung.titel}(#${props.abstimmung.version})`);
    }
  }, [null, props.abstimmung]);

  useEffect(() => {
    let sub: Subscription;
    if (!props.abstimmung) {
      loadingStatusController.setLoadingStatus(true);
      sub = getAbstimmungsInfoCall(abstimmungId).subscribe({
        next: (data) => {
          loadingStatusController.setLoadingStatus(false);
          setBreadcrumb(`${data.titel}(#${data.version})`);
          props.setAbstimmung(data);
        },
        error: (error: AjaxError) => {
          loadingStatusController.setLoadingStatus(false);
          errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
        },
      });
    }

    return () => {
      sub?.unsubscribe();
    };
  }, []);

  const setBreadcrumb = (abstimmungName: string) => {
    const tabLink = (
      <Link id="enorm-teilnahmeAnfragen-breadcrumbsTabNavigation-link" key={`enorm-${tabName}`} to={`/hra/${tabName}`}>
        {t(`enorm.breadcrumbs.projectName`)} - {t(`enorm.breadcrumbs.tabName.${tabName}`)}
      </Link>
    );
    const breadcrumbTitle = (
      <span key={`teilnahmeAnfragen-${action}`}>
        {t(`enorm.breadcrumbs.action.anfrageAnlegen`, { abstimmungName })}
      </span>
    );

    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[tabLink, breadcrumbTitle]} />],
      headerLast: [],
      headerCenter: [],
      headerRight: [],
    });
  };

  const onFinish = () => {
    setFormValues();
    history.push({
      pathname: `/hra/${routes.ANFRAGEN}/${abstimmungId}/${routes.ANFRAGEN_ANLEGEN}/${routes.ANFRAGEN_ANLEGEN_PRUEFEN}`,
    });
  };

  const setFormValues = () => {
    props.setFormValues(form.getFieldsValue(true) as AbstimmungsanfrageDTO);
  };

  return (
    <div className="beantworten-vote-page teilnahme-anfragen-page">
      <Title level={1}>
        {t(`enorm.breadcrumbs.action.anfrageAnlegen`, {
          abstimmungName: `${props.abstimmung?.titel as string}(#${props.abstimmung?.version as number})`,
        })}
      </Title>
      <p
        className="ant-typography p-no-style"
        dangerouslySetInnerHTML={{
          __html: t(`enorm.teilnahmeAnfragen.pflichtfelderText`),
        }}
      />
      <GeneralFormWrapper
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={() => document?.getElementById('errorBox')?.focus()}
        scrollToFirstError={true}
      >
        <AbstimmungInfoComponent abstimmungInfo={props.abstimmung} />
        <Form.Item
          name="kommentar"
          label={<>{t('enorm.teilnahmeAnfragen.commentBox.label')} </>}
          rules={[
            { required: true, whitespace: true, message: t('enorm.teilnahmeAnfragen.commentBox.error') },
            {
              max: Constants.TEXT_AREA_LENGTH,
              message: t('enorm.teilnahmeAnfragen.commentBox.errorMaxChars', {
                maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
              }),
            },
          ]}
        >
          <TextArea rows={8} />
        </Form.Item>
        <TeilnahmeAnfragenAnlegenFormControlComponent abstimmungId={abstimmungId} />
      </GeneralFormWrapper>
    </div>
  );
}
