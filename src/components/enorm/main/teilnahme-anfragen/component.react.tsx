// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useState } from 'react';
import { Route, Switch } from 'react-router-dom';

import { AbstimmungInfoDTO, AbstimmungsanfrageDTO } from '@plateg/rest-api';

import { routes } from '../../../../shares/routes';
import { TeilnahmeAnfragenReviewComponent } from './anfragen-anlegen-review/component.react';
import { TeilnahmeAnfragenAnlegenComponent } from './anfragen-anlegen/component.react';
import { TeilnahmeAnfragenSuccessPage } from './success-page/component.react';

export function TeilnahmeAnfragenRoutes(): React.ReactElement {
  const [formValues, setFormValues] = useState<AbstimmungsanfrageDTO | undefined>();
  const [abstimmung, setAbstimmung] = useState<AbstimmungInfoDTO>();
  return (
    <Switch>
      <Route exact path={[`/hra/:tabName/:id/${routes.ANFRAGEN_ANLEGEN}`]}>
        <TeilnahmeAnfragenAnlegenComponent
          abstimmung={abstimmung}
          setAbstimmung={setAbstimmung}
          formValues={formValues}
          setFormValues={setFormValues}
        />
      </Route>
      <Route exact path={[`/hra/:tabName/:id/${routes.ANFRAGEN_ANLEGEN}/${routes.ANFRAGEN_ANLEGEN_PRUEFEN}`]}>
        <TeilnahmeAnfragenReviewComponent abstimmung={abstimmung} formValues={formValues} />
      </Route>
      <Route exact path={[`/hra/:tabName/${routes.ANFRAGEN_ANLEGEN}/${routes.ERFOLG}`]}>
        <TeilnahmeAnfragenSuccessPage />
      </Route>
    </Switch>
  );
}
