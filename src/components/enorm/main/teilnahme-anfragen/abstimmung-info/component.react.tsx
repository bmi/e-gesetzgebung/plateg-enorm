// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { AbstimmungInfoDTO } from '@plateg/rest-api';
import { getDateTimeString } from '@plateg/theme';

interface AbstimmungInfoProps {
  abstimmungInfo?: AbstimmungInfoDTO;
  kommentar?: string;
}
export function AbstimmungInfoComponent({ abstimmungInfo, kommentar }: AbstimmungInfoProps): React.ReactElement {
  const { t } = useTranslation();
  const noAnswer = t(`enorm.teilnahmeAnfragen.info.noAnswer`);
  const fachreferat = abstimmungInfo?.ersteller.fachreferat || abstimmungInfo?.ersteller.abteilung;

  return (
    <section className="review-section">
      <Title level={2}>{t(`enorm.teilnahmeAnfragen.info.title`)}</Title>
      <dl>
        <dt>{t('enorm.teilnahmeAnfragen.info.generalInfo.regelungsvorhaben')}</dt>
        <dd>{abstimmungInfo?.regelungsvorhabenTitel || noAnswer}</dd>

        <dt>{t('enorm.teilnahmeAnfragen.info.generalInfo.abstimmungsschritt')}</dt>
        <dd>{abstimmungInfo?.typ ? t(`enorm.reviewVote.generalInfo.type${abstimmungInfo?.typ}`) : noAnswer}</dd>

        <dt>{t('enorm.teilnahmeAnfragen.info.generalInfo.abstimmungsrunde')}</dt>
        <dd>{abstimmungInfo?.version ? `${abstimmungInfo?.titel}(#${abstimmungInfo?.version})` : noAnswer}</dd>

        <dt>{t('enorm.teilnahmeAnfragen.info.generalInfo.fristDerAbstimmungsrunde')}</dt>
        <dd>{abstimmungInfo?.fristablauf ? getDateTimeString(abstimmungInfo?.fristablauf) : noAnswer}</dd>

        <dt>{t('enorm.teilnahmeAnfragen.info.generalInfo.einleiter')}</dt>
        <dd>
          {abstimmungInfo?.ersteller.name} ({fachreferat ? `${fachreferat}, ` : ''}
          {abstimmungInfo?.ersteller.ressortKurzbezeichnung})
        </dd>

        {kommentar && (
          <>
            <dt>{t('enorm.teilnahmeAnfragen.info.generalInfo.kommentar')}</dt>
            <dd>{kommentar}</dd>
          </>
        )}
      </dl>
    </section>
  );
}
