// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../teilnahme-anfragen.less';

import { Form } from 'antd';
import Title from 'antd/es/typography/Title';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory, useRouteMatch } from 'react-router-dom';
import { AjaxError } from 'rxjs/ajax';

import { AbstimmungInfoDTO, AbstimmungsanfrageDTO } from '@plateg/rest-api';
import {
  BreadcrumbComponent,
  ErrorController,
  GeneralFormWrapper,
  GlobalDI,
  HeaderController,
  LoadingStatusController,
} from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { createAbstimmungsanfrageCall } from '../../home-tabs/controller.react';
import { AbstimmungInfoComponent } from '../abstimmung-info/component.react';
import { TeilnahmeAnfragenAnlegenFormControlComponent } from '../form-control/component.react';

interface TeilnahmeAnfragenAnlegenProps {
  abstimmung?: AbstimmungInfoDTO;
  formValues?: AbstimmungsanfrageDTO;
}

export function TeilnahmeAnfragenReviewComponent(props: TeilnahmeAnfragenAnlegenProps): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const history = useHistory();
  const headerController = GlobalDI.get<HeaderController>('enormHeadercontroller');
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const routeMatcherVorbereitung = useRouteMatch<{ tabName: string; id: string; action: string; review: string }>(
    '/hra/:tabName/:id/:action/:review',
  );
  const tabName = routeMatcherVorbereitung?.params?.tabName as string;
  const action = routeMatcherVorbereitung?.params.action as string;
  const abstimmungId = routeMatcherVorbereitung?.params.id as string;

  useEffect(() => {
    setBreadcrumb(`${props.abstimmung?.titel as string}(#${props.abstimmung?.version as number})`);
  }, []);

  const setBreadcrumb = (abstimmungName: string) => {
    const tabLink = (
      <Link id="enorm-teilnahmeAnfragen-breadcrumbsTabNavigation-link" key={`enorm-${tabName}`} to={`/hra/${tabName}`}>
        {t(`enorm.breadcrumbs.projectName`)} - {t(`enorm.breadcrumbs.tabName.${tabName}`)}
      </Link>
    );
    const actionLink = (
      <Link
        id="enorm-teilnahmeAnfragen-breadcrumbsTabNavigation-linkAnlegen"
        key={`enorm-${tabName}`}
        to={`/hra/${tabName}/${abstimmungId}/${action}`}
      >
        {t(`enorm.breadcrumbs.action.anfrageAnlegen`, { abstimmungName })}
      </Link>
    );
    const breadcrumbTitle = (
      <span key={`teilnahmeAnfragen-${action}`}>{t(`enorm.teilnahmeAnfragen.reviewPage.title`)}</span>
    );

    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[tabLink, actionLink, breadcrumbTitle]} />],
      headerLast: [],
      headerCenter: [],
      headerRight: [],
    });
  };

  const onFinish = () => {
    loadingStatusController.setLoadingStatus(true);
    const sub = createAbstimmungsanfrageCall(abstimmungId, {
      kommentar: props.formValues?.kommentar as string,
    }).subscribe({
      next: () => {
        loadingStatusController.setLoadingStatus(false);
        history.push(`/hra/${routes.ANFRAGEN}/${routes.ANFRAGEN_ANLEGEN}/${routes.ERFOLG}`);
        sub?.unsubscribe();
      },
      error: (error: AjaxError) => {
        loadingStatusController.setLoadingStatus(false);
        errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
        sub?.unsubscribe();
      },
    });
  };

  return (
    <div className="beantworten-vote-page teilnahme-anfragen-page review-page">
      <Title level={1}>{t(`enorm.teilnahmeAnfragen.reviewPage.title`)}</Title>
      <GeneralFormWrapper
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={() => document?.getElementById('errorBox')?.focus()}
        scrollToFirstError={true}
      >
        <AbstimmungInfoComponent abstimmungInfo={props.abstimmung} kommentar={props.formValues?.kommentar} />
        <TeilnahmeAnfragenAnlegenFormControlComponent abstimmungId={abstimmungId} modify={true} />
      </GeneralFormWrapper>
    </div>
  );
}
