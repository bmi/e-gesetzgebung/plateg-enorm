// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { routes } from '../../../shares/routes';
import { EnormHelpComponent } from './help/component.react';
import { HomeTabsEnorm } from './home-tabs/component.react';
import { MitzeichnungRoutes } from './mitzeichnung-main/component.react';
import { TeilnahmeAnfragenRoutes } from './teilnahme-anfragen/component.react';
import { VoteDetails } from './vote-details/component.react';
import { VoteRoutes } from './vote-main/component.react';

export function MainRoutesEnorm(): React.ReactElement {
  return (
    <Switch>
      <Route
        exact
        path={[
          '/hra',
          `/hra/${routes.MEINE_ABSTIMMUNG}`,
          `/hra/${routes.BITTE_UM_MITZEICHNUNG}`,
          `/hra/${routes.ENTWUERFE}`,
          `/hra/${routes.ANFRAGEN}`,
          `/hra/${routes.ARCHIV}`,
          `/hra/${routes.UNTERABSTIMMUNG}`,
        ]}
      >
        <HomeTabsEnorm />
      </Route>
      <Route
        exact
        path={[
          `/hra/:tabName/${routes.ABSTIMMUNG_ANLEGEN}`,
          `/hra/:tabName/${routes.ABSTIMMUNG_ANLEGEN}/${routes.HAUSABSTIMMUNG}/:id`,
          `/hra/:tabName/${routes.ABSTIMMUNG_ANLEGEN}/${routes.RESSORTABSTIMMUNG}/:id`,
          `/hra/:tabName/${routes.ABSTIMMUNG_ANLEGEN}/${routes.VORHABENCLEARING}/:id`,
          `/hra/:tabName/${routes.ABSTIMMUNG_ANLEGEN}/${routes.SCHLUSSABSTIMMUNG}/:id`,
          `/hra/:tabName/${routes.ABSTIMMUNG_ANLEGEN}/${routes.ABSTIMMUNG_HAUSLEITUNG}/:id`,

          `/hra/:tabName/${routes.ABSTIMMUNG_PRUEFEN}`,
          `/hra/:tabName/${routes.ABSTIMMUNGSRUNDE_PRUEFEN}`,

          `/hra/:tabName/:id/${routes.ABSTIMMUNG_ANLEGEN}`,
          `/hra/:tabName/:id/${routes.UNTERABSTIMMUNG_ANLEGEN}`,
          `/hra/:tabName/:id/${routes.ABSTIMMUNG_BEARBEITEN}`,

          `/hra/:tabName/:id/${routes.NACHSTE_ABSTIMMUNGSRUNDE}`,

          `/hra/:id/${routes.ABSTIMMUNG_PRUEFEN}/${routes.ERFOLG}`,
          `/hra/:id/${routes.ABSTIMMUNG_PRUEFEN}/${routes.ERFOLGNURABSTIMMUNG}`,
          `/hra/:id/${routes.ABSTIMMUNG_PRUEFEN}/${routes.ERFOLGABSTIMMUNGPKP}`,
          `/hra/:id/${routes.ABSTIMMUNG_PRUEFEN}/${routes.ERFOLG_UA}`,
          `/hra/:id/${routes.ABSTIMMUNG_PRUEFEN}/${routes.ERFOLGUPD}`,
          `/hra/:id/${routes.ABSTIMMUNG_PRUEFEN}/${routes.ERFOLG_UA_UPD}`,
          `/hra/:tabName/${routes.ABSTIMMUNG_PRUEFEN}/${routes.FEHLER}`,
          `/hra/:tabName/:id/${routes.ABSTIMMUNG_PRUEFEN}/${routes.FEHLER}`,
          `/hra/:id/${routes.ABSTIMMUNG_PRUEFEN}/${routes.FEHLERPKP}`,
          `/hra/:tabName/:id/${routes.ABSTIMMUNGSRUNDE_PRUEFEN}/${routes.FEHLER}`,
        ]}
      >
        <VoteRoutes />
      </Route>
      <Route exact path={`/hra/${routes.HILFE}`}>
        <EnormHelpComponent />
      </Route>
      <Route
        exact
        path={[
          `/hra/:tabName/:id/${routes.ABSTIMMUNGSRUNDE}`,
          `/hra/:tabName/:id/${routes.ABSTIMMUNGSRUNDE}/${routes.ABSTIMMUNGSRUNDE_TEILNAHMER}`,
        ]}
      >
        <VoteDetails />
      </Route>
      <Route exact path={`/hra/:tabName/:id/${routes.ANFRAGE_TEILNAHME_DETAILS}`}>
        <VoteDetails />
      </Route>
      <Route
        path={[
          `/hra/:tabName/:id/${routes.ABSTIMMUNG_BEANTWORTEN}`,
          `/hra/:tabName/:id/${routes.ABSTIMMUNG_BEANTWORTEN_PRUEFEN}`,
        ]}
      >
        <MitzeichnungRoutes />
      </Route>
      <Route
        path={[
          `/hra/:tabName/:id/${routes.ANFRAGEN_ANLEGEN}`,
          `/hra/:tabName/:id/${routes.ANFRAGEN_ANLEGEN_PRUEFEN}`,
          `/hra/:tabName/${routes.ANFRAGEN_ANLEGEN}/${routes.ERFOLG}`,
        ]}
      >
        <TeilnahmeAnfragenRoutes />
      </Route>
    </Switch>
  );
}
