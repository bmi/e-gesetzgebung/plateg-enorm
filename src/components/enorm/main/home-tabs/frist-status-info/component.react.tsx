// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './frist-status-info.less';

import React from 'react';
import { useTranslation } from 'react-i18next';

import { InfoComponent } from '@plateg/theme';

interface FristStatusInfoProps {
  id: string;
}

export function FristStatusInfo(props: FristStatusInfoProps): React.ReactElement {
  const isBitteUmMitzeichnung = props.id.indexOf('bitte') !== -1;
  const { t } = useTranslation();
  return (
    <InfoComponent
      key={props.id}
      id={props.id}
      isContactPerson={false}
      titleWithoutPrefix={true}
      title={t('enorm.myVotes.fristStatusInfo.title')}
    >
      <p className="frist-status-info-content">
        <dl className="frist-status-wrapper-dl">
          <dd>
            {isBitteUmMitzeichnung
              ? t('enorm.myVotes.fristStatusInfo.content.paragraphMitzeichnung')
              : t('enorm.myVotes.fristStatusInfo.content.paragraphMeineAbstimmungen')}
          </dd>
          <dt>{t('enorm.myVotes.fristStatusInfo.content.subtitle')}</dt>
          <dd>
            <dl className="frist-status-entries-dl">
              {(t('enorm.myVotes.fristStatusInfo.content.entries', { returnObjects: true }) as []).map(
                (entry: { title: string; content: string }, index) => {
                  return (
                    <div key={`{${entry.title}-${index}}`} className="frist-status-entry">
                      <dt>{entry.title}</dt>
                      <dd>{entry.content}</dd>
                    </div>
                  );
                },
              )}
            </dl>
          </dd>
        </dl>
      </p>
    </InfoComponent>
  );
}
