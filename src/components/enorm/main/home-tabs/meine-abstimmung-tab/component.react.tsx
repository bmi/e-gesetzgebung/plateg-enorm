// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { AbstimmungTableDTO } from '@plateg/rest-api/models/';
import {
  ArchivConfirmComponent,
  EmptyContentComponent,
  ImageModule,
  TableComponent,
  TableComponentProps,
} from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { TabKeys } from '../component.react';
import {
  AbstimmungVersionenTableDTO,
  archiveTextKeys,
  getFilterButton,
  getFilteredRows,
  setArchived,
} from '../controller.react';
import { getMeineAbstimmungenTableVals } from './controller.react';

interface MeineAbstimmungTabComponentInterface {
  meineAbstimmungenData: AbstimmungTableDTO[];
  resetTab: (tab: string) => void;
  loggedUserEmail: string;
  tabKey: TabKeys;
  loadContent: (key: string, isFilterNeeded: boolean, shouldUpdateLoadedTabs: boolean) => void;
}

export type MeineAbstimmungenTableValsStateType = Pick<
  TableComponentProps<AbstimmungTableDTO>,
  | 'columns'
  | 'content'
  | 'className'
  | 'expandable'
  | 'sorterOptions'
  | 'customDefaultSortIndex'
  | 'expandableCondition'
  | 'filteredColumns'
>;

export function MeineAbstimmungTabComponent(props: MeineAbstimmungTabComponentInterface): React.ReactElement {
  const { t } = useTranslation();
  const [abstimmungenData, setAbstimmungenData] = useState(props.meineAbstimmungenData);
  const [confirmArchiveModalIsVisible, setConfirmArchiveModalIsVisible] = useState(false);
  const [selectedAbstimmung, setSelectedAbstimmung] = useState<AbstimmungVersionenTableDTO>(
    {} as AbstimmungVersionenTableDTO,
  );

  const [meineAbstimmungenTableVals, setMeineAbstimmungenTableVals] = useState<MeineAbstimmungenTableValsStateType>({
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
    customDefaultSortIndex: 0,
    expandableCondition: undefined,
    className: '',
  });

  const pagInfoResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).result;

  useEffect(() => {
    setAbstimmungenData(props.meineAbstimmungenData);
  }, [props.meineAbstimmungenData]);

  useEffect(() => {
    setMeineAbstimmungenTableVals(
      getMeineAbstimmungenTableVals(
        abstimmungenData,
        setConfirmArchiveModalIsVisible,
        setSelectedAbstimmung,
        props.loggedUserEmail,
      ),
    );
  }, [abstimmungenData]);

  const voteSubVote = props.tabKey === TabKeys.meineUnterabstimmung ? 'subVote' : 'myVotes';

  return (
    <>
      {!pagInfoResult.allContentEmpty && (
        <>
          <ArchivConfirmComponent
            actionTitle={t(archiveTextKeys.actionTitle)}
            okText={t(archiveTextKeys.okText)}
            cancelText={t(archiveTextKeys.cancelText)}
            visible={confirmArchiveModalIsVisible}
            setVisible={setConfirmArchiveModalIsVisible}
            setArchived={() => {
              setArchived(selectedAbstimmung.id, abstimmungenData, setAbstimmungenData, undefined, () => {
                setConfirmArchiveModalIsVisible(false);
                props.resetTab('archiv');
                props.loadContent(props.tabKey, true, false);
              });
            }}
          />
          <TableComponent
            bePagination
            tabKey={props.tabKey}
            id="hra-abstimmungen-table"
            columns={meineAbstimmungenTableVals.columns}
            content={meineAbstimmungenTableVals.content}
            filteredColumns={meineAbstimmungenTableVals.filteredColumns}
            filterRowsMethod={getFilteredRows}
            prepareFilterButtonMethod={getFilterButton}
            expandable={meineAbstimmungenTableVals.expandable}
            sorterOptions={meineAbstimmungenTableVals.sorterOptions}
            customDefaultSortIndex={meineAbstimmungenTableVals.customDefaultSortIndex}
            className={meineAbstimmungenTableVals.className}
            expandableCondition={meineAbstimmungenTableVals.expandableCondition}
          ></TableComponent>
        </>
      )}
      <EmptyContentComponent
        images={[
          {
            label: t(`enorm.myVotes.tabs.${voteSubVote}.imgText1`),
            // when pictures for subvotes are available source string should be updated with var 'voteSubVote'
            imgSrc: require(`../../../../../media/empty-content-images/myVotes/img1-myVotes.svg`) as ImageModule,
            height: 92,
          },
          {
            label: t(`enorm.myVotes.tabs.${voteSubVote}.imgText2`),
            imgSrc: require(`../../../../../media/empty-content-images/myVotes/img2-myVotes.svg`) as ImageModule,
            height: 126,
          },
        ]}
      >
        <p className="ant-typography p-no-style info-text">
          {t(`enorm.myVotes.tabs.${voteSubVote}.text`, {
            interpolation: { escapeValue: false },
          })}
        </p>
      </EmptyContentComponent>
    </>
  );
}
