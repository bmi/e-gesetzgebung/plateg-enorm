// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import Text from 'antd/lib/typography/Text';
import isFuture from 'date-fns/isFuture';
import isPast from 'date-fns/isPast';
import { createHashHistory } from 'history';
import i18n from 'i18next';
import React, { ReactElement } from 'react';

import { AbstimmungstypType, servers, VorhabenStatusType } from '@plateg/rest-api';
import { compareDates, DropdownMenu } from '@plateg/theme';
import { DropdownMenuItem } from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';
import { openFile } from '@plateg/theme/src/controllers/RestConfigController';

import { routes } from '../../../../../shares/routes';
import { MyVotesColumnNamesType } from '../../../../../utils/controller';
import {
  AbstimmungVersionenTableDTO,
  getAbstimmungsrundeRenderer,
  getFristRenderer,
  getMitzeichnungAbstimmungsrundeRenderer,
  getRegelungsvorhabenRenderer,
} from '../controller.react';
import { FristStatusInfo } from '../frist-status-info/component.react';
import { MeineAbstimmungenTableValsStateType } from './component.react';

const history = createHashHistory();

/**
 * helper function to replace multiple 'if' statements to reduce cognitive complexity
 */
export const getComparedData = (arg1: unknown, arg2: unknown, trusyRes: unknown, falsyRes: unknown) => {
  if (arg1 === arg2) {
    return trusyRes;
  }
  return falsyRes;
};

export function getMeineAbstimmungenTableVals(
  content: AbstimmungVersionenTableDTO[],
  setConfirmArchiveModalIsVisible: (confirmArchiveModalIsVisible: boolean) => void,
  setSelectedAbstimmung: (selectedAbstimmung: AbstimmungVersionenTableDTO) => void,
  loggedUserEmail: string,
): MeineAbstimmungenTableValsStateType {
  const columns: ColumnsType<AbstimmungVersionenTableDTO> = [
    {
      title: getComparedData(
        content[0]?.typ,
        AbstimmungstypType.Unterabstimmung,
        i18n.t('enorm.myVotes.tabs.subVote.table.thead1').toString(),
        i18n.t('enorm.myVotes.tabs.myVotes.table.thead1').toString(),
      ) as string,
      key: 'c1',
      render: (record: AbstimmungVersionenTableDTO): ReactElement =>
        getComparedData(
          record.typ,
          AbstimmungstypType.Unterabstimmung,
          getMitzeichnungAbstimmungsrundeRenderer(record?.oberabstimmung),
          getRegelungsvorhabenRenderer(record, loggedUserEmail),
        ) as ReactElement,
    },
    {
      title: getComparedData(
        content[0]?.typ,
        AbstimmungstypType.Unterabstimmung,
        i18n.t('enorm.myVotes.tabs.subVote.table.thead2').toString(),
        i18n.t('enorm.myVotes.tabs.myVotes.table.thead2').toString(),
      ) as string,
      key: 'erstellt',
      render: (record: AbstimmungVersionenTableDTO): ReactElement => {
        return getAbstimmungsrundeRenderer(
          `#/hra/${
            getComparedData(
              record.typ,
              AbstimmungstypType.Unterabstimmung,
              routes.UNTERABSTIMMUNG,
              routes.MEINE_ABSTIMMUNG,
            ) as string
          }/${record.id}/${routes.ABSTIMMUNGSRUNDE}`,
          record,
        );
      },
      sorter: (a, b) => compareDates(a.erstelltAm, b.erstelltAm),
    },
    {
      title: `${i18n.t('enorm.myVotes.tabs.myVotes.table.thead3')}`,
      key: 'c3',
      render: (element: AbstimmungVersionenTableDTO) => {
        if (element.regelungsentwurf?.dto?.fileName) {
          return (
            <a
              onClick={(e) => {
                e.preventDefault();
                openFile(
                  servers[0].getUrl() +
                    '/file/download/{fileId}'.replace(
                      '{fileId}',
                      encodeURI(element.regelungsentwurf?.base.id.toString()),
                    ),
                  {
                    fileName: element.regelungsentwurf?.dto?.fileName,
                    onError: (error) => {
                      console.error(error);
                    },
                  },
                );
              }}
              href="#"
            >
              {element.regelungsentwurf?.dto?.fileName}
            </a>
          );
        }
        if (element?.dokumentenmappe && element?.dokumentenmappe.dokumente) {
          return (
            <a target="_blank" href={`#/editor/document/${element?.dokumentenmappe.dokumente[0]?.id}`}>
              {' '}
              {element.dokumentenmappe?.titel}
            </a>
          );
        }
        return <Text> {element.dokumentenmappe?.titel}</Text>;
      },
    },
    {
      title: (
        <>
          {i18n.t('enorm.myVotes.tabs.myVotes.table.thead4')}
          <span
            onClick={(e) => {
              e.stopPropagation();
            }}
          >
            <FristStatusInfo
              id={
                getComparedData(
                  content[0]?.typ,
                  AbstimmungstypType.Unterabstimmung,
                  'frist-status-info-meine-unterabstimmungen',
                  'frist-status-info-meine-abstimmungen',
                ) as string
              }
            />
          </span>
        </>
      ),
      key: 'fristablauf',
      sorter: (a, b) => compareDates(a.fristablauf, b.fristablauf),
      render: (record: AbstimmungVersionenTableDTO) => {
        return getFristRenderer(record);
      },
    },
    {
      title: `${i18n.t('enorm.myVotes.tabs.myVotes.table.thead5')}`,
      key: 'c5',
      render: (record: AbstimmungVersionenTableDTO) =>
        getComparedData(
          record.currentVersion,
          false,
          <></>,
          <Text>
            {record.anzahlTeilnehmerMitAntwort} von {record.anzahlTeilnehmer}
          </Text>,
        ) as ReactElement,
    },
    {
      title: `${i18n.t('enorm.myVotes.tabs.myVotes.table.thead6')}`,
      key: 'c6',
      render: (element: AbstimmungVersionenTableDTO) => {
        if (!element.currentVersion) {
          return <> </>;
        }
        const items: DropdownMenuItem[] = [
          {
            element: i18n.t(`enorm.myVotes.tabs.myVotes.table.menuItemArchive`),
            onClick: () => {
              setSelectedAbstimmung(element);
              setConfirmArchiveModalIsVisible(true);
            },
            disabled: () => isFuture(new Date(element.fristablauf || '')),
          },
          {
            element: i18n.t(`enorm.myVotes.tabs.myVotes.table.menuItemAbstimmungBearbeiten`),
            onClick: () => {
              history.push(
                `/hra/${
                  getComparedData(
                    element.typ,
                    AbstimmungstypType.Unterabstimmung,
                    routes.UNTERABSTIMMUNG,
                    routes.MEINE_ABSTIMMUNG,
                  ) as string
                }/${element.id}/${routes.ABSTIMMUNG_BEARBEITEN}`,
              );
            },
            disabled: () => element.geschlossen || isPast(new Date(element.fristablauf || '')),
          },
        ];
        return (
          <div className="actions-holder">
            <Button
              id={`enorm-NaechsteAbstimmungEinleiten-btn-${element.id}`}
              disabled={
                element.geschlossen ||
                isFuture(new Date(element.fristablauf || '')) ||
                element.regelungsvorhaben?.dto.status !== VorhabenStatusType.InBearbeitung ||
                isPast(new Date(element.oberabstimmung?.fristablauf as string))
              }
              onClick={() => {
                history.push(
                  `/hra/${
                    getComparedData(
                      element.typ,
                      AbstimmungstypType.Unterabstimmung,
                      routes.UNTERABSTIMMUNG,
                      routes.MEINE_ABSTIMMUNG,
                    ) as string
                  }/${element.abstimmungMetadataId}/${routes.NACHSTE_ABSTIMMUNGSRUNDE}`,
                );
              }}
              className="ant-btn-secondary"
            >
              {i18n.t('enorm.myVotes.tabs.myVotes.table.btnTextNextRound').toString()}
            </Button>
            <DropdownMenu
              openLink={`/hra/${
                getComparedData(
                  element.typ,
                  AbstimmungstypType.Unterabstimmung,
                  routes.UNTERABSTIMMUNG,
                  routes.MEINE_ABSTIMMUNG,
                ) as string
              }/${element.id}/${routes.ABSTIMMUNGSRUNDE}`}
              items={items}
              elementId={element.id}
            />
          </div>
        );
      },
    },
  ];
  content = content.map(({ ...abstimmung }) => {
    abstimmung.currentVersion = true;
    if (abstimmung.versionen.length > 0) {
      abstimmung.children = abstimmung.versionen.map((version) => ({
        ...version,
        currentVersion: false,
        typ: abstimmung.typ,
      }));
    }
    return abstimmung;
  });

  const filteredColumns = getComparedData(
    content[0]?.typ,
    AbstimmungstypType.Unterabstimmung,
    [{ name: MyVotesColumnNamesType.mitzeichnungsanfrage, columnIndex: 0 }],
    [
      { name: MyVotesColumnNamesType.regelungsvorhaben, columnIndex: 0 },
      { name: MyVotesColumnNamesType.regelungsvorhabenNoSubvoteType, columnIndex: 0 },
    ],
  ) as { name: string; columnIndex: number }[];

  return {
    expandable: true,
    expandableCondition: (record: AbstimmungVersionenTableDTO) => {
      if (record.versionen?.length > 0) {
        return true;
      }
      return false;
    },
    columns,
    content,
    filteredColumns,
    sorterOptions: [
      {
        columnKey: 'erstellt',
        titleAsc: i18n.t('enorm.myVotes.tabs.myVotes.table.abstimmungsrundeAsc'),
        titleDesc: i18n.t('enorm.myVotes.tabs.myVotes.table.abstimmungsrundeDesc'),
      },
      {
        columnKey: 'fristablauf',
        titleAsc: i18n.t('enorm.myVotes.tabs.myVotes.table.fristAsc'),
        titleDesc: i18n.t('enorm.myVotes.tabs.myVotes.table.fristDesc'),
      },
    ],
    customDefaultSortIndex: 2,
    className: 'hidden-expanded-rows',
  };
}
