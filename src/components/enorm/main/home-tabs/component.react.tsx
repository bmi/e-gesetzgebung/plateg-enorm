// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './my-votes.less';

import { Button, Col, Row, TabsProps, Typography } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { Observable } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  AbstimmungTableDTO,
  AbstimmungTableDTOs,
  AnfrageTableDTO,
  AnfrageTableDTOs,
  ArchivTableDTO,
  ArchivTableDTOs,
  EntwurfTableDTO,
  EntwurfTableDTOs,
  MitzeichnungTableDTO,
  MitzeichnungTableDTOs,
  PaginierungDTO,
} from '@plateg/rest-api/models/';
import {
  BreadcrumbComponent,
  ErrorController,
  GlobalDI,
  HeaderController,
  LoadingStatusController,
  OpenEntriesIndicator,
  TabsWrapper,
  TitleWrapperComponent,
} from '@plateg/theme';
import { useAppDispatch, useAppSelector } from '@plateg/theme/src/components/store';
import {
  PagRequest,
  setPaginationFilterInfo,
  setPaginationInitState,
  setPaginationResult,
} from '@plateg/theme/src/components/store/slices/tablePaginationSlice';

import { routes } from '../../../../shares/routes';
import { EnormHelpBreadcrumb } from '../help/component.react';
import { AnfragenTabComponent } from './anfragen-tab/component.react';
import { ArchivTabComponent } from './archiv-tab/component.react';
import { BitteUmMitzeichnungTabComponent } from './bitte-um-mitzeichnung-tab/component.react';
import {
  getAnfragenCall,
  getArchivedCall,
  getEntwuerfeCall,
  getMeineAbstimmungenCall,
  getMeineBitteUmMitzeichnungCall,
  getMeineUnterabstimmungenCall,
} from './controller.react';
import { EntwuerfeTabComponent } from './entwuerfe-tab/component.react';
import { MeineAbstimmungTabComponent } from './meine-abstimmung-tab/component.react';

export enum TabKeys {
  meineAbstimmung = 'meineAbstimmung',
  bitteUmMitzeichnung = 'bitteUmMitzeichnung',
  meineUnterabstimmung = 'meineUnterabstimmung',
  entwuerfe = 'entwuerfe',
  anfragen = 'anfragen',
  archiv = 'archiv',
}

type ObserbvablesTypeUnion =
  | AbstimmungTableDTOs
  | EntwurfTableDTOs
  | ArchivTableDTOs
  | MitzeichnungTableDTOs
  | AnfrageTableDTOs;
interface MapTabsContentItemInterface {
  loadMethod: (isFilterNeeded: boolean, requestData: PaginierungDTO) => Observable<ObserbvablesTypeUnion>;
  setMethod: Function;
  getTableMethod?: Function;
}
interface MapTabsContentInterface {
  [key: string]: MapTabsContentItemInterface;
}

export function HomeTabsEnorm(): React.ReactElement {
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const headerController = GlobalDI.get<HeaderController>('enormHeadercontroller');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const { t } = useTranslation();
  const { Title } = Typography;
  const history = useHistory();
  const routeMatcherVorbereitung = useRouteMatch<{ tabKey: string }>('/hra/:tabKey');

  const tabKey =
    routeMatcherVorbereitung?.params?.tabKey != null
      ? routeMatcherVorbereitung?.params?.tabKey
      : routes.MEINE_ABSTIMMUNG;
  const [activeTab, setActiveTab] = useState<string>(tabKey);
  const [loadedTabs, setLoadedTabs] = useState<string[]>([]);
  const appStore = useAppSelector((state) => state.user);

  const [meineAbstimmungenData, setMeineAbstimmungenData] = useState<AbstimmungTableDTO[]>([]);
  const [bitteUmMitzeichnungData, setBitteUmMitzeichnungData] = useState<MitzeichnungTableDTO[]>([]);
  const [entwuerfeData, setEntwuerfeData] = useState<EntwurfTableDTO[]>([]);
  const [anfragenData, setAnfragenData] = useState<AnfrageTableDTO[]>([]);
  const [archivData, setArchivData] = useState<ArchivTableDTO[]>([]);
  const [openAnfragen, setOpenAnfragen] = useState(0);
  const [openMitzeichnungen, setOpenMitzeichnungen] = useState(0);
  const [meineUnterabstimmungenData, setMeineUnterabstimmungenData] = useState<AbstimmungTableDTO[]>([]);
  const [loggedUserEmail, setLoggedUserEmail] = useState<string>('');

  const dispatch = useAppDispatch();
  const initRender = useRef(true);
  const pagDataRequestAll = useAppSelector((state) => state.tablePagination.tabs);
  const pagDataRequestTab = pagDataRequestAll[tabKey].request;

  const getPagReqData = (pagDataRequest: PagRequest): PaginierungDTO => {
    return {
      filters: pagDataRequest?.filters || {},
      pageNumber: pagDataRequest?.currentPage || 0,
      pageSize: 20,
      sortBy: pagDataRequest?.columnKey,
      sortDirection: pagDataRequest?.sortOrder,
    };
  };

  const mapTabsContent: MapTabsContentInterface = {
    meineAbstimmung: {
      loadMethod: getMeineAbstimmungenCall,
      setMethod: setMeineAbstimmungenData,
    },
    meineUnterabstimmung: {
      loadMethod: getMeineUnterabstimmungenCall,
      setMethod: setMeineUnterabstimmungenData,
    },
    bitteUmMitzeichnung: {
      loadMethod: getMeineBitteUmMitzeichnungCall,
      setMethod: setBitteUmMitzeichnungData,
    },
    entwuerfe: {
      loadMethod: getEntwuerfeCall,
      setMethod: setEntwuerfeData,
    },
    anfragen: {
      loadMethod: getAnfragenCall,
      setMethod: setAnfragenData,
    },
    archiv: {
      loadMethod: getArchivedCall,
      setMethod: setArchivData,
    },
  };

  useEffect(() => {
    if (!initRender.current) {
      if (tabKey !== activeTab) {
        setActiveTab(tabKey);
        if (!loadedTabs.includes(tabKey)) {
          loadContent(tabKey, getPagReqData(pagDataRequestTab), true, true);
        }
      } else {
        loadContent(activeTab, getPagReqData(pagDataRequestTab), false);
      }
    }
  }, [
    pagDataRequestTab.currentPage,
    pagDataRequestTab.columnKey,
    pagDataRequestTab.sortOrder,
    pagDataRequestTab.filters,
    tabKey,
  ]);

  useEffect(() => {
    if (initRender.current) {
      loadContent(tabKey, getPagReqData(pagDataRequestTab)) as Observable<ObserbvablesTypeUnion>;

      const tabsForIntialLoad = [tabKey];
      if (tabKey !== routes.ANFRAGEN) {
        loadContent(TabKeys.anfragen, getPagReqData(pagDataRequestAll.anfragen.request));
        tabsForIntialLoad.push(TabKeys.anfragen);
      }
      if (tabKey !== routes.BITTE_UM_MITZEICHNUNG) {
        loadContent(TabKeys.bitteUmMitzeichnung, getPagReqData(pagDataRequestAll.bitteUmMitzeichnung.request));
        tabsForIntialLoad.push(TabKeys.bitteUmMitzeichnung);
      }

      setLoadedTabs([...tabsForIntialLoad]);
      setBreadcrumb(tabKey);

      initRender.current = false;
    }
  }, []);
  useEffect(() => {
    if (appStore.user) {
      setLoggedUserEmail(appStore.user.dto.email);
    }
  }, [appStore.user]);

  useEffect(() => {
    setBreadcrumb(activeTab || routes.MEINE_ABSTIMMUNG);
  }, [activeTab]);

  const loadContent = (
    key: string,
    requestData: PaginierungDTO,
    isFilterNeeded = true,
    shouldUpdateLoadedTabs = false,
  ) => {
    if (!mapTabsContent[`${key}`]) {
      return false;
    }

    if (isFilterNeeded) {
      loadingStatusController.setLoadingStatus(true);
      dispatch(setPaginationInitState({ tabKey: key }));
    }

    const loadRequest = mapTabsContent[`${key}`].loadMethod(isFilterNeeded, requestData);
    const loadSub = loadRequest.subscribe({
      next: (data) => {
        mapTabsContent[`${key}`].setMethod(data.dtos.content);
        if (shouldUpdateLoadedTabs) {
          setLoadedTabs([...loadedTabs, key]);
        }

        const { totalElements, number } = data.dtos;
        const { allContentEmpty } = data;
        dispatch(
          setPaginationResult({
            tabKey: key,
            totalItems: totalElements,
            currentPage: number,
            allContentEmpty,
          }),
        );
        if (isFilterNeeded) {
          dispatch(
            setPaginationFilterInfo({
              tabKey: key,
              ...data,
            }),
          );
        }

        if (key === TabKeys.bitteUmMitzeichnung.toString() && (data as MitzeichnungTableDTOs).badgeNumber !== -1) {
          setOpenMitzeichnungen((data as MitzeichnungTableDTOs).badgeNumber);
        }
        if (key === TabKeys.anfragen.toString() && (data as AnfrageTableDTOs).badgeNumber !== -1) {
          setOpenAnfragen((data as AnfrageTableDTOs).badgeNumber);
        }

        loadingStatusController.setLoadingStatus(false);
        loadSub.unsubscribe();
      },
      error: (error: AjaxError) => {
        loadingStatusController.setLoadingStatus(false);
        errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
        loadSub.unsubscribe();
      },
    });

    return loadRequest;
  };

  const resetTab = (key: string | string[]) => {
    if (Array.isArray(key)) {
      setLoadedTabs(
        loadedTabs.filter((tab: string) => {
          return !key.includes(tab);
        }),
      );
    } else {
      setLoadedTabs(
        loadedTabs.filter((tab: string) => {
          return tab !== key;
        }),
      );
    }
  };

  const setBreadcrumb = (tabName: string) => {
    const tabText = (
      <span>
        {t(`enorm.breadcrumbs.projectName`)} - {t(`enorm.breadcrumbs.tabName.${tabName}`)}
      </span>
    );
    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[tabText]} />],
      headerLast: [<EnormHelpBreadcrumb key="enormhelp-breadcrumb" />],
      headerCenter: [], // Remove all buttons that were added on internal pages
      headerRight: [], // Remove all buttons that were added on internal pages
    });
  };

  const tabsItems: TabsProps['items'] = [
    {
      key: TabKeys.meineAbstimmung,
      label: t('enorm.myVotes.tabs.myVotes.tabNav'),
      children: (
        <MeineAbstimmungTabComponent
          tabKey={TabKeys.meineAbstimmung}
          meineAbstimmungenData={meineAbstimmungenData}
          resetTab={resetTab}
          loggedUserEmail={loggedUserEmail}
          loadContent={(key: string, isFilterNeeded: boolean, shouldUpdateLoadedTabs: boolean) => {
            loadContent(key, getPagReqData(pagDataRequestTab), isFilterNeeded, shouldUpdateLoadedTabs);
          }}
        />
      ),
    },
    {
      key: TabKeys.bitteUmMitzeichnung,
      label: (
        <div>
          {t('enorm.myVotes.tabs.sign.tabNav')}
          <OpenEntriesIndicator numberOfOpenEntries={openMitzeichnungen} />
        </div>
      ),
      children: (
        <BitteUmMitzeichnungTabComponent
          tabKey={TabKeys.bitteUmMitzeichnung}
          resetTab={resetTab}
          bitteUmMitzeichnungData={bitteUmMitzeichnungData}
          loggedUserEmail={loggedUserEmail}
          loadContent={(key: string, isFilterNeeded: boolean, shouldUpdateLoadedTabs: boolean) => {
            loadContent(key, getPagReqData(pagDataRequestTab), isFilterNeeded, shouldUpdateLoadedTabs);
          }}
        />
      ),
    },
    {
      key: TabKeys.meineUnterabstimmung,
      label: t('enorm.myVotes.tabs.subVote.tabNav'),
      children: (
        <MeineAbstimmungTabComponent
          tabKey={TabKeys.meineUnterabstimmung}
          meineAbstimmungenData={meineUnterabstimmungenData}
          resetTab={resetTab}
          loggedUserEmail={loggedUserEmail}
          loadContent={(key: string, isFilterNeeded: boolean, shouldUpdateLoadedTabs: boolean) => {
            loadContent(key, getPagReqData(pagDataRequestTab), isFilterNeeded, shouldUpdateLoadedTabs);
          }}
        />
      ),
    },
    {
      key: TabKeys.entwuerfe,
      label: t('enorm.myVotes.tabs.drafts.tabNav'),
      children: (
        <EntwuerfeTabComponent
          tabKey={TabKeys.entwuerfe}
          entwuerfeData={entwuerfeData}
          setEntwuerfeData={setEntwuerfeData}
          loggedUserEmail={loggedUserEmail}
          loadContent={() => {
            loadContent(TabKeys.entwuerfe, getPagReqData(pagDataRequestTab), true, false);
          }}
        />
      ),
    },
    {
      key: TabKeys.anfragen,
      label: (
        <div>
          {t('enorm.myVotes.tabs.requests.tabNav')}
          <OpenEntriesIndicator numberOfOpenEntries={openAnfragen} />
        </div>
      ),
      children: (
        <AnfragenTabComponent
          tabKey={TabKeys.anfragen}
          resetTab={resetTab}
          anfragenData={anfragenData}
          setAnfragenData={setAnfragenData}
          loadContent={(key: string, isFilterNeeded: boolean, shouldUpdateLoadedTabs: boolean) => {
            loadContent(key, getPagReqData(pagDataRequestTab), isFilterNeeded, shouldUpdateLoadedTabs);
          }}
          loggedUserEmail={loggedUserEmail}
        />
      ),
    },
    {
      key: TabKeys.archiv,
      label: t('enorm.myVotes.tabs.archive.tabNav'),
      children: (
        <ArchivTabComponent tabKey={TabKeys.archiv} archivData={archivData} loggedUserEmail={loggedUserEmail} />
      ),
    },
  ];
  return (
    <div className="myVotesPage">
      <TitleWrapperComponent>
        <Row>
          <Col xs={{ span: 22, offset: 1 }}>
            <div className="heading-holder">
              <Title level={1}>{t('enorm.myVotes.mainTitle')}</Title>
              <Button
                id="enorm-neueAbstimmungAnlegen-btn"
                type="primary"
                size={'large'}
                onClick={() => {
                  history.push(`/hra/${routes.MEINE_ABSTIMMUNG}/${routes.ABSTIMMUNG_ANLEGEN}`);
                }}
              >
                {t('enorm.myVotes.btnNewVote')}
              </Button>
            </div>
          </Col>
        </Row>
      </TitleWrapperComponent>
      <Row>
        <Col xs={{ span: 22, offset: 1 }}>
          <TabsWrapper
            activeKey={activeTab}
            className="my-votes-tabs standard-tabs"
            onChange={(key: string) => history.push(`/hra/${key}`)}
            moduleName={t('enorm.myVotes.mainTitle')}
            items={tabsItems}
          />
        </Col>
      </Row>
    </div>
  );
}
