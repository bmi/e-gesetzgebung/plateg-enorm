// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Subscription } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  AbstimmungEntityResponseDTO,
  AnfrageStatusType,
  AnfrageTableDTO,
  AnfrageTableDTOOriginalTabEnum,
  AnfragetypType,
} from '@plateg/rest-api';
import {
  ArchivConfirmComponent,
  EmptyContentComponent,
  ErrorController,
  GlobalDI,
  ImageModule,
  LoadingStatusController,
  TableComponent,
  TableComponentProps,
} from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { routes } from '../../../../../shares/routes';
import { VoteBaseController } from '../../vote-main/vote-base/controller';
import { TabKeys } from '../component.react';
import {
  anfrageZurueckziehenTextKeys,
  archiveTextKeys,
  getFilterButton,
  getFilteredRows,
  setArchived,
} from '../controller.react';
import { FristAnfrageAntwortComponent } from './anfrage-antwort-modal/frist-anfrage-antwort/component.react';
import { TeilnahmeAnfrageAntwortComponent } from './anfrage-antwort-modal/teilnahme-anfrage-antwort-modal/component.react';
import { AnfrageZurueckziehenComponent } from './anfrage-zurueckziehe/component.react';
import { ExtendedAnfrageTableDTO, getAnfragenTableVals } from './controller.react';

interface AnfragenTabComponentInterface {
  anfragenData: AnfrageTableDTO[];
  setAnfragenData: (anfragenData: AnfrageTableDTO[]) => void;
  resetTab: (tab: string) => void;
  loadContent: (key: string, isFilterNeeded: boolean, shouldUpdateLoadedTabs: boolean) => void;
  loggedUserEmail: string;
  tabKey: TabKeys;
}
export function AnfragenTabComponent(props: AnfragenTabComponentInterface): React.ReactElement {
  const { t } = useTranslation();
  const [anfragenData, setAnfragenData] = useState(props.anfragenData);
  const [anfragenTableVals, setAnfragenTableVals] = useState<TableComponentProps<ExtendedAnfrageTableDTO>>({
    id: '',
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
  });
  const [anfrageAntwortModalIsVisible, setAnfrageAntwortModalIsVisible] = useState(false);
  const [confirmArchiveModalIsVisible, setConfirmArchiveModalIsVisible] = useState(false);
  const [anfrageZurueckziehenIsVisible, setAnfrageZurueckziehenIsVisible] = useState(false);
  const [selectedAnfrage, setSelectedAnfrage] = useState<ExtendedAnfrageTableDTO>();
  const [abstimmungDetails, setAbstimmungDetails] = useState<AbstimmungEntityResponseDTO>();

  const loadingStatusCtrl = GlobalDI.getOrRegister('loadingStatusController', () => new LoadingStatusController());

  const errorController = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const voteCtrl = GlobalDI.getOrRegister('enormVoteController', () => new VoteBaseController());

  const pagInfoResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).result;

  useEffect(() => {
    let subInitialData: Subscription;
    if (selectedAnfrage) {
      loadingStatusCtrl.setLoadingStatus(true);
      subInitialData = voteCtrl.getVote(selectedAnfrage?.abstimmungId).subscribe({
        next: (data: AbstimmungEntityResponseDTO) => {
          setAbstimmungDetails(data);
          loadingStatusCtrl.setLoadingStatus(false);
        },
        error: (error: AjaxError) => {
          errorController.displayErrorMsg(error, 'enorm.generalErrorMsg');
          loadingStatusCtrl.setLoadingStatus(false);
        },
      });
    }
    return () => {
      subInitialData?.unsubscribe();
    };
  }, [selectedAnfrage]);

  useEffect(() => {
    setAnfragenData(props.anfragenData);
  }, [props.anfragenData]);

  useEffect(() => {
    setAnfragenTableVals(
      getAnfragenTableVals(
        anfragenData,
        setSelectedAnfrage,
        setAnfrageAntwortModalIsVisible,
        setConfirmArchiveModalIsVisible,
        props.loggedUserEmail,
        setAnfrageZurueckziehenIsVisible,
      ),
    );
  }, [anfragenData]);

  return (
    <>
      {!pagInfoResult.allContentEmpty && (
        <>
          {selectedAnfrage && abstimmungDetails && (
            <>
              {selectedAnfrage.anfrage.dto.typ === AnfragetypType.Fristverlaengerung && (
                <FristAnfrageAntwortComponent
                  isVisible={
                    anfrageAntwortModalIsVisible &&
                    selectedAnfrage?.anfrage.dto.typ === AnfragetypType.Fristverlaengerung
                  }
                  setIsVisible={setAnfrageAntwortModalIsVisible}
                  selectedAnfrage={selectedAnfrage}
                  abstimmungDetails={abstimmungDetails}
                  resetTab={() => {
                    props.resetTab(routes.ANFRAGEN);
                    props.loadContent(routes.ANFRAGEN, true, false);
                  }}
                />
              )}
              {selectedAnfrage.anfrage.dto.typ === AnfragetypType.Abstimmungsteilnahme && (
                <TeilnahmeAnfrageAntwortComponent
                  abstimmungDetails={abstimmungDetails}
                  isVisible={
                    anfrageAntwortModalIsVisible &&
                    selectedAnfrage.anfrage.dto.typ === AnfragetypType.Abstimmungsteilnahme
                  }
                  setIsVisible={setAnfrageAntwortModalIsVisible}
                  selectedAnfrage={selectedAnfrage}
                  resetTab={() => {
                    props.resetTab(routes.ANFRAGEN);
                    props.loadContent(routes.ANFRAGEN, false, false);
                  }}
                />
              )}
              <AnfrageZurueckziehenComponent
                modalTitle={t(anfrageZurueckziehenTextKeys(selectedAnfrage.anfrage.dto.typ).modalTitle)}
                actionTitle={t(anfrageZurueckziehenTextKeys(selectedAnfrage.anfrage.dto.typ).actionTitle)}
                okText={t(anfrageZurueckziehenTextKeys(selectedAnfrage.anfrage.dto.typ).okText)}
                cancelText={t(anfrageZurueckziehenTextKeys(selectedAnfrage.anfrage.dto.typ).cancelText)}
                successMsg={t(anfrageZurueckziehenTextKeys(selectedAnfrage.anfrage.dto.typ).successMsg)}
                visible={anfrageZurueckziehenIsVisible}
                setVisible={setAnfrageZurueckziehenIsVisible}
                abstimmungId={abstimmungDetails.base.id}
                setZurueckziehen={() => {
                  setAnfrageZurueckziehenIsVisible(false);
                  props.resetTab(routes.ANFRAGEN);
                  props.loadContent(routes.ANFRAGEN, false, false);
                }}
              />
            </>
          )}
          <ArchivConfirmComponent
            actionTitle={
              selectedAnfrage?.anfrage.dto.status === AnfrageStatusType.Offen
                ? t('enorm.myVotes.tabs.requests.table.archiveModalContent.offen')
                : t('enorm.myVotes.tabs.requests.table.archiveModalContent.default')
            }
            okText={t(archiveTextKeys.okText)}
            cancelText={t(archiveTextKeys.cancelText)}
            visible={confirmArchiveModalIsVisible}
            setVisible={setConfirmArchiveModalIsVisible}
            setArchived={() => {
              if (selectedAnfrage) {
                setArchived(selectedAnfrage?.anfrage.base.id, props.anfragenData, props.setAnfragenData, true, () => {
                  setConfirmArchiveModalIsVisible(false);
                  props.resetTab(routes.ANFRAGEN);
                  props.loadContent(routes.ANFRAGEN, false, false);
                });
              }
            }}
          />
          <TableComponent
            bePagination
            tabKey={props.tabKey}
            id="hra-anfragen-table"
            expandable={anfragenTableVals.expandable}
            columns={anfragenTableVals.columns}
            content={anfragenTableVals.content}
            filteredColumns={anfragenTableVals.filteredColumns}
            filterRowsMethod={getFilteredRows}
            prepareFilterButtonMethod={getFilterButton}
            sorterOptions={anfragenTableVals.sorterOptions}
            customDefaultSortIndex={anfragenTableVals.customDefaultSortIndex}
            expandedRowRender={anfragenTableVals.expandedRowRender}
            expandableCondition={anfragenTableVals.expandableCondition}
            rowClassName={(element: AnfrageTableDTO) => {
              return element.anfrage.dto.status === AnfrageStatusType.Offen &&
                element.originalTab === AnfrageTableDTOOriginalTabEnum.MeineAbstimmungen &&
                !element.geschlossen
                ? 'open-anfrage-row'
                : '';
            }}
          ></TableComponent>
        </>
      )}
      <EmptyContentComponent
        images={[
          {
            label: t(`enorm.myVotes.tabs.requests.imgText1`),
            imgSrc: require(`../../../../../media/empty-content-images/requests/img1-requests.svg`) as ImageModule,
            height: 94,
          },
          {
            label: t(`enorm.myVotes.tabs.requests.imgText2`),
            imgSrc: require(`../../../../../media/empty-content-images/requests/img2-requests.svg`) as ImageModule,
            height: 94,
          },
        ]}
      >
        <p className="ant-typography p-no-style info-text">
          {t('enorm.myVotes.tabs.requests.text', {
            interpolation: { escapeValue: false },
          })}
        </p>
      </EmptyContentComponent>
    </>
  );
}
