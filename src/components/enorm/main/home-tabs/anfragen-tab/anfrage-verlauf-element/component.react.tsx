// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './anfrage-verlauf-element.less';

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import {
  AbstimmungstypType,
  AnfrageStatusType,
  AnfrageTableDTO,
  AnfragetypType,
  MitzeichnungTableDTO,
} from '@plateg/rest-api';
import { getDateTimeString } from '@plateg/theme';

import { getAbstimmungsrundeName } from '../../../../../../utils/controller';

interface AnfrageVerlaufElementComponentProps {
  element: AnfrageTableDTO | MitzeichnungTableDTO;
  response: boolean;
}
export function AnfrageVerlaufElementComponent(props: AnfrageVerlaufElementComponentProps): JSX.Element {
  const { t } = useTranslation();

  const titlePrefix = props.response
    ? t(`enorm.myVotes.tabs.requests.table.status.${props.element.anfrage?.dto.status as AnfrageStatusType}`) + ': '
    : '';
  const title = t(
    `enorm.myVotes.tabs.requests.table.verlauf.element.subject.${props.element.anfrage?.dto.typ as AnfragetypType}`,
    {
      abstimmungsTyp: t(`enorm.reviewVote.generalInfo.type${props.element.typ as AbstimmungstypType}`),
      title: getAbstimmungsrundeName(props.element.version, props.element.titel),
    },
  );
  const sentDate = props.response
    ? (props.element.anfrage?.dto.geantwortetAm as string)
    : props.element.anfrage?.base.erstelltAm;
  const from = props.response
    ? (props.element.ersteller.dto.name as string)
    : (props.element.anfrage?.dto.erstelltVon?.dto.name as string);
  const to = props.response
    ? (props.element.anfrage?.dto.erstelltVon?.dto.name as string)
    : (props.element.ersteller.dto.name as string);
  const comments = props.response
    ? props.element.anfrage?.dto.geantwortetKommentar
    : props.element.anfrage?.dto.kommentar;

  const commentTitle =
    props.response === false && props.element.anfrage?.dto.typ === AnfragetypType.Abstimmungsteilnahme
      ? t('enorm.myVotes.tabs.requests.table.verlauf.element.reasonTeilnahme')
      : t('enorm.myVotes.tabs.requests.table.verlauf.element.comments');

  return (
    <div className={`anfrage-verlauf-element ${props.response ? 'reponse-verlauf-element' : ''}`}>
      <div className="verlauf-meta-data">
        <span className="verlauf-title">
          {titlePrefix}
          {title}
        </span>
        <dl>
          <dt>{t('enorm.myVotes.tabs.requests.table.verlauf.element.sentDate')}</dt>
          <dd>{getDateTimeString(sentDate)}</dd>
          <br />
          <dt>{t('enorm.myVotes.tabs.requests.table.verlauf.element.from')}</dt>
          <dd>{from}</dd>
          <br />
          <dt>{t('enorm.myVotes.tabs.requests.table.verlauf.element.to')}</dt>
          <dd>{to}</dd>
        </dl>
      </div>
      <Title level={4} className="verlauf-anmerkung-title">
        {commentTitle}
      </Title>
      <span className="verlauf-anmerkung">
        {comments || <em>{t('enorm.myVotes.tabs.requests.table.verlauf.element.na')}</em>}
      </span>
    </div>
  );
}
