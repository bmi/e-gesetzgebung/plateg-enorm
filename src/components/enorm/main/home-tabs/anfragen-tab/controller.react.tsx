// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import Text from 'antd/lib/typography/Text';
import isPast from 'date-fns/isPast';
import i18n from 'i18next';
import React, { ReactElement } from 'react';

import {
  AnfrageStatusType,
  AnfrageTableDTO,
  AnfrageTableDTOOriginalTabEnum,
  AnfragetypType,
  ErstellerType,
} from '@plateg/rest-api';
import {
  compareDates,
  ContactPerson,
  DropdownMenu,
  getDateTimeString,
  InfoComponent,
  TableComponentProps,
  TwoLineText,
} from '@plateg/theme';
import { DropdownMenuItem } from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';
import { Filters } from '@plateg/theme/src/shares/filters';

import { routes } from '../../../../../shares/routes';
import { getAbstimmungsrundeName } from '../../../../../utils/controller';
import { getRegelungsvorhabenRenderer } from '../controller.react';
import { AnfrageVerlaufComponent } from './anfrage-verlauf/component.react';

export interface ExtendedAnfrageTableDTO extends AnfrageTableDTO {
  abstimmungId: string;
}

const getUrlTarget = (element: ExtendedAnfrageTableDTO) => {
  let target =
    element.originalTab === AnfrageTableDTOOriginalTabEnum.MeineAbstimmungen
      ? routes.ABSTIMMUNGSRUNDE
      : routes.ABSTIMMUNG_BEANTWORTEN_PRUEFEN;
  if (element.anfrage.dto.typ === AnfragetypType.Abstimmungsteilnahme) {
    target = routes.ANFRAGE_TEILNAHME_DETAILS;
  }
  return target;
};

export function getAnfragenTableVals(
  initalContent: AnfrageTableDTO[],
  setSelectedAnfrage: (selectedAnfrage: ExtendedAnfrageTableDTO) => void,
  setAnfrageAntwortModalIsVisible: (fristAnfrageAntwortModalIsVisible: boolean) => void,
  setConfirmArchiveModalIsVisible: (confirmArchiveModalIsVisible: boolean) => void,
  loggedUserEmail: string,
  setAnfrageZurueckziehenIsVisible: (anfrageZurueckziehenIsVisible: boolean) => void,
): TableComponentProps<ExtendedAnfrageTableDTO> {
  const columns: ColumnsType<ExtendedAnfrageTableDTO> = [
    {
      title: i18n.t('enorm.myVotes.tabs.requests.table.thead1').toString(),
      key: 'c1',
      render: (element: ExtendedAnfrageTableDTO): ReactElement => {
        return <Text>{i18n.t(`enorm.myVotes.tabs.requests.table.type.${element.anfrage.dto.typ}`).toString()}</Text>;
      },
    },
    {
      title: i18n.t('enorm.myVotes.tabs.requests.table.thead2').toString(),
      key: 'c2',
      render: (element: ExtendedAnfrageTableDTO): ReactElement => {
        return getRegelungsvorhabenRenderer(element);
      },
    },
    {
      title: i18n.t('enorm.myVotes.tabs.requests.table.thead3').toString(),
      key: 'c3',
      render: (element: ExtendedAnfrageTableDTO): ReactElement => {
        const link = `#/hra/${routes.ANFRAGEN}/${element.abstimmungId}/${getUrlTarget(element)}`;
        const anfrageTyp = i18n
          .t(`enorm.myVotes.tabs.requests.table.voteAnfrageType.${element.anfrage.dto.typ}`)
          .toString();

        return (
          <TwoLineText
            firstRowLink={link}
            firstRow={`${anfrageTyp} - ${getAbstimmungsrundeName(element.version, element.titel)}`}
            firstRowBold={false}
            secondRow={Filters.date(new Date(element.erstelltAm))}
            secondRowBold={false}
            secondRowLight={true}
            elementId={element.id}
          />
        );
      },
    },
    {
      title: i18n.t('enorm.myVotes.tabs.requests.table.thead4').toString(),
      key: 'c4',
      render: (element: ExtendedAnfrageTableDTO) => {
        if (element.anfrage.dto.erstelltVon) {
          const stellvertreter = element.bearbeiterStellvertreter;
          return (
            <>
              {stellvertreter && (
                <span className="stellvertretung-label">
                  {i18n.t('enorm.beantwortenVote.generalInfo.stellvertretungLabel').toString()}
                </span>
              )}
              <InfoComponent
                isContactPerson={true}
                title={i18n.t('enorm.myVotes.tabs.requests.table.contactPersonDrawerTitle')}
                buttonText={stellvertreter?.dto.name || element.anfrage.dto.erstelltVon.dto.name || ''}
                id={`${element.anfrage.dto.erstelltVon.base.id}-${element.abstimmungId}`}
              >
                <ContactPerson user={stellvertreter?.dto || element.anfrage.dto.erstelltVon.dto} />
              </InfoComponent>
            </>
          );
        } else {
          return <>{i18n.t('enorm.myVotes.tabs.requests.table.andererTeilnehmer')}</>;
        }
      },
    },
    {
      title: i18n.t('enorm.myVotes.tabs.requests.table.thead5').toString(),
      key: 'bearbeitetam',
      sorter: (a, b) => compareDates(a.anfrage.base.bearbeitetAm, b.anfrage.base.bearbeitetAm),
      render: (element: ExtendedAnfrageTableDTO) => {
        return getDateTimeString(element.anfrage.base.bearbeitetAm);
      },
    },
    {
      title: i18n.t('enorm.myVotes.tabs.requests.table.thead6').toString(),
      key: 'fristablaufNeu',
      sorter: (a, b) => compareDates(a.anfrage.dto.neuerFristablauf, b.anfrage.dto.neuerFristablauf),
      render: (element: ExtendedAnfrageTableDTO) => {
        const newFrist = element.anfrage.dto.neuerFristablauf;
        if (newFrist) {
          return getDateTimeString(newFrist);
        } else {
          return <></>;
        }
      },
    },
    {
      title: (
        <span>
          {i18n.t('enorm.myVotes.tabs.requests.table.thead7').toString()}
          <InfoComponent title={i18n.t('enorm.myVotes.tabs.requests.table.statusInfo.title')} titleWithoutPrefix={true}>
            <p
              dangerouslySetInnerHTML={{
                __html: i18n.t('enorm.myVotes.tabs.requests.table.statusInfo.content', {
                  interpolation: { escapeValue: false },
                }),
              }}
            ></p>
          </InfoComponent>
        </span>
      ),
      key: 'c7',
      render: (element: ExtendedAnfrageTableDTO) => {
        return (
          <Text>{i18n.t(`enorm.myVotes.tabs.requests.table.status.${element.anfrage.dto.status}`).toString()}</Text>
        );
      },
    },
    {
      title: i18n.t('enorm.myVotes.tabs.requests.table.thead8').toString(),
      key: 'c8',
      render: (element: ExtendedAnfrageTableDTO) => {
        const items: DropdownMenuItem[] = [
          {
            element: i18n.t(`enorm.myVotes.tabs.requests.table.archiveRequest`),
            disabled: () => {
              return (
                (element.erstellerType === ErstellerType.Selbst &&
                  element.anfrage.dto.typ === AnfragetypType.Fristverlaengerung) ||
                !element.anfrage.dto.erstelltVon ||
                (element.anfrage.dto.typ === AnfragetypType.Fristverlaengerung &&
                  element.anfrage.dto.status === AnfrageStatusType.Offen)
              );
            },
            onClick: () => {
              setSelectedAnfrage(element);
              setConfirmArchiveModalIsVisible(true);
            },
          },
        ];
        if (
          element.anfrage.dto.status === AnfrageStatusType.Offen &&
          element.anfrage.dto.erstelltVon?.dto.email === loggedUserEmail
        ) {
          items.push({
            element: i18n.t(`enorm.myVotes.tabs.myVotes.table.menuItemZurueckziehen.${element.anfrage.dto.typ}`, {
              type: element.anfrage.dto.typ,
            }),
            onClick: () => {
              setSelectedAnfrage(element);
              setAnfrageZurueckziehenIsVisible(true);
            },
            disabled: () => false,
          });
        }
        return (
          <div className="actions-holder">
            {element.fristablauf &&
              !isPast(new Date(element.fristablauf)) &&
              !element.geschlossen &&
              element.anfrage.dto.status === AnfrageStatusType.Offen &&
              element.originalTab === AnfrageTableDTOOriginalTabEnum.MeineAbstimmungen && (
                <Button
                  id={`enorm-tableBeantworten-btn-${element.id}`}
                  className="ant-btn-secondary"
                  onClick={() => {
                    setSelectedAnfrage(element);
                    setAnfrageAntwortModalIsVisible(true);
                  }}
                >
                  {i18n.t('enorm.myVotes.tabs.requests.table.answerButton').toString()}
                </Button>
              )}
            <DropdownMenu
              openLink={`/hra/${routes.ANFRAGEN}/${element.abstimmungId}/${getUrlTarget(element)}`}
              items={items}
              elementId={element.id}
            />
          </div>
        );
      },
    },
  ];

  const content: ExtendedAnfrageTableDTO[] = initalContent.map((item, index) => {
    return {
      ...item,
      abstimmungId: item.id,
      id: `${item.anfrage.base.id}-${index}`,
    };
  });

  return {
    id: 'hra-anfragen-table',
    expandable: true,
    columns,
    content,
    customDefaultSortIndex: 2,
    expandableCondition: (record) => record.anfrage.dto.erstelltVon !== null,
    expandedRowRender: (record) => <AnfrageVerlaufComponent element={record} />,
    sorterOptions: [
      {
        columnKey: 'bearbeitetam',
        titleAsc: i18n.t('enorm.myVotes.tabs.requests.table.sorter.gesendetAsc'),
        titleDesc: i18n.t('enorm.myVotes.tabs.requests.table.sorter.gesendetDesc'),
      },
      {
        columnKey: 'fristablaufNeu',
        titleAsc: i18n.t('enorm.myVotes.tabs.requests.table.sorter.fristAsc'),
        titleDesc: i18n.t('enorm.myVotes.tabs.requests.table.sorter.fristDesc'),
      },
    ],
    filteredColumns: [
      { name: 'anfragenType', columnIndex: 1 },
      { name: 'regelungsvorhabenType', columnIndex: 1 },
      { name: 'status', columnIndex: 4 },
    ],
  };
}
