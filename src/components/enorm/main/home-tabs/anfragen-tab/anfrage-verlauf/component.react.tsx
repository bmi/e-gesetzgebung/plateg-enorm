// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './anfrage-verlauf.less';

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { AnfrageStatusType, AnfrageTableDTO, MitzeichnungTableDTO } from '@plateg/rest-api';

import { AnfrageVerlaufElementComponent } from '../anfrage-verlauf-element/component.react';

interface AnfrageVerlaufComponentProps {
  element: AnfrageTableDTO | MitzeichnungTableDTO;
}
export function AnfrageVerlaufComponent(props: AnfrageVerlaufComponentProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <div className="anfrage-verlauf">
      <Title level={3}>{t('enorm.myVotes.tabs.requests.table.verlauf.title')}</Title>

      {props.element.anfrage?.dto.status !== AnfrageStatusType.Offen &&
        props.element.anfrage?.dto.status !== AnfrageStatusType.Zueruckgezogen && (
          <AnfrageVerlaufElementComponent element={props.element} response={true} />
        )}
      <AnfrageVerlaufElementComponent element={props.element} response={false} />
    </div>
  );
}
