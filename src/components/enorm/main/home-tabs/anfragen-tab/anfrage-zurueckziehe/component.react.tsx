// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { AjaxError } from 'rxjs/ajax';

import { displayMessage, ErrorController, GlobalDI, LoadingStatusController, ModalWrapper } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { cancelTeilnehmerRequestCall } from '../../controller.react';

interface AnfrageZurueckziehenComponentProps {
  okText: string;
  cancelText: string;
  modalTitle?: string;
  actionTitle: string;
  visible: boolean;
  setVisible: (visible: boolean) => void;
  setZurueckziehen: () => void;
  abstimmungId: string;
  successMsg: string;
}

export function AnfrageZurueckziehenComponent(props: AnfrageZurueckziehenComponentProps): React.ReactElement {
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const appStore = useAppSelector((state) => state.user);
  const onFinish = () => {
    loadingStatusController.setLoadingStatus(true);
    const sub = cancelTeilnehmerRequestCall(props.abstimmungId, appStore.user?.base.id || '').subscribe({
      next: () => {
        loadingStatusController.setLoadingStatus(false);
        props.setZurueckziehen();
        displayMessage(props.successMsg, 'success');
        sub?.unsubscribe();
      },
      error: (error: AjaxError) => {
        loadingStatusController.setLoadingStatus(false);
        errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
        sub?.unsubscribe();
      },
    });
  };
  return (
    <ModalWrapper
      open={props.visible}
      onOk={onFinish}
      onCancel={() => props.setVisible(!props.visible)}
      okText={props.okText}
      cancelText={props.cancelText}
      title={<h3>{props.modalTitle}</h3>}
    >
      {props.actionTitle}
    </ModalWrapper>
  );
}
