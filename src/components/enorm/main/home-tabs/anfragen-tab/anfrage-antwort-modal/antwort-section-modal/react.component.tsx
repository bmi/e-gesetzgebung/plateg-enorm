// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, FormInstance, Radio, RadioChangeEvent } from 'antd';
import TextArea from 'antd/es/input/TextArea';
import Title from 'antd/es/typography/Title';
import { ValidateErrorEntity } from 'rc-field-form/lib/interface';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { AnfrageStatusType } from '@plateg/rest-api';
import { Constants, GeneralFormWrapper } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

import { AnfrageOptionsType } from '../teilnahme-anfrage-antwort-modal/component.react';

interface Props {
  form: FormInstance;
  anfrageOptions: AnfrageOptionsType[];
  handleSubmit: () => void;
  formName: string;
  radioGroupOnChange?: (e: RadioChangeEvent) => void;
  children?: React.ReactElement;
}

export function AntwortSectionModal(props: Props): React.JSX.Element {
  const { t } = useTranslation();

  const { form, anfrageOptions, handleSubmit, formName, radioGroupOnChange, children } = props;

  const displayAnfrageOptions = anfrageOptions.map((option, index) => (
    <Radio key={index} value={option.value}>
      {option.label}
    </Radio>
  ));

  const handleError = (errorInfo: ValidateErrorEntity) => {
    (form.getFieldInstance(errorInfo.errorFields[0].name) as { focus: Function }).focus();
  };

  return (
    <>
      <div className="anfrage-antwort-form-wrapper">
        <Title level={2}>{t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.fields.subtitle')}</Title>
        <p
          dangerouslySetInnerHTML={{
            __html: t(`enorm.teilnahmeAnfragen.pflichtfelderText`),
          }}
          className="ant-typography p-no-style required-info"
        ></p>
        <GeneralFormWrapper
          form={form}
          onFinishFailed={handleError}
          name={formName}
          layout="vertical"
          onFinish={handleSubmit}
        >
          <fieldset className="fieldset-form-items">
            <legend className="seo">
              {t('enorm.beantwenorm.myVotes.tabs.requests.fristAnfrageAntwort.fields.approval.title')}
            </legend>
            <Form.Item
              name="status"
              label={<span>{t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.fields.approval.title')}</span>}
              required={true}
              initialValue={AnfrageStatusType.Zugestimmt}
            >
              <Radio.Group onChange={radioGroupOnChange} className="horizontal-radios" name="status">
                {displayAnfrageOptions}
              </Radio.Group>
            </Form.Item>
          </fieldset>
          {children}
          <Form.Item
            name="kommentar"
            label={<span>{t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.fields.anmerkungen')}</span>}
            rules={[
              {
                max: Constants.TEXT_AREA_LENGTH,
                message: t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.fields.textLengthError', {
                  maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
                }),
              },
            ]}
          >
            <TextArea rows={8} />
          </Form.Item>
        </GeneralFormWrapper>
      </div>
    </>
  );
}
