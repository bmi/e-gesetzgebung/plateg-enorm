// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../frist-anfrage-antwort/frist-anfrage-antwort.less';

import { Button, Form } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { AjaxError } from 'rxjs/ajax';

import {
  AbstimmungControllerApi,
  AbstimmungEntityResponseDTO,
  AnfrageStatusType,
  AnfrageTableDTO,
  FristtypType,
} from '@plateg/rest-api';
import {
  displayMessage,
  ErrorController,
  getDateTimeString,
  GlobalDI,
  LoadingStatusController,
  ModalComponent,
} from '@plateg/theme';

import { AntwortSectionModal } from '../antwort-section-modal/react.component';
import { InfoSectionModal } from '../info-section-modal/component.react';

export interface InfoDataType {
  name: string;
  description?: string;
}

export interface AnfrageOptionsType {
  label: string;
  value: AnfrageStatusType;
}

interface Props {
  selectedAnfrage: AnfrageTableDTO;
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
  abstimmungDetails?: AbstimmungEntityResponseDTO;
  resetTab: () => void;
}

export function TeilnahmeAnfrageAntwortComponent(props: Props): React.JSX.Element {
  const { isVisible, setIsVisible, selectedAnfrage, abstimmungDetails, resetTab } = props;

  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const enormController = GlobalDI.getOrRegister<AbstimmungControllerApi>(
    'enormController',
    () => new AbstimmungControllerApi(),
  );

  const { t } = useTranslation();
  const [form] = Form.useForm();

  const na = t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.infos.na');
  const formName = 'teilnahme-anfrage-antwort-form';

  const voteDeadline = abstimmungDetails?.dto.fristen.find(
    (frist) => frist.fristTyp === FristtypType.Abstimmungsende,
  )?.fristablauf;

  const voteNameRound = `${abstimmungDetails?.dto.regelungsvorhaben?.dto.abkuerzung as string} (#${
    abstimmungDetails?.dto.version.toString() as string
  })`;
  const participant = selectedAnfrage.anfrage.dto.erstelltVon?.dto.name;

  const infoData: InfoDataType[] = [
    {
      name: t('enorm.myVotes.tabs.requests.teilnahmeAnfrageAntwort.infoSection.rv'),
      description: abstimmungDetails?.dto.regelungsvorhaben?.dto.abkuerzung,
    },
    {
      name: t('enorm.myVotes.tabs.requests.teilnahmeAnfrageAntwort.infoSection.voteType'),
      description: t(`enorm.reviewVote.generalInfo.type${selectedAnfrage.typ as string}`),
    },
    {
      name: t('enorm.myVotes.tabs.requests.teilnahmeAnfrageAntwort.infoSection.votingRound'),
      description: voteNameRound,
    },
    {
      name: t('enorm.myVotes.tabs.requests.teilnahmeAnfrageAntwort.infoSection.deadlineVR'),
      description: voteDeadline && getDateTimeString(voteDeadline),
    },
    {
      name: t('enorm.myVotes.tabs.requests.teilnahmeAnfrageAntwort.infoSection.initiatorVR'),
      description: `${selectedAnfrage.anfrage.dto.erstelltVon?.dto.name as string} (${
        selectedAnfrage.anfrage.dto.erstelltVon?.dto.fachreferat || na
      }, ${selectedAnfrage.anfrage.dto.erstelltVon?.dto.ressort?.bezeichnung || na})`,
    },

    {
      name: t('enorm.myVotes.tabs.requests.teilnahmeAnfrageAntwort.infoSection.reasonRequest'),
      description: selectedAnfrage.anfrage.dto.kommentar,
    },
  ];

  const anfargeOptions: AnfrageOptionsType[] = [
    {
      label: t('enorm.myVotes.tabs.requests.teilnahmeAnfrageAntwort.fields.approval.approve'),
      value: AnfrageStatusType.Zugestimmt,
    },
    {
      label: t('enorm.myVotes.tabs.requests.teilnahmeAnfrageAntwort.fields.approval.decline'),
      value: AnfrageStatusType.Abgelehnt,
    },
  ];

  const getSuccessMessage = (status: AnfrageStatusType) => {
    let statusString = 'messageSuccessAdd';
    if (status === AnfrageStatusType.Abgelehnt) {
      statusString = 'messageSucessDenide';
    }
    return t(`enorm.myVotes.tabs.requests.teilnahmeAnfrageAntwort.${statusString}`, {
      participant,
      voteNameRound,
    });
  };

  const handleSubmit = () => {
    const formValues = form.getFieldsValue(true) as {
      status: AnfrageStatusType;
      kommentar: string;
    };

    if (selectedAnfrage) {
      loadingStatusController.setLoadingStatus(true);
      enormController
        .modifyTeilnehmerRequest({
          anfrageId: selectedAnfrage.anfrage.base.id,
          anfrageEntityModifyDTO: formValues,
        })
        .subscribe({
          next: () => {
            loadingStatusController.setLoadingStatus(false);
            displayMessage(getSuccessMessage(formValues.status), 'success');
            resetTab();
          },
          error: (error: AjaxError) => {
            errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
            console.error(error);
            loadingStatusController.setLoadingStatus(false);
          },
        });

      handleClose();
    }
  };

  const handleClose = () => {
    setIsVisible(false);
    form.resetFields();
  };

  return (
    <>
      <ModalComponent
        title={t('enorm.myVotes.tabs.requests.teilnahmeAnfrageAntwort.title', { voteNameRound })}
        content={
          <div className="frist-anfrage-antwort-modal">
            <InfoSectionModal
              title={t('enorm.myVotes.tabs.requests.teilnahmeAnfrageAntwort.infoHeading')}
              infoData={infoData}
            />
            {/* Ihre Antwort section */}
            <AntwortSectionModal
              handleSubmit={handleSubmit}
              anfrageOptions={anfargeOptions}
              form={form}
              formName={formName}
            />
          </div>
        }
        footerRight={[
          <Button
            key="submit-teilnahme-anfrage-antwort"
            id="submit-teilnahme-anfrage-antwort"
            htmlType="submit"
            type="primary"
            form={formName}
          >
            {t('enorm.myVotes.tabs.requests.teilnahmeAnfrageAntwort.submit')}
          </Button>,
        ]}
        footerLeft={[
          <Button key="cancel-teilnahme-anfrage-antwort" id="cancel-teilnahme-anfrage-antwort" onClick={handleClose}>
            {t('enorm.myVotes.tabs.requests.teilnahmeAnfrageAntwort.cancel')}
          </Button>,
        ]}
        isVisible={isVisible}
        setIsVisible={handleClose}
      />
    </>
  );
}
