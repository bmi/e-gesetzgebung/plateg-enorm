// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Observable } from 'rxjs';
import sinon from 'sinon';

import { AbstimmungControllerApi, AnfrageStatusType } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { displayErrorMsgStub, loadingStatusSetStub } from '../../../../../../../general.test';
import { ModifyAnfrageController } from './controller';

describe('TEST: ModifyAnfrageController', () => {
  const ctrl = GlobalDI.getOrRegister('modfifyfrageController', () => new ModifyAnfrageController());
  const enormController = GlobalDI.getOrRegister<AbstimmungControllerApi>(
    'enormController',
    () => new AbstimmungControllerApi(),
  );
  const modifyAnfrageStub = sinon.stub(enormController, 'modifyAnfrage');
  const callbackStub = sinon.stub();

  beforeEach(() => {
    loadingStatusSetStub.resetHistory();
    displayErrorMsgStub.resetHistory();
    callbackStub.resetHistory();
    modifyAnfrageStub.reset();
  });

  it('TEST: modifyAnfrage success - check if correct function are called', (done) => {
    modifyAnfrageStub.callsFake(() => {
      return new Observable<void>((observer) => {
        observer.next();
      });
    });
    ctrl.modifyAnfrage(
      { anfrageId: '', anfrageEntityModifyDTO: { status: AnfrageStatusType.Zugestimmt } },
      callbackStub,
    );
    setTimeout(function () {
      sinon.assert.calledOnce(modifyAnfrageStub);
      sinon.assert.calledTwice(loadingStatusSetStub);
      sinon.assert.calledOnce(callbackStub);
      done();
    }, 10);
  });
  it('TEST: modifyAnfrage error - check if correct function are called', () => {
    modifyAnfrageStub.callsFake(() => {
      return new Observable<void>((observer) => observer.error());
    });
    ctrl.modifyAnfrage(
      { anfrageId: '', anfrageEntityModifyDTO: { status: AnfrageStatusType.Zugestimmt } },
      callbackStub,
    );
    sinon.assert.calledOnce(modifyAnfrageStub);
    sinon.assert.calledTwice(loadingStatusSetStub);
    sinon.assert.notCalled(callbackStub);
  });
});
