// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { Observable } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import { AbstimmungControllerApi, ModifyAnfrageRequest } from '@plateg/rest-api';
import { displayMessage, ErrorController, GlobalDI, LoadingStatusController } from '@plateg/theme';

export class ModifyAnfrageController {
  private readonly enormController = GlobalDI.getOrRegister<AbstimmungControllerApi>(
    'enormController',
    () => new AbstimmungControllerApi(),
  );
  private readonly loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  private errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());

  private modifyAnfrageCall(modifyAnfrageRequestProps: ModifyAnfrageRequest): Observable<void> {
    return this.enormController.modifyAnfrage(modifyAnfrageRequestProps);
  }

  public modifyAnfrage(modifyAnfrageRequestProps: ModifyAnfrageRequest, resetTab: () => void, title?: string): void {
    const status = modifyAnfrageRequestProps.anfrageEntityModifyDTO.status.toLowerCase();
    this.loadingStatusController.setLoadingStatus(true);
    this.modifyAnfrageCall(modifyAnfrageRequestProps).subscribe({
      next: () => {
        this.loadingStatusController.setLoadingStatus(false);
        resetTab();
        displayMessage(
          i18n.t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.archiveSuccess', { titel: title, status }),
          'success',
        );
      },
      error: (error: AjaxError) => {
        this.errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
        console.error(error);
        this.loadingStatusController.setLoadingStatus(false);
      },
    });
  }
}
