// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../../bitte-um-mitzeichnung-tab/frist-anfrage/frist-anfrage.less';
import './frist-anfrage-antwort.less';

import { Button, Form } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { AbstimmungEntityResponseDTO, AnfrageStatusType, AnfrageTableDTO } from '@plateg/rest-api';
import { getDateTimeString, GlobalDI, ModalComponent } from '@plateg/theme';

import { getAbstimmungsrundeName } from '../../../../../../../utils/controller';
import { VoteBaseController } from '../../../../vote-main/vote-base/controller';
import { DateTimeComponent } from '../../../../vote-main/vote-base/dateTimeComponent/component.react';
import { AntwortSectionModal } from '../antwort-section-modal/react.component';
import { InfoSectionModal } from '../info-section-modal/component.react';
import { AnfrageOptionsType, InfoDataType } from '../teilnahme-anfrage-antwort-modal/component.react';
import { ModifyAnfrageController } from './controller';

export interface FristAnfrageAntwortComponentProps {
  selectedAnfrage: AnfrageTableDTO;
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
  resetTab: () => void;
  abstimmungDetails?: AbstimmungEntityResponseDTO;
}

export function FristAnfrageAntwortComponent(props: FristAnfrageAntwortComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();

  const ctrl = GlobalDI.getOrRegister('modifyAnfrageController', () => new ModifyAnfrageController());
  const voteCtrl = GlobalDI.getOrRegister('enormVoteController', () => new VoteBaseController());

  const na = t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.infos.na');
  const formName = 'frist-anfrage-antwort-form';

  const abstimmungWithVersion = props.selectedAnfrage
    ? getAbstimmungsrundeName(props.selectedAnfrage.version, props.selectedAnfrage.titel || na)
    : undefined;
  const [datePickerIsVisible, setDatePickerIsVisible] = useState(true);
  const handleClose = () => {
    form.resetFields();
    setDatePickerIsVisible(true);
    props.setIsVisible(false);
  };

  const handleSubmit = () => {
    const values = form.getFieldsValue(true) as {
      status: AnfrageStatusType;
      fristDate: Date;
      fristTime: Date;
      kommentar: string;
    };
    if (props.selectedAnfrage) {
      ctrl.modifyAnfrage(
        {
          anfrageId: props.selectedAnfrage.anfrage.base.id,
          anfrageEntityModifyDTO: {
            status: values.status,
            kommentar: values.kommentar,
            neuerFristablauf: voteCtrl.generateFristenString(values.fristDate, values.fristTime),
          },
        },
        () => {
          props.resetTab();
        },
        abstimmungWithVersion,
      );
      handleClose();
    }
  };

  const infoData: InfoDataType[] = [
    {
      name: t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.infos.abstimmungsrunde'),
      description: abstimmungWithVersion,
    },
    {
      name: t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.infos.absender'),
      description: `${props.selectedAnfrage.anfrage.dto.erstelltVon?.dto.name || na} (${
        props.selectedAnfrage.anfrage.dto.erstelltVon?.dto.fachreferat || na
      }, ${props.selectedAnfrage.anfrage.dto.erstelltVon?.dto.ressort?.bezeichnung || na})`,
    },
    {
      name: t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.infos.initialeFrist'),
      description: props.selectedAnfrage.fristablauf ? getDateTimeString(props.selectedAnfrage.fristablauf) : na,
    },

    {
      name: t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.infos.fristverlaengerung'),
      description: t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.infos.fristverlaengerungContent'),
    },
    {
      name: t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.infos.anmerkungen'),
      description: props.selectedAnfrage.anfrage.dto.kommentar || na,
    },
  ];

  const anfargeOptions: AnfrageOptionsType[] = [
    {
      label: t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.fields.approval.approve'),
      value: AnfrageStatusType.Zugestimmt,
    },
    {
      label: t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.fields.approval.decline'),
      value: AnfrageStatusType.Abgelehnt,
    },
  ];

  return (
    <>
      {props.selectedAnfrage && (
        <div
          // Fix to avoid propagation arrow keys pushing to tabs menu only in popup
          onKeyDown={(e) => {
            e.stopPropagation();
          }}
        >
          <ModalComponent
            title={t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.title', { title: abstimmungWithVersion })}
            isVisible={props.isVisible}
            setIsVisible={handleClose}
            content={
              <div className="frist-anfrage-antwort-modal">
                <InfoSectionModal
                  infoData={infoData}
                  title={t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.heading')}
                />

                {/* Ihre Antwort section */}
                <AntwortSectionModal
                  anfrageOptions={anfargeOptions}
                  form={form}
                  formName={formName}
                  handleSubmit={handleSubmit}
                  radioGroupOnChange={(e) => setDatePickerIsVisible(e.target.value === AnfrageStatusType.Zugestimmt)}
                  children={
                    <div hidden={!datePickerIsVisible}>
                      <fieldset>
                        <legend style={{ all: 'unset' }}>
                          <strong className="form-label">
                            {t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.fields.deadline')}
                          </strong>
                        </legend>
                        <DateTimeComponent
                          aliasDate="fristDate"
                          aliasTime="fristTime"
                          required={datePickerIsVisible}
                          form={form}
                          identifier="deadlineVote"
                          minDate={props.selectedAnfrage.fristablauf}
                          maxDate={props.abstimmungDetails?.dto.oberabstimmung?.fristablauf || undefined}
                        />
                      </fieldset>
                    </div>
                  }
                />
              </div>
            }
            footerRight={[
              <Button
                key="submit-frist-anfrage-antwort"
                id="submit-frist-anfrage-antwort"
                htmlType="submit"
                type="primary"
                form={formName}
              >
                {t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.submit')}
              </Button>,
            ]}
            footerLeft={[
              <Button key="cancel-frist-anfrage-antwort" id="cancel-frist-anfrage-antwort" onClick={handleClose}>
                {t('enorm.myVotes.tabs.requests.fristAnfrageAntwort.cancel')}
              </Button>,
            ]}
          />
        </div>
      )}
    </>
  );
}
