// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './info-section-modal.less';

import { Collapse, CollapseProps } from 'antd';
import React, { Fragment } from 'react';

import { InfoDataType } from '../teilnahme-anfrage-antwort-modal/component.react';

interface Props {
  title: string;
  infoData: InfoDataType[];
}

export function InfoSectionModal(props: Props): React.JSX.Element {
  const { title, infoData } = props;

  const displayInfodata = infoData.map((item, index) => {
    return (
      <Fragment key={index}>
        <dt>{item.name}</dt>
        <dd>{item.description}</dd>
      </Fragment>
    );
  });

  const infoItem: CollapseProps['items'] = [
    {
      key: '1',
      label: title,
      children: (
        <div className="infos-frist-anfrage">
          <dl>{displayInfodata}</dl>
        </div>
      ),
    },
  ];

  return (
    <>
      <Collapse ghost className="collapse-info-section" items={infoItem} />
    </>
  );
}
