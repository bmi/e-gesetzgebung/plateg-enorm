// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ArchivTableDTO } from '@plateg/rest-api';
import { EmptyContentComponent, ImageModule, TableComponent, TableComponentProps } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { TabKeys } from '../component.react';
import { getFilterButton, getFilteredRows } from '../controller.react';
import { getArchivTableVals } from './controller.react';

interface ArchivTabComponentInterface {
  archivData: ArchivTableDTO[];
  loggedUserEmail: string;
  tabKey: TabKeys;
}
export function ArchivTabComponent(props: ArchivTabComponentInterface): React.ReactElement {
  const { t } = useTranslation();

  const [archivTableVals, setArchivTableVals] = useState<TableComponentProps<ArchivTableDTO>>({
    id: '',
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
  });
  const pagInfoResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).result;

  useEffect(() => {
    setArchivTableVals(getArchivTableVals(props.archivData, props.loggedUserEmail));
  }, [props.archivData]);

  return (
    <>
      {!pagInfoResult.allContentEmpty && (
        <TableComponent
          bePagination
          tabKey={props.tabKey}
          id="hra-archiv-table"
          expandable={archivTableVals.expandable}
          columns={archivTableVals.columns}
          content={archivTableVals.content}
          filteredColumns={archivTableVals.filteredColumns}
          filterRowsMethod={getFilteredRows}
          prepareFilterButtonMethod={getFilterButton}
          sorterOptions={archivTableVals.sorterOptions}
          customDefaultSortIndex={archivTableVals.customDefaultSortIndex}
          expandableCondition={archivTableVals.expandableCondition}
        ></TableComponent>
      )}
      <EmptyContentComponent
        images={[
          {
            label: t(`enorm.myVotes.tabs.archive.imgText1`),
            imgSrc: require(`../../../../../media/empty-content-images/archive/img1-archive.svg`) as ImageModule,
            height: 131,
          },
          {
            label: t(`enorm.myVotes.tabs.archive.imgText2`),
            imgSrc: require(`../../../../../media/empty-content-images/archive/img2-archive.svg`) as ImageModule,
            height: 137,
          },
        ]}
      >
        <p className="ant-typography p-no-style info-text">
          {t('enorm.myVotes.tabs.archive.text', {
            interpolation: { escapeValue: false },
          })}
        </p>
      </EmptyContentComponent>
    </>
  );
}
