// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ColumnsType } from 'antd/lib/table';
import Text from 'antd/lib/typography/Text';
import i18n from 'i18next';
import React, { ReactElement } from 'react';

import { AnfrageTableDTOOriginalTabEnum, ArchivTableDTO, ArchivTableDTOOriginalTabEnum } from '@plateg/rest-api';
import { compareDates, DropdownMenu, getDateTimeString, TableComponentProps } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import {
  AbstimmungTableVersionCustomDTO,
  getAbstimmungsrundeRenderer,
  getRegelungsvorhabenRenderer,
} from '../controller.react';

export interface ExtendedArchivTableDTO extends ArchivTableDTO {
  abstimmungId: string;
  currentVersion?: boolean;
  children?: Array<AbstimmungTableVersionCustomDTO>;
}
const getTarget = (record: ExtendedArchivTableDTO) => {
  if (record.originalTab.toString() === AnfrageTableDTOOriginalTabEnum.Anfragen.toString()) {
    return routes.ANFRAGE_TEILNAHME_DETAILS;
  }
  return record.originalTab.toString() === AnfrageTableDTOOriginalTabEnum.MeineAbstimmungen.toString()
    ? routes.ABSTIMMUNGSRUNDE
    : routes.ABSTIMMUNG_BEANTWORTEN_PRUEFEN;
};

export function getArchivTableVals(
  initalContent: ArchivTableDTO[],
  loggedUserEmail: string,
): TableComponentProps<ExtendedArchivTableDTO> {
  const columns: ColumnsType<ExtendedArchivTableDTO> = [
    {
      title: i18n.t('enorm.myVotes.tabs.archive.table.thead1'),
      key: 'c1',
      render: (record: ExtendedArchivTableDTO): ReactElement => {
        return getRegelungsvorhabenRenderer(record, loggedUserEmail);
      },
    },
    {
      title: i18n.t('enorm.myVotes.tabs.archive.table.thead2'),
      key: 'c2',
      render: (record: ExtendedArchivTableDTO): ReactElement => {
        const target = getTarget(record);
        let tabName = routes.ARCHIV;
        if (record.originalTab === ArchivTableDTOOriginalTabEnum.Anfragen) {
          const foundArchivedVote = initalContent.find(
            (item) =>
              item.id === record.abstimmungId && item.originalTab === ArchivTableDTOOriginalTabEnum.MeineAbstimmungen,
          );
          if (!foundArchivedVote) {
            tabName = routes.MEINE_ABSTIMMUNG;
          }
        }
        return getAbstimmungsrundeRenderer(`#/hra/${tabName}/${record.abstimmungId}/${target}`, record);
      },
    },
    {
      title: i18n.t('enorm.myVotes.tabs.archive.table.thead3'),
      key: 'c3',
      render: (element: ExtendedArchivTableDTO) => {
        return <Text>{i18n.t(`enorm.myVotes.tabs.archive.table.filter.tabs.${element.originalTab}`)}</Text>;
      },
    },
    {
      title: i18n.t('enorm.myVotes.tabs.archive.table.thead4'),
      key: 'archiviertAm',
      sorter: (a, b) => compareDates(a.archiviertAm, b.archiviertAm),
      render: (element: ExtendedArchivTableDTO) => {
        if (!element.currentVersion) {
          return <> </>;
        }
        return <Text>{getDateTimeString(element.archiviertAm)}</Text>;
      },
    },
    {
      title: i18n.t('enorm.myVotes.tabs.drafts.table.thead6'),
      key: 'c5',
      render: (element: ExtendedArchivTableDTO) => {
        if (!element.currentVersion) {
          return <> </>;
        }
        return (
          <div className="actions-holder">
            <DropdownMenu
              openLink={`/hra/${routes.ARCHIV}/${element.abstimmungId}/${getTarget(element)}`}
              items={[]}
              elementId={element.id}
            />
          </div>
        );
      },
    },
  ];

  const content: ExtendedArchivTableDTO[] = initalContent.map((item, index) => {
    return {
      ...item,
      abstimmungId: item.id,
      id:
        item.originalTab.toString() === AnfrageTableDTOOriginalTabEnum.Anfragen.toString()
          ? `${item.id}-${index}`
          : item.id,
      currentVersion: item.versionen.length > 0,

      children:
        item.versionen.length > 0
          ? item.versionen.map((version) => ({
              ...version,
              currentVersion: false,
              typ: item.typ,
              originalTab: item.originalTab,
              abstimmungId: version.id,
              id:
                item.originalTab.toString() === AnfrageTableDTOOriginalTabEnum.Anfragen.toString()
                  ? `${version.id}-${index}`
                  : version.id,
            }))
          : undefined,
    };
  });

  return {
    id: 'hra-archiv-table',
    expandable: true,
    expandableCondition: (record: ExtendedArchivTableDTO) => {
      if (record.versionen?.length > 0) {
        return true;
      }
      return false;
    },
    columns,
    content,
    sorterOptions: [
      {
        columnKey: 'archiviertAm',
        titleAsc: i18n.t('enorm.myVotes.tabs.archive.table.sorter.archivedAsc'),
        titleDesc: i18n.t('enorm.myVotes.tabs.archive.table.sorter.archivedDesc'),
      },
    ],
    customDefaultSortIndex: 2,
    filteredColumns: [
      { name: 'regelungsvorhaben', columnIndex: 0 },
      { name: 'initalTab', columnIndex: 2 },
    ],
  };
}
