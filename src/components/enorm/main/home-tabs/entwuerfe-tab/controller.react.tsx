// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import Text from 'antd/lib/typography/Text';
import { createHashHistory } from 'history';
import i18n from 'i18next';
import React, { ReactElement } from 'react';
import { Observable } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import { AbstimmungControllerApi, EntwurfTableDTO } from '@plateg/rest-api';
import {
  compareDates,
  displayMessage,
  DropdownMenu,
  ErrorController,
  getDateTimeString,
  GlobalDI,
  LoadingStatusController,
  TableComponentProps,
  TextAndContact,
} from '@plateg/theme';
import { DropdownMenuItem } from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';

import { routes } from '../../../../../shares/routes';
import { getAbstimmungsrundeName } from '../../../../../utils/controller';
import { getRegelungsvorhabenRenderer } from '../controller.react';

const history = createHashHistory();
const getUrl = (record: EntwurfTableDTO) => {
  if (record.version > 1) {
    return `/hra/${routes.ENTWUERFE}/${record.abstimmungMetadataId}/${routes.NACHSTE_ABSTIMMUNGSRUNDE}`;
  }
  return `/hra/${routes.ENTWUERFE}/${record.id}/${routes.ABSTIMMUNG_ANLEGEN}`;
};
export function getEntwuerfeTableVals(
  content: EntwurfTableDTO[],
  setEntwuerfeData: (entwuerfeData: EntwurfTableDTO[]) => void,
  loggedUserEmail: string,
  loadContent: () => void,
): TableComponentProps<EntwurfTableDTO> {
  const columns: ColumnsType<EntwurfTableDTO> = [
    {
      title: i18n.t('enorm.myVotes.tabs.drafts.table.thead1'),
      key: 'c1',
      render: (record: EntwurfTableDTO): ReactElement => {
        return getRegelungsvorhabenRenderer(record, loggedUserEmail);
      },
    },
    {
      title: i18n.t('enorm.myVotes.tabs.drafts.table.thead2'),
      key: 'c2',
      render: (record: EntwurfTableDTO): ReactElement => {
        return (
          <a type="link" href={`#${getUrl(record)}`}>
            {getAbstimmungsrundeName(record.version, record.titel)}
          </a>
        );
      },
    },
    {
      title: i18n.t('enorm.myVotes.tabs.drafts.table.thead3'),
      key: 'c3',
      render: (element: EntwurfTableDTO) => {
        return (
          <div className="breakable-text">
            <Text> {element.regelungsentwurf?.dto?.fileName || element.dokumentenmappe?.titel}</Text>
          </div>
        );
      },
    },
    {
      title: i18n.t('enorm.myVotes.tabs.drafts.table.thead4'),
      key: 'bearbeitetam',
      sorter: (a, b) => compareDates(a.bearbeitetAm || a.erstelltAm, b.bearbeitetAm || b.erstelltAm),
      render: (record: EntwurfTableDTO) => {
        if (record.bearbeiter?.dto || record.ersteller.dto) {
          const user = record.bearbeiterStellvertreter || record.bearbeiter || record.ersteller;
          return (
            <TextAndContact
              drawerId={record.id}
              firstRow={getDateTimeString(record.bearbeitetAm || record.erstelltAm)}
              drawerTitle={i18n.t('enorm.myVotes.tabs.drafts.table.contactPersonDrawerTitle')}
              user={user.dto}
              isStellvertretung={!!record.bearbeiterStellvertreter}
            />
          );
        }
      },
    },
    {
      title: i18n.t('enorm.myVotes.tabs.drafts.table.thead5'),
      key: 'fristablauf',
      sorter: (a, b) => compareDates(a.fristablauf, b.fristablauf),
      render: (element: EntwurfTableDTO) => {
        if (element.fristablauf) {
          return <Text>{getDateTimeString(element.fristablauf)}</Text>;
        } else {
          return <></>;
        }
      },
    },
    {
      title: i18n.t('enorm.myVotes.tabs.drafts.table.thead6'),
      key: 'c6',
      render: (element: EntwurfTableDTO) => {
        const items: DropdownMenuItem[] = [
          {
            element: i18n.t(`enorm.myVotes.tabs.drafts.table.menuItem1`),
            disabled: () => false,
            onClick: () => deleteEntwurf(element.id, content, setEntwuerfeData, loadContent),
          },
        ];

        return (
          <div className="actions-holder">
            <Button
              id={`enorm-entwuerfBearbeiten-btn-${element.id}`}
              className="ant-btn-secondary"
              onClick={() => {
                if (element.version > 1) {
                  history.push(
                    `/hra/${routes.ENTWUERFE}/${element.abstimmungMetadataId}/${routes.NACHSTE_ABSTIMMUNGSRUNDE}`,
                  );
                } else {
                  history.push(`/hra/${routes.ENTWUERFE}/${element.id}/${routes.ABSTIMMUNG_ANLEGEN}`);
                }
              }}
            >
              {i18n.t('enorm.myVotes.tabs.drafts.table.btnTextEntwurfBearbeiten')}
            </Button>
            <DropdownMenu openLink={getUrl(element)} items={items} elementId={element.id} />
          </div>
        );
      },
    },
  ];

  return {
    id: 'hra-entwuerfe-table',
    expandable: false,
    columns,
    content,
    filteredColumns: [
      { name: 'regelungsvorhaben', columnIndex: 0 },
      { name: 'regelungsvorhabenType', columnIndex: 0 },
    ],
    sorterOptions: [
      {
        columnKey: 'fristablauf',
        titleAsc: i18n.t('enorm.myVotes.tabs.myVotes.table.fristAsc'),
        titleDesc: i18n.t('enorm.myVotes.tabs.myVotes.table.fristDesc'),
      },
      {
        columnKey: 'bearbeitetam',
        titleAsc: i18n.t('enorm.myVotes.tabs.myVotes.table.bearbeitetAsc'),
        titleDesc: i18n.t('enorm.myVotes.tabs.myVotes.table.bearbeitetDesc'),
      },
    ],
  };
}

function deleteEntwurf(
  id: string,
  content: EntwurfTableDTO[],
  setEntwuerfeData: (entwuerfeData: EntwurfTableDTO[]) => void,
  loadContent: () => void,
): void {
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  loadingStatusController.setLoadingStatus(true);
  const deleteEntwurfSubscription = deleteEntwurfCall(id).subscribe({
    next: () => {
      setEntwuerfeData(
        content.filter((entry) => {
          return entry.id !== id;
        }),
      );
      displayMessage(i18n.t('enorm.myVotes.tabs.drafts.table.deleteMsg.success'), 'success');
      loadingStatusController.setLoadingStatus(false);
      loadContent();
      deleteEntwurfSubscription.unsubscribe();
    },
    error: (error: AjaxError) => {
      loadingStatusController.setLoadingStatus(false);
      console.error(`unable to delete draft ${error.message}`, error);
      errorCtrl.displayErrorMsg(error, 'enorm.myVotes.tabs.drafts.table.deleteMsg.error');
      deleteEntwurfSubscription.unsubscribe();
    },
  });
}

function deleteEntwurfCall(id: string): Observable<void> {
  const abstimmungController = GlobalDI.get<AbstimmungControllerApi>('enormController');
  return abstimmungController.deleteAbstimmung({ id });
}
