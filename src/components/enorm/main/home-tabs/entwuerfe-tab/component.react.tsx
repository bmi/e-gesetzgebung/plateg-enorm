// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EntwurfTableDTO } from '@plateg/rest-api';
import { EmptyContentComponent, ImageModule, TableComponent, TableComponentProps } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { TabKeys } from '../component.react';
import { getFilterButton, getFilteredRows } from '../controller.react';
import { getEntwuerfeTableVals } from './controller.react';

interface EntwuerfeTabComponentInterface {
  entwuerfeData: EntwurfTableDTO[];
  setEntwuerfeData: (entwuerfeData: EntwurfTableDTO[]) => void;
  loggedUserEmail: string;
  tabKey: TabKeys;
  loadContent: () => void;
}
export function EntwuerfeTabComponent(props: EntwuerfeTabComponentInterface): React.ReactElement {
  const { t } = useTranslation();

  const [entwuerfeTableVals, setEntwuerfeTableVals] = useState<TableComponentProps<EntwurfTableDTO>>({
    id: 'hra-entwuerfe-table',
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
  });

  const pagInfoResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).result;

  useEffect(() => {
    setEntwuerfeTableVals(
      getEntwuerfeTableVals(props.entwuerfeData, props.setEntwuerfeData, props.loggedUserEmail, props.loadContent),
    );
  }, [props.entwuerfeData]);

  return (
    <>
      {!pagInfoResult.allContentEmpty && (
        <TableComponent
          bePagination
          tabKey={props.tabKey}
          id="hra-entwuerfe-table"
          expandable={entwuerfeTableVals.expandable}
          columns={entwuerfeTableVals.columns}
          content={entwuerfeTableVals.content}
          filteredColumns={entwuerfeTableVals.filteredColumns}
          filterRowsMethod={getFilteredRows}
          prepareFilterButtonMethod={getFilterButton}
          sorterOptions={entwuerfeTableVals.sorterOptions}
          customDefaultSortIndex={4}
        ></TableComponent>
      )}
      <EmptyContentComponent
        images={[
          {
            label: t(`enorm.myVotes.tabs.drafts.imgText1`),
            imgSrc: require(`../../../../../media/empty-content-images/drafts/img1-drafts.svg`) as ImageModule,
            height: 132,
          },
          {
            label: t(`enorm.myVotes.tabs.drafts.imgText2`),
            imgSrc: require(`../../../../../media/empty-content-images/drafts/img2-drafts.svg`) as ImageModule,
            height: 124,
          },
        ]}
      >
        <p className="ant-typography p-no-style info-text">
          {t('enorm.myVotes.tabs.drafts.text', {
            interpolation: { escapeValue: false },
          })}
        </p>
      </EmptyContentComponent>
    </>
  );
}
