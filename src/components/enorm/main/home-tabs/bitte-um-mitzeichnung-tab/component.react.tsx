// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { MitzeichnungTableDTO } from '@plateg/rest-api';
import {
  ArchivConfirmComponent,
  EmptyContentComponent,
  ImageModule,
  TableComponent,
  TableComponentProps,
} from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { routes } from '../../../../../shares/routes';
import { TabKeys } from '../component.react';
import { archiveTextKeys, getFilterButton, getFilteredRows, setArchived } from '../controller.react';
import { getBitteUmMitzeichnungTableVals } from './controller.react';
import { FristAnfrageComponent } from './frist-anfrage/component.react';
import { UnterabstimmungHintModal } from './unterabstimmung-hint-modal/component.react';

interface BitteUmMitzeichnungTabComponentInterface {
  bitteUmMitzeichnungData: MitzeichnungTableDTO[];
  resetTab: (key: string | string[]) => void;
  loggedUserEmail: string;
  tabKey: TabKeys;
  loadContent: (key: string, isFilterNeeded: boolean, shouldUpdateLoadedTabs: boolean) => void;
}
export function BitteUmMitzeichnungTabComponent(props: BitteUmMitzeichnungTabComponentInterface): React.ReactElement {
  const { t } = useTranslation();
  const [beantwortenModalIsVisible, setBeantwortenModalIsVisible] = useState(false);
  const [mitzeichnungData, setMitzeichnungData] = useState(props.bitteUmMitzeichnungData);
  const [confirmArchiveModalIsVisible, setConfirmArchiveModalIsVisible] = useState(false);
  const [selectedAbstimmung, setSelectedAbstimmung] = useState<MitzeichnungTableDTO>({} as MitzeichnungTableDTO);
  const [bitteUmMitzeichnungTableVals, setBitteUmMitzeichnungTableVals] = useState<
    TableComponentProps<MitzeichnungTableDTO>
  >({ id: '', columns: [], content: [], expandable: true, sorterOptions: [] });
  const [fristAnfrageModalProps, setFristAnfrageModalProps] = useState<{
    isVisible: boolean;
    selectedMitzeichnung?: MitzeichnungTableDTO;
  }>({
    isVisible: false,
  });

  const pagInfoResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).result;

  useEffect(() => {
    setMitzeichnungData(props.bitteUmMitzeichnungData);
  }, [props.bitteUmMitzeichnungData]);

  useEffect(() => {
    setBitteUmMitzeichnungTableVals(
      getBitteUmMitzeichnungTableVals(
        mitzeichnungData,
        setConfirmArchiveModalIsVisible,
        setBeantwortenModalIsVisible,
        setSelectedAbstimmung,
        setFristAnfrageModalProps,
        props.loggedUserEmail,
      ),
    );
  }, [mitzeichnungData]);

  return (
    <>
      {!pagInfoResult.allContentEmpty && (
        <>
          <FristAnfrageComponent
            isVisible={fristAnfrageModalProps.isVisible}
            setIsVisible={(isVisible: boolean) =>
              setFristAnfrageModalProps({ ...fristAnfrageModalProps, isVisible: isVisible })
            }
            selectedMitzeichnung={fristAnfrageModalProps.selectedMitzeichnung}
            resetTab={() => {
              props.resetTab([routes.ANFRAGEN, routes.BITTE_UM_MITZEICHNUNG]);
            }}
          />
          <ArchivConfirmComponent
            actionTitle={t(archiveTextKeys.actionTitle)}
            okText={t(archiveTextKeys.okText)}
            cancelText={t(archiveTextKeys.cancelText)}
            visible={confirmArchiveModalIsVisible}
            setVisible={setConfirmArchiveModalIsVisible}
            setArchived={() => {
              setConfirmArchiveModalIsVisible(false);
              setArchived(selectedAbstimmung.id, mitzeichnungData, setMitzeichnungData, undefined, () => {
                setConfirmArchiveModalIsVisible(false);
                props.resetTab('archiv');
                props.loadContent(props.tabKey, true, false);
              });
            }}
          />
          <UnterabstimmungHintModal
            abstimmungTitel={selectedAbstimmung?.titel}
            abstimmungVersion={selectedAbstimmung.version}
            route={`/hra/${routes.BITTE_UM_MITZEICHNUNG}/${selectedAbstimmung.id}/${routes.ABSTIMMUNG_BEANTWORTEN}`}
            beantwortenModalIsVisible={beantwortenModalIsVisible}
            setBeantwortenModalIsVisible={setBeantwortenModalIsVisible}
          />

          <TableComponent
            bePagination
            tabKey={props.tabKey}
            id="hra-bitte-um-mitzeichnung-table"
            className="bitte-um-mitzeichnung-table"
            columns={bitteUmMitzeichnungTableVals.columns}
            content={bitteUmMitzeichnungTableVals.content}
            filteredColumns={bitteUmMitzeichnungTableVals.filteredColumns}
            filterRowsMethod={getFilteredRows}
            prepareFilterButtonMethod={getFilterButton}
            expandable={bitteUmMitzeichnungTableVals.expandable}
            expandedRowRender={bitteUmMitzeichnungTableVals.expandedRowRender}
            customDefaultSortIndex={bitteUmMitzeichnungTableVals.customDefaultSortIndex}
            sorterOptions={bitteUmMitzeichnungTableVals.sorterOptions}
          ></TableComponent>
        </>
      )}

      <EmptyContentComponent
        images={[
          {
            label: t(`enorm.myVotes.tabs.sign.imgText1`),
            imgSrc: require(
              `../../../../../media/empty-content-images/mitzeichnungsbitten/mitzeichnungsbitten-img1.svg`,
            ) as ImageModule,
            height: 93,
          },
          {
            label: t(`enorm.myVotes.tabs.sign.imgText2`),
            imgSrc: require(
              `../../../../../media/empty-content-images/mitzeichnungsbitten/mitzeichnungsbitten-img2.svg`,
            ) as ImageModule,
            height: 118,
          },
          {
            label: t(`enorm.myVotes.tabs.sign.imgText3`),
            imgSrc: require(`../../../../../media/empty-content-images/sign/img1-sign.svg`) as ImageModule,
            height: 92,
          },
        ]}
      >
        <p className="ant-typography p-no-style info-text">
          {t('enorm.myVotes.tabs.sign.text', {
            interpolation: { escapeValue: false },
          })}
        </p>
      </EmptyContentComponent>
    </>
  );
}
