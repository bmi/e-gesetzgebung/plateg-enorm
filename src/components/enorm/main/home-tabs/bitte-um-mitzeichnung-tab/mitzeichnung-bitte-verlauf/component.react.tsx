// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './mitzeichnung-bitte-verlauf.less';

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { AnfrageStatusType, MitzeichnungTableDTO, TeilnehmerStatusType, UserEntityResponseDTO } from '@plateg/rest-api';
import { getDateTimeString } from '@plateg/theme';

import { getAbstimmungsrundeName } from '../../../../../../utils/controller';
import { MitzeichnungVerlaufElement } from './mitzeichnung-verlauf-element/component.react';

interface MitzeichnungBitteVerlaufProps {
  element: MitzeichnungTableDTO;
}
export function MitzeichnungBitteVerlauf(props: MitzeichnungBitteVerlaufProps): React.ReactElement {
  const { t } = useTranslation();
  const getVerlaufTitle = () => {
    switch (props.element.antwort.dto.status) {
      case TeilnehmerStatusType.MitzeichnungUnterVorbehalt:
        return 'mitgezeichnetUVTitle';
      case TeilnehmerStatusType.KeineMitzeichnung:
        return 'keineMitzeichnungTitle';
      case TeilnehmerStatusType.KeineMitzeichnungStellungnahmeDesNationalenNormenkontrollrats:
        return 'stellungnahmeTitle';
      default:
        return 'mitgezeichnetTitle';
    }
  };
  const withSie = (user: UserEntityResponseDTO | undefined) => {
    if (!user) {
      return '';
    }
    if (user.dto.name || user.dto.email) {
      return t('enorm.myVotes.tabs.sign.table.verlauf.toData', {
        name: user.dto.name || user.dto.email,
      });
    }
    return '';
  };
  const abstimmungName = getAbstimmungsrundeName(props.element.version, props.element.titel);
  const abstimmungType = t(`enorm.myVotes.tabs.sign.table.verlauf.abstimmungType.type${props.element.typ || ''}`);
  let showAnswer = false;
  if (
    props.element.antwort.dto.status === TeilnehmerStatusType.Mitzeichnung ||
    props.element.antwort.dto.status === TeilnehmerStatusType.MitzeichnungUnterVorbehalt ||
    props.element.antwort.dto.status === TeilnehmerStatusType.KeineMitzeichnung ||
    props.element.antwort.dto.status ===
      TeilnehmerStatusType.KeineMitzeichnungStellungnahmeDesNationalenNormenkontrollrats
  ) {
    showAnswer = true;
  }
  return (
    <div className="mitzeichnung-verlauf">
      <Title level={3}>{t('enorm.myVotes.tabs.sign.table.verlauf.title')}</Title>
      {showAnswer && (
        <MitzeichnungVerlaufElement
          from={withSie(props.element.antwort.dto.user)}
          sentDate={
            props.element.antwort.dto.geantwortetAm ? getDateTimeString(props.element.antwort.dto.geantwortetAm) : ''
          }
          title={t(`enorm.myVotes.tabs.sign.table.verlauf.${getVerlaufTitle()}`, {
            type: abstimmungType,
            name: abstimmungName,
          })}
          to={props.element.ersteller.dto.name || props.element.ersteller.dto.email}
          comments={props.element.antwort.dto.kommentar}
          className="response-verlauf-element"
        />
      )}
      {props.element.anfrage && (
        <>
          {props.element.anfrage?.dto.status !== AnfrageStatusType.Offen &&
            props.element.anfrage?.dto.status !== AnfrageStatusType.Zueruckgezogen && (
              <MitzeichnungVerlaufElement
                from={props.element.ersteller.dto.name || props.element.ersteller.dto.email}
                sentDate={
                  props.element.anfrage.dto.geantwortetAm
                    ? getDateTimeString(props.element.anfrage.dto.geantwortetAm)
                    : ''
                }
                title={t(
                  `enorm.myVotes.tabs.sign.table.verlauf.extendAnswerTitle.${props.element.anfrage?.dto.status}`,
                  {
                    type: abstimmungType,
                    name: abstimmungName,
                  },
                )}
                to={withSie(props.element.anfrage.dto.erstelltVon)}
                comments={props.element.anfrage.dto.geantwortetKommentar}
                className="response-verlauf-element"
              />
            )}
          <MitzeichnungVerlaufElement
            from={withSie(props.element.anfrage.dto.erstelltVon)}
            sentDate={getDateTimeString(props.element.anfrage?.base.erstelltAm)}
            title={t(`enorm.myVotes.tabs.sign.table.verlauf.extendQuestionTitle`, {
              type: abstimmungType,
              name: abstimmungName,
            })}
            to={props.element.ersteller.dto.name || props.element.ersteller.dto.email}
            comments={props.element.anfrage.dto.kommentar}
            className="response-verlauf-element"
          />
        </>
      )}
      <MitzeichnungVerlaufElement
        from={props.element.ersteller.dto.name || props.element.ersteller.dto.email}
        sentDate={getDateTimeString(props.element.erstelltAm)}
        title={t('enorm.myVotes.tabs.sign.table.verlauf.mitzeichnungsbitteTitle', {
          type: abstimmungType,
          name: abstimmungName,
        })}
        to={withSie(props.element.antwort.dto.user)}
        comments={props.element.einladungsMailKommentar}
      />
    </div>
  );
}
