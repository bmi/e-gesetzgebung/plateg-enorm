// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

interface MitzeichnungVerlaufElementProps {
  title: string;
  sentDate: string;
  from: string;
  to: string;
  comments?: string;
  className?: string;
}
export function MitzeichnungVerlaufElement(props: MitzeichnungVerlaufElementProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <div className={`mitzeichnung-verlauf-element ${props.className || ''}`}>
      <div className="verlauf-meta-data">
        <span className="verlauf-title">{props.title}</span>
        <dl>
          <dt>{t('enorm.myVotes.tabs.sign.table.verlauf.sentDate')}</dt>
          <dd>{props.sentDate}</dd>
          <br />
          <dt>{t('enorm.myVotes.tabs.sign.table.verlauf.from')}</dt>
          <dd>{props.from}</dd>
          <br />
          <dt>{t('enorm.myVotes.tabs.sign.table.verlauf.to')}</dt>
          <dd>{props.to}</dd>
        </dl>
      </div>
      <Title level={4} className="verlauf-anmerkung-title">
        {t('enorm.myVotes.tabs.sign.table.verlauf.comments')}
      </Title>
      <span className="verlauf-anmerkung">
        {props.comments || <em>{t('enorm.myVotes.tabs.sign.table.verlauf.na')}</em>}
      </span>
    </div>
  );
}
