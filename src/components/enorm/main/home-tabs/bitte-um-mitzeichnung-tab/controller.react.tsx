// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import Text from 'antd/lib/typography/Text';
import { isPast } from 'date-fns';
import isFuture from 'date-fns/isFuture';
import { createHashHistory } from 'history';
import i18n from 'i18next';
import React, { ReactElement } from 'react';

import {
  AbstimmungstypType,
  AnfrageStatusType,
  MitzeichnungTableDTO,
  TeilnehmerEntityDTO,
  TeilnehmerStatusType,
} from '@plateg/rest-api';
import {
  compareDates,
  DropdownMenu,
  getDateTimeString,
  TableComponentProps,
  TextAndContact,
  TwoLineText,
} from '@plateg/theme';
import { DropdownMenuItem } from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';

import { routes } from '../../../../../shares/routes';
import { getAbstimmungsrundeName } from '../../../../../utils/controller';
import {
  AbstimmungVersionenTableDTO,
  getFristWithAnfrageRenderer,
  getRegelungsvorhabenRenderer,
} from '../controller.react';
import { FristStatusInfo } from '../frist-status-info/component.react';
import { MitzeichnungBitteVerlauf } from './mitzeichnung-bitte-verlauf/component.react';

const history = createHashHistory();

export const getMailTextRenderer = (record: MitzeichnungTableDTO): ReactElement => {
  return (
    <>
      <label htmlFor={'expandable-mail' + record.id.toString()}>
        {`${i18n.t('enorm.myVotes.tabs.sign.table.titleExpandableMail')} ${record.titel || ''} #${record.version}`}
      </label>
      <Text id={'expandable-mail' + record.id.toString()} className="expandable-row-mailtext">
        {record.einladungsMailText}
      </Text>
    </>
  );
};

const compareStatus = (a?: TeilnehmerEntityDTO, b?: TeilnehmerEntityDTO) => {
  const statusA = a?.status ? a.status : '';
  const statusB = b?.status ? b?.status : '';
  const readonlyA = a?.readOnlyAccess;
  const readonlyB = b?.readOnlyAccess;
  if (statusA === TeilnehmerStatusType.NichtBearbeitet || statusB === TeilnehmerStatusType.NichtBearbeitet) {
    if ((statusA === TeilnehmerStatusType.NichtBearbeitet && statusB !== statusA) || readonlyB) {
      return 1;
    }
    if ((statusB === TeilnehmerStatusType.NichtBearbeitet && statusB !== statusA) || readonlyA) {
      return -1;
    } else {
      return 0;
    }
  }
  return 0;
};

export function getBitteUmMitzeichnungTableVals(
  content: MitzeichnungTableDTO[],
  setConfirmArchiveModalIsVisible: (confirmArchiveModalIsVisible: boolean) => void,
  setBeantwortenModalIsVisible: (beantwortenModalIsVisible: boolean) => void,
  setSelectedAbstimmung: (selectedAbstimmung: MitzeichnungTableDTO) => void,
  setFristAnfrageModalProps: (fristAnfrageModalProps: {
    isVisible: boolean;
    selectedMitzeichnung?: MitzeichnungTableDTO;
  }) => void,
  loggedUserEmail: string,
): TableComponentProps<MitzeichnungTableDTO> {
  const columns: ColumnsType<MitzeichnungTableDTO> = [
    {
      title: `${i18n.t('enorm.myVotes.tabs.sign.table.thead1')}`,
      key: 'c1',
      render: (record: MitzeichnungTableDTO): ReactElement => {
        return getRegelungsvorhabenRenderer(record, loggedUserEmail);
      },
    },
    {
      title: `${i18n.t('enorm.myVotes.tabs.sign.table.thead2')}`,
      key: 'c2',
      render: (record: AbstimmungVersionenTableDTO): ReactElement => {
        return (
          <a
            id={`enorm-bitteUmMitzeichnung-link-${record.id}`}
            type="link"
            href={`#/hra/${routes.BITTE_UM_MITZEICHNUNG}/${record.id}/${routes.ABSTIMMUNG_BEANTWORTEN_PRUEFEN}`}
          >
            {getAbstimmungsrundeName(record.version, record.titel)}
          </a>
        );
      },
    },
    {
      title: `${i18n.t('enorm.myVotes.tabs.sign.table.thead3')}`,
      key: 'c3',
      render: (element: MitzeichnungTableDTO) => {
        const secondRow =
          element.ersteller.dto.abteilung && element.ersteller.dto.fachreferat
            ? `${element.ersteller.dto.abteilung || ''}, ${element.ersteller.dto.fachreferat || ''}`
            : `${element.ersteller.dto.abteilung || ''}${element.ersteller.dto.fachreferat || ''}`;
        return (
          <TwoLineText
            firstRow={element.ersteller.dto.ressort?.kurzbezeichnung || ''}
            firstRowBold={true}
            secondRow={secondRow}
            secondRowBold={false}
            secondRowLight={false}
            elementId={element.id}
          />
        );
      },
    },
    {
      title: `${i18n.t('enorm.myVotes.tabs.sign.table.thead4')}`,
      key: 'erstellt',
      sorter: (a, b) => compareDates(a.erstelltAm, b.erstelltAm),
      render: (element: MitzeichnungTableDTO) => {
        const user = element.erstellerStellvertreter || element.ersteller;
        return (
          <TextAndContact
            drawerId={`erstellt-drawer-${element.id}`}
            drawerTitle={i18n.t('enorm.beantwortenVote.generalInfo.contactPersonTitle')}
            firstRow={getDateTimeString(element.erstelltAm)}
            user={user.dto}
            isStellvertretung={!!element.erstellerStellvertreter}
          />
        );
      },
    },
    {
      title: `${i18n.t('enorm.myVotes.tabs.sign.table.thead6')}`,
      key: 'c5',
      render: (record: MitzeichnungTableDTO) => {
        return (
          <TwoLineText
            firstRow={
              record.antwort.dto.readOnlyAccess
                ? i18n.t('enorm.myVotes.tabs.sign.table.status.CC')
                : i18n.t('enorm.myVotes.tabs.sign.table.status.' + record.antwort.dto.status)
            }
            firstRowBold={true}
            secondRow={record.antwort.dto.geantwortetAm ? getDateTimeString(record.antwort.dto.geantwortetAm) : ''}
            secondRowBold={false}
            secondRowLight={true}
            elementId={record.id}
          />
        );
      },
    },
    {
      title: (
        <>
          {i18n.t('enorm.myVotes.tabs.sign.table.thead7')}
          <span
            onClick={(e) => {
              e.stopPropagation();
            }}
          >
            <FristStatusInfo id="frist-status-info-bitte-um-mitzeichnung" />
          </span>
        </>
      ),
      key: 'fristablauf',
      sorter: (a, b) => compareDates(a.fristablauf, b.fristablauf),
      render: (record: MitzeichnungTableDTO) => {
        return getFristWithAnfrageRenderer(record);
      },
    },
    {
      title: `${i18n.t('enorm.myVotes.tabs.sign.table.thead8')}`,
      key: 'c7',

      render: (element: MitzeichnungTableDTO) => {
        const checkDate = element.fristablauf ? new Date(element.fristablauf) : new Date('');
        const items: DropdownMenuItem[] = [
          {
            element: i18n.t(`enorm.myVotes.tabs.sign.table.menuItemAusserhalbFristBeantworten`),
            disabled: () =>
              !(
                element.antwort.dto.status === TeilnehmerStatusType.NichtBearbeitet &&
                !element.antwort.dto.readOnlyAccess &&
                !element.finalisiert &&
                !element.geschlossen &&
                (!element.anfrage?.dto || element.anfrage?.dto.status !== AnfrageStatusType.Offen)
              ),
            onClick: () => {
              setFristAnfrageModalProps({ isVisible: true, selectedMitzeichnung: element });
            },
          },
          {
            element: i18n.t(`enorm.myVotes.tabs.sign.table.menuItemMitzeichnungArchiv`),
            onClick: () => {
              setSelectedAbstimmung(element);
              setConfirmArchiveModalIsVisible(true);
            },
            disabled: () => isFuture(checkDate),
          },
          {
            element: i18n.t(`enorm.myVotes.tabs.myVotes.table.menuItemUnterabstimmungEinleiten`),
            onClick: () => {
              history.push(
                `/hra/${routes.BITTE_UM_MITZEICHNUNG}/${element.abstimmungMetadataId}/${routes.UNTERABSTIMMUNG_ANLEGEN}`,
              );
            },
            disabled: () =>
              element.geschlossen ||
              isPast(checkDate) ||
              element.typ === AbstimmungstypType.SonstigeAbstimmung ||
              element.antwort.dto.readOnlyAccess ||
              element.antwort.dto.status === TeilnehmerStatusType.TeilnahmeAngefragt ||
              element.antwort.dto.status === TeilnehmerStatusType.TeilnahmeZurueckgezogen ||
              element.antwort.dto.status === TeilnehmerStatusType.TeilnahmeAbgelehnt,
          },
        ];
        if (
          !element.geschlossen &&
          isFuture(checkDate) &&
          element.antwort.dto.status === TeilnehmerStatusType.NichtBearbeitet &&
          !element.antwort.dto.readOnlyAccess
        ) {
          return (
            <div className="actions-holder">
              <Button
                key={element.id}
                id={`enorm-tableBeantworten-btn-${element.id}`}
                className="ant-btn-secondary"
                onClick={() => {
                  if (element.eingeleiteteUnterabstimmungenVorhanden) {
                    setBeantwortenModalIsVisible(true);
                    setSelectedAbstimmung(element);
                  } else {
                    history.push(`/hra/${routes.BITTE_UM_MITZEICHNUNG}/${element.id}/${routes.ABSTIMMUNG_BEANTWORTEN}`);
                  }
                }}
              >
                {`${i18n.t('enorm.myVotes.tabs.sign.table.btnAnswerButtonText')}`}
              </Button>
              <DropdownMenu items={items} elementId={element.id} />
            </div>
          );
        } else {
          if (
            isFuture(checkDate) &&
            element.antwort.dto.status !== TeilnehmerStatusType.NichtBearbeitet &&
            !element.antwort.dto.readOnlyAccess &&
            element.antwort.dto.status !== TeilnehmerStatusType.TeilnahmeAngefragt &&
            element.antwort.dto.status !== TeilnehmerStatusType.TeilnahmeZurueckgezogen &&
            element.antwort.dto.status !== TeilnehmerStatusType.TeilnahmeAbgelehnt
          ) {
            items.push({
              element: i18n.t(`enorm.reviewVoteAnswer.changeAnswerBtn`),
              onClick: () => {
                history.push(`/hra/${routes.BITTE_UM_MITZEICHNUNG}/${element.id}/${routes.ABSTIMMUNG_BEANTWORTEN}`);
              },
            });
          }
          return (
            <div className="actions-holder">
              <DropdownMenu
                openLink={`/hra/${routes.BITTE_UM_MITZEICHNUNG}/${element.id}/${routes.ABSTIMMUNG_BEANTWORTEN_PRUEFEN}`}
                items={items}
                elementId={element.id}
              />
            </div>
          );
        }
      },
    },
  ];

  return {
    id: 'hra-mitzeichnung-table',
    expandable: true,
    expandedRowRender: (record) => {
      return <MitzeichnungBitteVerlauf element={record} />;
    },
    columns,
    content,
    filteredColumns: [
      { name: 'regelungsvorhaben', columnIndex: 0 },
      { name: 'regelungsvorhabenType', columnIndex: 0 },
      { name: 'ersteller', columnIndex: 3 },
      { name: 'statusMitzeichnung', columnIndex: 4 },
    ],
    sorterOptions: [
      {
        columnKey: 'erstellt',
        titleAsc: i18n.t('enorm.myVotes.tabs.myVotes.table.EingangAsc'),
        titleDesc: i18n.t('enorm.myVotes.tabs.myVotes.table.EingangDesc'),
      },
      {
        columnKey: 'fristablauf',
        titleAsc: i18n.t('enorm.myVotes.tabs.myVotes.table.fristAsc'),
        titleDesc: i18n.t('enorm.myVotes.tabs.myVotes.table.fristDesc'),
      },
    ],
    customDefaultSortIndex: 2,
  };
}
