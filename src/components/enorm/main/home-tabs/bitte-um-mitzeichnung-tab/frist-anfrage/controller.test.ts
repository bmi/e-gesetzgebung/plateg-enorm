// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';
import sinon from 'sinon';

import { AbstimmungControllerApi, AnfrageEntityResponseDTO, AnfragetypType } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { displayErrorMsgStub, loadingStatusSetStub } from '../../../../../../general.test';
import { CreateAnfrageController } from './controller';

describe('TEST: CreateAnfrageController', () => {
  const ctrl = GlobalDI.getOrRegister('createAnfrageController', () => new CreateAnfrageController());
  const enormController = GlobalDI.getOrRegister<AbstimmungControllerApi>(
    'enormController',
    () => new AbstimmungControllerApi(),
  );
  const createAnfrageStub = sinon.stub(enormController, 'createAnfrage');
  const callbackStub = sinon.stub();

  beforeEach(() => {
    loadingStatusSetStub.resetHistory();
    displayErrorMsgStub.resetHistory();
    callbackStub.resetHistory();
    createAnfrageStub.reset();
  });

  it('TEST: createAnfrage success - check if correct function are called', (done) => {
    createAnfrageStub.callsFake(() => {
      return new Observable<AjaxResponse<AnfrageEntityResponseDTO>>((observer) => {
        observer.next();
      });
    });
    ctrl.createAnfrage(
      { abstimmungId: '1', anfrageCreateDTO: { typ: AnfragetypType.Fristverlaengerung, kommentar: '' } },
      callbackStub,
    );
    setTimeout(function () {
      sinon.assert.calledOnce(createAnfrageStub);
      sinon.assert.calledTwice(loadingStatusSetStub);
      sinon.assert.calledOnce(callbackStub);
      done();
    }, 10);
  });
  it('TEST: createAnfrage error - check if correct function are called', () => {
    createAnfrageStub.callsFake(() => {
      return new Observable<AjaxResponse<AnfrageEntityResponseDTO>>((observer) => observer.error());
    });
    ctrl.createAnfrage(
      { abstimmungId: '1', anfrageCreateDTO: { typ: AnfragetypType.Fristverlaengerung, kommentar: '' } },
      callbackStub,
    );
    sinon.assert.calledOnce(createAnfrageStub);
    sinon.assert.calledTwice(loadingStatusSetStub);
    sinon.assert.notCalled(callbackStub);
  });
});
