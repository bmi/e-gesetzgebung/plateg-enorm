// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { createHashHistory } from 'history';
import i18n from 'i18next';
import { Observable } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import { AbstimmungControllerApi, CreateAnfrageRequest } from '@plateg/rest-api';
import { AnfrageEntityResponseDTO } from '@plateg/rest-api/models';
import { displayMessage, ErrorController, GlobalDI, LoadingStatusController } from '@plateg/theme';

import { routes } from '../../../../../../shares/routes';
export class CreateAnfrageController {
  private readonly history = createHashHistory();
  private readonly enormController = GlobalDI.getOrRegister<AbstimmungControllerApi>(
    'enormController',
    () => new AbstimmungControllerApi(),
  );
  private readonly loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  private errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());

  private createAnfrageCall(createAnfrageRequestProps: CreateAnfrageRequest): Observable<AnfrageEntityResponseDTO> {
    return this.enormController.createAnfrage(createAnfrageRequestProps);
  }

  public createAnfrage(createAnfrageRequestProps: CreateAnfrageRequest, resetTab: () => void, title?: string): void {
    this.loadingStatusController.setLoadingStatus(true);
    this.createAnfrageCall(createAnfrageRequestProps).subscribe({
      next: (anfrage) => {
        this.loadingStatusController.setLoadingStatus(false);
        resetTab();
        displayMessage(i18n.t('enorm.myVotes.tabs.sign.fristAnfrage.archiveSuccess', { titel: title }), 'success');
        this.history.push(`/hra/${routes.ANFRAGEN}`);
      },
      error: (error: AjaxError) => {
        this.errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
        console.error('Could not fetch Regelungsentwurf details', error);
        this.loadingStatusController.setLoadingStatus(false);
      },
    });
  }
}
