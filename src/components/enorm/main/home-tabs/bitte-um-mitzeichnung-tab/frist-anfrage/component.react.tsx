// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './frist-anfrage.less';

import { Button, Form } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import Title from 'antd/lib/typography/Title';
import { ValidateErrorEntity } from 'rc-field-form/lib/interface';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { AnfragetypType, MitzeichnungTableDTO } from '@plateg/rest-api';
import { Constants, GeneralFormWrapper, getDateTimeString, GlobalDI, ModalComponent } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

import { getAbstimmungsrundeName } from '../../../../../../utils/controller';
import { CreateAnfrageController } from './controller';

export interface FristAnfrageComponentProps {
  selectedMitzeichnung?: MitzeichnungTableDTO;
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
  resetTab: () => void;
}

export function FristAnfrageComponent(props: FristAnfrageComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const ctrl = GlobalDI.getOrRegister('createAnfrageController', () => new CreateAnfrageController());
  const [form] = Form.useForm();
  const na = t('enorm.myVotes.tabs.sign.fristAnfrage.infos.na');
  const abstimmungWithVersion = props.selectedMitzeichnung
    ? getAbstimmungsrundeName(props.selectedMitzeichnung.version, props.selectedMitzeichnung.titel || na)
    : undefined;

  const handleClose = () => {
    form.resetFields();
    props.setIsVisible(false);
  };

  const handleSubmit = (values: { anmerkungen: string }) => {
    if (props.selectedMitzeichnung) {
      ctrl.createAnfrage(
        {
          anfrageCreateDTO: { kommentar: values.anmerkungen, typ: AnfragetypType.Fristverlaengerung },
          abstimmungId: props.selectedMitzeichnung?.id,
        },
        props.resetTab,
        abstimmungWithVersion,
      );
    }
    handleClose();
  };

  const handleError = (errorInfo: ValidateErrorEntity) => {
    (form.getFieldInstance(errorInfo.errorFields[0].name) as { focus: Function }).focus();
  };

  return (
    <>
      {props.selectedMitzeichnung && (
        <ModalComponent
          title={t('enorm.myVotes.tabs.sign.fristAnfrage.title', { title: abstimmungWithVersion })}
          isVisible={props.isVisible}
          setIsVisible={handleClose}
          footerLeft={[
            <Button key="cancel-frist-anfrage" id="cancel-frist-anfrage" onClick={handleClose}>
              {t('enorm.myVotes.tabs.sign.fristAnfrage.cancel')}
            </Button>,
          ]}
          footerRight={[
            <Button
              key="submit-frist-anfrage"
              id="submit-frist-anfrage"
              htmlType="submit"
              type="primary"
              form="frist-anfrage-form"
            >
              {t('enorm.myVotes.tabs.sign.fristAnfrage.submit')}
            </Button>,
          ]}
          content={
            <div className="frist-anfrage-modal">
              <Title level={2}>{t('enorm.myVotes.tabs.sign.fristAnfrage.heading')}</Title>
              <div className="infos-frist-anfrage">
                <dl>
                  <dt>{t('enorm.myVotes.tabs.sign.fristAnfrage.infos.abstimmungsrunde')}</dt>
                  <dd>{abstimmungWithVersion}</dd>
                  <dt>{t('enorm.myVotes.tabs.sign.fristAnfrage.infos.adressat')}</dt>
                  <dd>{`${props.selectedMitzeichnung.ersteller.dto.name || na} (${
                    props.selectedMitzeichnung.ersteller.dto.fachreferat || na
                  }, ${props.selectedMitzeichnung.ersteller.dto.ressort?.bezeichnung || na})`}</dd>
                  <dt>{t('enorm.myVotes.tabs.sign.fristAnfrage.infos.initialeFrist')}</dt>
                  <dd>
                    {props.selectedMitzeichnung.fristablauf
                      ? getDateTimeString(props.selectedMitzeichnung.fristablauf)
                      : na}
                  </dd>
                  <dt>{t('enorm.myVotes.tabs.sign.fristAnfrage.infos.fristverlaengerung')}</dt>
                  <dd>{t('enorm.myVotes.tabs.sign.fristAnfrage.infos.fristverlaengerungContent')}</dd>
                </dl>
              </div>
              <div>
                <GeneralFormWrapper
                  form={form}
                  onFinishFailed={handleError}
                  name="frist-anfrage-form"
                  layout="vertical"
                  onFinish={handleSubmit}
                >
                  <Form.Item
                    name="anmerkungen"
                    label={<span>{t('enorm.myVotes.tabs.sign.fristAnfrage.fields.anmerkungen')}</span>}
                    rules={[
                      {
                        max: Constants.TEXT_AREA_LENGTH,
                        message: t('enorm.myVotes.tabs.sign.fristAnfrage.fields.textLengthError', {
                          maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
                        }),
                      },
                    ]}
                  >
                    <TextArea rows={8} />
                  </Form.Item>
                </GeneralFormWrapper>
              </div>
            </div>
          }
        ></ModalComponent>
      )}
    </>
  );
}
