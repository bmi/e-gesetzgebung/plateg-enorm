// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';

import { ModalWrapper } from '@plateg/theme';

interface UnterabstimmungHintModalProps {
  abstimmungTitel?: string;
  abstimmungVersion: number;
  setBeantwortenModalIsVisible: (value: boolean) => void;
  beantwortenModalIsVisible: boolean;
  route: string;
}

export function UnterabstimmungHintModal(props: UnterabstimmungHintModalProps): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  return (
    <ModalWrapper
      width="635px"
      open={props.beantwortenModalIsVisible}
      closable={false}
      title={<h3>{t('enorm.beantwortenVote.beantwortenActionAbbrechen.modalTitle')}</h3>}
      cancelText={t('enorm.beantwortenVote.beantwortenActionAbbrechen.antwortCancel')}
      okText={t('enorm.beantwortenVote.beantwortenActionAbbrechen.antwortConfirm')}
      cancelButtonProps={{ onClick: () => props.setBeantwortenModalIsVisible(false), type: 'default' }}
      okButtonProps={{
        onClick: () => {
          props.setBeantwortenModalIsVisible(false);
          history.push(props.route);
        },
        type: 'primary',
      }}
    >
      <p>
        {t('enorm.beantwortenVote.beantwortenActionAbbrechen.actionTitle', {
          titel: props.abstimmungTitel,
          id: props.abstimmungVersion,
        })}
      </p>
    </ModalWrapper>
  );
}
