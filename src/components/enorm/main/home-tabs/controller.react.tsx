// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/restrict-template-expressions */
import isPast from 'date-fns/isPast';
import i18n from 'i18next';
import React, { ReactElement } from 'react';
import { Observable } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import { AbstimmungControllerApi } from '@plateg/rest-api';
import {
  AbstimmungInfoDTO,
  AbstimmungsanfrageDTO,
  AbstimmungstypType,
  AbstimmungTableDTO,
  AbstimmungTableDTOs,
  AbstimmungTableVersionDTO,
  AnfrageTableDTO,
  AnfrageTableDTOs,
  AnfragetypType,
  ArchivTableDTOs,
  EntwurfTableDTO,
  EntwurfTableDTOs,
  MitzeichnungTableDTO,
  MitzeichnungTableDTOs,
  OberabstimmungEntityDTO,
  PaginierungDTO,
} from '@plateg/rest-api/models';
import {
  CommonRow,
  displayMessage,
  ErrorController,
  getDateTimeString,
  GlobalDI,
  LoadingStatusController,
  TwoLineText,
} from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

import { getAbstimmungsrundeName, getTyp, MyVotesColumnNamesType } from '../../../../utils/controller';

export const archiveTextKeys = {
  okText: 'enorm.myVotes.archiveAction.archiveConfirm',
  cancelText: 'enorm.myVotes.archiveAction.archiveCancel',
  actionTitle: 'enorm.myVotes.archiveAction.archiveActionTitle',
};

export const anfrageZurueckziehenTextKeys = (type: AnfragetypType) => {
  const key = `zurueckziehen${type}`;
  return {
    okText: `enorm.teilnahmeAnfragen.${key}.confirm`,
    cancelText: `enorm.teilnahmeAnfragen.${key}.cancel`,
    actionTitle: `enorm.teilnahmeAnfragen.${key}.title`,
    modalTitle: `enorm.teilnahmeAnfragen.${key}.modalTitle`,
    successMsg: `enorm.teilnahmeAnfragen.${key}.successMsg`,
  };
};

export interface AbstimmungVersionenTableDTO extends AbstimmungTableDTO {
  currentVersion?: boolean;
  children?: Array<AbstimmungTableVersionCustomDTO>;
}
export interface AbstimmungTableVersionCustomDTO extends AbstimmungTableVersionDTO {
  currentVersion: boolean;
  typ?: AbstimmungstypType;
}
export function getFilterButton<T extends CommonRow>(
  initialContent: T[],
  column: { name: string; columnIndex: number },
): { displayName: string; labelContent: string; options: Set<string> } {
  const displayName = i18n.t(`enorm.myVotes.tabs.myVotes.table.filter.displayname.${column.name}`);
  let options: Set<string> = new Set();
  const labelContent = i18n.t(`enorm.myVotes.tabs.myVotes.table.filter.labelContent`);

  if (column.name === MyVotesColumnNamesType.regelungsvorhaben.toString()) {
    options = new Set(
      initialContent.map((row) => {
        return row.regelungsvorhaben?.dto?.abkuerzung as string;
      }),
    );
  }
  if (column.name === MyVotesColumnNamesType.mitzeichnungsanfrage.toString()) {
    options = new Set(
      initialContent.map((row) => {
        return row.oberabstimmung?.titel as string;
      }),
    );
  }
  if (column.name === MyVotesColumnNamesType.regelungsvorhabenType.toString()) {
    options = new Set(Object.values(AbstimmungstypType).map((item) => getTyp(item)));
  }

  if (column.name === MyVotesColumnNamesType.regelungsvorhabenNoSubvoteType.toString()) {
    options = new Set(Object.values(AbstimmungstypType).map((item) => getTyp(item)));
    options.delete('Unterabstimmung');
  }
  if (column.name === MyVotesColumnNamesType.initalTab.toString()) {
    options = new Set(
      initialContent.map((row) => {
        return i18n.t(`enorm.myVotes.tabs.archive.table.filter.tabs.${row.originalTab}`);
      }),
    );
  }
  if (column.name === MyVotesColumnNamesType.status.toString()) {
    options = new Set(
      initialContent.map((row) => {
        return i18n.t(`enorm.myVotes.tabs.requests.table.status.${row.anfrage.dto.status}`);
      }),
    );
  }
  if (column.name === MyVotesColumnNamesType.ersteller.toString()) {
    options = new Set(
      initialContent.map((row) => {
        return row.ersteller?.dto.name as string;
      }),
    );
  }
  if (column.name === MyVotesColumnNamesType.statusMitzeichnung.toString()) {
    options = new Set(
      initialContent.map((row) => {
        return row.antwort?.dto.readOnlyAccess
          ? i18n.t('enorm.myVotes.tabs.sign.table.status.CC')
          : i18n.t(`enorm.myVotes.tabs.sign.table.status.${row.antwort?.dto.status}`);
      }),
    );
  }
  if (column.name === MyVotesColumnNamesType.anfragenType.toString()) {
    options = new Set(
      initialContent.map((row) => {
        return i18n.t(`enorm.myVotes.tabs.requests.table.type.${row.anfrage.dto.typ}`).toString();
      }),
    );
  }
  return { displayName, labelContent, options };
}

export function getFilteredRows<T extends CommonRow>(
  actualContent: T[],
  filteredBy: string,
  column: { name: string; columnIndex: number },
): T[] {
  let newRows: Set<T> = new Set();
  if (
    column.name === MyVotesColumnNamesType.regelungsvorhabenType.toString() ||
    MyVotesColumnNamesType.regelungsvorhabenNoSubvoteType.toString()
  ) {
    newRows = new Set(
      actualContent.filter((row) => {
        const maxLength = AbstimmungstypType.AbstimmungDerVorlageFuerDenRegierungsentwurf.length;
        return row.typ === filteredBy.toUpperCase().split(' ').join('_').replace('FÜR', 'FUER').substring(0, maxLength);
      }),
    );
  }
  if (column.name === MyVotesColumnNamesType.mitzeichnungsanfrage.toString()) {
    newRows = new Set(
      actualContent.filter((row) => {
        return row.oberabstimmung?.titel === filteredBy;
      }),
    );
  }
  if (column.name === MyVotesColumnNamesType.regelungsvorhaben.toString()) {
    newRows = new Set(
      actualContent.filter((row) => {
        return row.regelungsvorhaben?.dto?.abkuerzung === filteredBy;
      }),
    );
  }
  if (column.name === MyVotesColumnNamesType.initalTab.toString()) {
    newRows = new Set(
      actualContent.filter((row) => {
        return i18n.t(`enorm.myVotes.tabs.archive.table.filter.tabs.${row.originalTab}`) === filteredBy;
      }),
    );
  }
  if (column.name === MyVotesColumnNamesType.status.toString()) {
    newRows = new Set(
      actualContent.filter((row) => {
        return i18n.t(`enorm.myVotes.tabs.requests.table.status.${row.anfrage.dto.status}`) === filteredBy;
      }),
    );
  }
  if (column.name === MyVotesColumnNamesType.ersteller.toString()) {
    newRows = new Set(
      actualContent.filter((row) => {
        return row.ersteller?.dto.name === filteredBy;
      }),
    );
  }
  if (column.name === MyVotesColumnNamesType.statusMitzeichnung.toString()) {
    newRows = new Set(
      actualContent.filter((row) => {
        return (
          (row.antwort?.dto.readOnlyAccess
            ? i18n.t('enorm.myVotes.tabs.sign.table.status.CC')
            : i18n.t(`enorm.myVotes.tabs.sign.table.status.${row.antwort?.dto.status}`)) === filteredBy
        );
      }),
    );
  }

  if (column.name === MyVotesColumnNamesType.anfragenType.toString()) {
    newRows = new Set(
      actualContent.filter((row) => {
        return i18n.t(`enorm.myVotes.tabs.requests.table.type.${row.anfrage.dto.typ}`).toString() === filteredBy;
      }),
    );
  }
  return Array.from(newRows);
}

export function getRegelungsvorhabenRenderer(
  record: AbstimmungTableDTO | MitzeichnungTableDTO | EntwurfTableDTO | AnfrageTableDTO,
): ReactElement {
  return (
    <div className="breakable-text">
      <TwoLineText
        firstRow={record.regelungsvorhaben?.dto.abkuerzung || ''}
        firstRowBold={true}
        secondRow={getTyp(record.typ)}
        secondRowBold={false}
        secondRowLight={false}
        elementId={`${record.regelungsvorhaben?.base.id}-${record.id}`}
      ></TwoLineText>
    </div>
  );
}

export function getFristRenderer(record: AbstimmungTableDTO | MitzeichnungTableDTO | EntwurfTableDTO): ReactElement {
  if (record.fristablauf) {
    return (
      <TwoLineText
        firstRow={getDateTimeString(record.fristablauf)}
        firstRowBold={true}
        secondRow={(() => {
          if (record?.hasOwnProperty('geschlossen') && (record as AbstimmungTableDTO).geschlossen) {
            return i18n.t('enorm.myVotes.tabs.myVotes.table.fristStatusGeschlossen');
          }
          if (isPast(new Date(record.fristablauf))) {
            return i18n.t('enorm.myVotes.tabs.myVotes.table.fristStatus');
          } else {
            return '';
          }
        })()}
        secondRowBold={false}
        secondRowLight={true}
        elementId={record.id}
      ></TwoLineText>
    );
  }
  return <></>;
}

export function getFristWithAnfrageRenderer(record: MitzeichnungTableDTO): ReactElement {
  if (!record.fristablauf) {
    return <></>;
  }
  if (!record.anfrage) {
    return getFristRenderer(record);
  }
  return (
    <TwoLineText
      firstRow={getDateTimeString(record.fristablauf)}
      firstRowBold={true}
      secondRow={(() => {
        if (record.anfrage.dto.neuerFristablauf && isPast(new Date(record.anfrage.dto.neuerFristablauf))) {
          return i18n.t(`enorm.myVotes.tabs.myVotes.table.fristStatusAnfrage.ABGELAUFEN`);
        }
        if (record.anfrage.dto.status && record.anfrage.dto.typ === AnfragetypType.Fristverlaengerung) {
          return i18n.t(`enorm.myVotes.tabs.myVotes.table.fristStatusAnfrage.${record.anfrage.dto.status}`);
        }
        return '';
      })()}
      secondRowBold={false}
      secondRowLight={true}
      elementId={record.id}
    ></TwoLineText>
  );
}

export function getAbstimmungsrundeRenderer(
  link: string,
  record: AbstimmungTableDTO | AnfrageTableDTO | EntwurfTableDTO,
): ReactElement {
  return (
    <TwoLineText
      firstRowLink={link}
      firstRow={getAbstimmungsrundeName(record.version, record.titel)}
      firstRowBold={false}
      secondRow={Filters.date(new Date(record.erstelltAm))}
      secondRowBold={false}
      secondRowLight={true}
      elementId={record.id}
    ></TwoLineText>
  );
}

export function getMitzeichnungAbstimmungsrundeRenderer(record?: OberabstimmungEntityDTO): ReactElement {
  if (record) {
    return (
      <TwoLineText
        firstRow={record.titel || ''}
        firstRowBold={false}
        secondRow={record.fristablauf ? Filters.date(new Date(record.fristablauf)) : ''}
        secondRowBold={false}
        secondRowLight={true}
        elementId={record.abstimmungId}
      />
    );
  } else {
    return <></>;
  }
}

export function getMeineAbstimmungenCall(
  isFilterNeeded: boolean,
  paginierungDTO: PaginierungDTO,
): Observable<AbstimmungTableDTOs> {
  const abstimmungController = GlobalDI.get<AbstimmungControllerApi>('enormController');
  return abstimmungController.getAbstimmungen({ isFilterNeeded, paginierungDTO });
}

export function getMeineUnterabstimmungenCall(
  isFilterNeeded: boolean,
  paginierungDTO: PaginierungDTO,
): Observable<AbstimmungTableDTOs> {
  const abstimmungController = GlobalDI.get<AbstimmungControllerApi>('enormController');
  return abstimmungController.getUnterabstimmungen({ isFilterNeeded, paginierungDTO });
}

export function getMeineBitteUmMitzeichnungCall(
  isFilterNeeded: boolean,
  paginierungDTO: PaginierungDTO,
): Observable<MitzeichnungTableDTOs> {
  const abstimmungController = GlobalDI.get<AbstimmungControllerApi>('enormController');
  return abstimmungController.getMitzeichnungAbstimmungen({ isFilterNeeded, paginierungDTO });
}

export function getEntwuerfeCall(
  isFilterNeeded: boolean,
  paginierungDTO: PaginierungDTO,
): Observable<EntwurfTableDTOs> {
  const abstimmungController = GlobalDI.get<AbstimmungControllerApi>('enormController');
  return abstimmungController.getEntwuerfe1({ isFilterNeeded, paginierungDTO });
}

export function getAnfragenCall(isFilterNeeded: boolean, paginierungDTO: PaginierungDTO): Observable<AnfrageTableDTOs> {
  const abstimmungController = GlobalDI.get<AbstimmungControllerApi>('enormController');
  return abstimmungController.getAnfragen({ isFilterNeeded, paginierungDTO });
}

function archiveAnfrageCall(anfrageId: string): Observable<void> {
  const abstimmungController = GlobalDI.get<AbstimmungControllerApi>('enormController');
  return abstimmungController.archiveAnfrage({ anfrageId });
}

export function getArchivedCall(isFilterNeeded: boolean, paginierungDTO: PaginierungDTO): Observable<ArchivTableDTOs> {
  const abstimmungController = GlobalDI.get<AbstimmungControllerApi>('enormController');
  return abstimmungController.getArchiv1({ isFilterNeeded, paginierungDTO });
}

export function setArchivedCall(id: string): Observable<void> {
  const abstimmungController = GlobalDI.get<AbstimmungControllerApi>('enormController');
  return abstimmungController.archiveAbstimmung({ id });
}

export function getAbstimmungsInfoCall(id: string): Observable<AbstimmungInfoDTO> {
  const abstimmungController = GlobalDI.get<AbstimmungControllerApi>('enormController');
  return abstimmungController.getAbstimmungsinfo({ id });
}

export function createAbstimmungsanfrageCall(
  id: string,
  abstimmungsanfrageDTO: AbstimmungsanfrageDTO,
): Observable<void> {
  const abstimmungController = GlobalDI.get<AbstimmungControllerApi>('enormController');
  return abstimmungController.createAbstimmungsanfrage({ id, abstimmungsanfrageDTO });
}

export function cancelTeilnehmerRequestCall(id: string, userId: string): Observable<void> {
  const abstimmungController = GlobalDI.get<AbstimmungControllerApi>('enormController');
  return abstimmungController.cancelTeilnehmerRequest({ id, userId });
}

export function addTeilnehmerCall(abstimmungId: string): Observable<void> {
  const abstimmungController = GlobalDI.get<AbstimmungControllerApi>('enormController');
  return abstimmungController.addTeilnehmer({ abstimmungId });
}

export interface TableDto {
  id: string;
}

export function setArchived<T extends TableDto>(
  id: string,
  content: Array<T>,
  setContent: (content: Array<T>) => void,
  anfrage?: boolean,
  onSuccess?: Function,
): void {
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  loadingStatusController.setLoadingStatus(true);
  const archiveFunction = anfrage ? archiveAnfrageCall(id) : setArchivedCall(id);
  archiveFunction.subscribe({
    next: () => {
      setContent(
        content.filter((entry: TableDto) => {
          return entry.id !== id;
        }),
      );
      displayMessage(
        i18n.t(
          anfrage ? 'enorm.myVotes.tabs.requests.table.archiveSuccess' : 'enorm.myVotes.archiveAction.archivedSuccess',
        ),
        'success',
      );
      loadingStatusController.setLoadingStatus(false);
      if (onSuccess) {
        onSuccess();
      }
    },
    error: (error: AjaxError) => {
      console.error('Error sub', error);
      errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
      loadingStatusController.setLoadingStatus(false);
    },
  });
}
