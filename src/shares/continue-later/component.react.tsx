// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import Paragraph from 'antd/lib/typography/Paragraph';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { InfoComponent, SaveOutlined } from '@plateg/theme';

interface ContinueLaterComponentProps {
  modify: boolean;
  saveCurrentState: () => void;
  isLeavingPageBlocked: boolean;
  translationKey: string;
}

export function ContinueLaterButton(props: ContinueLaterComponentProps): React.ReactElement {
  const { t } = useTranslation();

  if (props.modify) {
    return <></>;
  }

  return (
    <>
      <Button
        id="enorm-zwischenspeichern-btn"
        key="zwischenspeichern-button"
        onClick={props.saveCurrentState}
        disabled={!props.isLeavingPageBlocked}
      >
        {t('enorm.header.btnContinueLater.btnText')}
      </Button>
    </>
  );
}

export function ContinueLaterComponent(props: ContinueLaterComponentProps): React.ReactElement {
  const { t } = useTranslation();

  if (props.modify) {
    return <></>;
  }

  return (
    <>
      <Paragraph style={{ marginTop: '20px' }} className={props.modify ? 'hide' : ''}>
        {t(`enorm.${props.translationKey}.continueLater`)}
        <InfoComponent title={t(`enorm.${props.translationKey}.continueLater`)}>
          <p>{t(`enorm.${props.translationKey}.continueLaterTooltip`)}</p>
        </InfoComponent>
        &nbsp;&nbsp;&nbsp;
        <Button
          id="enorm-zwischenspeichernText-btn"
          type="text"
          className="blue-text-button"
          onClick={props.saveCurrentState}
          disabled={!props.isLeavingPageBlocked}
          icon={<SaveOutlined />}
        >
          {t(`enorm.${props.translationKey}.zwischenspeichernLink`)}
        </Button>
      </Paragraph>
    </>
  );
}
