// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export const routes = {
  MEINE_ABSTIMMUNG: 'meineAbstimmung',
  BITTE_UM_MITZEICHNUNG: 'bitteUmMitzeichnung',
  ENTWUERFE: 'entwuerfe',
  ANFRAGEN: 'anfragen',
  ARCHIV: 'archiv',
  UNTERABSTIMMUNG: 'meineUnterabstimmung',

  UNTERABSTIMMUNG_ANLEGEN: 'unterabstimmungAnlegen',
  ABSTIMMUNG_ANLEGEN: 'abstimmungAnlegen',
  ABSTIMMUNG_PRUEFEN: 'abstimmungPruefen',
  ABSTIMMUNG_BEARBEITEN: 'abstimmungBearbeiten',
  HAUSABSTIMMUNG: 'hausabstimmung',
  RESSORTABSTIMMUNG: 'ressortabstimmung',
  VORHABENCLEARING: 'vorhabenclearing',
  SCHLUSSABSTIMMUNG: 'schlussabstimmung',
  ABSTIMMUNG_HAUSLEITUNG: 'abstimmungHausleitung',

  NACHSTE_ABSTIMMUNGSRUNDE: 'nachsteAbstimmungsrunde',
  ABSTIMMUNGSRUNDE_PRUEFEN: 'abstimmungsrundePruefen',

  HILFE: 'hilfe',

  ABSTIMMUNG_BEANTWORTEN_PRUEFEN: 'abstimmungBeantwortenPruefen',

  ANFRAGE_TEILNAHME_DETAILS: 'teilnahmeanfrageDetails',

  ERFOLG: 'erfolg',
  ERFOLGUPD: 'erfolgAktualisiert',
  ERFOLG_UA: 'erfolgUnterabstimmung',
  ERFOLG_UA_UPD: 'erfolgUnterabstimmungAktualisiert',
  FEHLER: 'fehler',
  FEHLERPKP: 'fehlerpkp',
  ERFOLGNURABSTIMMUNG: 'erfolgnurabstimmung',
  ERFOLGABSTIMMUNGPKP: 'erfolgabstimmungpkp',

  ABSTIMMUNGSRUNDE: 'abstimmungsrunde',
  ABSTIMMUNGSRUNDE_TEILNAHMER: 'abstimmungsrundeTeilnahmer',

  ABSTIMMUNG_BEANTWORTEN: 'abstimmungBeantworten',
  ABSTIMMUNG_BEANTWORTEN_REVIEW: 'pruefen',

  ANFRAGEN_ANLEGEN: 'anfrageAnlegen',
  ANFRAGEN_ANLEGEN_PRUEFEN: 'anfragePruefen',
};
