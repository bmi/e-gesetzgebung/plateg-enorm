// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './download-link.less';

import React from 'react';

import { Download } from '@plateg/theme';
import { DownloadProps } from '@plateg/theme/src/components/table-component/table-sub-components/download-component/component.react';

export function EnormDownloadLink(props: DownloadProps): React.ReactElement {
  return (
    <div className="enorm-download-link-container">
      <Download {...props} />
    </div>
  );
}
