// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './upload-component.less';

import { Form, Upload } from 'antd';
import Text from 'antd/lib/typography/Text';
import Title from 'antd/lib/typography/Title';
import { UploadChangeParam } from 'antd/lib/upload';
import { UploadFile } from 'antd/lib/upload/interface';
import { UploadRequestOption } from 'rc-upload/lib/interface';
import React, { ReactElement, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import { Constants, DeleteOutlined, ExclamationMarkRed, ModalWrapper, UploadOutlined } from '@plateg/theme';

import { getFileExtension, incFileNameVersionNumber } from '../../utils/controller';
import { DeletePopupComponent } from './delete-popup/component.react';
import { FileRenameModalComponent } from './fileRenameModalComponent/component.react';
import { ItemRendererComponent } from './itemRendererComponent/component.react';

interface UploadProps {
  fileList?: UploadFile[];
  setFileList: React.Dispatch<React.SetStateAction<UploadFile[]>>;
  fileFormat: string;
  fileSize: number;
  onlySingleFileAllowed: boolean;
  fileNameSuggestion?: string;
  required?: boolean;
  restrictRenaming: boolean;
  messagesConfig: MessageItem;
  version?: number;
  isBearbeiten: boolean;
  uploadButtonRef?: React.Ref<HTMLElement>;
  errorProps: {
    errorTitle: string;
    formatString: string;
  };
  onFormChange?: () => void;
  handleDelete?: () => void;
}

interface MessageItem {
  [name: string]: string | React.JSX.Element;
}
export const standardMaximumSize = 20000000;
export function UploadComponent(props: UploadProps): React.ReactElement {
  const fileList = props.fileList;
  const setFileList = props.setFileList;
  const fileFormat = props.fileFormat;
  const fileSize = props.fileSize;
  const onlySingleFileAllowed = props.onlySingleFileAllowed;
  const restrictRenaming = props.restrictRenaming;
  const [uploadDisplayed, setUploadDisplayed] = useState('block');
  const [renameModalVisibility, setRenameModalVisibility] = useState<boolean>(false);
  const [selectedFileForRenaming, setSelectedFileForRenaming] = useState<UploadFile>();
  const generatedID = uuidv4();
  const [errorModalVisivility, setErrorModalVisibility] = useState<boolean>(false);

  const { Dragger } = Upload;

  const uploadCompRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    // Used to change the name of already choosen files
    const updateFileList = fileList?.map((file) => {
      file.name = incFileNameVersionNumber(file.name, true, props.fileNameSuggestion, props.version);
      return file;
    });
    setFileList(updateFileList || []);
  }, [props.fileNameSuggestion]);

  useEffect(() => {
    if (!renameModalVisibility) {
      document.getElementById(`enorm-dateiUmbenennen-btn-${selectedFileForRenaming?.uid || ''}`)?.focus();
    }
  }, [renameModalVisibility]);

  const uploadElement = uploadCompRef.current?.querySelector('.ant-upload.ant-upload-btn') as HTMLElement;
  const onFileChange = (info: UploadChangeParam) => {
    // If file size does not fit, the status will be error and we remove it from the list
    if (info.file.status === 'error') {
      info.fileList.splice(info.fileList.indexOf(info.file), 1);
    } else {
      // Will rename the file, when uploaded and a fileNameSuggestion is there
      info.file.name = incFileNameVersionNumber(info.file.name, true, props.fileNameSuggestion, props.version);
    }
    setFileList(info.fileList);
    setTimeout(() => {
      (uploadCompRef.current?.querySelector('.ant-upload.ant-upload-btn') as HTMLElement).focus();
    }, 200);
  };

  // dummy request to trick Upload component
  const dummyRequest = (options: UploadRequestOption) => {
    const dummy = new XMLHttpRequest();
    setTimeout(() => {
      if (options.onSuccess) {
        options.onSuccess({}, dummy);
      }
    }, 0);
  };

  const setUploadCompRef = () => {
    setTimeout(() => {
      (uploadCompRef.current?.querySelector('.ant-upload.ant-upload-btn') as HTMLSpanElement).focus();
    }, 200);
  };

  const itemRender = (_originNode: ReactElement, file: UploadFile, fileListLocal?: UploadFile[]) => {
    return (
      <ItemRendererComponent
        setUploadCompRef={setUploadCompRef}
        file={file}
        fileList={fileListLocal}
        setFileList={(uploadFile: React.SetStateAction<UploadFile<any>[]>) => {
          setFileList(uploadFile);
        }}
        renameHandler={renameHandler}
        disabledRename={restrictRenaming && props.fileNameSuggestion === undefined}
        isBearbeiten={props.isBearbeiten}
        onFormChange={props.onFormChange}
      />
    );
  };
  useEffect(() => {
    const keyUpEvent = (e: KeyboardEvent) => {
      if (e.key === ' ' || e.code === 'Space') {
        uploadElement.click();
      }
    };
    uploadElement?.addEventListener('keyup', keyUpEvent);
    return () => {
      uploadElement?.removeEventListener('keyup', keyUpEvent);
    };
  }, [props.fileList]);
  function beforeUpload(file: UploadFile) {
    if (file?.size > fileSize || !fileFormat.split(',').includes(getFileExtension(file.name).toLowerCase())) {
      file.status = 'error';
      setErrorModalVisibility(true);
      return false;
    }
    return true;
  }

  const renameHandler = (file: UploadFile) => {
    setRenameModalVisibility(true);
    setSelectedFileForRenaming(file);
  };

  const setFileListAfterRename = (file: UploadFile) => {
    const newFilenameFileList = props.fileList?.map((oldFile) => (oldFile.uid === file.uid ? file : oldFile));
    setFileList(newFilenameFileList || []);
  };

  const { t } = useTranslation();

  useEffect(() => {
    if (fileList && fileList.length > 0 && onlySingleFileAllowed) {
      setUploadDisplayed('none');
    } else {
      setUploadDisplayed('block');
    }

    // bitv
    const listElement = document.getElementsByClassName('ant-upload-list ant-upload-list-text');
    listElement[0]?.setAttribute('role', 'list');
  }, [fileList]);

  const SingleFileItem = () => {
    if (fileList && fileList.length > 0 && onlySingleFileAllowed) {
      return (
        <div className="upload-item-row">
          <Title level={3}>{t('enorm.vote.uploadedDraftTitle')}</Title>
          <Upload fileList={fileList} itemRender={itemRender}></Upload>
        </div>
      );
    } else {
      return null;
    }
  };

  const uploadHintText = onlySingleFileAllowed
    ? props.messagesConfig.uploadHintRegelungsentwurf
    : props.messagesConfig.uploadHintAttachments;
  const uploadButtonText = onlySingleFileAllowed
    ? props.messagesConfig.uploadBtnRegelungsentwurf
    : props.messagesConfig.uploadBtnAttachments;

  return (
    <div className="upload-holder" ref={uploadCompRef}>
      <ModalWrapper
        title={
          <h3>
            <ExclamationMarkRed style={{ verticalAlign: 'top' }} /> {props.errorProps.errorTitle}
          </h3>
        }
        open={errorModalVisivility}
        onOk={() => {
          setErrorModalVisibility(false);
        }}
        okText={t('enorm.vote.uploadErrorConfirm')}
        cancelButtonProps={{ style: { visibility: 'hidden' } }}
        closable={false}
      >
        <Text>
          {t('enorm.vote.uploadError', { fileSize: props.fileSize / 1000000, format: props.errorProps.formatString })}
        </Text>
      </ModalWrapper>
      <div id={`file-name-modal${generatedID}`}>
        <FileRenameModalComponent
          setFileListAfterRename={setFileListAfterRename}
          fileForRenaming={selectedFileForRenaming}
          visibility={renameModalVisibility}
          setVisibility={setRenameModalVisibility}
          title={props.messagesConfig.changeFileNameTitle as string}
          cancelText={props.messagesConfig.changeFileNameCancelText as string}
          itemLabel={props.messagesConfig.changeFileNameItemLabel as string}
          errorMsg={props.messagesConfig.changeFileNameErrorMsg as string}
          id={`${generatedID}`}
        />
      </div>

      <Form.Item
        name={`upload${generatedID}`}
        id={`upload${generatedID}`}
        rules={[
          {
            validator: () => {
              if (props.required && !fileList?.length) {
                return Promise.reject(props.messagesConfig.uploadError);
              } else {
                return Promise.resolve();
              }
            },
            validateTrigger: 'onBlur',
          },
          {
            max: Constants.TEXT_BOX_LENGTH,
            message: t('enorm.vote.uploadErrorFileNameLength', {
              maxChars: Constants.TEXT_BOX_LENGTH,
            }),
          },
        ]}
        label={<span>{props.messagesConfig.title}</span>}
      >
        <div className="upload-drag-container" style={{ display: uploadDisplayed }}>
          <Dragger
            fileList={fileList}
            name="file"
            customRequest={dummyRequest}
            onChange={onFileChange}
            itemRender={itemRender}
            accept={fileFormat}
            beforeUpload={beforeUpload}
            multiple={!onlySingleFileAllowed}
          >
            <p className="ant-upload-drag-icon">
              <UploadOutlined />
            </p>
            <p className="ant-upload-text">{uploadButtonText}</p>
            <p className="ant-upload-hint">{uploadHintText}</p>
          </Dragger>
        </div>
        <SingleFileItem />
      </Form.Item>
      {props.handleDelete && props.fileList && props.fileList.length > 0 && (
        <div style={{ textAlign: 'right' }}>
          <DeletePopupComponent
            title={t('enorm.vote.uploadAttachmentsDeleteAllPopover.title')}
            content={t('enorm.vote.uploadAttachmentsDeleteAllPopover.text')}
            deleteBtnText={t('enorm.vote.uploadAttachmentsDeleteAllPopover.delete')}
            cancelBtnText={t('enorm.vote.uploadAttachmentsDeleteAllPopover.cancel')}
            handleDelete={props.handleDelete}
            id={uuidv4()}
            triggerBtnProps={{
              id: `enorm-anlagenEntfernen-btn-${uuidv4()}`,
              size: 'small',
              type: 'text',
              icon: <DeleteOutlined />,
              children: t('enorm.vote.uploadAttachmentsDeleteAll'),
              className: 'blue-text-button delete-all-uploaded-items',
            }}
          />
        </div>
      )}
    </div>
  );
}
