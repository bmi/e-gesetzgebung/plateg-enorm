// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input } from 'antd';
import { UploadFile } from 'antd/lib/upload/interface';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { Constants, GeneralFormWrapper, ModalWrapper } from '@plateg/theme';

import { getFileExtension } from '../../../utils/controller';

interface FileRenameModalProps {
  visibility: boolean;
  setFileListAfterRename: (file: UploadFile) => void;
  setVisibility: Function;
  fileForRenaming?: UploadFile;
  title: string;
  cancelText: string;
  itemLabel: string;
  errorMsg: string;
  id: string;
}

export function FileRenameModalComponent(props: FileRenameModalProps): React.ReactElement {
  const [form] = Form.useForm();

  const visibility = props.visibility;
  const setVisibility = props.setVisibility;
  const [dateiEndung, setDateiEndung] = useState<string>('');
  const fileNameID = props.id;

  useEffect(() => {
    const dateiname = props.fileForRenaming?.name;
    const dateiEndungString = getFileExtension(dateiname);
    setDateiEndung(dateiEndungString);

    form.setFieldsValue({ [fileNameID]: dateiname?.replace(dateiEndungString, '') as string });
  });

  const handleOk = () => {
    form.submit();
  };

  const { t } = useTranslation();
  const saveChangedFileName = () => {
    if (props.fileForRenaming) {
      props.fileForRenaming.name = (form.getFieldValue(fileNameID) as string) + dateiEndung;
      props.setFileListAfterRename(props.fileForRenaming);
    }
    setVisibility(false);
  };

  const handleCancel = () => {
    setVisibility(false);
  };

  return (
    <>
      <ModalWrapper
        title={<h3>{props.title}</h3>}
        open={visibility}
        onOk={handleOk}
        onCancel={handleCancel}
        cancelText={props.cancelText}
        forceRender
      >
        <GeneralFormWrapper
          form={form}
          layout="vertical"
          noValidate
          onFinish={saveChangedFileName}
          onFinishFailed={() => document?.getElementById('errorBox')?.focus()}
        >
          <Form.Item
            required
            name={`${fileNameID}`}
            label={<span>{props.itemLabel}</span>}
            rules={[
              { required: true, whitespace: true, message: props.errorMsg },
              {
                max: Constants.TEXT_BOX_LENGTH,
                message: t('enorm.vote.uploadErrorFileNameLength', {
                  maxChars: Constants.TEXT_BOX_LENGTH,
                }),
              },
            ]}
          >
            <Input />
          </Form.Item>
        </GeneralFormWrapper>
      </ModalWrapper>
    </>
  );
}
