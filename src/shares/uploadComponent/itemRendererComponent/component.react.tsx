// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Col, Row } from 'antd';
import Text from 'antd/lib/typography/Text';
import { UploadFile } from 'antd/lib/upload/interface';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { DeleteOutlined } from '@plateg/theme/src/components/icons/DeleteOutlined';
import { DocumentOutlined } from '@plateg/theme/src/components/icons/DocumentOutlined';
import { EditOutlined } from '@plateg/theme/src/components/icons/EditOutlined';

import { DeletePopupComponent } from '../delete-popup/component.react';

interface ItemRendererProps {
  file: UploadFile;
  fileList?: UploadFile[];
  setFileList: Function;
  renameHandler: Function;
  disabledRename: boolean;
  isBearbeiten: boolean;
  onFormChange?: () => void;
  setUploadCompRef: () => void;
}

export function ItemRendererComponent(props: ItemRendererProps): React.ReactElement {
  const file = props.file;
  const fileList = props.fileList;
  const setFileList = props.setFileList;
  const renameHandler = props.renameHandler;
  const { t } = useTranslation();

  const handleDelete = () => {
    if (fileList && fileList?.length > 0) {
      setFileList(
        fileList.filter((el) => {
          return el.uid !== file.uid;
        }),
      );
      props.onFormChange?.();
    }
  };

  const handleRename = () => {
    renameHandler(file);
  };

  return (
    <Row className="item-row" role="listitem">
      <Col className="ant-col-2 blue-text-button">
        <DocumentOutlined />
      </Col>
      <Col className="ant-col-19">
        <Text className="blue-text-button">{file.name}</Text>
      </Col>
      <Col className="ant-col-1">
        <Button
          id={`enorm-dateiUmbenennen-btn-${file.uid}`}
          disabled={props.disabledRename || props.isBearbeiten}
          onClick={handleRename}
          type="text"
          aria-label={t('enorm.beantwortenVote.upload.rename')}
          icon={<EditOutlined />}
        ></Button>
      </Col>
      <Col className="ant-col-1">
        <div className="vertical-seperator"></div>
      </Col>
      <Col className="ant-col-1">
        <DeletePopupComponent
          setOnDeleteCompRef={props.setUploadCompRef}
          title={t('enorm.beantwortenVote.upload.popover.title')}
          content={t('enorm.beantwortenVote.upload.popover.text', { title: file.name })}
          deleteBtnText={t('enorm.beantwortenVote.upload.popover.delete')}
          cancelBtnText={t('enorm.beantwortenVote.upload.popover.cancel')}
          handleDelete={handleDelete}
          id={file.uid}
          triggerBtnProps={{
            id: `enorm-hochgeladenDateiLoeschen-btn-${file.uid}`,
            disabled: props.isBearbeiten,
            type: 'text',
            icon: <DeleteOutlined />,
            'aria-label': t('enorm.beantwortenVote.upload.delete'),
          }}
        />
      </Col>
    </Row>
  );
}
