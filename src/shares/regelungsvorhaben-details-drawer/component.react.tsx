// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './regelungsvorhaben-details-drawer.less';

import React from 'react';
import { useTranslation } from 'react-i18next';

import { CodelistenResponseDTO, RegelungsvorhabenEntityWithDetailsResponseDTO } from '@plateg/rest-api';
import { InfoComponent, RVInfoComponent } from '@plateg/theme';

interface RegelungsvorhabenDetailsDrawerInterface {
  regelungsvorhaben?: RegelungsvorhabenEntityWithDetailsResponseDTO;
  id?: string;
  isCreator?: boolean;
  codeList?: CodelistenResponseDTO;
}
export function RegelungsvorhabenDetailsDrawer(props: RegelungsvorhabenDetailsDrawerInterface): React.ReactElement {
  const { t } = useTranslation();

  if (!props.regelungsvorhaben) {
    return <></>;
  }
  const rvDTO = props.regelungsvorhaben.dto;
  let buttonText = rvDTO.abkuerzung;
  if (!props.id) {
    buttonText = t(`enorm.votingRound.header.rvDrawer.buttonLabel`);
  }
  return (
    <div className="rv-details-drawer">
      <InfoComponent
        buttonText={buttonText}
        titleWithoutPrefix={true}
        title={t(`enorm.votingRound.header.rvDrawer.mainTitle`)}
        id={`info-drawer-regelungsvorhaben-metadata-${props.id || props.regelungsvorhaben?.base.id}`}
        buttonId="rvInfo-drawer-btn"
      >
        <>
          <p>{t(`enorm.votingRound.header.rvDrawer.introText`)}</p>
          <RVInfoComponent
            regelungsvorhaben={props.regelungsvorhaben}
            codeList={props.codeList}
            isCreator={props.isCreator}
          />
        </>
      </InfoComponent>
    </div>
  );
}
