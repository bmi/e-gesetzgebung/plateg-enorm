// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { UploadFile } from 'antd/lib/upload/interface';
import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';

import { UploadComponent } from '../uploadComponent/component.react';

interface AdditionalUploadProps {
  fileList?: UploadFile[];
  setFileList: React.Dispatch<React.SetStateAction<UploadFile[]>>;
  fileFormat: string;
  fileSize: number;
  singleFile: boolean;
  messagesConfig: MessageItem;
  onFormChange?: () => void;
}

interface MessageItem {
  [name: string]: string | React.ReactElement;
}

export function AdditionalUploadComponent(props: AdditionalUploadProps): React.ReactElement {
  const { t } = useTranslation();
  const fileList = props.fileList;
  const setFileList = props.setFileList;
  const fileFormat = props.fileFormat;
  const fileSize = props.fileSize;
  const singleFile = props.singleFile;
  const uploadButtonRef = useRef<HTMLButtonElement>(null);

  const deleteAll = () => {
    setFileList([]);
    props.onFormChange?.();
  };

  return (
    <div>
      <UploadComponent
        onFormChange={props.onFormChange}
        messagesConfig={props.messagesConfig}
        fileList={fileList}
        setFileList={setFileList}
        fileFormat={fileFormat}
        fileSize={fileSize}
        onlySingleFileAllowed={singleFile}
        restrictRenaming={false}
        uploadButtonRef={uploadButtonRef}
        errorProps={{
          errorTitle: t('enorm.vote.uploadAttachmentsErrorTitle'),
          formatString: t('enorm.vote.uploadAttachmentsFormatShort'),
        }}
        handleDelete={deleteAll}
      />
    </div>
  );
}
